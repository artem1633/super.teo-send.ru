<?php

/**
 * функция запросов в ВК
 * @param string $url - URL
 * @param array|false $post - массив POST параметров или false для GET
 * @param bool $follow - автоматически обрабатывать редиректы
 */
function vkCurl(string $url, $post = false, $follow = true, $proxy) {

//    define('TIMEOUT'  , 500); // 0.5 секунды между запросами
//    define('CLIENT_ID', 6651556);

    error_reporting(E_ALL);
    ini_set('display_errors', true);

    // чтобы не инициализировать по 10 раз CURL
    // делаем один раз и храним в статической переменной
    static $ch = null;
    if ( ! $ch) {
        $ch = curl_init();
    }
    else {
        curl_reset($ch);
        usleep(500 * 1000); // спим. микросекунды (не милли)
    }

    // подготовка CURL параметров запроса
    curl_setopt_array($ch, [
        CURLOPT_COOKIEJAR  => __DIR__ . '/uploads/cookie.txt',
        CURLOPT_COOKIEFILE => __DIR__ . '/uploads/cookie.txt',
        CURLOPT_PROXY => $proxy,
        //CURLOPT_VERBOSE    => true,
        CURLOPT_HEADER     => true,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_USERAGENT      => 'Opera/9.80 (Android; Opera Mini/7.5.33361/31.1350; U; en) Presto/2.8.119 Version/11.11',
        CURLOPT_FOLLOWLOCATION => $follow,
        CURLOPT_URL => $url,
    ]);

    // параметры CURL если POST
    if ($post !== false) {
        curl_setopt_array($ch, [
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $post,
        ]);
    }

    // выполняем запрос
    $data = curl_exec($ch);

    // если ошибка
    if ($err_no = curl_errno($ch)) {
        throw new Exception("Ошибка get($url) #$err_no: " . curl_error($ch), 100);
    }

    return $data;
}

/**
 * функция получения токена от VK
 * @param string $login
 * @param string $pass
 */
function get_token(string $login, string $pass, $client_id, $scope, $proxy) {

//    define('TIMEOUT'  , 500); // 0.5 секунды между запросами
//    define('CLIENT_ID', 6651556);

    error_reporting(E_ALL);
    ini_set('display_errors', true);

    // удаляем старый файл с куками если есть
    @unlink(__DIR__ . '/uploads/cookie.txt');

    // получаем страницу входа и получаем URL куда постить форму
    // (заодно проверяя тем самым html страницы)
    $data = vkCurl('https://m.vk.com/', false, true, $proxy);
    if ( ! preg_match('~post" action="(.*?)"~s', $data, $result)) {
        throw new Exception("Ошибка страницы входа: \n\n" . $data, 200);
    }

    // пытаемся логиниться и проверям вошли или нет
    $data = vkCurl($result[1], ['email' => $login, 'pass' => $pass, 'submit' => 'Войти'], true, $proxy);
    if (strpos($data, 'op_owner') === false) {
        throw new Exception("Ошибка входа: \n\n" . $data, 300);
    }


    // пытаемся авторизоваться в нужном приложении
    // либо сразу получим токен, либо нас бросит на страницу подтверждения
    $data = vkCurl('https://oauth.vk.com/authorize?client_id=' . $client_id . '&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope='.$scope.'&response_type=token&v=5.37', false, true, $proxy);
    if ( ! preg_match('~access_token=([^&]+)~s', $data, $result)) {

        // если не получили сразу токен проверяем страница ли это подтверждения
        // если нет - это однозначно ошибка и так быть не должно
        if ( ! preg_match('~action="([^"]+)"~s', $data, $result)) {
            throw new Exception("Ошибка страницы подтверждения: \n\n" . $data, 400);
        }

        // "жмем" на подтверождение и проверяем вернулся ли токен
        $data = vkCurl($result[1], false, false, $proxy);
        if ( ! preg_match('~access_token=([^&]+)~s', $data, $result)) {
            throw new Exception("Ошибка получения токена: \n\n" . $data, 500);
        }
    }

    // возвращаем токен
    return $result[1];
}

function getHistory($id,$token,$proxy)
{
    $url = 'https://api.vk.com/method/messages.getHistory';
    $params = array(
        'filter' => 'all',
        'count' => '200',
        'rev' => 1,
        'user_id' => $id,
        'access_token' => $token,
        'v' => '5.74'

    );

// В $result вернется id отправленного сообщения
    $result = file_get_contents($url, false, stream_context_create(array(
        'http' => array(
            'proxy' => $proxy,
            'request_fulluri' => true,
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($params)
        )
    )));
    $result = json_decode($result,true);
    return $result;

}


function getHistoryUnread($id,$token,$proxy)
{
    $url = 'https://api.vk.com/method/messages.getHistory';
    $params = array(
        'filter' => 'unread',
        'count' => '200',
        'rev' => 1,
        'user_id' => $id,
        'access_token' => $token,
        'v' => '5.74'

    );

// В $result вернется id отправленного сообщения
    $result = file_get_contents($url, false, stream_context_create(array(
        'http' => array(
            'proxy' => $proxy,
            'request_fulluri' => true,
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($params)
        )
    )));
    $result = json_decode($result,true);
    return $result;

}

function dateDifference($date_1 )
{

    $date_2 = date("Y-m-d H:i:s");

    $diff = strtotime($date_2) - strtotime($date_1);

    return $diff;

}

function send($userVk, $text, $token,$proxy)
{
    $url = 'https://api.vk.com/method/messages.send';
    $params = array(
        'user_id' => $userVk,    // Кому отправляем
        'message' => $text,   // Что отправляем
        'access_token' => $token, // Токен того кто отправляет
        'v' => '5.37',
    );

    // В $result вернется id отправленного сообщения
    $result = file_get_contents($url, false, stream_context_create(array(
        'http' => array(
            'proxy' => $proxy,
            'request_fulluri' => true,
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($params)
        )
    )));
    $result = json_decode($result,true);
    return $result;
}


