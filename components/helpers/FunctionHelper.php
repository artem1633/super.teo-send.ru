<?php

namespace app\components\helpers;

use app\models\DataRecipient;
use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\Proxy;
use app\models\Settings;
use Yii,
    app\models\CompanySettings,
    app\models\Companies;

/**
 * Class FunctionHelper
 * @package app\components\helpers
 */
class FunctionHelper
{
    /**
     * Получить ID компании
     * @return null
     */
    public static function getCompanyId()
    {
        if (Yii::$app->user->identity->isSuperAdmin()) {
            return 1;
        }
        return ($userModel = Yii::$app->user->identity) ? $userModel->company_id : null;
    }

    public static function getCountSendedMessages()
    {
        return DispatchRegist::find()->sum('all_sended_message_count');
    }

    public static function getCountAccounts()
    {
        return DispatchRegist::find()->count();
    }

    /**
     * Получить модель CompanySettings
     * @return null|CompanySettings
     */
    public static function getCompanySettings()
    {
        if ($companyId = self::getCompanyId()) {
            $settings = CompanySettings::findOne(['company_id' => $companyId]);
            if($settings == null){
                $settings = new CompanySettings(['company_id' => $companyId]);
                $settings->save(false);
            }

            return $settings;
        }
        return null;
    }

    /**
     * @return \app\models\Companies|null
     */
    public static function getCompanyModel()
    {
        return Companies::findOne($companyId = self::getCompanyId());
    }

    /**
     * @return array
     */
    public static function getDaysWeekList()
    {
        return [
            'Mon' => 'Понедельник',
            'Tue' => 'Вторник',
            'Wed' => 'Среда',
            'Thu' => 'Четверг',
            'Fri' => 'Пятница',
            'Sat' => 'Суббота',
            'Sun' => 'Воскресенье',
        ];
    }

    /**
     * @param $format
     * @param $date
     * @param bool $hmi
     * @return array|mixed
     */
    private static function dateFor($format, $date, $hmi = false)
    {
        if (!is_array($date)) {
            $date = [$date];
        }
        $dates = [];
        foreach ($date as $d) {
            $dates[] = !empty($d) ? date($format . ($hmi ? " (H:i)" : ""), strtotime($d)) : null;
        }
        return count($dates) > 1 ? $dates : $dates[0];
    }

    /**
     * @param array|string $date
     * @param bool $hmi
     * @return array|mixed
     */
    public static function dateForMysql($date, $hmi = false)
    {
        return empty($date) ? null : self::dateFor("Y-m-d", $date, $hmi);
    }

    /**
     * @param array|string $date
     * @param bool $hmi
     * @return array|mixed
     */
    public static function dateForForm($date, $hmi = false)
    {
        return ($date == '0000-00-00' || empty($date)) ? null : self::dateFor("d.m.Y", $date, $hmi);
    }

    /**
     * @param string $date
     * @return array|mixed
     */
    public static function dateForChart($date)
    {
        return empty($date) ? null : self::dateFor("d.m", $date);
    }

    /**
     * @param $line
     * @param $model
     * @return null
     */
    public static function addRecipient($line, $model)
    {
        if(stripos($line, 'vk.com/'))
        {
            if(stripos($line, 'vk.com/id'))
            {
                $line = explode('vk.com/id', $line)[1];
            } else {
                $line = explode('vk.com/', $line)[1];
            }
        } else if(strripos($line, 'id') === 0){
            $line = substr($line, 2);
        }

        $status = new DispatchStatus;
        $status->default_account_id = $line;
        $status->company_id = $model->company_id;
        $status->dispatch_id = $model->id;
        $status->save();

        return null;
    }

    /**
     * @param array $arr
     * @param $model
     * @return null
     */
    public static function addRecipientFromArray($arr, $model)
    {
        foreach ($arr as $id){
            $status = new DispatchStatus;
            $status->account_id = $id;
            $status->company_id = $model->company_id;
            $status->dispatch_id = $model->id;
            $status->save();
        }
        return null;
    }

    /**
     * @param $model
     * @param $data
     */
    public static function saveDataRecipient($model, $data)
    {
        Yii::warning($model, __METHOD__);

        $filename = Yii::getAlias('@webroot') . '/data/data-' . $model->company_id . '-' . time();
        $filename = self::checkDoubleFile($filename);
        if ($file = fopen($filename, "w")) {
            fwrite($file, $data);
            fclose($file);
            $recipient = new DataRecipient([
                'count' => DispatchStatus::find()->where(['dispatch_id' => $model->id])->count(),
                'company_id' => $model->company_id,
                'type' => DataRecipient::TYPE_UPLOAD,
                'file' => $filename,
                'name' => $model->base_name ? $model->base_name : 'Без названия',
                'description' => 'Загружено ' . date('d.m.Y') . ' в' . date('H:i'),
            ]);
            $recipient->save();
            Yii::warning($model->base_name, __METHOD__);
            Yii::warning(serialize($recipient), __METHOD__);

        }
    }

    /**
     * Метод проверяет есть ли файл с таким названием, если есть то добавляет к имени рандомное значение и возвращает новое имя.
     *
     * @param $filename
     * @return string
     */
    private static function checkDoubleFile($filename)
    {
        if(file_exists($filename)){
            return $filename . rand(0,100);
        } else {
            return  $filename;
        }

    }

    /**
     * Возвращает количество мест для прокси
     * @param int $id
     * @return int
     */
    public static function getAvailableProxyAccountsCount($id)
    {
//        $maxProxy = Settings::findByKey('max_proxy');
//        $proxyAccounts = DispatchRegist::find()->where('proxy is not null')->count();
//
//        if($maxProxy != null)
//        {
//            $result = intval($maxProxy->value) - $proxyAccounts;
//
//            return $result < 0 ? 0 : $result;
//        }
//
//        return 0;

        $proxyCount = Proxy::getAccountsUsesProxyCount($id);
        $setting = Settings::findByKey('max_proxy');
        if($setting != null)
        {
            $maxProxyCount = intval($setting->value);

            $result = $maxProxyCount - $proxyCount;

            return $result < 0 ? 0 : $result;
        }

        return 0;
    }

    /**
     * Проверка прокси на работоспособность true - работает
     *
     * @param $proxy
     * @return bool
     */
    public static function checkProxy($proxy)
    {

        $data = explode(':', $proxy);

        $fp = fsockopen($data[0], $data[1]);

        return $fp ? true : false;

    }


}