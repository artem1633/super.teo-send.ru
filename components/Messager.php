<?php

namespace app\components;

use app\modules\api\controllers\ClientController;
use yii\base\Component;
use app\models\Settings;
use app\modules\api\controllers\DispatchController;

/**
 * Class Messager
 * @package app\components
 * Занимается отправкой сообщений через ВК
 */
class Messager extends Component
{
    private $adminVkId;

    private $proxy;

    private $adminToken;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->adminVkId = Settings::find()->where(['key' => 'akk_notify'])->one()->value;
        $this->proxy = Settings::find()->where(['key' => 'proxy_server'])->one()->value;
        $this->adminToken = Settings::find()->where(['key' => 'vk_access_token'])->one()->value;
    }

    /**
     * Отправляет уведомление админу
     * @param string $message
     */
    public function sendToAdmin($message)
    {
        DispatchController::sendTelMessage('247187885', $message);
    }
}