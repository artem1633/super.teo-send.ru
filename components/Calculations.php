<?php

namespace app\components;

use app\components\helpers\FunctionHelper;
use app\models\calculation\Calculation;
use app\models\Settings;
use yii\base\Component;

/**
 * Class Calculations
 * @package app\components
 * Класс содержит в себе алгоритмы калькуляции для рассылок
 */
class Calculations extends Component
{
    /**
     * Стандартный алгоритм калькуляции
     * @param $count
     * @return array
     */
    public static function defaultCalculation($count)
    {
        $companySettings = FunctionHelper::getCompanySettings();
        $messageDailyLimit = $companySettings->messages_daily_limit == null ? 15 : $companySettings->messages_daily_limit;
        $priceAkkSetting = Settings::findByKey('account_price')->value;
        $proxyPriceSetting = Settings::findByKey('proxy_price')->value;
        $maxProxySetting = Settings::findByKey('max_proxy')->value;
        $calculation = new Calculation([
            'base' => $count,
            'step' => $messageDailyLimit,
            'speed_akk' => $messageDailyLimit,
            'price_akk' => $priceAkkSetting,
            'proxy_day' => $proxyPriceSetting,
            'proxy' => $maxProxySetting,
        ]);

        if($calculation->calculate())
        {
            $calculationResultCount = Settings::findByKey('calculation_result_count')->value;

            usort($calculation->calculatedData, function($a, $b){
                return ($a['sum'] > $b['sum']) ? 1 : -1;
            });

            $data = (count($calculation->calculatedData) > intval($calculationResultCount)) ? array_slice($calculation->calculatedData, 0, intval($calculationResultCount)) : $calculation->calculatedData;


            usort($data, function($a, $b){
                return ($a['speed'] > $b['speed']) ? 1 : -1;
            });

            return $data;
        }

        return [];
    }
}