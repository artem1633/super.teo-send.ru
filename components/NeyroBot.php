<?php

namespace app\components;

use yii\base\Component;

class NeyroBot extends Component
{
    const URL = 'http://neyro.teo-bot.ru/api/default/one';

    public static function sendInfo($data)
    {
        // $data = http_build_query($data);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, self::URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }
}

?>