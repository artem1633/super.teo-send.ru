<?php

namespace app\components\widgets;

use Yii,
    yii\helpers\Html;

$bootstrapModal = false;

/**
 * Class BootstrapModal
 * @package app\components\widgets
 *
 * JavaScript functions:
 * @BootstrapModal(id, header = '', body = '', footer = '', show = true, backdrop = false)
 * @BootstrapModalHeader(id, header)
 * @BootstrapModalShow(id, backdrop = false)
 * @BootstrapModalHide(id)
 * @BootstrapModalIFrame(src, height = auto)
 *
 * footer button class: Yes = js-modal-save; No = js-modal-close
 */
class BootstrapModal
{
    const SIZE_MAX = 'max';
    const SIZE_LARGE = 'lg';
    const SIZE_SMALL = 'sm';

    /**
     * @param widget array $params
     * >> string id:          default 'modal'
     * >> string size:        default null (sm|lg|max)
     * >> string header:      default null (null|true|string)
     * >> string headerClass: default null (primary|success|info|warning|danger)
     * >> string footer:      default null (null|true)
     * >> string show:        default false (false|true)
     * >> array options: html_attributes
     * @param bool|string $return false (false|begin|end)
     * @param bool $widgetReturn false
     * @return string
     */
    public static function widget($params = [], $return = false, $widgetReturn = false)
    {
        global $bootstrapModal;
        $widgets = (isset($params[0]) && is_array($params[0])) ? $params : [$params];
        if (!$bootstrapModal && !$widgetReturn) {
            $bootstrapModal = true;
            self::declareScript();
        }
        foreach ($widgets as $param) {
            $widget = '';
            $id = isset($param['id']) ? $param['id'] : 'modal';
            $body = isset($param['body']) ? $param['body'] : '';
            if (isset($param['size'])) {
                $size = $param['size'] == self::SIZE_MAX ? [' modal-lg', ' style="width: 92%;"'] : [' modal-' . $param['size'], ''];
            } else {
                $size = ['', ''];
            }
            if (!$return || $return == 'begin') {
                $widget .= '<div id="' . $id . '"' . (isset($param['options']) ? self::getOptions($param['options']) : '') . ' class="modal fade" tabindex="-1" role="dialog">';
                $widget .= '<div class="modal-dialog' . $size[0] . '" role="document"' . $size[1] . '>';
                $widget .= '<div class="modal-content">';
                if (isset($param['header']) && $param['header']) {
                    $widget .= '<div class="modal-header' . (isset($param['headerClass']) ? ' bg-' . $param['headerClass'] : '') . '">';
                    $widget .= '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                    $widget .= '<h4 class="modal-title">' . $param['header'] . '</h4>';
                    $widget .= '</div>';
                }
                $widget .= '<div class="modal-body' . ($body === false ? ' hide' : '') . '" style="min-height: 60px;">';
            }
            $widget .= $body;
            if (!$return || $return == 'end') {
                $widget .= '</div>';
                if (isset($param['footer']) && $param['footer']) {
                    $widget .= '<div class="modal-footer">';
                    $widget .= '<span></span>&emsp;';
                    $widget .= Html::button('Отмена', ['class' => 'btn btn-default pull-left js-modal-close', 'data-dismiss' => 'modal']) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary js-modal-save']);
                    $widget .= '</div>';
                }
                $widget .= '</div>';
                $widget .= '</div>';
                $widget .= '</div>';
                if (isset($param['show']) && $param['show']) {
                    $widget .= '<script>$(function () {$("#' . $id . '").modal("show");});</script>';
                }
            }
            if ($widgetReturn) {
                return $widget;
            } else {
                echo $widget;
            }
        }
    }

    /**
     * @param array $params
     */
    public static $params;

    public static function widgetBegin($params = [])
    {
        self::$params = $params;
        ob_start();
    }

    public static function widgetEnd()
    {
        $block = ob_get_contents();
        ob_end_clean();
        self::widget(self::$params, 'begin');
        echo $block;
        self::widget(self::$params, 'end');
    }

    /**
     * @return string
     */
    protected static function getOptions($options)
    {
        $html = '';
        if (!empty($options) && is_array($options)) {
            foreach ($options as $option => $value) {
                $html .= " {$option}=\"{$value}\"";
            }
        }
        return $html;
    }

    public static function declareScript()
    {
        echo <<<SCRIPT
<script>
    function BootstrapModal(id, header, body, footer, show, backdrop) {
        header = header || '';
        body = body || '';
        footer = footer || '';
        show = show || true;
        backdrop = backdrop || false;
        var modal = $('#' + id);
        modal.find('.modal-header .modal-title').html(header);
        modal.find('.modal-body').html(body);
        if (footer.length > 1) {
            modal.find('.modal-footer').html(footer);
        }
        if (show) {
            if (backdrop) {
                modal.modal({backdrop: 'static'});
            } else {
                modal.modal();
            }
        }
    }
    function BootstrapModalHeader(id, header) {
        $('#' + id).find('.modal-header .modal-title').html(header);
    }
    function BootstrapModalShow(id, backdrop) {
        backdrop = backdrop || false;
        if (backdrop) {
            $('#' + id).modal({backdrop: 'static'});
        } else {
            $('#' + id).modal();
        }
    }
    function BootstrapModalHide(id) {
        $('#' + id).modal('hide');
    }
    function BootstrapModalIFrameLoaded(iframe) {
        $('#iframe-first').remove();
        //var height = iframe.contentWindow.document.body.scrollHeight - 100;
        $(iframe).animate({opacity: 1}, 400);
  }
    function BootstrapModalIFrame(src, height) {
        height = height || document.documentElement.clientHeight - 225;
        return '<div id="iframe-first" style="position: absolute; opacity: 0.5;"><img height="24px" src="/img/loader-big.gif"></div>' +
        '<iframe id="bootstrapmodal-iframe" src="' + src + '" width="100%" height="' + height + '" frameborder="0" onload="javascript:BootstrapModalIFrameLoaded(this);" style="opacity: 0;">';
    }
    
</script>
SCRIPT;
    }
}