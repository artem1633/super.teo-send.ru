<?php

namespace app\components;

use app\models\Settings;
use app\modules\api\controllers\ClientController;
use yii\base\Component;

class Proxy extends Component
{

    public $apiKey;

    private $profile;

    private $isProxyPurchased = false;

    public function init()
    {
        parent::init();

        $apiKeySetting = Settings::findByKey('proxy6_api_key');

        if($apiKeySetting != null && $apiKeySetting->value != null)
        {
            $this->apiKey = $apiKeySetting->value;
        } else {
            return false;
        }
    }

    public function getBalance()
    {
        return $this->getProfile()->balance;
    }

    public function getCurrency()
    {
        return $this->getProfile()->currency;
    }

    public function getProxy()
    {
        return json_decode(file_get_contents($this->getBaseUrl().'/getproxy'));
    }

    public function buy($count = 1, $period = 30, $country = 'ru', $version = '4')
    {
        if($this->isProxyPurchased == false)
        {
            $result = json_decode(file_get_contents($this->getBaseUrl()."/buy?count={$count}&period={$period}&country={$country}&version={$version}"));
            //Отправка уведомления
            $vk_id = Settings::find()->where(['key' => 'akk_notify'])->one()->value;
            $proxy = Settings::find()->where(['key' => 'proxy_server'])->one()->value;
            $token = Settings::find()->where(['key' => 'vk_access_token'])->one()->value;


            if(isset($result->error))
            {
                ClientController::request('messages.send', $proxy, [
                    'access_token' => $token,
                    'user_id' => $vk_id,
                    'message' => "Ошибка при покупке прокси: {$result->error}",
                ]);
            } else {
                $proxy = $result->list[0];
                $proxy = \app\models\Proxy::createFromApiInstance($proxy);
                $proxy->save();

                ClientController::request('messages.send', $proxy, [
                    'access_token' => $token,
                    'user_id' => $vk_id,
                    'message' => "Был купленновый прокси на период в {$period} дней",
                ]);
            }

            $this->isProxyPurchased = true;
            return $result;
        }


        return [];

    }

    public function prolong($ids, $period = 3)
    {
        $result = json_decode(file_get_contents($this->getBaseUrl()."/prolong?period={$period}&ids={$ids}&nokey"));


        //Отправка уведомления
        $vk_id = Settings::find()->where(['key' => 'akk_notify'])->one()->value;
        $proxy = Settings::find()->where(['key' => 'proxy_server'])->one()->value;
        $token = Settings::find()->where(['key' => 'vk_access_token'])->one()->value;

        ClientController::request('messages.send', $proxy, [
            'access_token' => $token,
            'user_id' => $vk_id,
            'message' => "Прокси с ID {$ids} был(и) продлены на период в {$period} д. Текущий баланс: {$result->balance} {$result->currency}",
        ]);

        if(isset($result->error))
        {
            ClientController::request('messages.send', $proxy, [
                'access_token' => $token,
                'user_id' => $vk_id,
                'message' => "Ошибка при продлении прокси под ID {$ids}: {$result->error}",
            ]);
        }



        return $result;
    }

    private function getProfile()
    {
        if ($this->profile == null)
        {
            $this->profile = json_decode(file_get_contents($this->getBaseUrl()));
        }

        return $this->profile;
    }

    private function getBaseUrl()
    {
        return 'https://proxy6.net/api/'.$this->apiKey;
    }
}