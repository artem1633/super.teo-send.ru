<?php

namespace app\models\bots;

use app\models\Bots;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UsersSearch represents the model behind the search form about `app\models\Users`.
 */
class BotSearch extends Bots
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'companyId'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $company)
    {
        $query = self::find()
        ->alias('bot')
        ->joinWith(['companyModel company']);
        if ($company) {
            $query->where(['companyId' => $company]);
        }
        $this->load($params);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere(['like', 'bot.name', $this->name]);
        $query->andFilterWhere(['like', 'company.company_name', $this->companyId]);

        return $dataProvider;
    }
}
