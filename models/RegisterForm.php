<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\modules\api\controllers\DispatchController;

/**
 * Class RegisterForm
 * @package app\models
 */
class RegisterForm extends Model
{
    const EVENT_REGISTERED = 'event_registered';

    public $fio;
    public $company_name;
    public $login;
    public $telephone;
    public $password;
    public $rate_id;
    public $data_processing;
    public $vk_id;

    /**
     * @var Users содержит созраненную модель users для возможности доступа к ней
     * через обработчик событий
     * @see \app\event_handlers\RegisteredFormEventHandler
     */
    public $_user;

    /**
     * @param $attribute
     * валидатор согласия на обработку данных
     */
    public function dataProcessing ( $attribute ) {
        if(!$this->data_processing){
            $this->addError ( $attribute, "Необходимо Ваше согласие" );
        }
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'class' => \app\event_handlers\RegisteredFormEventHandler::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'login', 'rate_id', 'password', 'telephone'], 'required'],
            [['telephone', 'company_name'], 'string'],
            [['rate_id'], 'integer'],
            [['data_processing'], 'dataProcessing'],
            [['login'], 'email'],
            [['fio'], 'unique', 'targetClass' => '\app\models\Companies', 'targetAttribute' => 'company_name'],
            [['login'], 'unique', 'targetClass' => '\app\models\User'],
            [['company_name'], 'unique', 'targetClass' => '\app\models\Companies'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_name' => 'Названия компании',
            'fio' => 'ФИО',
            'login' => 'Логин',
            'telephone' => 'Телефон',
            'password' => 'Пароль',
            'rate_id' => 'Тариф',
            'data_processing' => 'Я согласен с обработкой персональных данных'
        ];
    }

    /**
     * Регистрирует нового пользователя
     * @param string $type
     * @return Users|null
     * @throws \Exception
     */
    public function register($type = Users::USER_ROLE)
    {
        if ($this->validate() === false) {
            return null;
        }

        $user = new Users();
        $cookies = Yii::$app->request->cookies;
        $transaction = Yii::$app->db->beginTransaction();

        //$user->attributes = $this->attributes;
        $user->name = $this->fio;
        $user->login = $this->login;
        $user->telephone = $this->telephone;
        $user->password = $this->password;
        if ($this->vk_id) $user->vk_id = $this->vk_id;
        $dateNow = time();
        $rate = Rates::findOne($this->rate_id);
        if ($rate != null) {
            $accessEndDateTime = date('Y-m-d H:i:s', strtotime("+ {$rate->time} seconds", $dateNow));
        } else {
            $accessEndDateTime = date('Y-m-d H:i:s', strtotime("+ 14 days", $dateNow));
        }
        $user->role_id = $type;

        if ($user->save()) {
            // echo '<br>USER SAVED<br>';
            $this->_user = $user;
            $company = new Companies([
                'company_name' => $this->fio,
                'admin_id' => $user->id,
                'access_end_datetime' => $accessEndDateTime,
                'created_date' => date('Y-m-d'),
                'rate_id' => $this->rate_id,
                'account_count' => $rate->account_count,
                'dispatch_count' => $rate->dispatch_count,
                'referal' => $cookies->getValue('referal', '1'),
                'general_balance' => $rate->bonus,
                'unique_code' => $this->generateUniqueCode(),
            ]);

            $ratesModel = Rates::findOne($this->rate_id);

            $resultCompany = $company->save();
            if($resultCompany == false){
                $errorsString = array_shift($company->errors)[0];
                DispatchController::sendTelMessage('247187885',  "Ошибка при регистрации. Ошибка произошла в валидации компании: {$errorsString}.
                    Данные при регистрации
                    ФИО: {$this->fio}
                    Логин: {$this->login}
                    Телефон: {$this->telephone}");
            }
            //$user->link('companies', $company);

            $this->trigger(self::EVENT_REGISTERED);

            $template = EmailTemplates::findByKey('success_registration');

            if ($template != null) {
                $body = $template->applyTags($user);

                try {
                    Yii::$app->mailer->compose()
                        ->setFrom('zvonki.crm@mail.ru')
                        ->setTo($user->login)
                        ->setSubject('Успешная регистрация')
                        ->setHtmlBody($body)
                        ->send();
                } catch (\Exception $e) {

                }
            }

            $user->company_id = $company->id;
            $user->save();

            $bot = new Bots([
                'name' => Bots::BASIC_NAME,
                'companyId' => $company->id,
            ]);
            $bot->save();

            if ($freeProxy = Proxy::getFreeProxy()) {
                $i = 1;
                foreach ($freeProxy as $busyProxy) {
                    if ($i > $ratesModel->proxy_count) {
                        continue;
                    }
                    $busyProxy->companyId = $company->id;
                    $busyProxy->busy = 0;
                    $busyProxy->save();
                    $i++;
                }
            }

            if ($freeAccounts = DispatchRegist::getFreeAccounts()) {
                $i = 1;
                foreach ($freeAccounts as $busyAccount) {
                    if ($i > $ratesModel->account_count) {
                        continue;
                    }
                    $busyAccount->company_id = $company->id;
                    $busyAccount->busy = 0;
                    $busyAccount->save();
                    $i++;
                }
            }

            try {
                $dialog1 = new BotsDialogs([
                    'botId' => $bot->id,
                    'stepNumber' => 2,
                    'answerSet' => ['Нет', 'Нет спасибо', 'Нет не требуеться', 'Нет не хочу'],
                    'result' => 'Хорошо спасибо за ответ',
                    'notify' => 0,
                ]);

                $dialog2 = new BotsDialogs([
                    'botId' => $bot->id,
                    'stepNumber' => 2,
                    'answerSet' => ['Да', 'Да требуеться', 'Да нужно', 'Интересно', 'Подробней'],
                    'result' => 'Отлично, Вот информация подробней',
                    'notify' => 1,
                ]);

                $companySettings = new CompanySettings([
                    'company_id' => $company->id,
                    'account_count' => $ratesModel->account_count,
                    'dispatch_count' => $ratesModel->dispatch_count,
                    'proxy_count' => $ratesModel->proxy_count,
                    'bots_count' => $ratesModel->bots_count,
                    'messages_in_transaction' => $ratesModel->messages_in_transaction,
                    'messages_daily_limit' => $ratesModel->messages_daily_limit,
                    'send_interval' => $ratesModel->send_interval,
                    'add_accounts' => $ratesModel->add_accounts,
                    'add_bots' => $ratesModel->add_bots,
                    'add_proxy' => $ratesModel->add_proxy,
                    'dispatch_rules' => $ratesModel->dispatch_rules,
                    'affiliate_percent' => $ratesModel->affiliate_percent ? $ratesModel->affiliate_percent : 0,
                ]);

                if ($dialog1->save() && $dialog2->save() && $companySettings->save()) {
                    $transaction->commit();
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }

            return $user;
        } else {
            return $user;
        }

        return null;
    }


    private function generateUniqueCode()
    {
        $length = 5;
        $chars ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

        do {
            $string ='';
            for( $i = 0; $i < $length; $i++) {
                $string .= $chars[rand(0,strlen($chars)-1)];
            }
        } while ( Companies::find()->where(['unique_code' => $string])->one() != null );

        return $string;
    }

}