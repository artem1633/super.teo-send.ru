<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "global_messages".
 *
 * @property int $id
 * @property string $name Наименование
 * @property resource $text Текст
 * @property int $views_count Кол-во просмотров
 * @property string $created_at
 */
class GlobalMessages extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'global_messages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'text'], 'required'],
            [['text'], 'string'],
            [['views_count'], 'integer'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'text' => 'Текст',
            'views_count' => 'Просмотры',
            'created_at' => 'Дата и время',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if($insert){
            Users::updateAll(['messages_read_status' => false]);
        }
    }

    /**
     * Возращает последнюю запись
     * @return array|\yii\db\ActiveRecord
     */
    public static function getLastMessage()
    {
        return self::find()->orderBy('created_at desc')->one();
    }
}
