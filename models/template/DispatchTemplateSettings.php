<?php

namespace app\models\template;

use yii\base\Model;
use app\components\helpers\FunctionHelper;
use app\models\AutoRegistr;
use app\models\AutoRegistsTemplates;

/**
 * Class DispatchTemplateSettings
 * @package app\models\template
 *
 * @property string $usePosting
 * @property string $useGroups
 * @property string $status
 * @property string $posts
 * @property string $groups
 */
class DispatchTemplateSettings extends Model
{
    public $usePosting;
    public $useGroups;
    public $status;

    public $posts;
    public $groups;

    /**
     * @var \app\models\Dispatch
     */
    public $dispatch;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usePosting', 'useGroups', 'status', 'posts', 'groups'], 'safe'],
            [['posts', 'groups'], function($attribute){
                $array = $this->$attribute;
                if(count($array) > 5) {
                    $this->addError($attribute, 'Не может превышать более 5 записей');
                }
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'usePosting' => 'Репост',
            'useGroups' => 'Группы',
            'status' => 'Статус',
            'posts' => 'Посты',
            'groups' => 'Группы'
        ];
    }

    /**
     * Создает шаблон на базе данной модели
     * @return boolean
     */
    public function makeTemplate()
    {
        if($this->validate()){
            $companyId = FunctionHelper::getCompanyId();

            $autoRegistTemplate = new AutoRegistsTemplates([
                'name' => $this->dispatch->name,
                'company_id' => $companyId,
            ]);
            $autoRegistTemplate->save(false);

            if($this->usePosting == 1){
                foreach ($this->posts as $post){
                    $autoRegistr = new AutoRegistr([
                        'name' => 'Репост',
                        'type' => AutoRegistr::REPOST,
                        'action' => $post,
                        'status' => 1,
                        'template_id' => $autoRegistTemplate->id,
                        'company_id' => $companyId,
                    ]);
                    $autoRegistr->save(false);
                }
            }

            if($this->useGroups == 1){
                foreach ($this->groups as $group){
                    $autoRegistr = new AutoRegistr([
                        'name' => 'Группа',
                        'type' => AutoRegistr::ADDED_TO_GROUP,
                        'action' => $group,
                        'status' => 1,
                        'template_id' => $autoRegistTemplate->id,
                        'company_id' => $companyId,
                    ]);
                    $autoRegistr->save(false);
                }
            }

            if($this->status != null){
                $autoRegistr = new AutoRegistr([
                    'name' => 'Статус',
                    'type' => AutoRegistr::STATUS,
                    'action' => $this->status,
                    'status' => 1,
                    'template_id' => $autoRegistTemplate->id,
                    'company_id' => $companyId,
                ]);
                $autoRegistr->save(false);
            }

            $this->dispatch->auto_regists_template_id = $autoRegistTemplate->id;
            $this->dispatch->save(false);

            return true;
        }

        return false;
    }
}