<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\BlameableBehavior;
use app\base\AppActiveQuery;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $name
 * @property string $login
 * @property string $password
 * @property integer $access_for_moving
 * @property string $telephone
 * @property string $role_id
 * @property string $status
 * @property integer $atelier_id
 * @property double $sales_percent
 * @property double $cleaner_percent
 * @property double $sewing_percent
 * @property double $repairs_percent
 * @property string $auth_key
 * @property string $data_cr
 * @property string $vk_id
 * @property integer $company_id
 * @property bool $messages_read_status
 *
 * @property Atelier $atelier
 */
class Users extends \yii\db\ActiveRecord
{
    const USER_ROLE = 'user';
    const USER_TYPE_SUPER_ADMIN = 'super_admin';
    const USER_ROLE_WAREHOUSE_MANAGER = 'warehouse_manager';
    const USER_ROLE_MASTER = 'master';
    const ACCESS_MOVING_YES = 1;
    const ACCESS_MOVING_NO = 0;
    const USER_STATUS_WORKING = 'working';
    const USER_STATUS_OUT_OF_SERVICE = 'out_of_service';

    public $new_password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function ($event) {
                    if (isset(Yii::$app->user->identity->id)) return Yii::$app->user->identity->company_id;
                    else return null;
                },
            ],
        ];
    }

    public static function find()
    {
        if (isset(Yii::$app->user->identity->id)) {
            $companyId = Yii::$app->user->identity->company_id;
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'password', 'telephone', 'login'], 'required'],
            [['company_id'], 'integer'],
            [['data_cr', 'last_activity_datetime'], 'safe'],
            // [['login'], 'email'],
            [['password', 'new_password', 'vk_id'], 'string', 'max' => 255, 'min' => 6],
            [['name', 'login', 'telephone', 'role_id', 'status', 'auth_key'], 'string', 'max' => 255],
            [['login'], 'unique'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::class, 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'login' => 'Email',
            'password' => 'Пароль',
            'telephone' => 'Телефон',
            'role_id' => 'Должность',
            'status' => 'Статус',
            'auth_key' => 'Пароль',
            'data_cr' => 'Дата регистрации',
            'company_id' => 'Компания, к которой принадлежит пользователь',
            'vk_id' => 'ВК Ид',
            'last_activity_datetime' => 'Последняя активность',
            'new_password' => 'Новый пароль',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->auth_key = $this->password;
            $this->password = md5($this->password);
            $this->data_cr = date('Y-m-d');

            if (isset(Yii::$app->user->identity->id)) {
                $log = new Logs([
                    'user_id' => Yii::$app->user->identity->id,
                    'event_datetime' => date('Y-m-d H:i:s'),
                    'event' => Logs::EVENT_USER_ADDED,
                ]);
                $log->description = $log->generateEventDescription();
                $log->save();
            } else {
                $log = new Logs([
                    'user_id' => null,
                    'event_datetime' => date('Y-m-d H:i:s'),
                    'event' => Logs::EVENT_USER_ADDED,
                ]);
                $log->description = $log->generateEventDescription();
                $log->save();
            }
        }

        if ($this->new_password != null) {
            $this->auth_key = $this->new_password;
            $this->password = md5($this->new_password);
        }
        return parent::beforeSave($insert);
    }

    public function getRoleList()
    {
        return ArrayHelper::map([
            ['id' => self::USER_ROLE, 'name' => 'Пользователь',],

        ], 'id', 'name');
    }

    public function getRoleDescription()
    {
        if (self::USER_ROLE == $this->role_id) return 'Пользователь';

    }

    public function getMovingList()
    {
        return ArrayHelper::map([
            ['id' => self::ACCESS_MOVING_YES, 'name' => 'Да',],
            ['id' => self::ACCESS_MOVING_NO, 'name' => 'Нет',],
        ], 'id', 'name');
    }

    public function getMovingDescription()
    {
        if (self::ACCESS_MOVING_YES == $this->access_for_moving) return 'Да';
        if (self::ACCESS_MOVING_NO == $this->access_for_moving) return 'Нет';
    }

    public static function getCompanyId($id)
    {
        $user = Users::find()->where(['id' => $id])->one();
        return $user->company_id;
    }

    public function getStatusList()
    {
        return ArrayHelper::map([
            ['id' => self::USER_STATUS_WORKING, 'name' => 'Работает',],
            ['id' => self::USER_STATUS_OUT_OF_SERVICE, 'name' => 'Не работает',],
        ], 'id', 'name');
    }

    public function getStatusDescription()
    {
        if (self::USER_STATUS_WORKING == $this->status) return 'Работает';
        if (self::USER_STATUS_OUT_OF_SERVICE == $this->status) return 'Не работает';
    }

    public function getAtelierList()
    {
        $ateliers = Atelier::find()->all();
        return ArrayHelper::map($ateliers, 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorages()
    {
        return $this->hasMany(Storage::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtelier()
    {
        return $this->hasOne(Atelier::className(), ['id' => 'atelier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvailables()
    {
        return $this->hasMany(Available::className(), ['creator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResources()
    {
        return $this->hasMany(Resource::className(), ['creator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplications()
    {
        return $this->hasMany(Application::className(), ['creator' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Companies::className(), ['admin_id' => 'id']);
    }

    public function isAdmin()
    {
        $role = Users::findOne(Yii::$app->user->id)->role_id;
        Yii::warning($role, __METHOD__);
        if ($role == self::USER_TYPE_SUPER_ADMIN) {
            return true;
        } else {
            return false;
        }
    }
}
