<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dispatch_connect".
 *
 * @property int $id
 * @property int $dispatch_id Ид рассылок
 * @property int $dispatch_regist_id Ид акаунтов
 * @property int $company_id
 * @property DispatchRegist $dispatchRegist
 * @property Dispatch $dispatch
 */
class DispatchConnect extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dispatch_connect';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dispatch_id', 'dispatch_regist_id', 'company_id'], 'integer'],
            [['dispatch_regist_id'], 'exist', 'skipOnError' => true, 'targetClass' => DispatchRegist::className(), 'targetAttribute' => ['dispatch_regist_id' => 'id']],
            [['dispatch_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dispatch::className(), 'targetAttribute' => ['dispatch_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dispatch_id' => 'Dispatch ID',
            'dispatch_regist_id' => 'Dispatch Regist ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDispatchRegist()
    {
        return $this->hasOne(DispatchRegist::className(), ['id' => 'dispatch_regist_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDispatch()
    {
        return $this->hasOne(Dispatch::className(), ['id' => 'dispatch_id']);
    }
}
