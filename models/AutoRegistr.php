<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "auto_registr".
 *
 * @property int $id
 * @property string $name Название
 * @property int $type Тип
 * @property bool $status Статус
 * @property string $action Действие
 * @property int $company_id Компания
 * @property int $template_id Шаблон
 *
 * @property Companies $company
 * @property AutoRegistsTemplates $template
 */
class AutoRegistr extends \yii\db\ActiveRecord
{
    const SCENARIO_ACTION = 'action';

    /**
     * @const Добавление в группу
     */
    const ADDED_TO_GROUP = 1;

    /**
     * @const Изменение статуса
     */
    const STATUS = 2;

    /**
     * @const Репост записи
     */
    const REPOST = 3;

    /**
     * @const Изменение города
     */
    const SET_CITY = 4;

    /**
     * @const Интересы
     */
    const INTERESTS = 5;

    /**
     * @const Музыка
     */
    const MUSIC = 6;

    /**
     * @const ФОто
     */
    const PHOTO = 7;

    /**
     * @var string Наименование нового шаблона
     */
    public $templateName;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auto_registr';
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => ['name', 'type', 'status', 'action', 'template_id', 'templateName'],
            self::SCENARIO_ACTION => ['name', 'type', 'status', 'action', 'template_id', 'templateName'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $rules = [
            [['name', 'type', 'status'], 'required'],
            [['action'], 'required', 'when' => function($model){
                return $model->scenario == self::SCENARIO_ACTION;
            }],
            [['type', 'company_id'], 'integer'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['template_id'], 'exist', 'skipOnError' => true, 'targetClass' => AutoRegistsTemplates::className(), 'targetAttribute' => ['template_id' => 'id']],
        ];

        switch ($this->type)
        {
            case self::PHOTO:
                $rules[] = [['action'], 'file'];
                break;

            default:
                $rules[] = [['action'], 'string'];
                break;
        }

        return $rules;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        $labels = [
            'id' => 'ID',
            'name' => 'Название',
            'type' => 'Тип',
            'status' => 'Статус',
            'action' => 'Действие',
            'company_id' => 'Компания',
            'template_id' => 'Шаблон',
            'templateName' => 'Наименование нового шаблона',
        ];

        switch ($this->type)
        {
            case self::ADDED_TO_GROUP:
                $labels['action'] = 'Группа';
                break;

            case self::STATUS:
                $labels['action'] = 'Статус';
                break;

            case self::REPOST:
                $labels['action'] = 'Запись';
                break;

            case self::SET_CITY:
                $labels['action'] = 'Город';
                break;

            case self::INTERESTS:
                $labels['action'] = 'Интересы';
                break;

            case self::MUSIC:
                $labels['action'] = 'Музыка';
                break;

            case self::PHOTO:
                $labels['action'] = 'Фото';
                break;
        }

        return $labels;
    }

    /**
     * @return array
     */
    public static function getTypesList()
    {
        return [
            self::ADDED_TO_GROUP => 'Вступить в группу',
            self::STATUS => 'Смена статуса',
            self::REPOST => 'Репост',
//            self::SET_CITY => 'Установить город',
//            self::INTERESTS => 'Интересы',
//            self::PHOTO => 'Фото',
        ];
    }

    /**
     * @return array
     */
    public static function getStatusList()
    {
        return [
            1 => 'ВКЛ',
            0 => 'ВЫКЛ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(AutoRegistsTemplates::className(), ['id' => 'template_id']);
    }
}
