<?php

namespace app\models\calculation;

use app\components\helpers\FunctionHelper;
use yii\base\Model;

class Calculation extends Model
{
    public $base;

    public $step;

    public $speed_akk;

    public $price_akk;

    public $calculatedData;

    public $proxy;

    public $proxy_day;

    public function rules()
    {
        return [
            [['proxy', 'proxy_day', 'base', 'step', 'speed_akk', 'price_akk'], 'required'],
            [['proxy', 'proxy_day', 'base', 'step', 'speed_akk', 'price_akk'], 'number']
        ];
    }

    public function attributeLabels()
    {
        return [
            'base' => 'Объем базы',
            'step' => 'Шаг расчета',
            'price_akk' => 'Стоимость аккаунта',
            'speed_akk' => 'Сообщений в сутки',
            'proxy' => 'Кол-во акк на прокси',
            'proxy_day' => 'Стоимоть прокси',
        ];
    }

    /**
     * @return boolean
     */
    public function calculate()
    {
        if($this->validate())
        {
            $companySettings = FunctionHelper::getCompanySettings();
            $priceMessage = $companySettings->getPriceByMessage();

            $data = [];
            $count = $this->base/$this->step;
            $i = 0;
            while ( $i < $count) {
                $i++;
                $speed = $this->step * $i;
                $day = ceil($this->base / $speed);
                ///
                $akk = ceil($speed / $this->speed_akk);
                $ressed_akk = ceil($day / 3);
                $all_akk = $ressed_akk * $akk;
                $akk_money = (($all_akk * $this->price_akk));
                ///
                $proxy = ceil($akk / $this->proxy);
                $proxy_day = $proxy * $this->proxy_day;
                $proxy_money_all = $proxy_day * $day;
                ///
                $sum = ceil(($akk_money + $proxy_money_all));

                $data[] = [
                    'number' => "№$i/$count",
                    'speed' => $speed,
                    'day' => $day,
                    'all_akk' => $all_akk,
                    'akk' => $akk,
                    'sum' => $sum,
                    'price_by_message' => $priceMessage,
                    'total_price' => $priceMessage * $this->base,
                ];
            }

            usort($data, function($a, $b){
                return ($a['speed'] > $b['speed']) ? 1 : -1;
            });

            $counter = 1;
            foreach ($data as &$row){

                $row['number'] =  "№{$counter}/".count($data);

                $counter++;
            }


            $this->calculatedData = $data;
            return true;
        }

        return false;
    }
}