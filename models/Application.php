<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "application".
 *
 * @property integer $id
 * @property string $date_cr
 * @property integer $atelier_id
 * @property integer $creator
 * @property integer $product_id
 * @property integer $count
 * @property integer $status_id
 *
 * @property Atelier $atelier
 * @property Users $creator0
 * @property Product $product
 * @property ClaimStatuses $status
 */
class Application extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $search_atelier;
    public $search_product;
    public $active_window;
    public static function tableName()
    {
        return 'application';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_cr'], 'safe'],
            [['atelier_id', 'product_id', 'count'], 'required'],
            [['atelier_id', 'creator', 'product_id', 'status_id', 'count', 'active_window'], 'integer'],
            [['atelier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Atelier::className(), 'targetAttribute' => ['atelier_id' => 'id']],
            [['creator'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['creator' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => manual\Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => manual\ClaimStatuses::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_cr' => 'Дата создания',
            'atelier_id' => 'Ателье',
            'creator' => 'Создатель',
            'product_id' => 'Товар',
            'count' => 'Количество',
            'status_id' => 'Статус',
            'search_product' => 'Товар',
            'search_atelier' => 'Ателье',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->creator = Yii::$app->user->identity->id;
            $this->status_id = 1;
            $this->date_cr = date('Y-m-d H:i:s');
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtelier()
    {
        return $this->hasOne(Atelier::className(), ['id' => 'atelier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator0()
    {
        return $this->hasOne(Users::className(), ['id' => 'creator']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(manual\Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(manual\ClaimStatuses::className(), ['id' => 'status_id']);
    }

    public function getAtelierList()
    {
        $atelier = Atelier::find()->all();
        return ArrayHelper::map($atelier, 'id', 'name');
    }

    public function getProductList()
    {
        $product = manual\Product::find()->all();
        return ArrayHelper::map($product, 'id', 'name');
    }

    public function getStatusList()
    {
        $status = manual\ClaimStatuses::find()->all();
        return ArrayHelper::map($status, 'id', 'name');
    }
}
