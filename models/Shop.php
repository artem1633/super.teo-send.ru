<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shop".
 *
 * @property int $id
 * @property string $date_added Дата и время создания
 * @property string $date_updated Дата обновления
 * @property string $name Заголовок
 * @property string $description_short Краткое описание
 * @property string $description_long Описание
 * @property string $additional_info Дополнительная информация
 * @property int $type Тип товара
 * @property string $image Путь к изображению
 * @property int $access Доступность
 * @property string $price Цена
 * @property int $remains Остаток
 * @property string $accounts_data Аккаунты на продажу
 * @property string $itemsAccounts Список аккаунтов
 * @property string $itemsProxy Список прокси
 * @property string $itemsData Список баз данных
 * @property string $itemsBots Список ботов
 *
 * @property int $accountsRemain
 */
class Shop extends \yii\db\ActiveRecord
{
    const MAX_ACCOUNTS_DATA = 1000;

    public $itemsAccounts;
    public $itemsProxy;
    public $itemsData;
    public $itemsBots;


    const TYPE_ACCOUNT = 1; //Аккаунт
    const TYPE_PROXY = 2; //Прокси
    const TYPE_DATABASE = 3; //База данных
    const TYPE_BOT = 4; //Бот
    const TYPE_ACCOUNT_DATA = 5; // Аккауты

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shop';
    }


    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
//        if($this->new_accounts_data != null)
//        {
//            $this->accounts_data .= $this->accounts_data == null ? $this->new_accounts_data : "\n".$this->new_accounts_data;
//        }

        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($this->type) {
            if ($this->itemsBots != null && is_array($this->itemsBots)) {
                if ($olds = Bots::find()->where(['shop_id' => $this->id])->all()) {
                    foreach ($olds as $old) {
                        $old->shop_id = null;
                        $old->save();
                    }
                }
                foreach ($this->itemsBots as $item) {
                    $this->afterSaveSwitch($item);
                }
            }

            if ($this->itemsData != null && is_array($this->itemsData)) {
                if ($olds = DataRecipient::find()->where(['shop_id' => $this->id])->all()) {
                    foreach ($olds as $old) {
                        $old->shop_id = null;
                        $old->save();
                    }
                }
                foreach ($this->itemsData as $item) {
                    $this->afterSaveSwitch($item);
                }
            }
            if ($this->itemsAccounts != null && is_array($this->itemsAccounts)) {
                if ($olds = DispatchRegist::find()->where(['shop_id' => $this->id])->all()) {
                    foreach ($olds as $old) {
                        $old->shop_id = null;
                        $old->busy = 1;
                        $old->save();
                    }
                }
                foreach ($this->itemsAccounts as $item) {
                    $this->afterSaveSwitch($item);
                }

            }
            if ($this->itemsProxy != null && is_array($this->itemsProxy)) {
                if ($olds = Proxy::find()->where(['shop_id' => $this->id])->all()) {
                    foreach ($olds as $old) {
                        $old->shop_id = null;
                        $old->busy = 1;
                        $old->save();
                    }
                }
                foreach ($this->itemsProxy as $item) {
                    $this->afterSaveSwitch($item);
                }
            }
        }
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_added', 'accounts_data', 'date_updated', 'itemsAccounts', 'itemsProxy', 'itemsData', 'itemsBots'], 'safe'],
            [['type', 'price'], 'required'],
            [['type', 'access', 'remains'], 'integer'],
            [['price'], 'number'],
            [['name', 'description_short', 'description_long', 'additional_info', 'image'], 'string', 'max' => 255],

            ['accounts_data', function($attribute, $params, $validator){
                $countLines = count(explode("\n", $this->$attribute));
                $sumLines = $countLines + $this->accountsRemain;

                if($sumLines > self::MAX_ACCOUNTS_DATA)
                {
                    $this->addError($attribute, 'Вы превысили лимит загрузки данных');
                }
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_added' => 'Дата и время создания',
            'date_updated' => 'Дата обновления',
            'name' => 'Заголовок',
            'description_short' => 'Краткое описание',
            'description_long' => 'Описание',
            'additional_info' => 'Дополнительная информация',
            'type' => 'Тип товара',
            'image' => 'Путь к изображению',
            'access' => 'Доступен',
            'price' => 'Цена',
            'remains' => 'Остаток',
            'itemsAccounts' => 'Выберите аккаунты для продажи',
            'itemsProxy' => 'Выберите прокси для продажи',
            'itemsData' => 'Выберите базы для продажи',
            'itemsBots' => 'Выберите ботов для продажи',
            'accounts_data' => 'Аккаунты',
//            'new_accounts_data' => 'Аккаунты'
        ];
    }

    /**
     * Возвращает кол-во акканутов
     * @return int
     */
    public function getAccountsRemain()
    {
        if($this->accounts_data == null)
            return 0;

        return count(explode("\n", $this->accounts_data));
    }

    /**
     * @param $item
     * @return null
     */
    public function afterSaveSwitch($item)
    {
        switch ($this->type) {
            case self::TYPE_ACCOUNT:
                $model = DispatchRegist::findOne($item);
                $model->shop_id = $this->id;
                $model->busy = null;
                $model->save();
                break;
            case self::TYPE_PROXY:
                $model = Proxy::findOne($item);
                $model->shop_id = $this->id;
                $model->busy = null;
                $model->save();
                break;
            case self::TYPE_DATABASE:
                $model = DataRecipient::findOne($item);
                $model->shop_id = $this->id;
                $model->save();
                break;
            case self::TYPE_BOT:
                $model = Bots::findOne($item);
                $model->shop_id = $this->id;
                $model->save();
                break;
            default:
                $model = null;
        }
        return null;
    }

    /**
     * @return array|null
     */
    public function getSelectBot()
    {
        if ($bots = Bots::find()->where(['shop_id' => $this->id])->all()) {
            $result = [];
            foreach ($bots as $bot) {
                $result[] = $bot->id;
            }
            return $result;
        }
        return null;
    }

    /**
     * @return array|null
     */
    public function getSelectAccounts()
    {
        if ($accs = DispatchRegist::find()->where(['shop_id' => $this->id])->all()) {
            $result = [];
            foreach ($accs as $acc) {
                $result[] = $acc->id;
            }
            return $result;
        }
        return null;
    }

    /**
     * @return array|null
     */
    public function getSelectProxy()
    {
        if ($proxy = Proxy::find()->where(['shop_id' => $this->id])->all()) {
            $result = [];
            foreach ($proxy as $prox) {
                $result[] = $prox->id;
            }
            return $result;
        }
        return null;
    }

    /**
     * @return array|null
     */
    public function getSelectData()
    {
        if ($datas = DataRecipient::find()->where(['shop_id' => $this->id])->all()) {
            $result = [];
            foreach ($datas as $data) {
                $result[] = $data->id;
            }
            return $result;
        }
        return null;
    }

    /**
     * @return string
     */
    public function getNameType($id = 0)
    {

        if ($id){
            $obj = $id;
        } else {
            $obj = $this->type;
        }

        switch ($obj) {
            case self::TYPE_PROXY:
                return 'Прокси';
            case self::TYPE_DATABASE:
                return 'Базы данных';
            case self::TYPE_BOT:
                return 'Боты';
            case self::TYPE_ACCOUNT:
                return 'Аккаунты';
            case self::TYPE_ACCOUNT_DATA:
                return 'Аккаунты (список)';
            default:
                return 'Ошибка! Перезапишите товар!';
        }
    }

    public function getRemains()
    {
        switch ($this->type) {
            case self::TYPE_PROXY:
                return Proxy::find()->where(['shop_id' => $this->id])->count();
            case self::TYPE_DATABASE:
                return DataRecipient::find()->where(['shop_id' => $this->id])->count();
            case self::TYPE_BOT:
                return Bots::find()->where(['shop_id' => $this->id])->count();
            case self::TYPE_ACCOUNT:
                return DispatchRegist::find()->where(['shop_id' => $this->id])->count();
            case self::TYPE_ACCOUNT_DATA:
                return $this->accountsRemain;
            default:
                return 1000000;
        }
    }


}
