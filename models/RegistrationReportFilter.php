<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class RegistrationReportFilter
 * @package app\models
 */
class RegistrationReportFilter extends Model
{
    /**
     * @var string
     */
    public $dates;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dates'], 'safe']
        ];
    }

    /**
     * Производит поиск
     * @return array
     */
    public function search()
    {
        $report = [];
        $report[1] = [];//кол-во регистраций
        $report[2] = [];//кол-во операций
        $report[3] = [];//сумма операций
        $report[4] = [];//кол-во блокир акк
        $report[5] = [];//кол-во онлайн
        $report[6] = [];//кол-во купленых акк
        $report[7] = [];//кол-во отправленых  сообщений
        $report[8] = [];//список компания кто пополнил
        if($this->dates != null) {
            $this->dates = explode(' - ', $this->dates);
            $dateStart = $this->dates[0];
            $dateEnd = $this->dates[1];
        } else {
            $year = date('Y');
            $month = date('m');

            $lastDayMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year);

            $dateStart = date('Y-m-1');
            $dateEnd = date('Y-m-'.$lastDayMonth);
        }

        $query = Users::find();

        $query->andFilterWhere(['between', 'data_cr', $dateStart, $dateEnd]);

        $report[0] = array_reverse(array_unique(ArrayHelper::getColumn($query->orderBy('data_cr desc')->all(), 'data_cr')));

        $report[8] = AccountingReport::find()
            ->andfilterWhere(['between', 'date_report', $dateStart, $dateEnd])
            ->andfilterWhere(['=','operation_type','1'])->groupBy('company_id')->all();
        foreach ($report[0] as $date)
        {
            $report[1][] = intval(Users::find()->where(['data_cr' => $date])->count());
//            $finCount = intval(AccountingReport::find()->andfilterWhere(['like','date_report',$date])->andfilterWhere(['=','operation_type','1'])->count());
//            if ($finCount > 0) {
            $report[2][] = intval(AccountingReport::find()->andfilterWhere(['like','date_report',$date])->andfilterWhere(['=','operation_type','1'])->count());
            // }
            $report[3][] = intval(AccountingReport::find()->andfilterWhere(['like','date_report',$date])->andfilterWhere(['=','operation_type','1'])->sum('amount'));
            $report[4][] = intval(DailyReport::find()->andfilterWhere(['like','date_event',$date])->andfilterWhere(['=','type',DailyReport::TYPE_BLOCK_DISPATCH_REGIST])->count());
            $report[5][] = intval(DailyReport::find()->andfilterWhere(['like','date_event',$date])->andfilterWhere(['=','type',DailyReport::TYPE_VISIT])->count());
            $report[6][] = intval(AccountingReport::find()->andfilterWhere(['like','date_report',$date])->andfilterWhere(['=','operation_type','6'])->count());
            $report[7][] = intval(AccountingReport::find()->andfilterWhere(['like','date_report',$date])->andfilterWhere(['=','operation_type','3'])->sum('amount'));

        }

//        $queryFinance = AccountingReport::find();
//        $dateStart .= ' 00:00:00';
//        $dateEnd .= ' 00:00:00';
//        var_dump($dateStart);
////        $dateStart = date('Y-m-1 00:00:00');
////        $dateEnd = date('Y-m-'.$lastDayMonth.' 24:00:00');
//        $queryFinance->andFilterWhere(['between', 'date_report', $dateStart, $dateEnd]);
//        $queryFinance->andFilterWhere(['=', 'operation_type', '1']);
//        $queryFinance->groupBy('date_report');

//        $report[2] = array_reverse(array_unique(ArrayHelper::getColumn($queryFinance->orderBy('date_report desc')->all(), 'date_report')));
//
//        var_dump($report[2]);
//        foreach ($report[2] as $date1)
//        {
//            $report[3][] = intval(AccountingReport::find()->where(['date_report' => $date1])->sum('amount'));
//        }

        return $report;
    }
}