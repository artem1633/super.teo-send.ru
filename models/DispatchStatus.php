<?php

namespace app\models;

use Yii;
use app\components\NeyroBot;
use app\modules\api\controllers\DispatchController;

/**
 * This is the model class for table "dispatch_status".
 *
 * @property int $id
 * @property int $account_id
 * @property string $status
 * @property string $data
 * @property int $send_account_id
 * @property int $company_id
 * @property int $dispatch_id Ид отправленного сообщении
 * @property string $read Информация о прочтаных
 * @property string $send Информация об отправке
 * @property string $new_message Новое сообщение
 * @property string $photo URL фотографии
 * @property string $name Имя пользователя
 * @property string $age Возраст
 * @property string $gender Пол
 * @property string $city Город
 * @property string $default_account_id
 * @property int $unique_message_id
 * @property boolean $check_bot
 *
 *
 * @property Dispatch $dispatch
 * @property Statuses $statusModel
 */
class DispatchStatus extends \yii\db\ActiveRecord
{
    const EVENT_CHANGED_STATUS = 'changed_status';

    /**
     * @const Статус "В обработке"
     */
    const STATUS_HANDLE = 'handle';

    /**
     * @const Статус "В ожидании"
     */
    const STATUS_WAIT = "wait";

    /**
     * @const Статус "В работе"
     */
    const STATUS_JOB = "job";

    /**
     * @const Статус "Завершен"
     */
    const STATUS_FINISH = "finish";

    /**
     * @const Статус "Есть непрочитанное сообщение"
     */
    const STATUS_UNREAD_MESSAGE = "unread_message";

    /**
     * @const Статус "Переписка"
     */
    const STATUS_CONVERSATION = "conversation";

    /**
     * @const Статус "Аккаунт заблокирован"
     */
    const STATUS_ACCOUNT_BLOCKED = "account_blocked";

    /**
     * @const Статус "Отклонен"
     */
    const STATUS_REFUSED = "refused";


    const IS_READ = 'yes';
    public $dialog;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dispatch_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dispatch_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dispatch::className(), 'targetAttribute' => ['dispatch_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ИД',
            'account_id' => 'ИД получателя',
            'status' => 'Статус',
            'data' => 'Дата отправки',
            'send_account_id' => 'С какого акк было отправленно',
            'dispatch_id' => 'Ид сообщении',
            'read' => 'Прочитан',
            'dialog' => 'Перейти в переписку',
            'status_push_sales' => 'Отправили в слс',
            'send' => 'Отправлено',
            'new_message' => 'Новое сообщение',
            'photo' => 'URL фотографии',
            'name' => 'Имя',
            'age' => 'Возраст',
            'gender' => 'Пол',
            'city' => 'Город',
            'check_bot' => 'Проверка ботом'
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $changedAttributesNames = array_keys($changedAttributes);
        if(in_array('status', $changedAttributesNames)){
            $this->trigger(self::EVENT_CHANGED_STATUS);
            $boolStatus = null;
            if(in_array($this->status, [1, 2, 3])){
                $boolStatus = 1;
            } else if($this->status == 4) {
                $boolStatus = 0;
            }
            $message = Message::find()->where(['dispatch_status_id' => $this->id])->one();
            if($message != null){
                $text = $message->text;
                $day = date('d', strtotime($message->date_time));
                $month = date('m', strtotime($message->date_time));
            } else {
                $text = '';
                $day = '';
                $month = '';
            }

            $age = intval($this->age);
            $gender = intval($this->gender);

            $dispatch = Dispatch::findOne($this->dispatch_id);

            if($dispatch != null){
                $dispatchId = $dispatch->id;
            } else {
                $dispatchId = null;
            }

            $response = NeyroBot::sendInfo([
                'text' => $text,
                'gender' => $gender,
                'age' => $age,
                'city' => $this->city,
                'month' => $month,
                'day' => $day,
                'server_id' => $dispatchId,
                'status' => $boolStatus,
            ]);
            // $response = NeyroBot::sendInfo([
            //     'text' => 'Добрый вечер',
            //     'gender' => 0,
            //     'age' => '23',
            //     'city' => 'Москва',
            //     'month' => '04',
            //     'day' => '09',
            //     'server_id' => 10,
            //     'status' => 1,
            // ]);
            DispatchController::sendTelMessage('300640816', $response);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDispatch()
    {
        return $this->hasOne(Dispatch::className(), ['id' => 'dispatch_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusModel()
    {
        return $this->hasOne(Statuses::className(), ['id' => 'status']);
    }

    /**
     * @return array
     */
    public static function getDispatchStatuses()
    {
        $statuses = [
            self::STATUS_HANDLE => 'В обработке',
            self::STATUS_WAIT => 'Ожидание',
            self::STATUS_JOB => 'В работе',
            self::STATUS_FINISH => 'Завершено',
            self::STATUS_UNREAD_MESSAGE => 'Есть непрочитанные сообщения',
            self::STATUS_CONVERSATION => 'Диалог',
            self::STATUS_ACCOUNT_BLOCKED => 'Аккаунт заблокирован',
            self::STATUS_REFUSED => 'Отклонён',
        ];
        return $statuses;
    }
}
