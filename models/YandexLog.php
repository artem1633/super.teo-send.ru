<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "yandex_log".
 *
 * @property int $id
 * @property string $secret secret
 * @property string $notification_type notification_type
 * @property string $operation_id operation_id
 * @property string $amount amount
 * @property string $currency currency
 * @property string $datetime datetime
 * @property string $sender sender
 * @property string $codepro codepro
 * @property string $label label
 * @property string $sha1_hash sha1_hash
 * @property string $sha1_res sha1_res
 */
class YandexLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yandex_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['secret', 'notification_type', 'operation_id', 'amount', 'currency', 'datetime', 'sender', 'codepro', 'label', 'sha1_hash', 'sha1_res'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'secret' => 'Secret',
            'notification_type' => 'Notification Type',
            'operation_id' => 'Operation ID',
            'amount' => 'Amount',
            'currency' => 'Currency',
            'datetime' => 'Datetime',
            'sender' => 'Sender',
            'codepro' => 'Codepro',
            'label' => 'Label',
            'sha1_hash' => 'Sha1 Hash',
            'sha1_res' => 'Sha1 Res',
        ];
    }
}
