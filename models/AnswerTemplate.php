<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "answer_template".
 *
 * @property int $id
 * @property string $name
 * @property string $text
 */
class AnswerTemplate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'answer_template';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Ид',
            'name' => 'Название',
            'text' => 'Текст',
        ];
    }
}
