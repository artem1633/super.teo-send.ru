<?php

namespace app\models\textGeneration;

use app\components\helpers\FunctionHelper;
use app\components\helpers\TagHelper;
use yii\helpers\ArrayHelper;
use yii\base\Model;
use app\models\TemplateMessages;

class TextGeneration extends Model
{
	public $text;

	public $base;

	public $companyId;

	private $generatedText;


	public function rules()
	{
		return [
			[['text'], 'required'],
			['text', 'string'],
			[['base', 'companyId'], 'number'],
		];
	}

	public function attributeLabels()
	{
		return [
			'text' => 'Текст',
			'base' => 'Объем базы',
			'compantId' => 'Компания',
		];
	}

	public function getGeneratedText()
	{
		return $this->generatedText;
	}

	public function generate()
	{
		if($this->validate())
		{
			$tags = TemplateMessages::find()->where(['or', ['company_id' => FunctionHelper::getCompanyId()], ['company_id' => 1]]);

			if($this->companyId != null){
				$tags->orFilterWhere(['company_id' => $this->companyId]);
			}

			$tags = $tags->all();

	        $countCopy = 0;
	        $copy = [];
	        $ic = 0;
	        for ($i =0; $i < $this->base; $i++){
	            $ic++;
	            $msg = $this->text;


	            $msg = TagHelper::handleTemplateMessages($msg, $tags);

	            if (array_search($msg, $copy)) {
	                $countCopy++;
	                // echo "<br/>{$ic}   ---- <font size='2' color='red'>$ic{$msg}</font>";
	            } else {
	                // echo "<br/>{$ic}   ---- <font size='2' color='green'>{$msg}</font>";
	            }
	            $copy[] = [
	            	'text' => $msg,
	            ];
	        }


	        $uniqueText = array_unique(ArrayHelper::getColumn($copy, 'text'));
	        $uniqueText = array_fill_keys($uniqueText, 0);
	        // var_dump($uniqueText);
	        // exit;

	        foreach ($copy as $row) {
	        	foreach ($uniqueText as $text => $value) {
	        		if($text == $row['text']){
	        			$uniqueText[$text]++;
	        			continue;
	        		}
	        	}
	        }

	        $generatedText = [];
	       
	       	foreach ($uniqueText as $text => $value) {
	       		$generatedText[] = [
	       			'text' => $text,
	       			'repeats' => $value,
	       		];
	       	}

	        $this->generatedText = $generatedText;
	        return true;
		} else {
			return false;
		}
	}
}