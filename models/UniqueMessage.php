<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "unique_message".
 *
 * @property int $id
 * @property int $dispatch_id Рассылка
 * @property string $text Текст
 * @property int $repeat_count Кол-во повторений
 *
 * @property Dispatch $dispatch
 */
class UniqueMessage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'unique_message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dispatch_id', 'repeat_count'], 'integer'],
            [['text'], 'string'],
            ['repeat_count', 'default', 'value' => 0],
            [['dispatch_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dispatch::className(), 'targetAttribute' => ['dispatch_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dispatch_id' => 'Рассылка',
            'text' => 'Текст',
            'repeat_count' => 'Кол-во повторов',
        ];
    }

    /**
     * @param string $text
     * @return bool
     */
    public static function hasWithText($text, $dispatch_id = null)
    {
        $model = self::find()->where(['text' => $text])->andFilterWhere(['dispatch_id' => $dispatch_id])->one();

        return $model != null;
    }

    /**
     * @param string $text
     * @return $this
     */
    public static function findByText($text)
    {
        $model = self::findOne(['text' => $text]);
        return $model;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDispatch()
    {
        return $this->hasOne(Dispatch::className(), ['id' => 'dispatch_id']);
    }
}
