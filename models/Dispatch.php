<?php

namespace app\models;

use app\components\helpers\FunctionHelper;
use app\controllers\DispatchController;
use app\modules\api\controllers\ClientController;
use app\modules\api\controllers\VkController;
use app\validators\DeniedWordsValidator;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use app\modules\api\controllers\DispatchController as ApiDispatchController;

/**
 * This is the model class for table "dispatch".
 *
 * @property int $id
 * @property string $name
 * @property string $text1
 * @property string $text2
 * @property string $text3
 * @property string $text4
 * @property string $status
 * @property string $account_id
 * @property string $data
 * @property int $company_id
 * @property int $chat
 * @property int $botId
 * @property int $autoAnswer
 * @property string $days_week
 * @property string $start_time
 * @property boolean $to_stat
 * @property string $text_data
 * @property int $select_data
 * @property int $auto_regists_template_id
 * @property int|boolean $use_auto_registr
 * @property int $accounts_count Кол-во аккаунтов на данную рассылку
 * @property int $day_speed Суточная скорость (акк/день)
 * @property int $blocked_accounts_count Кол-во заблокированных аккаунтов
 *
 * @property int $remainAccountsCount Кол-во аккаунтов которые необхадимо подключить
 *
 * @property DispatchRegist[] $dispatchRegists
 */
class Dispatch extends \yii\db\ActiveRecord
{
    const USERS_SESSION_KEY = 'groupUsers';
    //Тэги
    const TAG_DISPATCH_NAME = '{%dispatchName%}'; //Наименование раcсылки
    const TAG_MESSAGE_SENDER = '{%msgSender%}'; //Отправитель сообщения
    const TAG_MESSAGE_RECIPIENT = '{%msgRecipient%}'; //Получатель сообщения
    const TAG_CITY_RECIPIENT = '{%cityRecipient%}'; //Город получателя сообщения
    const TAG_BASE_NAME = '{%baseName%}'; //Наименование базы.

    public $file;
    public $filename;
    public $accounts;
    public $view;
    public $chat;
    public $text_data;
    public $select_data;
    public $recived_data;
    public $selected_dispatches;
    public $selected_statuses;
    public $base_name;

    public $speed;
    public $price;

    const STATUS_ACCOUNTS_HANDLING = 'accounts_handling';

    const STATUS_DAILY_LIMIT = 'daily_limit';

    const STATUS_WRONG_IDS = 'wrong_ids';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dispatch';
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->days_week != null && is_array($this->days_week)) {
            $this->days_week = implode($this->days_week, ',');
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        $statuses = DispatchStatus::find()->where(['dispatch_id' => $this->id])->all();

        foreach ($statuses as $status) {
            $status->delete();
        }

        return parent::beforeDelete();
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @return void
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function afterSave($insert, $changedAttributes)
    {
        if($insert){
            $company = FunctionHelper::getCompanyModel();
            ApiDispatchController::sendTelMessage('247187885', "Создана новая рассылка id: {$this->id} у компании {$company->company_name}");
        }

        if ($this->chat != null && is_array($this->chat)) {
            if ($model = DispatchChats::find()->where(['dispatchId' => $this->id])->all()) {
                foreach ($model as $chat) {
                    $chat->delete();
                }
            }
            foreach ($this->chat as $item) {
                $modelAnswer = new DispatchChats([
                    'dispatchId' => $this->id,
                    'chatId' => $item,
                ]);
                $modelAnswer->save();
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['auto_regists_template_id'], 'required', 'when' => function($model){
                return $model->use_auto_registr == 1;
            }],
            [['text1', 'text2', 'text3', 'text4', 'base_name', 'selected_statuses','selected_dispatches', 'data', 'chat', 'days_week', 'recived_data'], 'safe'],
            [['file'], 'file', 'extensions' => 'txt'],
            [['text_data'], 'string'],
            [['botId', 'autoAnswer', 'company_id', 'select_data', 'auto_regists_template_id', 'accounts_count', 'day_speed', 'price', 'blocked_accounts_count'], 'integer'],
            [['name', 'status', 'start_time'], 'string'],
            [['to_stat', 'use_auto_registr'], 'boolean'],
            ['speed', function($attribute){
                $company = FunctionHelper::getCompanyModel();

                if($company->general_balance < $this->price){
                    $this->addError($attribute, 'Недостаточно средств');
                }
            }],

            [['text1', 'text2', 'text3', 'text4'], DeniedWordsValidator::class],



        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ИД',
            'name' => 'Название',
            'text1' => 'Текст 1',
            'text2' => 'Текст 2',
            'text3' => 'Текст 3',
            'text4' => 'Текст 4',
            'status' => 'Статус',
            'account_id' => 'Ид отправителя',
            'data' => 'Дата',
            'view' => 'просмотр',
            'accounts' => 'Акк для рассылки',
            'file' => 'Файл с базой формате txt',
            'chat' => 'Куда отправлять уведомления',
            'botId' => 'Бот',
            'autoAnswer' => 'Включить авто ответ',
            'days_week' => 'Дни недели',
            'start_time' => 'Время начала',
            'to_stat' => 'Отображать в показателях',
            'text_data' => 'База получателей',
            'select_data' => 'Сохраненная база',
            'base_name' => 'Сборная база',
            '$selected_statuses' => 'Выбранные статусы',
            '$selected_dispatches' => 'Выбранные расслки',
            'use_auto_registr' => 'Использовать шаблон',
            'auto_regists_template_id' => 'Шаблон',
            'accounts_count' => 'Кол-во аккаунтов',
            'day_speed' => 'Суточная скорость (акк/день)',
            'dispatch_id' => 'Рассылка',
            'blocked_accounts_count' => 'Кол-во заблокированных аккаунтов',
            'company_id' => 'Компания',
        ];
    }

    public function getDispatchList()
    {
        $dispatchs = DispatchRegist::find()->where(['company_id' => FunctionHelper::getCompanyId()])->all();
        return ArrayHelper::map($dispatchs, 'id', 'username');
    }

    /**
     * @return int
     */
    public function getRemainAccountsCount()
    {
        $accounts = DispatchRegist::find()->where(['dispatch_id' => $this->id])->count();
        return $this->accounts_count - $accounts;
    }

    /**
     * @param null $ids
     * @return array
     */
    public function DispatchRegistList($ids = null)
    {
        if ($ids == null) {
            $dispatch_regist = DispatchRegist::find()->where(['company_id' => FunctionHelper::getCompanyId()])->all();
            $result = [];

            foreach ($dispatch_regist as $value) {
                $result [] = $value->id;
            }
            return $result;
        } else {


            $conns = \app\models\DispatchConnect::findAll(['dispatch_id' => $ids]);
            $result = [];
            foreach ($conns as $con) {
                $d = \app\models\DispatchRegist::findOne(['id' => $con->dispatch_regist_id]);
                $result [] = $d->id;
            }


            return $result;


        }

    }

    /**
     * @return bool
     */
    public function upload()
    {
        if ($this->validate()) {
            if (!file_exists('uploads')) {
                mkdir('uploads', 0777, true);
            }
            $name = time();
            $this->file->saveAs('uploads/' . $name . '.' . $this->file->extension);
            $this->filename = 'uploads/' . $name . '.' . $this->file->extension;
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return int
     */
    public function getMaxCountDispatch()
    {

        $company = Companies::findOne(FunctionHelper::getCompanyId());
        $rate = Rates::findOne($company->rate_id);
        return ($settings = FunctionHelper::getCompanySettings()) ? $settings->dispatch_count : $rate->dispatch_count;

    }

    /**
     * @return int|string
     */
    public function getCountDispatch()
    {
        return self::find()->where(['company_id' => FunctionHelper::getCompanyId()])->count();
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getChatsId()
    {
        $chatsId = DispatchChats::find()->where(['dispatchId' => $this->id])->asArray()->all();
        $chatsId = ArrayHelper::getColumn($chatsId, 'chatId');
        return $chatsId;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDispatchRegists()
    {
        return $this->hasMany(DispatchRegist::class, ['dispatch_id' => 'id']);
    }

    /**
     * Возвращает последнию зарегистрированную последовательность получателей уведомлений
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getLastChatsId()
    {
        $chatsId = [];
        $lastDispatches = Dispatch::find()->where(['company_id' => FunctionHelper::getCompanyId()])->orderBy('id DESC')->all();
        $chatsIds = [];
        if($lastDispatches != null)
        {
            foreach ($lastDispatches as $dispatch)
            {
                $chatsId = DispatchChats::find()->where(['dispatchId' => $dispatch->id])->asArray()->all();
                $chatsId = ArrayHelper::getColumn($chatsId, 'chatId');
                $chatsIds = array_merge($chatsIds, $chatsId);
            }
        }
        return $chatsIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChatsModel()
    {
        return $this->hasMany(DispatchChats::className(), ['dispatchId' => 'id']);
    }

    /**
     * Метод получает список пользователей группы
     *
     * @param string $urlGroup Ссыылка на группу
     * @return array Индексный массив с ID пользователей
     */
    public static function getGroupUsers($urlGroup)
    {
        $groupId = self::getGroupId($urlGroup);
        $proxy_server = Settings::find()->where(['key' => 'proxy_server'])->one()->value;

        FunctionHelper::checkProxy($proxy_server) ? $message = 'Прокси сервер доступен' : $message = 'Прокси сервер недоступен';

        Yii::warning($message, __METHOD__);

        $params['group_id'] = $groupId;

        $users = ClientController::request('groups.getMembers', "tcp://{$proxy_server}", $params);

        return $users['response']['items'];
    }

    /**
     * @param int $id Код группы
     * @param string $fields Запрашиваемые поля. Значения, разделенные запятыми.
     * @return array
     */
    public static function getGroupInfoById($id, $fields)
    {

        $proxy_server = Settings::find()->where(['key' => 'proxy_server'])->one()->value;

        $params['group_id'] = $id;
        $params['fields'] = $fields;

        $groupInfo = ClientController::request('groups.getById', $proxy_server, $params);
        return $groupInfo['request'];


    }

    /**
     * Метод возвращает ифнормацию о группе
     *
     * @param string $urlGroup URL адрес группы
     * @param string $fields Запрашиваемые поля. Значения, разделенные запятыми.
     * @return mixed
     */
    public static function getGroupInfoByUrl($urlGroup, $fields)
    {
        $id = self::getGroupId($urlGroup);

        $proxy_server = Settings::find()->where(['key' => 'proxy_server'])->one()->value;

        $params['group_id'] = $id;
        $params['fields'] = $fields;

        $groupInfo = ClientController::request('groups.getById', $proxy_server, $params);

        return $groupInfo['response'];

    }

    /**
     * Метод парсит URL группы и возвращает id или screen_name группы
     *
     * @param string $urlGroup URL адрес группы
     * @return string
     */
    private static function getGroupId($urlGroup)
    {
        //Парсим имя группы
        if (strpos($urlGroup, 'com/public') > 0) {
            //имя группы = int
            $groupId = mb_substr($urlGroup, mb_strpos($urlGroup, 'com/public') + 10);
        } else {
            //имя группы произвольная строка
            $groupId = mb_substr($urlGroup, mb_strrpos($urlGroup, '/') + 1);
        }
        return $groupId;
    }

}
