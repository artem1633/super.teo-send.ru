<?php

namespace app\models;

use app\components\helpers\FunctionHelper;
use app\models\DispatchRegist;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "proxy".
 *
 * @property int $id
 * @property string $internal_id Внутренний id в системе API proxy6.net
 * @property string  $name Наименование
 * @property string $ip_adress ip адрес
 * @property int $port порт
 * @property string $status Статус
 * @property string $accounts Аккаунты
 * @property int $companyId Компания
 * @property int $count_block Кол-во блок акк
 * @property int $count_add Кол-во добавленых
 * @property boolean $busy Занятость
 * @property int $shop_id ID товара
 * @property string $login Логин
 * @property string $password Пароль
 * @property string $purchase_date Дата покупки
 * @property string $expire_date Дата окончания срока действия прокси
 *
 * @property string $fullIp IP вместе с портом
 * @property int $expireDays Остаток дней до истечения срока действия прокси
 * @property boolean $haveToProlong
 *
 * @property CompanySettings[] $companySettings
 *
 * @property Companies $companyModel
 */
class Proxy extends \yii\db\ActiveRecord
{
    public $check;
    public $accounts;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proxy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['port', 'companyId', 'shop_id','count_block','count_add'], 'integer'],
            [['internal_id', 'name', 'status', 'login', 'password'], 'string', 'max' => 255],
            [['ip_adress'], 'string', 'max' => 15],
            [['busy'], 'boolean'],
            [['purchase_date', 'expire_date', 'accounts'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Ид',
            'internal_id' => 'Внутренний ID',
            'name' => 'Название',
            'ip_adress' => 'Ип адрес ',
            'port' => 'Порт',
            'status' => 'Статус',
            'companyId' => 'Компания',
            'busy' => 'Занятость',
            'login' => 'Логин',
            'password' => 'Пароль',
            'purchase_date' => 'Дата покупки',
            'expire_date' => 'Дата окончания срока действия прокси',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyModel()
    {
        return $this->hasOne(Companies::className(), ['id' => 'companyId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanySettings()
    {
        return $this->hasMany(CompanySettings::className(), ['proxy_id' => 'id']);
    }

    /**
     * Отвязвает прокси от компании, делая тем самым его доступным
     * @return boolean
     */
    public function makeAvailable()
    {
        $this->companyId = 0;
        return $this->save(false);
    }

    /**
     * Синхронизирует данные БД и данные
     * полученные с помощью метода API proxy6.net getproxy
     */
    public static function synchronize()
    {
        $proxyList = self::find()->all();

        $proxyApiList = Yii::$app->proxy->proxy->list;

        $hasIds = [];

        // Добавляем недостоющие прокси в БД
        foreach ($proxyApiList as $proxyApi)
        {
            $has = false;
            $hasIds[] = $proxyApi->id;
            foreach ($proxyList as $proxy)
            {
                if($proxy->internal_id == $proxyApi->id)
                {
                    $has = true;
                }
            }

            if($has == false)
            {
                $proxy = self::createFromApiInstance($proxyApi);

                $result = $proxy->save(false);
            }
        }


        // Удаляем лишнии прокси из БД
        foreach ($proxyList as $proxy)
        {
            if(in_array($proxy->internal_id, $hasIds) == false)
            {
                $proxy->delete();
            }
        }
    }

    /**
     * @return array|bool
     */
    public function getDispatchList()
    {
        $userModel = Yii::$app->user->identity;
        if ($model = DispatchRegist::find()->where(['company_id' => $userModel->company_id])->all()) {
            return ArrayHelper::map($model, 'id', 'username');
        }
        return false;
    }

    /**
     * Создает
     * @return self
     */
    public static function createFromApiInstance($instance)
    {
        $proxy = new self([
            'internal_id' => $instance->id,
            'ip_adress' => $instance->host,
            'port' => $instance->port,
            'status' => $instance->active == 1 ? 'yes' : 'no',
            'busy' => 0,
            'login' => $instance->user,
            'password' => $instance->pass,
            'purchase_date' => $instance->date,
            'expire_date' => $instance->date_end,
        ]);

        return $proxy;
    }

    /**
     * Возвращает IP вместе с портом
     * @return string
     */
    public function getFullIp()
    {
        return "{$this->ip_adress}:{$this->port}";
    }

    /**
     * Возвращает остаток дней до истечения срока действия прокси
     * @return int|null
     */
    public function getExpireDays()
    {
        if($this->expire_date != null)
        {
            $date_end = strtotime($this->expire_date);
            $now = strtotime(date('Y-m-d H:i:s'));

            $diff = $date_end - $now;

            $result = round($diff / 86400);

            return $result > 0 ? $result : 0;
        }

        return null;
    }

    /**
     * @return array|bool
     */
    public function getCompaniesList()
    {
        if ($model = Companies::find()->all()) {
            return ArrayHelper::map($model, 'id', 'company_name');
        }
        return false;
    }

    /**
     * @return int
     */
    public function getMaxCountProxy()
    {

        $company = Companies::findOne(FunctionHelper::getCompanyId());
        $rate = Rates::findOne($company->rate_id);
        return ($settings = FunctionHelper::getCompanySettings()) ? $settings->proxy_count : $rate->proxy_count;

    }

    /**
     * @return int|string
     */
    public function getCountProxy()
    {
        return self::find()->where(['companyId' => FunctionHelper::getCompanyId()])->count();
    }

    /**
     * @param null $ids
     * @return array
     */
    public function DispatchRegistList($ids = null)
    {
        if ($ids == null) {
            $dispatch_regist = DispatchRegist::find()->all();
            $result = [];

            foreach ($dispatch_regist as $value) {
                $result [] = $value->id;
            }
            return $result;
        } else {

            $conns = DispatchRegist::findAll(['proxy' => $ids]);
            $result = [];
            foreach ($conns as $con) {

                $result [] = $con->id;
            }

            return $result;
        }
    }

    /**
     * @param null $id
     * @return string
     */
    public static function getAccountsUsesProxy($id)
    {
        $accounts = DispatchRegist::findAll(['proxy' => $id]);
        $usernames = '';
        foreach ($accounts as $acc) {
            $usernames .= $acc->username . "(K {$acc->company_id}),";
        }

        return $usernames;
    }

    /**
     * @param null $id
     * @return int
     */
    public static function getAccountsUsesProxyCount($id)
    {
        return DispatchRegist::find()->where(['proxy' => $id])->count();
    }


    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public
    static function getFreeProxy()
    {
        return self::find()->where(['busy' => true])->all();
    }

    /**
     * Получает список доступных прокси для присвоения к компании и аккаунту
     * @param $canBeExpired boolean
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getAvailableProxy($canBeExpired = false)
    {
        if ($canBeExpired)
        {
            $proxy = self::find()->where(['busy' => 0])->all();
        } else {
            $proxy = self::find()->where(['busy' => 0])->andWhere(['>', 'expire_date', date('Y-m-d H:i:s')])->all();
        }

        $setting = Settings::findByKey('max_proxy');

        if($setting != null)
        {
            $maxProxy = intval($setting->value);
            $proxy = array_filter($proxy, function($model) use ($maxProxy){
                $usesProxyCount = self::getAccountsUsesProxyCount($model->id);
                return $usesProxyCount < $maxProxy;
            });

            return $proxy;
        }

        return [];
    }

    /**
     * Возвращает требует ли прокси продления срока действия
     * @return boolean
     */
    public function getHaveToProlong()
    {
        $dateEnd = strtotime($this->expire_date);
        $now = strtotime(date('Y-m-d H:i:s'));

        $diff = ($dateEnd - 7200) - $now; // 7200 сек = 2 часа

        return $diff < 0 ? true : false;
    }

    /**
     * Возвращает
     * @param $maxCountUses
     * @return array
     */
    public
    function getListProxy($maxCountUses = 0)
    {
        $targetProxys = [];
//        $proxys = Proxy::find()->where(['companyId' => FunctionHelper::getCompanyId()])->all();
        $proxys = Proxy::find()->all();

        foreach ($proxys as $proxy) {
            $countUses = DispatchRegist::find()->where(['proxy' => $proxy->id])->count();
            if ($countUses < $maxCountUses) {
                $targetProxys[$proxy->id] = $proxy->fullIp;
            }
        }

        return $targetProxys;

    }
}
