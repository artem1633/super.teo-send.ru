<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "applied_auto_registr".
 *
 * @property int $id
 * @property int $dispatch_id Рассылка
 * @property int $dispatch_regist_id Аккаунт
 * @property int $template_id Шаблон
 * @property int $auto_registr_id Запись шаблона
 * @property string $status Статус
 * @property string $comment Комментарий
 * @property string $created_at
 *
 * @property AutoRegistr $autoRegistr
 * @property Dispatch $dispatch
 * @property DispatchRegist $dispatchRegist
 * @property AutoRegistsTemplates $template
 */
class AppliedAutoRegistr extends \yii\db\ActiveRecord
{
    /**
     * @const Статус успешно примененного шаблона
     */
    const STATUS_DONE = 'done';

    /**
     * @const Статус ошибки при применении шаблона
     */
    const STATUS_ERROR = 'error';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'applied_auto_registr';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s')
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dispatch_id', 'dispatch_regist_id', 'template_id', 'auto_registr_id'], 'integer'],
            [['comment'], 'string'],
            [['created_at'], 'safe'],
            [['status'], 'string', 'max' => 255],
            [['auto_registr_id'], 'exist', 'skipOnError' => true, 'targetClass' => AutoRegistr::class, 'targetAttribute' => ['auto_registr_id' => 'id']],
            [['dispatch_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dispatch::class, 'targetAttribute' => ['dispatch_id' => 'id']],
            [['dispatch_regist_id'], 'exist', 'skipOnError' => true, 'targetClass' => DispatchRegist::class, 'targetAttribute' => ['dispatch_regist_id' => 'id']],
            [['template_id'], 'exist', 'skipOnError' => true, 'targetClass' => AutoRegistsTemplates::class, 'targetAttribute' => ['template_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dispatch_id' => 'Рассылка',
            'dispatch_regist_id' => 'Аккаунт',
            'template_id' => 'Шаблон',
            'auto_registr_id' => 'Запись шаблона',
            'status' => 'Статус',
            'comment' => 'Комменатрий',
            'created_at' => 'Выполнен',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutoRegistr()
    {
        return $this->hasOne(AutoRegistr::class, ['id' => 'auto_registr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDispatch()
    {
        return $this->hasOne(Dispatch::class, ['id' => 'dispatch_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDispatchRegist()
    {
        return $this->hasOne(DispatchRegist::class, ['id' => 'dispatch_regist_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(AutoRegistsTemplates::class, ['id' => 'template_id']);
    }
}
