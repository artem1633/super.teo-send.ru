<?php

namespace app\models;

use app\components\helpers\FunctionHelper;
use Yii;

/**
 * This is the model class for table "daily_report".
 *
 * @property int $id
 * @property string $date_event Дата и время события
 * @property int $company_id ID компании
 * @property int $sended_message_count Кол-во отправленных сообщений
 * @property int $account_id ID аккаунта
 * @property string $type Тип события
 * @property int $dispatch_id
 * @property int $status Пользовательский статус
 * @property string $comments Комментарии
 *
 */
class DailyReport extends \yii\db\ActiveRecord
{
    const TYPE_SENT = 1; // отправлено сообщений
    const TYPE_READ = 2; // прочитано
    const TYPE_USER_STATUS = 3; // пользовательский статус
    const TYPE_BLOCK_DISPATCH_REGIST = 4; // Блокировка бота
    const TYPE_VISIT = 5; // пользователь зашол сегодня



    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'daily_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_event'], 'safe'],
            [['comments'], 'string'],
            [['company_id', 'dispatch_id', 'sended_message_count', 'account_id', 'type'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_event' => 'Дата и время события',
            'company_id' => 'ID Компании',
            'dispatch_id' => 'ID Рассылки',
            'sended_message_count' => 'Кол-во отправленных сообщений',
            'account_id' => 'ID Аккаунта',
            'type' => 'Тип события',
            'comments' => 'Комментарии'
        ];
    }

    /**
     * @param null $company
     * @return $this
     */
    public function getDates($company = null, $dispatchId = null)
    {
        $where = $company
            ? [
                'AND',
                ['>', 'sended_message_count', 0],
                ['company_id' => $company],
            ]
            : ['>', 'sended_message_count', 0];

        $startDate = date('Y-m-01 00:00:00');
        $endDate = date('Y-m-d H:i:s');


//        $result = self::find()->select('DATE(date_event) as date')->where($where)->distinct()->asArray()->orderBy(['date_event' => SORT_ASC])->all();
        //Поменял т.к. выдавало ошибку - SQLSTATE[HY000]: General error: 3065 Expression #1 of ORDER BY clause is not in SELECT list, references column 'teo-sklad.daily_report.date_event' which is not in SELECT list; this is incompatible with DISTINCT
        $result = self::find()->select('DATE(date_event) as date')->where($where)->andWhere(['between', 'date_event', $startDate, $endDate]);

        if($dispatchId != null){
            $result->andWhere(['dispatch_id' => $dispatchId]);
        }

        $result = $result->distinct()->asArray()->orderBy(['date' => SORT_ASC])->all();

        return $result;
    }

    public static function getReportBySendMesasges($company = null, $dispatchId = null, $getDays = null, $format = 0)
    {
        if($getDays == null){
            $getDays = self::getDates($company, $dispatchId);
        }
        $days = $messages = $read = $answers = $interest = $buy = $refuse = [];

        $array = [];

        foreach ($getDays as $day) {
            $whereForSend = $company
                ? [
                    'AND',
                    ['company_id' => $company],
                    ['>', 'sended_message_count', 0],
                    ['type' => self::TYPE_SENT],
                    ['between', "date_event", $day['date'] . ' 00:00:00', $day['date'] . ' 23:59:59']
                ]
                : [
                    'AND',
                    ['>', 'sended_message_count', 0],
                    ['type' => self::TYPE_SENT],
                    ['between', "date_event", $day['date'] . ' 00:00:00', $day['date'] . ' 23:59:59']
                ];

            $whereForRead = $company
                ? [
                    'AND',
                    ['company_id' => $company],
                    ['>', 'sended_message_count', 0],
                    ['type' => self::TYPE_READ],
                    ['between', "date_event", $day['date'] . ' 00:00:00', $day['date'] . ' 23:59:59']
                ]
                : [
                    'AND',
                    ['>', 'sended_message_count', 0],
                    ['type' => self::TYPE_READ],
                    ['between', "date_event", $day['date'] . ' 00:00:00', $day['date'] . ' 23:59:59']
                ];

            $whereForAnswer = $company
                ? [
                    'AND',
                    ['company_id' => $company],
                    ['>', 'sended_message_count', 0],
                    ['type' => self::TYPE_USER_STATUS],
                    ['status' => Statuses::STATUS_CLIENT_CONVERSATION],
                    ['between', "date_event", $day['date'] . ' 00:00:00', $day['date'] . ' 23:59:59']
                ]
                : [
                    'AND',
                    ['>', 'sended_message_count', 0],
                    ['type' => self::TYPE_USER_STATUS],
                    ['between', "date_event", $day['date'] . ' 00:00:00', $day['date'] . ' 23:59:59']
                ];
            $whereForInterest = $company
                ? [
                    'AND',
                    ['company_id' => $company],
                    ['>', 'sended_message_count', 0],
                    ['type' => self::TYPE_USER_STATUS],
                    ['status' => Statuses::STATUS_CLIENT_INTERESTED],
                    ['between', "date_event", $day['date'] . ' 00:00:00', $day['date'] . ' 23:59:59']
                ]
                : [
                    'AND',
                    ['>', 'sended_message_count', 0],
                    ['type' => self::TYPE_USER_STATUS],
                    ['status' => Statuses::STATUS_CLIENT_INTERESTED],
                    ['between', "date_event", $day['date'] . ' 00:00:00', $day['date'] . ' 23:59:59']
                ];
            $whereForBuy = $company
                ? [
                    'AND',
                    ['company_id' => $company],
                    ['>', 'sended_message_count', 0],
                    ['type' => self::TYPE_USER_STATUS],
                    ['status' => Statuses::STATUS_CLIENT_BUY],
                    ['between', "date_event", $day['date'] . ' 00:00:00', $day['date'] . ' 23:59:59']
                ]
                : [
                    'AND',
                    ['>', 'sended_message_count', 0],
                    ['type' => self::TYPE_USER_STATUS],
                    ['status' => Statuses::STATUS_CLIENT_BUY],
                    ['between', "date_event", $day['date'] . ' 00:00:00', $day['date'] . ' 23:59:59']
                ];

            $whereForRefuse = $company
                ? [
                    'AND',
                    ['company_id' => $company],
                    ['>', 'sended_message_count', 0],
                    ['type' => self::TYPE_USER_STATUS],
                    ['status' => Statuses::STATUS_CLIENT_REFUSED],
                    ['between', "date_event", $day['date'] . ' 00:00:00', $day['date'] . ' 23:59:59']
                ]
                : [
                    'AND',
                    ['>', 'sended_message_count', 0],
                    ['type' => self::TYPE_USER_STATUS],
                    ['status' => Statuses::STATUS_CLIENT_REFUSED],
                    ['between', "date_event", $day['date'] . ' 00:00:00', $day['date'] . ' 23:59:59']
                ];


            if($format == 0){
                $days[] = date(FunctionHelper::dateForChart($day['date']));
                $messages[] = floatval(self::find()->where($whereForSend)->sum('sended_message_count'));
                $read[] =  floatval(self::find()->where($whereForRead)->sum('sended_message_count'));
                $answers[] =  floatval(self::find()->where($whereForAnswer)->sum('sended_message_count'));
                $interest[] = floatval(self::find()->where($whereForInterest)->sum('sended_message_count'));
                $buy[] = floatval(self::find()->where($whereForBuy)->sum('sended_message_count'));
                $refuse[] = floatval(self::find()->where($whereForRefuse)->sum('sended_message_count'));
            } else if($format == 1) {
                $messages = floatval(self::find()->where($whereForSend)->sum('sended_message_count'));
                $read = floatval(self::find()->where($whereForRead)->sum('sended_message_count'));
                $answers = floatval(self::find()->where($whereForAnswer)->sum('sended_message_count'));
                $interest = floatval(self::find()->where($whereForInterest)->sum('sended_message_count'));
                $buy = floatval(self::find()->where($whereForBuy)->sum('sended_message_count'));
                $refuse = floatval(self::find()->where($whereForRefuse)->sum('sended_message_count'));
                $array[] = [
                    'date' => $day['date'],
                    'messages' => $messages,
                    'read' => $read,
                    'answers' => $answers,
                    'interest' => $interest,
                    'interest_per' => round($messages * $interest / 100, 2),
                    'buy' => $buy,
                    'buy_per' => round($messages * $buy / 100, 2),
                    'refuse' => $refuse,
                    'refuse_per' => round($messages * $refuse / 100, 2),
                ];
            }

        }

        if($format == 0){
            return [$days, $messages, $read, $answers, $interest, $buy, $refuse];
        } else if($format == 1){
            return $array;
        }
    }

    public function getGeneralReport()
    {
        $company_id = FunctionHelper::getCompanyId();
        $dispatchs = Dispatch::find() -> where(['company_id' => $company_id])->all();
        $dispatch_rate  = 0;
        foreach ($dispatchs as $dispatch){
            $count_dr = DispatchRegist::find()
                ->alias('dr')
                ->joinWith('dispatchs d')
                ->where(['d.dispatch_id' => $dispatch->id])
                ->andWhere('dr.status != :status',['status' => 'account_blocked'])
                ->count();
            $limit = CompanySettings::find()->where(['company_id' => $company_id])->one();
            $speed = $count_dr * $limit->messages_daily_limit;
            $dispatch_rate += $speed;
        }

        $active_recipient_count = DailyReport::find()->where([
            'AND',
            ['company_id' => $company_id],
            ['not', ['status' => null]],
        ])->count();
        $all_recipient_count = DailyReport::find()->where(['company_id' => $company_id])->count();

        $recipients = round($active_recipient_count / ($all_recipient_count/100), 2);

        return $gReport = [$dispatch_rate, $recipients];
    }


}
