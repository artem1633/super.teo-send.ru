<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "message".
 *
 * @property int $id
 * @property string $date_time Дата и время
 * @property int $dispatch_registr_id Аккаунт
 * @property int $dispatch_status_id Статус
 * @property int $dispatch_id Рассылка
 * @property int $database_id База
 * @property int $company_id Компания
 * @property string $text
 * @property string $from От кого
 */
class Message extends \yii\db\ActiveRecord
{
    const FROM_CLIENT = 'client';
    const FROM_ACCOUNT = 'account';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_time', 'from'], 'safe'],
            [['dispatch_registr_id', 'dispatch_status_id', 'dispatch_id', 'database_id', 'company_id'], 'integer'],
            [['text'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_time' => 'Дата и время',
            'dispatch_registr_id' => 'Dispatch Registr ID',
            'dispatch_status_id' => 'Dispatch Status ID',
            'dispatch_id' => 'Рассылка',
            'database_id' => 'База данных',
            'company_id' => 'Компания',
            'text' => 'Текст',
            'from' => 'От кого'
        ];
    }
}
