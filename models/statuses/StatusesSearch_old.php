<?php

namespace app\models\statuses;

use app\models\Statuses;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UsersSearch represents the model behind the search form about `app\models\Users`.
 */
class StatusesSearch_old extends Statuses
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type','sort'], 'integer'],
            [['name','color'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param $type
     * @return ActiveDataProvider
     */
    public function search($params, $type)
    {
        $query = self::find()->where(['type' => $type])->orderBy(['sort' => SORT_ASC]);

        $this->load($params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
