<?php

namespace app\models\Dispatch;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Dispatch;

/**
 * DispatchSearch represents the model behind the search form about `app\models\Dispatch`.
 */
class DispatchSearch extends Dispatch
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'autoAnswer', 'botId'], 'integer'],
            [['name', 'text1', 'text2', 'text3', 'text4', 'status', 'data', 'days_week', 'start_time', 'to_stat'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dispatch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'data' => $this->data,
            'company_id' => $this->company_id,
            'autoAnswer' => $this->autoAnswer,
            'botId' => $this->botId,
            'start_time' => $this->start_time,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'text1', $this->text1])
            ->andFilterWhere(['like', 'text2', $this->text2])
            ->andFilterWhere(['like', 'text3', $this->text3])
            ->andFilterWhere(['like', 'text4', $this->text4])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'days_week', $this->days_week])
            ->andFilterWhere(['like', 'to_stat', $this->to_stat]);

        return $dataProvider;
    }
}
