<?php

namespace app\models;

use app\components\helpers\FunctionHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AutoRegistsTemplates;

/**
 * AutoRegistsTemplatesSearch represents the model behind the search form about `app\models\AutoRegistsTemplates`.
 */
class AutoRegistsTemplatesSearch extends AutoRegistsTemplates
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AutoRegistsTemplates::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->with('company');

        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
        ]);

        if(Yii::$app->user->identity->isSuperAdmin() == false){
            $query->andWhere(['company_id' => FunctionHelper::getCompanyId()]);
        }

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
