<?php

namespace app\models;

use app\components\helpers\FunctionHelper;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bots".
 *
 * @property int $id
 * @property string $name Имя бота
 * @property int $companyId Компания
 * @property int $shop_id ID товара
 */
class Bots extends \yii\db\ActiveRecord
{

    const BASIC_NAME = 'Бот Тестовый';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bots';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['companyId', 'shop_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название бота',
            'companyId' => 'Компания'
        ];
    }

    /**
     * @param $companyId
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getListBotByCompany($companyId)
    {
        return self::find()->where(['companyId' => $companyId])->all();
    }

    /**
     * @return int
     */
    public function getMaxCountBots(){

        $company = Companies::findOne(FunctionHelper::getCompanyId());
        $rate = Rates::findOne($company->rate_id);
        return ($settings = FunctionHelper::getCompanySettings()) ? $settings->bots_count : $rate->bots_count;

    }

    /**
     * @return int|string
     */
    public function getCountBots()
    {
        return self::find()->where(['companyId' => FunctionHelper::getCompanyId()])->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDialogModel()
    {
        return $this->hasOne(BotsDialogs::className(), ['id' => 'dialogId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyModel()
    {
        return $this->hasOne(Companies::className(), ['id' => 'companyId']);
    }

}
