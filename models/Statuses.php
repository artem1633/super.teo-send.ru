<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "statuses".
 *
 * @property int $id
 * @property int $type Тип статуса
 * @property string $name Название статуса
 * @property string $color Цвет кнопки/ссылки
 * @property string $sort Сортировка
 */
class Statuses extends \yii\db\ActiveRecord
{
    const TYPE_CLIENT = 1;

    const STATUS_CLIENT_CONVERSATION = 1;
    const STATUS_CLIENT_INTERESTED = 2;
    const STATUS_CLIENT_BUY = 3;
    const STATUS_CLIENT_REFUSED = 4;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'statuses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type','sort'], 'integer'],
            [['name','color'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип статуса',
            'name' => 'Cтатус',
            'color' => 'Цвет кнопки',
            'sort' => 'Сортировка',
        ];
    }

    /**
     * @param $type
     * @return int|mixed
     */
    public function getNewSort($type)
    {
        if (($model = self::find()->where(['type' => $type])->orderBy(['sort' => SORT_DESC])->one()) !== null) {
            return $model->sort+1;
        } else {
            return 1;
        }
    }
}
