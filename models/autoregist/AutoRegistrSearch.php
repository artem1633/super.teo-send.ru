<?php

namespace app\models\autoregist;

use app\components\helpers\FunctionHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AutoRegistr;

/**
 * AutoRegistrSearch represents the model behind the search form about `app\models\AutoRegistr`.
 */
class AutoRegistrSearch extends AutoRegistr
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'status', 'action', 'company_id', 'template_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AutoRegistr::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->with('template');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'status' => $this->status,
            'action' => $this->action,
            'company_id' => $this->company_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     * Searching by company of current user
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchByCompany($params)
    {
        $query = AutoRegistr::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->with('company');
        $query->with('template');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'status' => $this->status,
            'action' => $this->action,
            'company_id' => FunctionHelper::getCompanyId(),
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     * Searching by company of current user
     *
     * @param array $params
     * @param integer $templateId
     *
     * @return ActiveDataProvider
     */
    public function searchByTemplate($params, $templateId)
    {
        $query = AutoRegistr::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->with('company');
        $query->with('template');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'status' => $this->status,
            'action' => $this->action,
            'company_id' => Yii::$app->user->identity->isSuperAdmin() ? null : FunctionHelper::getCompanyId(),
            'template_id' => $templateId
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
