<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bots_answers".
 *
 * @property int $id
 * @property int $dialogId ID диалога
 * @property string $tag Тег (ответ)
 */
class BotsAnswers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bots_answers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dialogId', 'tag'], 'required'],
            [['dialogId'], 'integer'],
            [['tag'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dialogId' => 'Dialog ID',
            'tag' => 'Tag',
        ];
    }
}
