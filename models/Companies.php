<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "companies".
 *
 *
 * @property int $admin_id Id администратора компании
 * @property int $rate_id Тариф
 * @property string $last_activity_datetime Дата и время последней активности
 * @property string $access_end_datetime Дата и время потери доступа к системе
 * @property int $is_super Супер компания
 * @property string $company_name Название компании
 * @property string $created_date Дата создания
 * @property int $referal ID пригласившей компании
 * @property string $affiliate_amount Отчисления по партнерке
 * @property string $general_balance Основной счет
 *
 * @property bool $isConnectedTelegram
 * @property string $telegramId
 *
 * @property Users $admin
 * @property Rates $rate
 * @property DispatchRegist $accountsModel
 * @property self $referalModel
 */
class Companies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'companies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['admin_id', 'is_super', 'account_count', 'dispatch_count', 'referal'], 'integer'],
            [['admin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['admin_id' => 'id']],
            [['rate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rates::className(), 'targetAttribute' => ['rate_id' => 'id']],
            [['access_end_datetime', 'created_date', 'last_activity_datetime'], 'safe'],
            [['company_name'], 'string', 'max' => 255],
            [['company_name'], 'unique'],
            [['affiliate_amount', 'general_balance'],  'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'access_end_datetime' => 'Дата и время окончания доступа',
            'admin_id' => 'Администратор компании',
            'rate_id' => 'Тариф',
            'last_activity_datetime' => 'Последняя активность',
            'company_name' => 'Название компании',
            'created_date' => 'Дата регистрации',
            'is_super' => 'Супер компания',
            'account_count' => 'Количество Аккаунтов',
            'dispatch_count' => 'Количество Рассылок',
            'referal' => 'Кто приглачил в проект',
            'affiliate_amount' => 'Партнерские отчисления',
            'general_balance' => 'Основной счет'
        ];
    }

    /**
     * @return bool
     */
    public function getIsConnectedTelegram()
    {
        $tel = Telegram::findOne(['company_id' => $this->id]);

        return $tel != null;
    }

    /**
     * @return string
     */
    public function getTelegramId()
    {
        $tel = Telegram::findOne(['company_id' => $this->id]);

        return $tel->telegram_id;
    }

    /**
     * @return int
     */
    public static function getAllTelegramConnectedCount()
    {
        $companies = array_unique(ArrayHelper::getColumn(Telegram::find()->all(), 'company_id'));

        return count($companies);
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_date = date("Y-m-d");
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return bool
     */
    public function isSuperCompany()
    {
        return $this->is_super == 1 ? true : false;
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if($this->isSuperCompany() === false)
        {
            $this->deleteAllObjects($this->id);
            return parent::beforeDelete();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAteliers()
    {
        return $this->hasMany(Atelier::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvailables()
    {
        return $this->hasMany(Available::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashboxes()
    {
        return $this->hasMany(Cashbox::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Clients::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(Users::className(), ['id' => 'admin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRate()
    {
        return $this->hasOne(Rates::className(), ['id' => 'rate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getListServices()
    {
        return $this->hasMany(ListServices::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResources()
    {
        return $this->hasMany(Resource::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorages()
    {
        return $this->hasMany(Storage::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuppliers()
    {
        return $this->hasMany(Suppliers::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccountsModel()
    {
        return $this->hasMany(DispatchRegist::className(), ['company_id' => 'id']);
    }

    /**
     * @return mixed
     */
    public function getCountSendedMessages()
    {
        return $this->hasMany(DispatchRegist::className(), ['company_id' => 'id'])
            ->sum('all_sended_message_count');
    }

    /**
     * @return int|string
     */
    public function getCountAccounts()
    {
        return $this->hasMany(DispatchRegist::className(), ['company_id' => 'id'])
            ->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferalModel()
    {
        return $this->hasOne(self::className(), ['id' => 'referal']);
    }


    public function getMaxTermWithoutBlocking()
    {
        if ($this->accountsModel){
            $dates = [];
            foreach ($this->accountsModel as $account) {
                $blockingDate = !$account->blocking_date ? date('Y-m-d') : $account->blocking_date;
                $days = (strtotime($blockingDate)-strtotime($account->data))/(60*60*24);
                $dates[] = $days;
            }
            return max($dates);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['company_id' => 'id']);
    }

    /**
     * @param $company_id
     */
    public function deleteAllObjects($company_id)
    {
        $user = Users::find()->where(['company_id' => $company_id])->all();
        foreach ($user as $value)
        {
            $value->delete();
        }

        $atelier = Atelier::find()->where(['company_id' => $company_id])->all();
        foreach ($atelier as $value)
        {
            $value->delete();
        }

    }

    /**
     * @return int|string
     */
    public function getCountMyReferal()
    {
        return self::find()->where(['referal' => $this->id])->andWhere(['!=', 'id', 1])->count();
    }

    /**
     * @return array
     */
    public static function getAllInArray()
    {
        $companies = self::find()
            ->orderBy(['id' => SORT_DESC]);

        return ArrayHelper::map($companies->all(), 'id', 'company_name');
    }

}
