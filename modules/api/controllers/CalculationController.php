<?php

namespace app\modules\api\controllers;

use app\components\Calculations;
use app\components\helpers\FunctionHelper;
use app\models\calculation\Calculation;
use app\models\Settings;
use yii\rest\ActiveController;
use yii\web\Response;

/**
 */
class CalculationController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['calculate'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @param $count
     * @return mixed
     */
    public function actionCalculate($count)
    {
        return Calculations::defaultCalculation($count);
    }
}