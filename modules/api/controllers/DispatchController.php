<?php

namespace app\modules\api\controllers;

use app\components\AccountsDataPurchase;
use app\components\helpers\TagHelper;
use app\models\TemplateMessages;
use app\models\Companies;
use app\models\CompanySettings;
use \app\models\DailyReport;
use app\models\Dispatch;
use app\models\DispatchConnect;
use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\Logs;
use app\models\Message;
use app\models\Proxy;
use app\models\Settings;
use app\models\Shop;
use app\models\UniqueMessage;
use app\models\Users;
use Yii;
use yii\db\Exception;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\rest\ActiveController;
use yii\web\Response;
use app\modules\api\controllers\MessagesController;
use app\models\AccountingReport;
use app\models\Telegram;
use yii\helpers\ArrayHelper;

/**
 */
class DispatchController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => [],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionTestSendTelMessage()
    {
        self::sendTelMessage(['300640816', '247187885'],"Зарегистрирован новый пользователь:
                    ФИО: ncurses_assume_default_colors(fg, bg)
                    Логин: 123
                    Телефон: 234234234234");
    }

    public function actionTestTags()
    {
        $dispatch = Dispatch::findOne(37);
        $msg = 'adasdasd {my_tag} adhasdajkqrwrlnlmwklqwrqwl;er;lqwkrl;qwekr {dispatchRegist.login}';
        $account = DispatchRegist::find()->one();
        $tags = TemplateMessages::find()->where(['or', ['company_id' => $dispatch->company_id], ['company_id' => 1]])->all();
        $msg = TagHelper::handle($msg, [$account], $tags);

        echo $msg;
    }

    /**
     * Отправляет отчет о новый пользователях,
     * - сумму пополнений
     * - кол-во сообщений
     * за сутки
     */
    public function actionDailyReport()
    {
        $hours24 = time() - 86400;

        $hours24DateTime = date('Y-m-d H:i:s', $hours24);
        $hours24Date = date('Y-m-d', $hours24);


        $messages = DailyReport::find()->where(['type' => DailyReport::TYPE_SENT])->andWhere(['>', 'date_event', $hours24DateTime])->sum('sended_message_count');
        $users = Users::find()->disableCompanyRequire()->where(['data_cr' => date('Y-m-d')])->count();
        $payments = AccountingReport::find()->where(['between', 'date_report', date('Y-m-d').' 00:00:00', date('Y-m-d').' 23:59:59'])->andWhere(['operation_type' => AccountingReport::TYPE_INCOME_BALANCE])->sum('amount');

        self::sendTelMessage('247187885', "ОТЧЕТ ЗА ДЕНЬ:
            Кол-во сообщений: {$messages}
            Кол-во зарегистрированных пользователей: {$users}
            Сумма пополнений: {$payments}
        ");
    }

    public function actionHandleNew()
    {
        $globalList = '';
        $setingProxy = Settings::find()->where(['key' => 'proxy_server'])->one();
        $setingToken = Settings::find()->where(['key' => 'vk_access_token'])->one();
        $dispatches = Dispatch::find()->where(['status' => DispatchStatus::STATUS_HANDLE])->limit(5)->all();

        $counter = 0;
        foreach ($dispatches as $dispatch)
        {
            if($counter >= 10)
                break;

            $list = '';
            $clients = DispatchStatus::find()
                ->where('(photo is  null or name is null) and (dispatch_id = :dispatch_id)', [':dispatch_id' => $dispatch->id])
                ->limit(100)
                ->all();

            if(count($clients) == 0){
                $company = Companies::findOne($dispatch->company_id);
                if($company != null){
                    $telId = $company->telegramId;
                    if($telId != null){
                        self::sendTelMessage($telId, "Рассылка «{$dispatch->name}» изменила свой статус на «Обработка аккаунтов»");
                    }
                }
                $dispatch->status = Dispatch::STATUS_ACCOUNTS_HANDLING;
                $dispatch->save();
                continue;
            }



            foreach ($clients as $client)
            {
                // if(stripos($client->default_account_id, 'vk.com/'))
                // {
                //     if(stripos($client->default_account_id, 'vk.com/id'))
                //     {
                //         $client->default_account_id = explode('vk.com/id', $client->default_account_id)[1];
                //     } else {
                //         $client->default_account_id = explode('vk.com/', $client->default_account_id)[1];
                //     }
                // }

                $list .= "{$client->default_account_id},";
                $list2 .= "{$client->id} - {$client->default_account_id},";
                $list3[] = $client->default_account_id;
                // $list3[] = [$client->id => $client->default_account_id];
            }

            $results = $this->getUser($list, 'photo_100,city,country,bdate,sex,domain', $setingToken->value, $setingProxy->value);
            $i = 0;

            echo "<pre>";
            print_r($list3);
            echo "</pre>";

            if(isset($results['error']))
            {
                echo '1';
                if($results['error']['error_code'] == '113' || $results['error']['error_msg'] == 'Invalid user id')
                {
                    echo 'IN ERROR BODY';
                    echo "<br>REQUEST START<br>";
                    var_dump( $results['error']['request_params']);
                    echo "<br>REQUEST END<br>";
//                    $id = $results['error']['request_params'][2]['value'];
//                    $id = $results['error']['request_params'][1]['value'];
                    $id = $results['error']['request_params'][2]['value'];
                    echo "<br>$id<br>";
                    $id = str_replace(',', '', $id);
                    $client = DispatchStatus::find()->where(['like', 'default_account_id', $id])->one();
                    var_dump($id);
                    if($client != null){
                        echo "DispatchStatus id: {$client->id}";
                        $client->delete();
                    }
                }
            }

            foreach ($results['response'] as $item)
            {
                // $i22 = 0;
                echo '<br>ID: '.$item['id'].' DOMAIN'.$items['domain'].'<br>';
                $users = DispatchStatus::find()
                    // ->where(['or', ['LIKE', 'default_account_id', strval($item['id'])], ['LIKE', 'default_account_id', strval($item['domain'])]])
                    // ->where(['or', ['default_account_id' => strval($item['id'])], ['default_account_id' => strval($item['domain'])]])
                    ->orFilterWhere(['default_account_id' => $item['id']])
                    ->orFilterWhere(['default_account_id' => strval($item['domain'])])
                    ->andWhere(['dispatch_id' => $dispatch->id])
                    ->all();



                echo 'COUNT: '.count($users);
                if (count($users) > 1){
                    self::sendTelMessage('247187885', "Серьезный баг - {$item['id']}");
                    //print_r();


                    echo "<pre>";
                    print_r(array_search([ '(.*)',$item['id']], $list3) );
                    echo "</pre>";
                    // echo 'exit';
                    // exit;
                }

                foreach ($users as $user) {
                    // $i22++;
                    // if($i22 == 2){
                    //     self::sendTelMessage('247187885', "Серьезный баг - {$item['id']}");

                    //     echo array_search($item['id'], $list3);
                    //     // echo 'exit';
                    //     // exit;
                    // }
                    if ($user) {

                        $user->account_id = $item['id'];
                        $user->name = "{$item['first_name']}";
                        $user->photo = $item['photo_100'];
                        $user->age = $item['bdate'];
                        $user->gender = 'Ж';
                        if ($item['sex'] == 2) {
                            $user->gender = 'М';
                        }
                        $user->city = $item['country']['title'] . " " . $item['city']['title'];
                        $user->save();
                        $i++;
                        $list2 .= "__{$i}
                 {$item['first_name']}  {$item['last_name']} __
                 {$item['country']['title']}  {$item['city']['title']}__
                 {$item['sex']}___
                 {$user->gender}___
                 {$item['bdate']}___
                 {$user->age}___



                 Akk {$item['id']} Status";
                        if ($user->errors) {
                            $list2 .= "error";
                        } else {
                            $list2 .= "good";
                        }
                    }
                }
            }

            $globalList .= $list2;
            $counter++;
        }

        return $globalList;
    }

    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionAkkunread()//Проверяем все акк на ограничения
    {
        $i = 0;

        $setingToken = Settings::find()->where(['key' => 'vk_access_token'])->one();
        $setingProxy = Settings::find()->where(['key' => 'proxy_server'])->one();


        $akkAll = DispatchRegist::find()->where(['!=', 'status', DispatchRegist::STATUS_SLEEPING])->andWhere(['!=', 'status', 'account_blocked'])->all();
        /** @var DispatchRegist $akk */
        foreach ($akkAll as $akk) {
            $i++;

            $my_arr[$i]['acc'] = $akk->id;
            $my_arr[$i]['acc_status_start'] = $akk->status;

            /** @var DispatchRegist $data */
            /** @var CompanySettings $setting */
            $setting = CompanySettings::find()->where(['company_id' => $akk->company_id])->one();
            // кол-во сообщения в день
            $acc_message_count = $setting->messages_daily_limit;
            $my_arr[$i]['acc_message_count'] = $acc_message_count;
            $my_arr[$i]['sended_message_count'] = $akk->sended_message_count;


            //  Проверяем не перевышан лимит сообщения в день (статус становиться limit_exceeded при отправке)
            $d = date("Y-m-d H:i:s", strtotime($akk->last_dispatch_time . " +24 hours"));
            $now = date("Y-m-d H:i:s");
            $hour = round((strtotime($d) - strtotime($now)) / (60 * 60));
            $my_arr[$i]['hour'] = $hour;

            $my_arr[$i]['hour_status'] = 'false';
            if ($hour < 0) {
                $akk->sended_message_count = 0;
                $akk->status = "ok";
                $my_arr[$i]['hour_status'] = 'true';
            }


            // Проверяем еще не прошел интервал (статус становиться interval_not_end при отправке)
            $date_2 = date("Y-m-d H:i:s");
            $interval_min = Settings::find()->where(['key' => 'interval_in_minutes'])->one();
            $diff = strtotime($date_2) - strtotime($akk->last_dispatch_time);
            $diff = round($interval_min->value - $diff / 60);
            $my_arr[$i]['diff'] = $diff;

            $my_arr[$i]['diff_count_status'] = 'false';
            if ($diff < 0 && $akk->sended_message_count < $acc_message_count) {
                $akk->status = 'ok';
                $limitedAccounts = DispatchRegist::find()->where(['dispatch_id' => $account->dispatch_id, 'status' => 'ok'])->count();
                if($limitedAccounts == 0){
                    $dispatch = Dispatch::findOne($account->dispatch_id);
                    if($dispatch != null)
                    {
                        self::sendTelMessage('247187885', "Рассылка {$dispatch->name} запущена после суточного лимита");
                    }
                }
                $my_arr[$i]['diff_count_status'] = 'true';
            }

            if ($akk->sended_message_count >= $acc_message_count) {
                $akk->status = "limit_exceeded";
            }

            $proxy = Proxy::findOne(['id' => $akk->proxy]);

            if (!$proxy) {
//                    $akk->status = DispatchStatus::STATUS_ACCOUNT_BLOCKED;
//                    $akk->coment = 'Прокси не подключен';
                $akk->makeBlock('Прокси не подключен');
                $akk->update();
                continue;
            }
            $url = 'https://api.vk.com/method/account.setOnline';
            $params = array(
                'access_token' => $akk->token,
                'v' => '5.80'
            );

            // В $result вернется id отправленного сообщения
            $result = file_get_contents($url, false, stream_context_create(array(
                'http' => array(
                    'proxy' => "tcp://{$proxy->ip_adress}:{$proxy->port}",
                    'request_fulluri' => true,
                    'method' => 'POST',
                    'header' => 'Content-type: application/x-www-form-urlencoded',
                    'content' => http_build_query($params)
                )
            )));
            $result = json_decode($result, true);

            //Если проверка показада ошибку и ранее ошибки не было то нужно поставить статус и отправить уведомление
            if ($result['error'] and $akk->status != DispatchStatus::STATUS_ACCOUNT_BLOCKED) {
                //Если проблемма в том что акк заморозили
                if ($result['error']["error_msg"] = 'User authorization failed: invalid access_token (2).') {
                    $my_arr[$i]['error'] = $result['error']["error_msg"];
                    $akk->makeBlock('Заморожен');
                    if (!$akk->error_notif) {
                        $company = Users::find()->where(['company_id' => $akk->company_id])->one();
                        // $connect = DispatchConnect::find()->where(['dispatch_regist_id' => $akk->id])->one();
                        if ($company->vk_id) {
                            $akk->error_notif = true;
                            $text = "Аккаунт \"{$akk->username}\" не доступен по причине \"{$result['error']["error_msg"]}\"
                            Для проверки перейдите http://super.teo-send.ru/dispatch-regist/show-akk?block={$akk->dispatch_id}";
                            self::sendTelMessage('247187885', $text);
                        }
                    }

                    // Проверка на массовую блокировку аккаунтов
                    $hourAgo = time() - 3600;
                    $hourAgoDate = date('Y-m-d H:i:s', $hourAgo);
                    $totalBlockedAccounts = DispatchRegist::find()->where(['status' => DispatchStatus::STATUS_ACCOUNT_BLOCKED])->andWhere(['>', 'blocking_datetime', $hourAgoDate])->count();

                    $warningCountSetting = intval(Settings::findByKey('warning_blocking_accounts_count')->value);

                    if($totalBlockedAccounts >= $warningCountSetting){
                        $workingDispatches = Dispatch::find()->where(['status' => 'job'])->all();
                        foreach ($workingDispatches as $workingDispatch) {
                            $workingDispatch->status = 'wait';
                            $workingDispatch->save(false);
                            $userCompany = Companies::findOne($workingDispatch->company_id);
                            if($userCompany != null){
                                $tel = Telegram::findOne(['company_id' => $userCompany->id]);
                                if($tel != null){
                                    self::sendTelMessage($tel->telegram_id, "Рассылка «{$workingDispatch->name}» была приостановлена в работе в связи с массовой блокировкой аккаунтов");
                                }
                            }
                        }
                        $workingDispatchesNames = implode(', ', ArrayHelper::getColumn($workingDispatches, 'name'));
                        self::sendTelMessage('247187885', 'За 1 час было заблокировано '.$totalBlockedAccounts.' акк. Остановлены следущие работующие рассылки: '.$workingDispatchesNames);
                    }

                    //Если поблемма не в том что акк заморозили
                } else {
                    $my_arr[$i]['error'] = $result['error']["error_msg"];
//                    $akk->status = DispatchStatus::STATUS_ACCOUNT_BLOCKED;
//                    $akk->blocking_date = date('Y-m-d'); //Запись даты блокировки аккаунта
//                    $akk->coment = $result['error']["error_msg"];
                    $akk->makeBlock($result['error']["error_msg"]);
                    if (!$akk->error_notif) {
                        $company = Users::find()->where(['company_id' => $akk->company_id])->one();
                        // $connect = DispatchConnect::find()->where(['dispatch_regist_id' => $akk->id])->one();
                        if ($company->vk_id) {
                            $akk->error_notif = true;
                            $text = "Аккаунт \"{$akk->username}\" не доступен по причине \"{$result['error']["error_msg"]}\"
                            Для проверки перейдите http://super.teo-send.ru/dispatch-regist/show-akk?block={$akk->dispatch_id} ";
                            self::sendTelMessage('247187885', $text);
                        }
                    }
                }
                //Если ранее акк был заблокирована а теперь нет
            } elseif (!$result['error'] and $akk->status == DispatchStatus::STATUS_ACCOUNT_BLOCKED) {
                // $my_arr[$i]['error'] = $result['error']["error_msg"];
                $akk->status = 'ok';
                $akk->coment = $result['error']["error_msg"];
                $akk->error_notif = false;
            }
            $my_arr[$i]['acc_status_finish'] = $akk->status;
            //Проверка на заполнение полей username и account_url
            if ($akk->status == 'ok' && !$akk->username && !$akk->account_url) {
                DispatchRegist::setVkInfo($akk);
                Yii::warning("Данные обновлены. Логин: $akk->login", __METHOD__);
            }
            $akk->update();
        }

        echo "<pre>";
        print_r($my_arr);
        echo "</pre>";

    }

    public function actionHandleAccounts()
    {
        $dispatches = Dispatch::find()->where(['or', ['status' => Dispatch::STATUS_ACCOUNTS_HANDLING], ['status' => 'job']])->all();
        /** @var \app\models\Shop $shop */
        $shop = Shop::find()->one();


        $send_vk_id = Settings::find()->where(['key' => 'akk_notify'])->one()->value;
        $send_proxy = Settings::find()->where(['key' => 'proxy_server'])->one()->value;
        $send_token = Settings::find()->where(['key' => 'vk_access_token'])->one()->value;

        if($shop->accounts_data == ''){
            self::sendTelMessage('247187885', 'Закончились аккаунты');

            return "Accounts data end";
        }

        echo 'DISPATCHES COUNT: '.count($dispatches).'<br>';


        $a1 = 0;
        /** @var \app\models\Dispatch $dispatch */
        foreach ($dispatches as $dispatch)
        {
            if ($a1 == 1) {
                exit;
            }
            //sleep(30);

            $dispatchRegist = $dispatch->dispatchRegists;
            $dispatchRegistCount = count($dispatchRegist);

            $remainsCount = $dispatch->accounts_count - $dispatchRegistCount;

            echo '<br/><br><br>REMAINS COUNT: '.$remainsCount;


            if($remainsCount == 0) {
                if ($dispatch->status == Dispatch::STATUS_ACCOUNTS_HANDLING) {
                    $dispatch->status = 'wait';
                    $company = Companies::findOne($dispatch->company_id);
                    if($company != null){
                        $telId = $company->telegramId;
                        if($telId != null){
                            self::sendTelMessage($telId, "Рассылка «{$dispatch->name}» изменила свой статус на «Готова к запуску»");
                        }
                    }
                    $dispatch->save(false);
                    echo '<br>hand in <wait></wait>';
                }
                echo '<br>CONTINUED';
                continue;
            }

            $sleepingAccounts = DispatchRegist::find()->where(['status' => DispatchRegist::STATUS_SLEEPING])->limit($remainsCount)->all();


            /** @var \app\models\DispatchRegist $account */
            foreach ($sleepingAccounts as $account)
            {
                $account->status = DispatchRegist::STATUS_OK;
                $account->template_id = $dispatch->auto_regists_template_id;
                $account->dispatch_id = $dispatch->id;
                $account->company_id = $dispatch->company_id;
                if($account->save(false))
                {
                    echo "DISPATCH REGIST: <br>";
                    echo "<br> END DISPATCH REGIST";
                }
                $account->attachProxy();
                $account->makeAutoPage();
            }
            echo '<br>AFTER FOR';
            $remainsCount -= count($sleepingAccounts);

            if($remainsCount > 0){

                Proxy::synchronize();

                // Начало БД транзакции
                $transaction = Yii::$app->db->beginTransaction();

//                $purchase = AccountsDataPurchase::makePurchase($shop, $dispatch->company_id, $remainsCount);
                // Покупаем 1 акканут
                $purchase = AccountsDataPurchase::makePurchase($shop, $dispatch->company_id, 1);
                echo 'Purchase: <br/>';
                //var_dump($purchase);
                // Получаем аккаунты в виде: логин:пароль в том кол-ве что указали (1)
                $purchasedAccounts = $purchase->purchasedAccounts;
                $accounts = $purchasedAccounts;

                if($accounts === null)
                {
                    self::sendTelMessage('247187885', 'В магазине закончились аккаунты');
                }

                echo 'Accounts: <br>';
                var_dump($accounts);

                try {
                    foreach ($accounts as $account)
                    {
                        $account = explode(":", $account);

                        $dispatchRegist = new DispatchRegist();
                        $dispatchRegist->login = trim($account[0]);
                        $dispatchRegist->password = trim($account[1]);
                        $dispatchRegist->template_id = $dispatch->auto_regists_template_id;
                        $dispatchRegist->company_id = $dispatch->company_id;
                        $dispatchRegist->data = date('Y-m-d H:i:s');
                        $dispatchRegist->dispatch_id = $dispatch->id;
                        echo "RESULT: ___ ";
                        var_dump($dispatchRegist->save(false));
//                        if($dispatchRegist->save(false))
//                        {
//                            echo "DISPATCH REGIST: <br>";
//                            var_dump($dispatchRegist);
//                            echo "<br> END DISPATCH REGIST";
//                        }
                        // Автооформление
                        $dispatchRegist->makeAutoPage();
                        DispatchRegist::setVkInfo($dispatchRegist);

                        Proxy::synchronize();
                        $a1++;
                    }
                } catch (\Exception $e) {

                    $company = Companies::findOne($dispatch->company_id);

                    // Отменяем все записи в бд
//                    $transaction->rollBack();

                    self::sendTelMessage('247187885', "Пользователь \"{$company->company_name}\" пытался совершить покупку в количестве 1 ед. Но получил ошибку: \"{$e->getMessage()}\"");

//                    return 'error: '.$e->getMessage();
                }

                // Подтверждаем изменения в БД
                $transaction->commit();
            }

            $dispatchRegistTemplated = $dispatch->getDispatchRegists()->andWhere(['auto_view' => 0])->all();
            $dispatchRegists = $dispatch->dispatchRegists;

            Proxy::synchronize();

            if(count($dispatchRegistTemplated) == 0) {
                echo '<br>dispatchRegistTemplated: TRUE';
            }
            if(count($dispatchRegists) == $dispatch->accounts_count) {
                echo '<br>accounts remains: TRUE';
            } else {
                echo '<br>ERROR: count dispatch regists: '.count($dispatchRegists);
            }
            if($dispatch->status != 'job') {
                echo '<br>Is not job: TRUE';
            }

            if(count($dispatchRegistTemplated) == 0 && count($dispatchRegists) == $dispatch->accounts_count && $dispatch->status != 'job'){
                $dispatch->status = 'wait';
                $company = Companies::findOne($dispatch->company_id);
                if($company != null){
                    $telId = $company->telegramId;
                    if($telId != null){
                        self::sendTelMessage($telId, "Рассылка «{$dispatch->name}» изменила свой статус на «Готова к запуску»");
                    }
                }
                $dispatch->save(false);
                continue;
            }
        }
    }

    public function actionCheckBlockingAccounts()
    {
        // $dispatches = Dispatch::find()->where(['or', ['status' => 'job'], ['status' => 'wait']])->limit(1)->all();
        $dispatches = Dispatch::find()->where(['status' => 'job'])->limit(1)->all();

        /** @var \app\models\Dispatch $dispatch */
        foreach ($dispatches as $dispatch)
        {
            $blockingRegists = DispatchRegist::find()->where(['status' => DispatchRegist::STATUS_BLOCKED, 'dispatch_id' => $dispatch->id])->all();
            $blockingCount = count($blockingRegists);

            $existsRegists = DispatchRegist::find()->where(['dispatch_id' => $dispatch->id])->all();

            if($dipsatch->accounts_count <= count($existsRegists)){
                continue;
            }

            foreach ($blockingRegists as $regist)
            {
                // $regist->delete();
                $regist->dispatch_id = null;
                $regist->save(false);
                self::sendTelMessage('247187885', 'Аккаунт '.$regist->login.':'.$regist->password.' был заблокирован');
            }

            /** @var \app\models\Shop $shop */
            $shop = Shop::find()->one();
            $purchase = AccountsDataPurchase::makePurchase($shop, $dispatch->company_id, $blockingCount);
            $purchasedAccounts = $purchase->purchasedAccounts;
            $accounts = $purchasedAccounts;

            try {
                foreach ($accounts as $account)
                {
                    $dispatchRegist = new DispatchRegist();
                    $dispatchRegist->login = trim($account[0]);
                    $dispatchRegist->password = trim($account[1]);
                    $dispatchRegist->company_id = $dispatch->company_id;
                    $dispatchRegist->data = date('Y-m-d H:i:s');
                    $dispatchRegist->template_id = $dispatch->auto_regists_template_id;
                    $dispatchRegist->dispatch_id = $dispatch->id;
                    if($dispatchRegist->save(false))
                    {
                        echo "DISPATCH REGIST: <br>";
                        var_dump($dispatchRegist);
                        echo "<br> END DISPATCH REGIST";
                    }
                    $dispatchRegist->makeAutoPage();
                    DispatchRegist::setVkInfo($dispatchRegist);
                }
            } catch (Exception $e)
            {

            }
        }
    }

    public static function sendTelMessage($userId, $text)
    {
        //$token = Settings::find()->where(['key' => 'telegram_access_token'])->one();
        $token = Settings::findByKey('telegram_access_token')->value;
        //$proxy_server = Settings::find()->where(['key' => 'proxy_server'])->one();
        $proxy_server = '45.32.155.5:35200';
        // $proxy = $proxy_server->value;

        if(is_array($userId)){
            foreach ($userId as $id) {
                $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';
                $url=$url.'?'.http_build_query(['chat_id' => $id, 'text' => $text, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
                //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HEADER, 1);
                $curl_scraped_page = curl_exec($ch);
                curl_close($ch);

                curl_setopt($curl, CURLOPT_PROXY, $proxy);

            }
        } else {
            $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';
            $url=$url.'?'.http_build_query(['chat_id' => $userId, 'text' => $text, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
            //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            $curl_scraped_page = curl_exec($ch);
            curl_close($ch);
        }


        return true;
    }

    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionSend()
    {
        $i = 0;

        $interval_min = Settings::find()->where(['key' => 'interval_in_minutes'])->one();

        // $dispatchLimited = Dispatch::find()->where(['status' => Dispatch::STATUS_DAILY_LIMIT])->all();
        // /** @var \app\models\Dispatch $dispatch */
        // foreach ($dispatchLimited as $dispatch)
        // {
        //     $now = time();
        //     $lastDay = date('Y-m-d H:i:s', $now - 86400);

        //     $dispatchRegist = $dispatch->getDispatchRegists()->andWhere(['<=', 'last_dispatch_time', $lastDay])->one();

        //     if($dispatchRegist){
        //         $dispatch->status = 'job';
        //         $dispatch->save(false);
        //     }
        // }

        // Взять сообщение статусом В очериди
        $messages = Dispatch::find()->where(['status' => 'job'])->all();

        Yii::warning($messages, __METHOD__);

        if ($messages) {
            foreach ($messages as $message) {

                Yii::warning(Json::encode($message), __METHOD__);

                $string_acc = '';
                $string_count = 0;

                Yii::warning( $message->id, __METHOD__);


                /** @var \app\models\DispatchRegist[] $dispatchRegists */
                $dispatchRegists = $message->getDispatchRegists()->andWhere(['status' => 'ok'])->all();
                // if(count($dispatchRegists) == 0)
                // {
                // $message->status = Dispatch::STATUS_DAILY_LIMIT;
                // $message->save(false);
                // continue;
                // }

                echo 'accounts -- <br>';
                var_dump($dispatchRegists);
                echo '<br>accounts --';

                foreach ($dispatchRegists as $dispatchRegist) {
                    $iLim = 0; //Интервал между отправками
                    $i++;
                    $my_arr[$i]['dispatch_id'] = $dispatch->id;
                    $my_arr[$i]['dispatch_regist_id'] = $dispatchRegist->id;
                    $string_count = 0;
                    $payCount = 0;
                    $payAmount = 0;
                    $account = $dispatchRegist;

                    //VarDumper::dump($account, 10, true); die();

                    if ($account) {
                        $my_arr[$i]['account'] = $account->id;
                        $my_arr[$i]['account_status_start'] = $account->status;
                        $my_arr[$i]['account_name'] = $account->username;

                        /** @var DispatchRegist $data */
                        $setting = CompanySettings::find()->where(['company_id' => $account->company_id])->one();
                        // кол-во сообщения за раз
                        $count_message = $setting->messages_in_transaction;
                        // кол-во сообщения в день
                        $acc_message_count = $setting->messages_daily_limit;

                        // Получаем клиенты кто в очереди кому надо отправить в колчестве кол-во сообщения за раз
                        /** @var DispatchStatus[] $clients */
                        $clients = DispatchStatus::find()->where(['send' => false, 'dispatch_id' => $message->id])->orderBy(['id' => SORT_ASC,])->limit($count_message)->all();

                        //VarDumper::dump($clients, 10, true); die();

                        // если нет таких, то статус сообщения становиться отправлено (finish)
                        if (!$clients) {
                            $message->status = "finish";
                            $company = Companies::findOne($message->company_id);
                            if($company != null){
                                $telId = $company->telegramId;
                                if($telId != null){
                                    // self::sendTelMessage($telId, "Рассылка «{$message->name}» изменила свой статус на «Завершена»");
                                }
                            }
                            $message->update();
                        } else {
                            // Отправляем сообщения каждому
                            foreach ($clients as $user) {

                                //VarDumper::dump($user, 10, true); die();

                                $iLim++;
                                $proxy = Proxy::findOne(['id' => $account->proxy]);
                                if (!$proxy) {
                                    continue;
                                }
                                if ($proxy->status == 'yes') {
                                    $send_proxy = "{$proxy->ip_adress}:{$proxy->port}";
                                    // задешка 5 секунд
                                    if ($iLim == 8) {
                                        sleep(5);
                                        $iLim = 0;
                                    }

                                    // Выбираем случайное сообщение
                                    for ($i = 0; $i < 20; $i++) {
                                        $num = 'text' . random_int(1, 4);
                                        $msg = $message[$num];
                                        if ($msg) {//Если сообщение не пустое - ввыходим из цикла
                                            break 1;
                                        }
                                    }

                                    $tags = TemplateMessages::find()->where(['or', ['company_id' => $account->company_id], ['company_id' => 1]])->all();
                                    $msg = TagHelper::handleTemplateMessages($msg, $tags);
                                    if(UniqueMessage::hasWithText($msg, $message->id) == false){
                                        $uniqueMessage = new UniqueMessage(['dispatch_id' => $message->id, 'text' => $msg]);
                                    } else {
                                        $uniqueMessage = UniqueMessage::findByText($msg);
                                    }
                                    $msg = TagHelper::handleModel($msg, [$account, $user]);

                                    $result = MessagesController::sendMessage((int)trim($user->account_id), $msg, $account, $send_proxy);

                                    $mesHistory = new Message();
                                    $mesHistory->date_time = date("Y-m-d H:i:s");
                                    $mesHistory->company_id = $account->company_id;
                                    $mesHistory->dispatch_id = $message->id;
                                    $mesHistory->dispatch_registr_id = $account->id;
                                    $mesHistory->dispatch_status_id = $user->id;
                                    $mesHistory->text = $msg;
                                    $mesHistory->from = Message::FROM_ACCOUNT;
                                    $mesHistory->save();

                                    $string_count++;

                                    // сообщения успешно отправлено
                                    if ($result == true) {
                                        $uniqueMessage->repeat_count++;
                                        $uniqueMessage->save(false);
                                        $user->unique_message_id = $uniqueMessage->id;
                                        $user->send = true;
                                        $user->check_bot = true;
                                        $user->data = date("Y-m-d H:i:s");
                                        $user->read = "no";
                                        $user->send_account_id = $account->id;
                                        $user->response_id = $result['response'];
                                        $user->update();

                                        Yii::warning('Сообщение успешно отправлено', __METHOD__);

                                        // увеличеваем кол-во отправленных сообщения с аккаунта (изначально он равно к 0)
                                        $account->sended_message_count++;
                                        $account->all_sended_message_count++;
                                        $account->last_dispatch_time = $user->data;

                                        // БЛОК СПИСАНИЯ ДЕНЕГ С БАЛАНСА ЮЗЕРА

                                        // if ($companySetting->payed()) {
                                        // $payCount++;
                                        // $payAmount += $priceByMessage;
                                        // }

                                        // сообщения не отправилось записаваем причину в логах
                                    } else {
                                        $my_arr[$i]['account'] = $user->account_id;
                                        // $account->status = DispatchStatus::STATUS_ACCOUNT_BLOCKED;
                                        //$account->blocking_date = date('Y-m-d'); //Запись даты блокировки аккаунта
                                        $account->coment = $result;

                                        $account->update();
                                        continue;
                                    }

                                    // считаем отправленный сообщения с аккаунта если он равно к кол-во сообщения за раз то статус меняем на (interval_not_end)
                                    $my_arr[$i]['string_count'] = $string_count;
                                    $my_arr[$i]['count_message'] = $count_message;
                                    $my_arr[$i]['interval_not_end'] = 'false';
                                    if ($string_count >= $count_message) {
                                        $account->status = "interval_not_end";
                                        $my_arr[$i]['interval_not_end'] = 'true';
                                    }

                                    // кол-во отправленых сообщения равно к кол-во сообщения в день то статус меняем на (limit_exceeded)
                                    $my_arr[$i]['sended_message_count'] = $account->sended_message_count;
                                    $my_arr[$i]['count_message'] = $acc_message_count;
                                    $my_arr[$i]['limit_exceeded'] = 'false';

                                    //отчет отправки в реальном времени
                                    $date = date('Y-m-d');
                                    $rep = DailyReport::find()
                                        ->andfilterWhere(['like','company_id',$account->company_id])
                                        ->andfilterWhere(['like','dispatch_id',$message->id])
                                        ->andfilterWhere(['like','account_id',$account->id])
                                        ->andfilterWhere(['=','type',DailyReport::TYPE_SENT])
                                        ->andfilterWhere(['like','date_event',$date])
                                        ->one();
                                    if ($rep) {
                                        $rep->sended_message_count = $rep->sended_message_count + 1;
                                        $rep->save(false);
                                    } else {
                                        $report = new DailyReport([
                                            'company_id' => $account->company_id,
                                            'dispatch_id' => $message->id,
                                            'account_id' => $account->id,
                                            'sended_message_count' => 1,
                                            'type' => DailyReport::TYPE_SENT,
                                        ]);
                                        $report->save();
                                    }

                                    if ($account->sended_message_count >= $acc_message_count) {
                                        $account->status = "limit_exceeded";
                                        $my_arr[$i]['limit_exceeded'] = 'true';

                                        $limitedAccounts = DispatchRegist::find()->where(['dispatch_id' => $message->id, 'status' => 'limit_exceeded'])->count();
                                        if($limitedAccounts == 0){
                                            self::sendTelMessage('247187885', "Рассылка {$message->name} дошла до суточного лимита");
                                        }



//                                        $report = new DailyReport([
//                                            'company_id' => $account->company_id,
//                                            'dispatch_id' => $message->id,
//                                            'account_id' => $account->id,
//                                            'sended_message_count' => $account->sended_message_count,
//                                            'type' => DailyReport::TYPE_SENT,
//                                        ]);
//                                        $report->save();

                                        // if ($payCount > 0) {
                                        //     $accountReport = new AccountingReport([
                                        //         'company_id' => $account->company_id,
                                        //         'operation_type' => AccountingReport::TYPE_DISPATCH_PAYED,
                                        //         'amount' => $payAmount,
                                        //         'description' => 'Списание средств за рассылку "' . $message->name . '". Отправлено ' . $payCount . ' сообщений',
                                        //     ]);
                                        //     $accountReport->save();
                                        // }
                                    }
                                    $account->update();
//                                    } else {
//                                        Yii::warning('Недостаточно информации для замены тегов. Отправка пропущена', __METHOD__);
//                                        $my_arr['convert_tags'] =  'Недостаточно информации для замены тегов. Отправка пропущена';
//                                    }
                                }
                            }

                            $my_arr[$i]['account_status_finish'] = $account->status;

                        }

                    } else {
                        Yii::$app->session->setFlash('message_fail', "Нет готовых аккаунтов для отправки сообщении");
                    }

                    if ($string_count != 0) {
                        $string_acc .= "С аккаунта {$account->username} отправлен {$string_count} сообщения, ";
                        Yii::$app->session->setFlash('message_success', $string_acc);
                    }
                }

            }
        } else {

            Yii::$app->session->setFlash('message_info', "Нет готовых сообщение для отправки");
        }

        VarDumper::dump($my_arr, 10, true);
    }

    /**
     * Это метод который ищет "спящие аккаунты", которые не используются более указанного кол-ва дней
     */
    public function actionSearchSleepingDispatchRegists()
    {
        $sleepingIntervalSetting = Settings::findByKey('sleeping_interval');
        if($sleepingIntervalSetting != null && $sleepingIntervalSetting->value != null)
        {
            $sleepingIntervalValue = $sleepingIntervalSetting->value;
        } else {
            $sleepingIntervalValue = 5;
        }
        $sleepingInterval = time() - ($sleepingIntervalValue * 86400); // 86400 — кол-во секунд в сутках
        $sleepingStartDate = date('Y-m-d H:i:s', $sleepingInterval);
        //$accounts = DispatchRegist::find()->where(['<', 'last_dispatch_time', $sleepingStartDate])->andWhere(['or', ['status' => 'sleeping'], ['status' => 'account_blocked']])->all();
        $accounts = DispatchRegist::find()
            ->where(['<', 'last_dispatch_time', $sleepingStartDate])
            ->andWhere(['status' => 'ok'])
            ->all();

        foreach ($accounts as $account) {
            $account->makeSleep();
        }
    }

    /**
     * @param $token
     * @param $proxy
     * @return mixed
     */
    static function getUser($users, $fields, $token, $proxy)
    {
        $url = 'https://api.vk.com/method/users.get';
        $params = array(
            'user_ids' => $users,
            'fields' => $fields,
            'name_case' => 'Nom', // Токен того кто отправляет
            'access_token' => $token, // Токен того кто отправляет
            'v' => '5.85',
        );

        // В $result вернется id отправленного сообщения

        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'proxy' => $proxy,
                'request_fulluri' => true,
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));

        return json_decode($result, true);
    }


    function send($userVk, $text, $token, $proxy)
    {
        $url = 'https://api.vk.com/method/messages.send';
        $params = array(
            'user_id' => $userVk,    // Кому отправляем
            'message' => $text,   // Что отправляем
            'access_token' => $token, // Токен того кто отправляет
            'v' => '5.37',
        );

        // В $result вернется id отправленного сообщения
        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'proxy' => $proxy,
                'request_fulluri' => true,
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));
        $result = json_decode($result, true);
        return $result;
    }
}