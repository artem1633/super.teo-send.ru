<?php

namespace app\modules\api\controllers;

use app\components\helpers\FunctionHelper;
use app\models\Alert;
use app\models\Bots;
use app\models\BotsAnswers;
use app\models\BotsDialogs;
use app\models\Chat;
use app\models\Companies;
use app\models\Dispatch;
use app\models\DispatchChats;
use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\Message;
use app\models\Proxy;
use app\models\Questionary;
use app\models\ReferalRedirects;
use app\models\Resume;
use app\models\Settings;
use app\models\Telegram;
use app\models\Users;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;

/**
 * Default controller for the `api` module
 */
class BotinfoController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['bot-in','testpush','bot-update','send-refer','getbotdialog'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }




    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBotIn()
    {

        $content = file_get_contents('php://input'); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $report = json_decode($content); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $result = json_decode($content,true); //декодируем апдейт json, пришедший с телеграмма


        // if ($result["callback_query"]) {
        //     $this->botCall($result);
        //     return true;
        // }

        $text = $result["message"]["text"]; //Текст сообщения
        $chat_id = $result["message"]["chat"]["id"]; //Уникальный идентификатор пользователя
        $username = $result["message"]["chat"]["username"]; //Уникальный идентификатор пользователя
        $name = $result["message"]["from"]["first_name"]; //Юзернейм пользователя

        // $file = file_put_contents('income.txt', $content); // создаем текстовый файл для отладки(по желанию)



        if ($text == '/start') {

            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=>" {$name} Привет! Меня создали для того чтобы я информарировал вас, о всех событиях
               Чтобы я начал работать введи свой код:",
                //'reply_markup' => json_encode($key),
                // 'parse_mode'=>'HTML',
            ]);

            return true;

        }


        if ($text != '/start') {
            //проверяем может он уже подключен
            $telegtamC = Telegram::find()->where(['telegram_id' => $chat_id])->one();
            if (!$telegtamC) {
                //Проверяем подключаеться супер компания или просто пользователь
                /** @var Companies $company */
                $company = Companies::find()->where(['unique_code' => $text])->one();
                if ($company) {
                    $telegtamC = new Telegram();
                    $telegtamC->name = $name  . ' ' . $username;
                    $telegtamC->telegram_id = (string)$chat_id;
                    $telegtamC->company_id = $company->id;
                    if ($telegtamC->save()) {
                        $this->getReq('sendMessage', [
                            'chat_id' => $chat_id,
                            'text' => "{$name}, Поздравляю с успешным подключением. Жди новостей! Так же вы сюда можете писать любые вопросы, администратор ввам ответит",
                            //'parse_mode'=>'HTML',
                        ]);

                        $fullUrl = Url::toRoute(['/companies/view', 'id' => $company->id], true);
                        $a = "Подключен новый пользователь к боту {$company->company_name}
                        {$fullUrl}";
                        $this->sendadmin(['chat_id' => '247187885', 'parse_mode' => 'HTML', 'text' => $a]);
                        return true;
                    } else {
                        $a = serialize($telegtamC->getErrors());

                        $this->getReq('sendMessage',[
                            'chat_id'=> $chat_id,
                            'text'=>"{$name}, Что то пошло не так напишите администратору @technology_to_everyone {$a}",
                            'parse_mode'=>'HTML',
                        ]);

                        return true;
                    }
                } else {
                    $this->getReq('sendMessage',[
                        'chat_id'=> $chat_id,
                        'text'=>"{$name}, мы не нашли ваш ключ! Попробуйте еще раз",
                        //'parse_mode'=>'HTML',
                    ]);
                    return true;

                }

            } else {
                $this->getReq('sendMessage',[
                    'chat_id'=> $chat_id,
                    'text'=>"{$name}, вы уже подключили уведомления",
                    //'parse_mode'=>'HTML',
                ]);
                return true;
            }
        }

        $a = serialize($telegtamC->getErrors());

        $this->getReq('sendMessage',[
            'chat_id'=> $chat_id,
            'text'=>"{$name}, Что то пошло не так напишите администратору @technology_to_everyone {$a}",
            'parse_mode'=>'HTML',
        ]);

        return true;
    }



    public static function getReq($method,$params=[],$decoded=0){ //параметр 1 это метод, 2 - это массив параметров к методу, 3 - декодированный ли будет результат будет или нет.


        $token = Settings::findByKey('telegram_access_token')->value;
        $proxy = '45.32.155.5:35200';

        $url =  "https://api.telegram.org/bot{$token}/{$method}"; //основная строка и метод
        if(count($params)){
            $url=$url.'?'.http_build_query($params);//к нему мы прибавляем парметры, в виде GET-параметров
        }


        $curl = curl_init($url);    //инициализируем curl по нашему урлу

        curl_setopt($curl, CURLOPT_PROXY, $proxy);

        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);   //здесь мы говорим, чтобы запром вернул нам ответ сервера телеграмма в виде строки, нежели напрямую.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   //Не проверяем сертификат сервера телеграмма.
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $result = curl_exec($curl);   // исполняем сессию curl
        curl_close($curl); // завершаем сессию
        if($decoded){
            return json_decode($result);// если установили, значит декодируем полученную строку json формата в объект языка PHP
        }
        return $result; //Или просто возращаем ответ в виде строки
    }

    public function actionWebhook()
    {
        $token = Settings::findByKey('telegram_access_token')->value;
        $proxy = '45.32.155.5:35200';

        $url = 'https://api.telegram.org/bot'.$token.'/setWebhook?url=https://super.teo-send.ru/api/botinfo/bot-in';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);

        echo $curl_scraped_page;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @return array
     */
    public function actionSendRefer()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $headers = Yii::$app->response->headers;
        $headers->add('Access-Control-Allow-Origin', '*');

        $refer = new ReferalRedirects([
            'ip' => $_SERVER['REMOTE_ADDR'],
            'refer_company_id' => $_GET['ref'],
            'user_agent' => $_SERVER['HTTP_USER_AGENT'],
        ]);

        return ['result' => $refer->save()];
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionGetbotdialog()//проверяем диалоги на не прочитанные сообщения
    {
        /** @var Settings $setingToken */
        /** @var Settings $setingProxy */

        //список всех рассылок с ботом
        $dispatchAll = Dispatch::find()->where(['not', ['botId' => null]])->all();

        $i = 0;
        /** @var Dispatch $dispatch */
        foreach ($dispatchAll as $dispatch) {
            $i2 = 0;
            $log[$i]['dispatch'] = $dispatch->name;
            $dispatchStatussAll = DispatchStatus::find()->where(['dispatch_id' => $dispatch->id, 'check_bot' => false])->all();
            /** @var DispatchStatus $dispatchStatus */
            foreach ($dispatchStatussAll as $dispatchStatus) {
                $i2++;
                $log[$i][$i2]['dispatch_id'] = $dispatch->id;
                $myHistory = Message::find()->where([
                    'dispatch_id' => $dispatch->id,
                    'dispatch_status_id' => $dispatchStatus->id,
                ])->orderBy(['id' => SORT_DESC])->one();
                $log[$i][$i2]['mesage'] = $myHistory->id;
                $hisCount = Message::find()->where([
                    'dispatch_id' => $dispatch->id,
                    'dispatch_status_id' => $dispatchStatus->id,
                ])->count();
                $log[$i][$i2]['count'] = $hisCount;

                /** @var BotsDialogs $botDialog */
                $botDialogAll = BotsDialogs::find()
                    ->where('botId = :id and stepNumber = :stepNumber',
                        ['id' => $dispatch->botId, 'stepNumber' => $hisCount])
                    ->all();

                $log[$i][$i2]['sql'] = BotsDialogs::find()
                    ->where('botId = :id and stepNumber = :stepNumber',
                        ['id' => $dispatch->botId, 'stepNumber' => $hisCount])
                    ->createCommand()->rawSql;
                $log[$i][$i2]['sql_count'] = count($botDialogAll);


                $account = DispatchRegist::find()->where(['id' => $dispatchStatus->send_account_id])->one();
                $proxy = Proxy::findOne(['id' => $account->proxy]);
                $send_proxy = "tcp://{$proxy->ip_adress}:{$proxy->port}";
                if ($botDialog) {
                    $dispatchStatus->check_bot = true;
                    $dispatchStatus->save(false);
                    $fullUrl = Url::toRoute(['/dispatch-status/dialog', 'id' => $dispatchStatus->id], true);
                    $text = "Бот  не нашол ответ на сообщение соответствуещего номера 
                              пользователю https://vk.com/id{$dispatchStatus->account_id}  
                              Посмотреть переписку можно тут
                              {$fullUrl}
                              ";
                    $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $dispatchStatus->dispatch_id])->all(), 'chatId');
                    DispatchController::sendTelMessage($userAll, $text);
                    continue;
                }


                $bodyText = $myHistory->text;
                $log[$i]['bodyText'] = $bodyText;
                $i3 = 0;
                foreach ($botDialogAll as $botDialog) {
                    $i3++;

                    //стоит ответ на любой вопрос
                    if ($botDialog->anyAnswer) {
                        // $log[$i][$i2][$i3]['$botDialogAvto'] = 'true';

                        $result = MessagesController::sendMessage($dispatchStatus->account_id, $botDialog->result, $account, $send_proxy);

                        //стоит уведомление
                        if ($botDialog->notify) {
                            /** @var Bots $bot */
                            if ($botDialog->status) $dispatchStatus->status = $botDialog->status;

                            $dispatchStatus->check_bot = true;
                            $bot = Bots::find()->where(['id' => $botDialog->botId])->one();
                            $fullUrl = Url::toRoute(['/dispatch-status/dialog', 'id' => $dispatchStatus->id], true);
                            $text = "Бот \"{$bot->name}\" автоматически отправил сообщение \"{$botDialog->result}\" 
                              пользователю https://vk.com/id{$dispatchStatus->account_id}  
                              Посмотреть переписку можно тут
                              {$fullUrl}
                              ";
                            $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $dispatchStatus->dispatch_id])->all(), 'chatId');
                            DispatchController::sendTelMessage($userAll, $text);

                            $mesHistory = new Message();
                            $mesHistory->date_time = date("Y-m-d H:i:s");
                            $mesHistory->company_id = $dispatch->company_id;
                            $mesHistory->dispatch_id = $dispatch->id;
                            $mesHistory->dispatch_registr_id = $account->id;
                            $mesHistory->dispatch_status_id = $dispatchStatus->id;
                            $mesHistory->text = $botDialog->result;
                            $mesHistory->from = Message::FROM_ACCOUNT;
                            $mesHistory->save();
                        }
                        break;
                    }
                    if ($botDialog->check_search) {
                        $step = BotsAnswers::find()
                            ->where('dialogId = :dialogId and tag LIKE :bodyText',
                                ['dialogId' => $botDialog->id, 'bodyText' => "{$bodyText}"])
                            ->one();
                        $log[$i][$i2][$i3]['sql'] = BotsAnswers::find()
                            ->where('dialogId = :dialogId and tag LIKE :bodyText',
                                ['dialogId' => $botDialog->id, 'bodyText' => "{$bodyText}"])
                            ->createCommand()->rawSql;
                    } else {
                        $step = BotsAnswers::find()
                            ->where('dialogId = :dialogId and tag LIKE :bodyText',
                                ['dialogId' => $botDialog->id, 'bodyText' => "% {$bodyText} %"])
                            ->one();
                        $log[$i][$i2][$i3]['sql'] = BotsAnswers::find()
                            ->where('dialogId = :dialogId and tag LIKE :bodyText',
                                ['dialogId' => $botDialog->id, 'bodyText' => "% {$bodyText} %"])
                            ->createCommand()->rawSql;
                    }
                    if ($step) break;
                }

                //нашли совпадение
                if ($step) {
                    $log[$i][$i2][$i3]['$botDialog'] = 'true';
                    //отправляем авто ответ
                    $result = MessagesController::sendMessage($dispatchStatus->account_id, $botDialog->result, $account, $send_proxy);

                    if ($botDialog->notify) {
                        if ($botDialog->status) $dispatchStatus->status = $botDialog->status;
                        $dispatchStatus->check_bot = true;
                        /** @var Bots $bot */
                        $bot = Bots::find()->where(['id' => $botDialog->botId])->one();
                        $fullUrl = Url::toRoute(['/dispatch-status/dialog', 'id' => $dispatchStatus->id], true);
                        $text = "Бот \"{$bot->name}\" автоматически отправил сообщение \"{$botDialog->result}\" 
                              пользователю https://vk.com/id{$dispatchStatus->account_id}  
                              Посмотреть переписку можно тут
                              {$fullUrl}
                              ";
                        $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $dispatchStatus->dispatch_id])->all(), 'chatId');

                        DispatchController::sendTelMessage($userAll, $text);
                        $mesHistory = new Message();
                        $mesHistory->date_time = date("Y-m-d H:i:s");
                        $mesHistory->company_id = $dispatch->company_id;
                        $mesHistory->dispatch_id = $dispatch->id;
                        $mesHistory->dispatch_registr_id = $account->id;
                        $mesHistory->dispatch_status_id = $dispatchStatus->id;
                        $mesHistory->text = $botDialog->result;
                        $mesHistory->from = Message::FROM_ACCOUNT;
                        $mesHistory->save();
                    }
                    //не нашли нужного ответа
                } else {

                    $dispatchStatus->check_bot = true;
                    $bot = Bots::find()->where(['id' => $botDialog->botId])->one();
                    $fullUrl = Url::toRoute(['/dispatch-status/dialog', 'id' => $dispatchStatus->id], true);
                    $text = "В рассылки \"{$dispatch->name} \" в Боте {$bot->name} нету подходящего ответа
                              Диалог с пользователем https://vk.com/id{$dispatchStatus->account_id}  
                                Посмотреть переписку можно тут  
                              {$fullUrl}
                              ";

                    $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $dispatchStatus->dispatch_id])->all(), 'chatId');
                    DispatchController::sendTelMessage($userAll, $text);
                }

                $dispatchStatus->save(false);



            }

            echo "<pre>";
            print_r($log);
            echo "</pre>";
        }
    }





}
