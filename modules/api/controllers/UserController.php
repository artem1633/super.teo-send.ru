<?php

namespace app\modules\api\controllers;

use app\components\helpers\FunctionHelper;
use app\models\calculation\Calculation;
use app\models\Settings;
use yii\rest\ActiveController;
use yii\web\Response;

/**
 * Class UserController
 * @package app\modules\api\controllers
 * Контроллер предаставляет интерфейс для получения
 * данных о текущем авторизованом пользователе
 */
class UserController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['get-company-model'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * Возвращет модель
     * @return mixed
     */
    public function actionGetCompanyModel()
    {
        return FunctionHelper::getCompanyModel();
    }
}