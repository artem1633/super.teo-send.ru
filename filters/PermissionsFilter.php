<?php

namespace app\filters;

use Yii;
use yii\base\ActionFilter;
use yii\web\ForbiddenHttpException;

/**
 * Class PermissionsFilter
 * @package app\filters
 *
 * Делает проверку, имеет ли права пользователь
 */
class PermissionsFilter extends ActionFilter
{
    /**
     * @var array
     */
    public $rules;

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        foreach ($this->rules as $rule) {
            if(isset($rule['actions']) == false || isset($rule['roles']) == false){
                continue;
            }
            if(is_array($rule['actions'])){
                $actions = $rule['actions'];
            } else {
                $actions = [$rule['actions']];
            }
            if(is_array($rule['roles'])){
                $roles = $rule['roles'];
            } else {
                $roles = [$rule['roles']];
            }
            if(in_array($action->id, $actions) || in_array('*', $actions)){
                if(in_array(Yii::$app->user->identity->role_id, $roles) == false){
                    throw new ForbiddenHttpException('У вас недостаточно прав');
                }
            }
        }

        return parent::beforeAction($action);
    }
}