<?php

namespace app\assets;

use yii\web\AssetBundle;

class HandlebarsAsset extends AssetBundle
{
    public $basePath = '@bower';
    public $baseUrl = '@bower';

    public $css = [
        ''
    ];
    public $js = [
        'https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.12/handlebars.min.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
    ];
}
