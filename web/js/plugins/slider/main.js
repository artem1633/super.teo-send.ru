$(document).ready(function(){
        var thumb = document.getElementById('thumb');
        var bg 	  = document.getElementById('bg');
        var drag_status = false;
        var count = 0;
        var x;
        thumb.onmousedown = function(e){
            drag_status = true;
            x = e.pageX - thumb.offsetLeft;
        }

        document.onmousemove = function(e){
            if(!drag_status) return false;
            thumb.style.left = e.pageX - x  + 'px';
            if(thumb.offsetLeft < 0) thumb.style.left =  0 + 'px';
            if(thumb.offsetLeft > bg.offsetWidth - thumb.offsetWidth)thumb.style.left =  bg.offsetWidth - thumb.offsetWidth + 'px';
            count = Math.round(thumb.offsetLeft/10)*10;
            document.getElementsByTagName('p')[0].innerHTML = count;
        }

        document.onmouseup = function(){
            drag_status = false;
        }
});