<?php

use skeeks\widget\highcharts\Highcharts;


?>

<div class="col-md-12">
    <div class="panel panel-success" style="margin-top: 20px;">
        <div class="panel-heading">
            <h3 class="panel-title">Сообщения</h3>
        </div>
        <div class="panel-body" style="min-height: 200px;padding-bottom: 15px">
            <?php
            echo Highcharts::widget([
                'options' => [
                    'chart' => [
                        'height' => 225,
                        'type' => 'line'

                    ],
                    'title' => false,//['text' => 'Всего отправлено сообщений'],
                    //'subtitle' => ['text' => 'Notice the difference between a 0 value and a null point'],
                    'plotOptions' => [
                        'column' => ['depth' => 25],
                        'line' => [
                            'dataLabels' => [
                                'enabled' => true
                            ]
                        ],
                        'enableMouseTracking' => false,
                    ],
                    'xAxis' => [
                        'categories' => $report[0],
                        'labels' => [
                            'skew3d' => true,
                            'style' => ['fontSize' => '10px']
                        ]
                    ],
                    'yAxis' => [
                        'title' => ['text' => null]
                    ],
                    'series' => [
                        ['name' => 'Сообщений отправлено', 'data' => $report[1]],
                        ['name' => 'Прочитано', 'data' => $report[2]],
                        ['name' => 'Ответило', 'data' => $report[3]],
                        ['name' => 'Заинтересовано', 'data' => $report[4]],
                        ['name' => 'Продажи', 'data' => $report[5]],
                        ['name' => 'Отказы', 'data' => $report[6]],
                    ],
                    'credits' => ['enabled' => false],
                    'legend' => ['enabled' => false],
                ]
            ]);
            ?>
        </div>
    </div>
</div>

<?= $this->render('@app/views/dispatch/report', ['stts' => $stts]) ?>
</div>


<?php
$this->registerJs("
 $(function () {
            $('.close-btn').click(function () {
                var Id = $(this).attr('id');
                console.log(Id)
                $.ajax({
                    url: window.location.href,
                    timeout: 0,
                    data: {
                        'id': Id // данные, которые отправляются на сервер.
                    },
                    success: function (data) {
                        location.reload()
                        // $('.chart-' + Id).css('display', 'none');
                    },
                    error: function (data) {
                        alert('Ошибка ' + data);
                    }
                });
            })
        })    
",  yii\web\View::POS_READY);
?>
