<?php

namespace app\widgets;

use yii\base\Widget;
use yii\base\InvalidConfigException;
use yii\data\ArrayDataProvider;
use app\models\Dispatch;
use app\models\DailyReport;

class DispatchReport extends Widget
{
    /**
     * @var int
     */
    public $dispatchId;

    /**
     * @var int
     */
    public $companyId = null;

    /**
     * @var array
     */
    public $gridViewOptions = [];

    /**
     * @var \app\models\Dispatch
     */
    private $dispatch;

    /**
     * @var \yii\data\ArrayDataProvider
     */
    private $dataProvider;

    public function init()
    {
        parent::init();

        if($this->dispatchId == null){
            throw new InvalidConfigException('dispatchId является обязательным свойством');
        }

        $this->dispatch = Dispatch::findOne($this->dispatchId);

        if($this->dispatch == null){
            throw new InvalidConfigException('Рассылка под ID "'.$this->dipsatchId.'" не была найдена');
        }

        $dates = DailyReport::getDates($this->companyId, $this->dispatchId);

        $report = DailyReport::getReportBySendMesasges($this->companyId, $this->dispatchId, $dates, 1);

        $this->dataProvider = new ArrayDataProvider([
            'allModels' => $report,
        ]);
    }

    public function run()
    {
        return $this->render('dispatch-report', [
            'dataProvider' => $this->dataProvider,
            'dispatchId' => $this->dispatchId,
            'companyId' => $this->companyId,
            'gridViewOptions' => $this->gridViewOptions
        ]);
    }
}


?>