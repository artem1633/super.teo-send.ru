<?php

namespace app\widgets\owlCarousel;

use yii\web\AssetBundle;

class OwlCarouselAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'js/plugins/owlCarousel/owl.carousel.min.css',
    ];

    public $js = [
        'js/plugins/owlCarousel/owl.carousel.min.js'
    ];

    public $depends = [
        'app\assets\AppAsset',
    ];
}