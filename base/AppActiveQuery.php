<?php

namespace app\base;

use Yii;
use yii\db\ActiveQuery;

/**
 * Class AppActiveQuery
 * @package app\models
 * @see \app\models\Clients
 */
class AppActiveQuery extends ActiveQuery
{
    /** @var integer Если пользователь является членом какой либо компании, то он должен видеть только объекты своей компании
     *  Эта переменная хранит в себе id компании к которой привязан пользователь
     */
    public $companyId;

    /**
     * @var string Наименования отношения к компании
     */
    public $companyRelationName = 'company';

    public $companyRequire = true;

    /**
     * @return $this
     */
    public function disableCompanyRequire()
    {
        $this->companyRequire = false;
        return $this;
    }

    /**
     * @return $this
     */
    public function enableCompanyRequire()
    {
        $this->companyRequire = true;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function all($db = null)
    {
        if($this->companyRequire){
            if($this->companyId != null) {
                $tableName = $this->getPrimaryTableName();

                if (Yii::$app->user->identity->isSuperAdmin() === false)
                {
                    $this->andWhere([$tableName . '.company_id' => $this->companyId]);
                }
            }
        }

        return parent::all($db);
    }

    /**
     * @param string $q
     * @param null $db
     * @return array|int|string|\yii\db\ActiveRecord[]
     */
    public function count($q = '*', $db = null)
    {
        if($this->companyRequire){
            if($this->companyId != null)
            {
                $tableName = $this->getPrimaryTableName();

                if (Yii::$app->user->identity->isSuperAdmin() === false)
                {
                    $this->andWhere([$tableName . '.company_id' => $this->companyId]);
                }
            }
        }

        return parent::count($q, $db);
    }
}