<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Message */
?>
<div class="message-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date_time',
            'dispatch_registr_id',
            'dispatch_status_id',
            'dispatch_id',
            'database_id',
            'company_id',
        ],
    ]) ?>

</div>
