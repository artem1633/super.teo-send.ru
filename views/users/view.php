<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
?>
<div class="users-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'name',
            'login',
            'vk_id',
            //'password',
            'telephone',
            [
                'attribute' => 'role_id',
                'value' => function($data){
                    return $data->getRoleDescription();
                }
            ],
            [
                'attribute' => 'status',
                'value' => function($data){
                    return $data->getStatusDescription();
                }
            ],
            //'auth_key',
            'data_cr',
        ],
    ]) ?>

</div>
