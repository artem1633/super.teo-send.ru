<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TemplateMessagesContent */
?>
<div class="template-messages-content-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'content:ntext',
            'template_message_id',
            'created_at',
        ],
    ]) ?>

</div>
