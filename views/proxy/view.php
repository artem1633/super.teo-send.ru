<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Proxy */
?>
<div class="proxy-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => 'Компания',
                'value' => $model->companyModel ? $model->companyModel->company_name : null,
                'visible' => Yii::$app->user->identity->isSuperAdmin(),
            ],
            'name',
            'ip_adress',
            'port',
            'status',
            'accounts',
        ],
    ]) ?>

</div>
