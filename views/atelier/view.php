<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Atelier */
?>
<div class="atelier-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'name',
            'director',
            'inn',
            'telephone',
            'email:email',
            'kpp',
            'ogrn',
            'bank',
            'kor_schot',
            'checking_accaunt',
            'okpo',
            'bik',
            'address',
        ],
    ]) ?>

</div>
