<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var $model \app\models\RegisterForm
 * @var $slider app\models\Slider[]
 */

$this->title = 'Востановить пароль';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
];

if(count($slider) > 0 && Yii::$app->mobileDetect->isMobile() == false)
{
    \app\assets\pages\LoginPageAsset::register($this);
    \app\widgets\owlCarousel\OwlCarouselAsset::register($this);
}

$image = \app\models\Settings::findByKey('home_form_image')->value;

if($image == null)
{
    $image = '';
}

?>

    <div class="register-box" style="background: url('<?=$image?>')">
        <div class="register-logo">
            <a href="#"><b>TEO</b>-SEND</a>
        </div>

        <div class="register-box-body">
            <p class="login-box-msg">Введите данные для востановления пароля</p>
            <div class="login-content">
                <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false], $options = ['class' => 'margin-bottom-0']); ?>
                <div class="form-group has-feedback">
                    <?= $form
                        ->field($model, 'login', $fieldOptions1)
                        ->label(false)
                        ->textInput(['placeholder' => $model->getAttributeLabel('login'), 'class' => 'form-control']) ?>
                </div>
                <div class="row">
                    <div class="col-xs-12 ">
                        <?= Html::submitButton('Востановить', ['class' => 'btn btn-primary', 'style' => 'width:100%', 'name' => 'login-button']) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <p style="text-align: center; margin-top: 10px;">
                            https://vk.com/teo_target - напишите в поддержку для востановвления почты если письмо не пришло
                        </p>
                    </div>
                </div>
                <br>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

<?php if(count($slider) > 0 && Yii::$app->mobileDetect->isMobile() == false): ?>

    <div class="slider">
        <div class="login-carousel">
            <?php foreach ($slider as $item): ?>
                <div class="item">
                    <div class="item-body">
                        <img class="item-image" src="/<?=$item->image_path?>" alt="<?=$item->image_alt?>">
                        <div class="overlay"></div>
                        <div class="item-text-content">
                            <h2><?=$item->header?></h2>
                            <p><?=$item->content?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

<?php endif; ?>

<?php

$delayTime = \app\models\Settings::findByKey('slider_delay')->value;

if($delayTime == null){
    $delayTime = 10000;
}

$script = <<< JS
$('.login-carousel').owlCarousel({
    loop:true,
    nav:true,
    items: 1,
    dots: false,
    nav: false,
    autoplay:true,
    autoplayTimeout:{$delayTime},
    autoplayHoverPause:true
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>