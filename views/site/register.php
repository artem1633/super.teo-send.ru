<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var $model \app\models\RegisterForm
 * @var $slider app\models\Slider[]
 */

$this->title = 'Регистрация';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
];
$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-earphone form-control-feedback'></span>"
];
$fieldOptions3 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];
$fieldOptions4 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-asterisk form-control-feedback'></span>"
];
$fieldOptions5 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-phone form-control-feedback'></span>"
];
$fieldOptions6 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon glyphicon-ruble form-control-feedback'></span>"
];

if(count($slider) > 0 && Yii::$app->mobileDetect->isMobile() == false)
{
    \app\assets\pages\LoginPageAsset::register($this);
    \app\widgets\owlCarousel\OwlCarouselAsset::register($this);
}

$image = \app\models\Settings::findByKey('home_form_image')->value;

if($image == null)
{
    $image = '';
}

?>

<div class="register-box" style="background: url('<?=$image?>')">
    <div class="register-logo">
        <a href="#"><b>TEO</b>-SEND</a>
    </div>

    <div class="register-box-body">
        <p class="login-box-msg">Введите данные для регистрации</p>

        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false], $options = ['class' => 'margin-bottom-0']); ?>
        <div class="form-group has-feedback">
            <?= $form
                ->field($model, 'company_name', $fieldOptions1)
                ->label(false)->hiddenInput();
                //->textInput(['placeholder' => $model->getAttributeLabel('company_name'), 'class' => 'form-control'])
            ?>
        </div>
        <div class="form-group has-feedback">
            <?= $form
                ->field($model, 'fio', $fieldOptions1)
                ->label(false)
                ->textInput(['placeholder' => $model->getAttributeLabel('fio'), 'class' => 'form-control']) ?>
        </div>
        <div class="form-group has-feedback">
            <?= $form
                ->field($model, 'telephone', $fieldOptions5)
                ->label(false)
                ->textInput(['placeholder' => $model->getAttributeLabel('telephone'), 'class' => 'form-control']) ?>
        </div>
        <div class="form-group has-feedback">
            <?= $form
                ->field($model, 'login', $fieldOptions3)
                ->label(false)
                ->textInput(['placeholder' => $model->getAttributeLabel('login'), 'class' => 'form-control']) ?>
        </div>
        <div class="form-group has-feedback">
            <?= $form
                ->field($model, 'password', $fieldOptions4)
                ->label(false)
                ->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'class' => 'form-control']) ?>
        </div>
        <div class="form-group has-feedback">
            <?= $form->field($model, 'data_processing')->checkbox(['label' => 'Я согласен с обработкой персональных данных'])?>
        </div>
        <div class="row">
            <div class="col-xs-12 ">
                <?= Html::submitButton('Регистрация', ['class' => 'btn btn-primary', 'style' => 'width:100%', 'name' => 'login-button']) ?>
            </div>
            <!-- /.col -->
        </div>
        <?php ActiveForm::end(); ?>

    </div>
    <!-- /.form-box -->
</div>

<?php if(count($slider) > 0 && Yii::$app->mobileDetect->isMobile() == false): ?>

    <div class="slider">
        <div class="login-carousel">
            <?php foreach ($slider as $item): ?>
                <div class="item">
                    <div class="item-body">
                        <img class="item-image" src="/<?=$item->image_path?>" alt="<?=$item->image_alt?>">
                        <div class="overlay"></div>
                        <div class="item-text-content">
                            <h2><?=$item->header?></h2>
                            <p><?=$item->content?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

<?php endif; ?>

<?php

$delayTime = \app\models\Settings::findByKey('slider_delay')->value;

if($delayTime == null){
    $delayTime = 10000;
}

$script = <<< JS
$('.login-carousel').owlCarousel({
    loop:true,
    nav:true,
    items: 1,
    dots: false,
    nav: false,
    autoplay:true,
    autoplayTimeout:{$delayTime},
    autoplayHoverPause:true
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>
