<?php

use yii\widgets\DetailView;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use app\models\DispatchConnect;
use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\Dispatch;
use app\models\UniqueMessage;

\johnitvn\ajaxcrud\CrudAsset::register($this);

$this->title = "Рассылка «{$model->name}»";

/* @var $this yii\web\View */
/* @var $model app\models\Dispatch */
?>
    <div class="dispatch-view">

        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <?= \app\widgets\Dashboard::widget(['companyId' => $model->company_id, 'dispatchId' => $model->id]) ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-success panel-toggled" style="margin-top: 20px;">
                    <div class="panel-heading ui-draggable-handle">
                        <h1 class="panel-title"> <b data-introindex="5-3">Информация о рассылке</b>

                        </h1>

                        <ul class="panel-controls">
                            <li>
                                <?= Html::a('<i class="fa fa-pencil"></i>', ['update', 'id' => $model->id], [
                                    'title' => 'Редактировать',
                                    'role' => 'modal-remote'
                                ]); ?>
                            </li>
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <?php \yii\widgets\Pjax::begin(['id' => 'dispatch-info-pjax', 'enablePushState' => false]) ?>
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'id',
                                'name',
                                [
                                    'attribute' => 'status',
                                    'format' => 'html',
                                    'value'=> function($data) {

                                        $connectsAll = DispatchConnect::find()->where(['dispatch_id' => $data->id])->all();
                                        /** @var DispatchConnect $connects */
                                        foreach ($connectsAll as $connects) {
                                            $akk = DispatchRegist::find()->where(
                                                ['id' => $connects->dispatch_regist_id, 'status' => 'limit_exceeded' ])
                                                ->one();
                                            if ($akk) {
                                                break;
                                            }
                                        }
                                        $status = '';
                                        if($akk->status == "limit_exceeded" ) {
                                            $d = date("Y-m-d H:i:s", strtotime($akk->last_dispatch_time . " +1 days"));
                                            $now = date("Y-m-d H:i:s");
                                            $hour = round((strtotime($d) - strtotime($now)) / (60 * 60));
                                            $status = "повтор через {$hour} часов";
                                            if ($hour <= 0) {
                                                $status = "В работе";
                                            }
                                        }
                                        if($data->status == Dispatch::STATUS_DAILY_LIMIT){
                                            $content .=  '<span class=\'text-danger\'><b>Суточный лимит</b> </span>';
                                        }
                                        if($data->status == DispatchStatus::STATUS_HANDLE){
                                            $content .=  '<span class=\'text-warning\'><b>В обработке</b> </span><i class="glyphicon glyphicon-check"></i>';
                                        }
                                        if($data->status == Dispatch::STATUS_ACCOUNTS_HANDLING){
                                            $content .=  '<span class=\'text-warning\'><b>Обрабатываются аккаунты</b> </span><i class="glyphicon glyphicon-check"></i>';
                                        }
                                        if($data->status == "wait"){
                                            $content .=  \yii\helpers\Html::a('<span class=\'text-danger\'><b>Запустить?</b> </span><i class="glyphicon glyphicon-check"></i>', ['faster','id' => $data->id, 'status' =>'job'], ['data-pjax' => 1]);
                                        }
                                        if($data->status == "job"){
                                            $content .= \yii\helpers\Html::a("<span class='text-info'><b>Остановить? {$status}</b> </span><i class='glyphicon glyphicon-check'></i>", ['faster','id' => $data->id,'status' =>'wait'], ['data-pjax' => 1]);
                                        }
                                        if($data->status == "finish"){
                                            $content .= "<span class='text-success'><b>Отправлено</b></span>";
                                        }

                                        return $content;
                                    },
                                ],
                                [
                                    'attribute' => 'text1',
                                    'format' => 'html',
                                ],
                                [
                                    'attribute' => 'text2',
                                    'format' => 'html',
                                ],
                                [
                                    'attribute' => 'text3',
                                    'format' => 'html',
                                ],
                                [
                                    'attribute' => 'text4',
                                    'format' => 'html',
                                ],
                                [
                                    'attribute' => 'accounts_count',
                                    'visible' => Yii::$app->user->identity->isSuperAdmin(),
                                ],
                                [
                                    'attribute' => 'accounts_count',
                                    'label' => 'Сейчас подключено аккаунтов',
                                    'value' => function($model){
                                        return DispatchRegist::find()->where(['dispatch_id' => $model->id])->count();
                                    },
                                    'visible' => Yii::$app->user->identity->isSuperAdmin(),
                                ],
                                [
                                    'attribute' => 'accounts_count',
                                    'label' => 'Осталось подключить аккаунтов',
                                    'value' => function($model){
                                        return $model->remainAccountsCount;
                                    },
                                    'visible' => Yii::$app->user->identity->isSuperAdmin(),
                                ],
                                [
                                    'attribute' => 'blocked_accounts_count',
                                    'visible' => Yii::$app->user->identity->isSuperAdmin(),
                                ],
                                'chat',
                                [
                                    'attribute' => 'accounts',
                                    'format' => 'raw',
                                    'value' => function ($model){

                                        $accs = \app\models\DispatchRegist::findAll(['dispatch_id' => $model->id]);

                                        $html = [];

                                        foreach ($accs as $account) {
                                            $html[] .= Html::a($account->username, $account->account_url, ['target' => '_blank']);
                                        }

                                        return implode(',', $html);

                                        return count(\app\models\DispatchConnect::findAll(['dispatch_id' => $model->id]));

                                        // $arry = '';
                                        // $conns =  \app\models\DispatchConnect::findAll(['dispatch_id' => $model->id]);

                                        // foreach ($conns as $con){
                                        //     $d =  \app\models\DispatchRegist::findOne(['id' => $con->dispatch_regist_id]);
                                        //     $arry .= $d->username . ",";
                                        // }

                                        // return $arry;
                                    },
                                    // 'visible' => Yii::$app->user->identity->isSuperAdmin(),
                                ],
                                'data',
                            ],
                        ]) ?>
                        <?php \yii\widgets\Pjax::end() ?>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default" style="margin-top: 20px;">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Отчет
                        </div>
                    </div>
                    <div class="panel-body">
                        <?php
                        echo app\widgets\DispatchReport::widget([
                            'dispatchId' => $model->id,
                            'companyId' => $model->company_id,
                            'gridViewOptions' => [
                                'responsiveWrap' => false,
                            ],
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default" style="margin-top: 20px;">
                    <div class="panel-heading">
                        <div class="panel-title">Статистика сообщений</div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Текст</th>
                                <th>Отправлено</th>
                                <th>Заинтересовались</th>
                                <th>Купили</th>
                                <th>Отказались</th>
                                <th>Переписка</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $counter = 1; ?>
                            <?php foreach(UniqueMessage::find()->where(['dispatch_id' => $model->id])->all() as $message): ?>
                                <?php
                                $dispatchStatusesAll = DispatchStatus::find()->where(['unique_message_id' => $message->id])->count();
                                $dispatchStatusesConversation = DispatchStatus::find()->where(['unique_message_id' => $message->id, 'status' => 1])->count();
                                $dispatchStatusesInterested = DispatchStatus::find()->where(['unique_message_id' => $message->id, 'status' => 2])->count();
                                $dispatchStatusesBought = DispatchStatus::find()->where(['unique_message_id' => $message->id, 'status' => 3])->count();
                                $dispatchStatusesRefused = DispatchStatus::find()->where(['unique_message_id' => $message->id, 'status' => 4])->count();

                                $text = '';
                                $cut = false;

                                if(iconv_strlen($message->text) > 200){
                                    $text = iconv_substr($message->text, 0 , 200, "UTF-8") . '...';
                                    $cut = true;
                                } else {
                                    $text = $message->text;
                                }

                                ?>
                                <tr>
                                    <td><?=$counter?></td>
                                    <td>
                                        <?=Html::a($text, ['/dispatch-regist/show-all', 'message' => $message->id], ['target' => '_blank'])?>
                                        <?php if($cut): ?>
                                        <div class="panel panel-default" style="margin-top: 10px;">
                                            <div class="panel-heading">
                                                <h6 class="panel-title" style="font-size: 14px;">
                                                    <a class="collapse-toggle" href="#w<?=$message->id?>-collapse<?=$message->id?>" data-toggle="collapse" data-parent="#w1">Читать далее</a>
                                                </h6>
                                            </div>
                                            <div id="w<?=$message->id?>-collapse<?=$message->id?>" class="panel-collapse collapse"><div class="panel-body">
                                                    <?=$message->text?>
                                                </div>
                                            </div>
                                            <?php endif; ?>
                                    </td>
                                    <td><?=$dispatchStatusesAll?></td>
                                    <td><?=$dispatchStatusesInterested?></td>
                                    <td><?=$dispatchStatusesBought?></td>
                                    <td><?=$dispatchStatusesRefused?></td>
                                    <td><?=$dispatchStatusesConversation?></td>
                                </tr>
                                <?php $counter++; ?>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>


<?php Modal::end(); ?>