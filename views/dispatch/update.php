<?php

use yii\helpers\Html,
    app\models\Companies,
    app\models\Bots;

/* @var $this yii\web\View */
/* @var $model app\models\Dispatch */
$company = Companies::findOne(['admin_id' => Yii::$app->user->getId()]);
$bots = $company ? Bots::getListBotByCompany($company->id) : null;

?>
<div class="dispatch-update">

    <?= $this->render('_alternative_form', [
        'model' => $model,
        'bots' => $bots
    ]) ?>

</div>
