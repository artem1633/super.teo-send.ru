<?php

use app\models\Dispatch;
use app\models\Bots;
use yii\helpers\ArrayHelper;
use yii\helpers\Html,
    app\components\helpers\FunctionHelper,
    yii\widgets\ActiveForm,
    kartik\select2\Select2,
    kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model app\models\Dispatch */
/* @var $form yii\widgets\ActiveForm */
/* @var $templateModel app\models\template\DispatchTemplateSettings */

$help_text = Dispatch::TAG_DISPATCH_NAME . " - Наименование раcсылки <br>" .
    Dispatch::TAG_MESSAGE_SENDER . " - Отправитель сообщения <br>".
    Dispatch::TAG_MESSAGE_RECIPIENT . " - Получатель сообщения <br>".
    Dispatch::TAG_CITY_RECIPIENT . " - Город получателя сообщения <br>".
    Dispatch::TAG_BASE_NAME . " - Наименование базы";


\app\assets\HandlebarsAsset::register($this);

?>

<style>
    .info {
        text-align: center;
    }

    .info-header {
        text-transform: uppercase;
        color: #6d6d6d;
    }

    .info-body {
        font-size: 20px;
        font-weight: 600;
    }
</style>

<div class="dispatch-form row">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <div class="first-step">
        <!--        Наименование-->
        <div class="col-md-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>

        <!--        Текст-->

        <div class="col-md-12">

            <div class="panel panel-default tabs">

                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#txt-tab1" data-toggle="tab"><i class="fa fa-file-text-o"
                                                                                aria-hidden="true"></i></a></li>
                    <li><a href="#txt-tab2" data-toggle="tab"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></li>
                    <li><a href="#txt-tab3" data-toggle="tab"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></li>
                    <li><a href="#txt-tab4" data-toggle="tab"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></li>
                </ul>
                <div class="panel-body tab-content">
                    <div class="tab-pane active" id="txt-tab1">
                        <?= $form->field($model, 'text1')->textarea(['rows' => 10, 'cols' => 10]) ?>
                    </div>
                    <div class="tab-pane" id="txt-tab2">
                        <?= $form->field($model, 'text2')->textarea(['rows' => 10, 'cols' => 10]) ?>
                    </div>
                    <div class="tab-pane" id="txt-tab3">
                        <?= $form->field($model, 'text3')->textarea(['rows' => 10, 'cols' => 10]) ?>
                    </div>
                    <div class="tab-pane" id="txt-tab4">
                        <?= $form->field($model, 'text4')->textarea(['rows' => 10, 'cols' => 10]) ?>
                    </div>

                </div>
            </div>
            <p>Пример использования тегов: <i>«Привет {dispatchStatus.name}, меня зовут {dispatchRegist.username}, приглашаю вас на распродажу  городе {dispatchStatus.city}!»</i></p>
            <p>Шаг 1. Пишем тексты <?= Html::a('https://youtu.be/ktgeGvT5JBM ', 'https://youtu.be/ktgeGvT5JBM ', ['target' => '_blank']) ?></p>
            <p>
                Тэги:
                <?php foreach ($tags as $tag => $title): ?>
                    <?= $tag.' - '.$title.', ' ?>
                <?php endforeach; ?>
            </p>
        </div>

    </div>

    <div class="second-step hidden">

        <!-- Табы-->

        <div class="col-md-12">
            <div class="panel panel-default tabs">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-pencil-square-o"
                                                                            aria-hidden="true"></i></a></li>
                    <li><a href="#tab2" data-toggle="tab"><i class="fa fa-files-o" aria-hidden="true"></i></a></li>
                </ul>
                <div class="panel-body tab-content">
                    <div class="tab-pane active" id="tab1">
                        <?= $form->field($model, 'text_data')->textarea(['rows' => 10, 'placeholder' => 'Введите аккаунты получателей по одному на строку'])->label(''); ?>
                    </div>
                    <div class="tab-pane" id="tab2">
                        <?= $form->field($model, 'file')->fileInput(); ?>
                    </div>
                    <div class="tab-pane" id="tab3">

                        <div class="row">
                            <div class="col-md-12">
                                <div id="get-users" class="hidden alert"><strong id="get-users-group-info"></strong>
                                </div>

                                <div id="get-users-group" style="padding-bottom: 15px">
                                    <div class="input-group">
                                        <?= $form->field($model, 'recived_data')->textInput([
                                            'placeholder' => 'Например: https://vk.com/teo_finance'
                                        ])->label('URL группы ВКонтакте') ?>
                                        <span class="input-group-btn">
                            <button id="getUsersBtn" class="btn btn-primary" type="button"
                                    onclick="getGroupUsers(this);">Получить список участников группы</button>
                                </span>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="tab-pane" id="tab4">
                        <?= $form->field($model, 'select_data')->widget(Select2::className(), [
                            'data' => \app\models\DataRecipient::getAllInArray(),
                            'options' => [
                                'placeholder' => 'Выберите базу',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <p style="margin-top: 10px;">2 способа добавления аккаунтов <?= Html::a('https://youtu.be/I4uw04ytck0', 'https://youtu.be/I4uw04ytck0', ['target' => '_blank']) ?>. Шаг 2. Выбираем аккаунты от которых будем рассылать и вставляем базу получателей <?= Html::a('https://youtu.be/Qofe0k0EMwo', 'https://youtu.be/Qofe0k0EMwo', ['target' => '_blank']) ?></p>
            <i style="display: inline-block; margin-top: 5px;">Для сбора базы перейдите по ссылке <a
                        href="https://vk.targethunter.ru/" target="_blank">https://vk.targethunter.ru/</a>  и введите промокод teo_send_ru (вы получите 2 бесплатных дня)</i>
        </div>
    </div>

    <div class="second-half-step hidden">
        <div class="info" style="display: none; margin-top: 20px;">
            <div class="col-md-12">
                <p class="info-header">Объем базы</p>
                <p id="info-base" class="info-body text-warning"></p>
            </div>
        </div>
        <?= $form->field($model, 'speed')->input('range') ?>
        <div class="info" style="display: none;">
            <div class="col-md-4">
                <p class="info-header">Стоимость</p>
                <p id="info-price" class="info-body text-info"></p>
            </div>
            <div class="col-md-4">
                <p class="info-header">Сообщений в сутки</p>
                <p id="info-speed" class="info-body text-info"></p>
            </div>
            <div class="col-md-4">
                <p class="info-header">Кол-во дней</p>
                <p id="info-day" class="info-body text-info"></p>
            </div>
        </div>
        <div class="hidden">
            <?= $form->field($model, 'day_speed')->hiddenInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'accounts_count')->hiddenInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'price')->hiddenInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="third-step hidden">
        <div class="col-md-12">
            <?= $form->field($model, 'chat')->label()->widget(Select2::classname(), [
                'data' => ArrayHelper::map(\app\models\Telegram::find()->where(['company_id' => FunctionHelper::getCompanyId()])->all(), 'telegram_id', 'name'),
                'options' => [
                    'placeholder' => 'Введите id ...',
//                    'value' => $model->isNewRecord ? Dispatch::getLastChatsId() : $model->getChatsId(),
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'tags' => true,
                    'multiple' => true,
                ],
            ]); ?>
        </div>

        <div class="hidden">
            <div class="col-md-12">
                <?= $form->field($model, 'to_stat')->checkbox() ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'autoAnswer')->checkbox() ?>
            </div>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'use_auto_registr')->checkbox(['id' => 'template-visible-checkbox']) ?>
        </div>
        <div id="templates-wrapper" <?=$model->use_auto_registr == 1 ? '' : 'style="display: none;"'?>>
            <div class="col-md-12">
                <?= $form->field($model, 'auto_regists_template_id')->widget(Select2::class, [
                    'options' => ['placeholder' => 'Выберите шаблон'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'data' => ArrayHelper::map(\app\models\AutoRegistsTemplates::find()->where(['company_id' => FunctionHelper::getCompanyId()])->all(), 'id', 'name'),
                ]) ?>
            </div>
        </div>
        <div id="new-template-wrapper">
            <div class="col-md-12">
                <?= $form->field($templateModel, 'usePosting')->checkbox(['id' => 'template-use-posting']) ?>
            </div>
            <div class="data-posts" style="display: none;">
                <div class="col-md-12">
                    <?=$form->field($templateModel, 'posts')->widget(Select2::class, [
                        'data' => [],
                        'options' => ['placeholder' => 'Впишите посты', 'multiple' => true],
                        'pluginOptions' => [
                            'tags' => true,
                            'tokenSeparators' => [',', ' '],
                        ],
                    ])?>
                </div>
            </div>
            <div class="col-md-12">
                <?= $form->field($templateModel, 'useGroups')->checkbox(['id' => 'template-use-groups']) ?>
            </div>
            <div class="data-groups" style="display: none;">
                <div class="col-md-12">
                    <?=$form->field($templateModel, 'groups')->widget(Select2::class, [
                        'data' => [],
                        'options' => ['placeholder' => 'Впишите группы', 'multiple' => true],
                        'pluginOptions' => [
                            'tags' => true,
                            'tokenSeparators' => [',', ' '],
                        ],
                    ])?>
                </div>
            </div>

            <div class="col-md-12">
                <?= $form->field($model, 'botId', ['options' => []])->widget(Select2::className(), [
                    'data' => \yii\helpers\ArrayHelper::map(Bots::getListBotByCompany(FunctionHelper::getCompanyId()), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выбор автоответчик',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($templateModel, 'status')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="hidden">
            <div class="col-md-6">
                <?= $form->field($model, 'days_week')->label()->widget(Select2::classname(), [
                    'data' => FunctionHelper::getDaysWeekList(),
                    'options' => [
                        'placeholder' => 'Выберите',
                        'multiple' => true,
                        'value' => $model->isNewRecord ? null : explode(',', $model->days_week),
                        'class' => 'hidden'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'start_time')->widget(DateControl::classname(), [
                    'type' => DateControl::FORMAT_TIME,
                    'displayFormat' => 'php:H:i',
                    'options' => ['class' => 'hidden'],
                ]); ?>
            </div>
        </div>
        <div class="col-md-12">
            <p style="margin-top: 10px;">Шаг 3. Настройка уведомлений о новых ответах  <?= Html::a('https://youtu.be/llvVV1m3PTY', 'https://youtu.be/llvVV1m3PTY', ['target' => '_blank']) ?> Показатели рассылки <?= Html::a('https://youtu.be/xik0YVj4lc8', 'https://youtu.be/xik0YVj4lc8', ['target' => '_blank']) ?></p>
        </div>
    </div>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::Button('Далее', [
                'type' => 'button',
                'class' => 'btn btn-primary',
                'id' => 'next',
                'onclick' => 'nextStep()']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
</div>

<script id="template-alert-danger" type="text/x-handlebars-template">
    <p>
    <div class="alert alert-danger error-notification" role="alert" style="margin-bottom: 10px;">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
        <strong>Ошибка!</strong> {{body}}
    </div>
    </p>
</script>

<script>

    var counted = false;

    $(function () {
        $('.dispatch-form').on('change', '#dispatch-autoanswer', function () {
            if ($(this).is(":checked")) {
                $('.field-dispatch-botid').removeClass('hidden');
            } else {
                $('.field-dispatch-botid').addClass('hidden');
                $('#dispatch-botid').select2('val', 0);

            }
        });

    });

    function handMode() {
        $('.ja-btns').removeClass('btn-warning');
        $('.js-hand').addClass('btn-warning');
        emptyMode();
        $('.js-mode').hide();
        $('.js-input-hand').show();
    }

    function uploadMode() {
        $('.ja-btns').removeClass('btn-warning');
        $('.js-upload').addClass('btn-warning');
        emptyMode();
        $('.js-mode').hide();
        $('.js-file').show();
    }

    function selectMode() {
        $('.ja-btns').removeClass('btn-warning');
        $('.js-select').addClass('btn-warning');
        emptyMode();
        $('.js-mode').hide();
        $('.js-select-base').show();
    }

    function emptyMode() {
        $('#dispatch-text_data').val('');
        $('#dispatch-file').val(null);
        $('#dispatch-select_data').select2('val', 0);
    }

    function step() {

        var btnId = $('.nextBtn').attr('id');

        console.log(btnId);
        if (btnId == 1) {
            $('.first-step').addClass('hidden'); //Скрываем первый блок
            $('.second-step').removeClass('hidden'); //Показываем второй блок
            $('.nextBtn').attr('id', 2);
            $('.modal-title').text('Создать новую рассылку. Шаг 2 из 4');

            $('#2').bind('click', calculateFunction);

        } else if (btnId == 2) {
            if(counted == true) {

                $('.second-step').addClass('hidden'); //Скрываем второй блок
                $('.second-half-step').removeClass('hidden');//Показываем второй с половиной блок блок
                $('.modal-title').text('Создать новую рассылку. Шаг 3 из 4');

                $('.nextBtn').attr('id', 3);

                $('#3').unbind('click', calculateFunction);
            }
        } else if (btnId == 3) {
            $('.second-half-step').addClass('hidden'); //Скрываем второй блок
            $('.third-step').removeClass('hidden');//Показываем второй с половиной блок блок
            $('.modal-title').text('Создать новую рассылку. Шаг 4 из 4');

            $('.saveBtn').removeClass('hidden');
            $('.nextBtn').addClass('hidden');
        }
    }

    function getGroupUsers() {
        $('#getUsersBtn').addClass('disabled');
        $('#get-users').addClass('hidden');
        var urlGroup = $('#dispatch-recived_data').val();
        console.log(urlGroup);
        $.ajax({
            url: 'dispatch/parsegroup',
            type: 'POST',
            data: 'url=' + urlGroup,
            success: function (data) {
                var recivedData = jQuery.parseJSON(data);
                $('#get-users-group-info').text(recivedData[1]);
                if (recivedData[0]) {
                    $('#get-users').addClass('alert-success');
                } else {
                    $('#get-users').addClass('alert-danger');
                }
                $('#get-users').removeClass('hidden');
                $('#getUsersBtn').removeClass('disabled');
            },
            error: function (jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                $('#get-users').addClass('alert-danger');
                $('#get-users-group-info').text(msg);
                $('#getUsersBtn').removeClass('disabled');
            }
        })
    }


</script>

<?php
$script = <<< JS

    var animateOptions = {
      useEasing: true, 
      useGrouping: true, 
      separator: ',', 
      decimal: '.', 
    };

    var textData = null;
    var fileData = null;
    var calculatedResponse = null;
    var baseCount = 0;
    
    $('#template-visible-checkbox').change(function(){
        if(this.checked){
            $('#templates-wrapper').slideDown();
            $('#new-template-wrapper').slideUp();
        } else {
            $('#templates-wrapper').slideUp();
            $('#new-template-wrapper').slideDown();
        }
    });

    $('#dispatch-file').change(function(){
        var input = document.getElementById('dispatch-file');
        if(input.files[0] != false){
            var file = input.files[0];
            var fr = new FileReader();
            fr.onload = function(){
                var data = fr.result;
                fileData = data;
            };
            fr.readAsText(file);
        }
    });
    
    var calculateFunction = function(e) {
        e.preventDefault();
        
        textData = $('#dispatch-text_data').val();

        countTextData = (textData != '' && textData != null) ? textData.split('\\n').length : 0;   
        countFileData = (fileData != '' && fileData != null) ? fileData.split('\\n').length : 0;   
        var count = countTextData + countFileData;   
        
        baseCount = count;
        
        if(baseCount == 0){
           var source = document.getElementById('template-alert-danger').innerHTML;
           var template = Handlebars.compile(source);
                            
           var context = {body: 'База пуста'};
           var html = template(context);
           
           $('.dispatch-create').prepend(html);
        } else if (baseCount > 5000) {
           var source = document.getElementById('template-alert-danger').innerHTML;
           var template = Handlebars.compile(source);
                            
           var context = {body: 'Максимальный размер базы 5000 шт. Ваш размер'+baseCount};
           var html = template(context);
           
           $('.dispatch-create').prepend(html);
        } else {
            counted = true;
        }
        
        $('#info-base').text(baseCount);
       
        var baseUrl = window.location.origin; 
        
        if(count > 0)
        {
            $.get(baseUrl+'/api/calculation/calculate?count='+count).done(function(response){
                if(response != null){
                    calculatedResponse = response;
                    var minValue = 1;
                    var maxValue = response.length;
                    
                    $('#dispatch-speed').attr('min', minValue);
                    $('#dispatch-speed').attr('max', maxValue);
                    $('#dispatch-speed').val(minValue);
                    
                    var speed = calculatedResponse[0].speed;
                    var day = calculatedResponse[0].day;
                    var price = calculatedResponse[0].total_price;
                    var accountsCount = calculatedResponse[0].akk;
                    
                    $.get(baseUrl+'/api/user/get-company-model').done(function(response){
                        
                        if(response.general_balance >= price)
                        {
                            $('.info').show();
                        
                            $('#info-speed').text(speed);
                            $('#info-day').text(day);
                            $('#info-price').text(price);
                            
                            $('#dispatch-day_speed').val(speed);
                            $('#dispatch-accounts_count').val(accountsCount);
                            $('#dispatch-price').val(price);
                            
                            $('#2').show();
                            
                            $('.error-notification').each(function(){
                                $(this).remove();
                            });
                            
                            if(counted == true) {
                                $('.second-step').addClass('hidden'); //Скрываем второй блок
                                $('.second-half-step').removeClass('hidden');//Показываем второй с половиной блок блок
                                $('.modal-title').text('Создать новую рассылку. Шаг 3 из 4');
                
                                $('.nextBtn').attr('id', 3);
                
                                $('#3').unbind('click', calculateFunction);
                            }
                        } else {
                            var source = document.getElementById('template-alert-danger').innerHTML;
                            var template = Handlebars.compile(source);
                            
                            var context = {body: 'Недостаточно средств для обработки данного объема базы ('+baseCount+' шт.)'};
                            var html = template(context);
                            
                            $('#dispatch-day_speed').val(null);
                            $('#dispatch-accounts_count').val(null);
                            $('#dispatch-price').val(price);
                            
                            $('.dispatch-create').prepend(html);
                            
                            counted = false;
                            
                            $('.info').hide();
                        }
                    });  
                }
            });   
        }
    };
    
    $('#dispatch-speed').change(function(){
        var value = $(this).val();
        
        if(calculatedResponse != null){
            var speed = calculatedResponse[value-1].speed;
            var day = calculatedResponse[value-1].day;
            var price = calculatedResponse[value-1].total_price;
            var accountsCount = calculatedResponse[value-1].akk;

            $('.info').show();
            
            var currentTextOne = $('#info-speed').text();
            var currentTextTwo = $('#info-day').text();
            var currentTextThree = $('#info-price').text();
            
            var animateOne = new CountUp('info-speed', parseInt(currentTextOne), speed, 0, 0.5, animateOptions);
            var animateTwo = new CountUp('info-day', parseInt(currentTextTwo), day, 0, 0.5, animateOptions);
            var animateThree = new CountUp('info-price', parseInt(currentTextThree), price, 0, 0.5, animateOptions);
            
            animateOne.start();
            animateTwo.start();
            animateThree.start();
            
            $('#info-speed').text(speed);
            $('#info-day').text(day);
            $('#info-price').text(price);
            
            $('#dispatch-day_speed').val(speed);
            $('#dispatch-accounts_count').val(accountsCount);
            $('#dispatch-price').val(price);
        }
    });
    
    $('#template-use-posting').change(function(){
        if(this.checked){
            $('.data-posts').show();
        } else {
            $('.data-posts').hide();
        }
    });
    
    $('#template-use-groups').change(function(){
        if(this.checked){
            $('.data-groups').show();
        } else {
            $('.data-groups').hide();
        }
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>


