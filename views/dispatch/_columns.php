<?php

use app\models\Companies;
use app\models\CompanySettings;
use app\models\Dispatch;
use app\models\DispatchConnect;
use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\Users;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use kartik\export\ExportMenu;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],


//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'company_id',
//        'content' => function($model){
//            $d = \app\models\User::findIdentity($model->company_id);
//            return $d->name;
//        }
//    ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => Yii::$app->user->identity->isSuperAdmin() ? '{export} {back-to-handle} {kill} {change_status} {view} {delete}' : '{export} {back-to-handle} {change_status} {view}',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'buttons'=>[
            'export' => function ($url, $model, $key) {
                return Html::a('<span class="glyphicon glyphicon-share"></span>', ['status-export', 'id' => $model->id], [
                    'class' => 'btn btn-success btn-xs',
                    'role' => 'modal-remote',
                ]);
            },
            'change_status' => function ($url, $model, $key) {
                if (Yii::$app->user->identity->isSuperAdmin() && $model->status != 'finish'){
                    return Html::a('<i class="fa fa-exchange" aria-hidden="true"></i>', ['change-status', 'id'=>$model->id],['class' => 'btn btn-warning btn-xs', 'title'=>'Сменить статус на "Отправлено"']);
                }
            },
            'kill' => function ($url, $model, $key) {
                if ($model->status != 'finish'){
                    return Html::a('kill', ['kill', 'id'=>$model->id],['class' => 'btn btn-warning btn-xs', 'title'=>'Удалить не обработанные']);
                }
            },
            'back-to-handle' => function ($url, $model, $key) {
                if (Yii::$app->user->identity->isSuperAdmin() && $model->status != \app\models\DispatchStatus::STATUS_HANDLE){
                    return Html::a('О', ['back-to-handle', 'id'=>$model->id],['class' => 'btn btn-success btn-xs', 'title'=>'Сменить статус на "В обработке"']);
                }
            },
        ],
        'viewOptions'=>['class' => 'btn btn-primary btn-xs', 'data-pjax'=>'0', 'target' => '_blank', 'title'=>'View', 'data-toggle'=>'tooltip'],
        'updateOptions'=>['class' => 'btn btn-success btn-xs', 'role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['class' => 'btn btn-danger btn-xs', 'role'=>'modal-remote','title'=>'Delete',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Are you sure?',
            'data-confirm-message'=>'Are you sure want to delete this item'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        'content' => function($data) {

            $content = $data->name.'<br>';

            $connectsAll = DispatchConnect::find()->where(['dispatch_id' => $data->id])->all();
            /** @var DispatchConnect $connects */
            foreach ($connectsAll as $connects) {
                $akk = DispatchRegist::find()->where(
                    ['id' => $connects->dispatch_regist_id, 'status' => 'limit_exceeded' ])
                    ->one();
                if ($akk) {
                    break;
                }
            }
            $status = '';
            if($akk->status == "limit_exceeded" ) {
                $d = date("Y-m-d H:i:s", strtotime($akk->last_dispatch_time . " +1 days"));
                $now = date("Y-m-d H:i:s");
                $hour = round((strtotime($d) - strtotime($now)) / (60 * 60));
                $status = "повтор через {$hour} часов";
                if ($hour <= 0) {
                    $status = "В работе";
                }
            }
            if($data->status == Dispatch::STATUS_DAILY_LIMIT){
                $content .=  '<span class=\'text-danger\'><b>Суточный лимит</b> </span>';
            }
            if($data->status == DispatchStatus::STATUS_HANDLE){
                $content .=  '<span class=\'text-warning\'><b>В обработке</b> </span><i class="glyphicon glyphicon-check"></i>';
            }
            if($data->status == Dispatch::STATUS_ACCOUNTS_HANDLING){
                $content .=  '<span class=\'text-warning\'><b>Обрабатываются аккаунты</b> </span><i class="glyphicon glyphicon-check"></i>';
            }
            if($data->status == "wait"){
                $content .=  \yii\helpers\Html::a('<span class=\'text-danger\'><b>Запустить?</b> </span><i class="glyphicon glyphicon-check"></i>', ['faster','id' => $data->id, 'status' =>'job'], ['data-pjax' => 1]);
            }
            if($data->status == "job"){
                $content .= \yii\helpers\Html::a("<span class='text-info'><b>Остановить? {$status}</b> </span><i class='glyphicon glyphicon-check'></i>", ['faster','id' => $data->id,'status' =>'wait'], ['data-pjax' => 1]);
            }
            if($data->status == "finish"){
                $content .= "<span class='text-success'><b>Отправлено</b></span>";
            }

            return $content;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Результаты',
        'content' => function($data){
            $unread = DispatchStatus::find()->where(['new_message' => true,'dispatch_id' => $data->id])->count();
            $send = DispatchStatus::find()->where(['send' => true,'dispatch_id' => $data->id])->count();
            $conv = DispatchStatus::find()
                ->where('status != :status and status IS NOT NULL and dispatch_id = :dispatch_id',
                    ['status' => 4,'dispatch_id' => $data->id])->count();
            $ref = DispatchStatus::find()->where(['status' => 4,'dispatch_id' => $data->id])->count();
            $view = DispatchStatus::find()->where(['read' => 'yes','dispatch_id' => $data->id])->count();

            $dataProvider = new ActiveDataProvider([
                'query' => DispatchStatus::find()->where(['dispatch_id' => $data->id]),
            ]);


            $hrefs =
                Html::a("<span style='color:orange'>{$unread} <i class='glyphicon glyphicon-bell'></i></span>", ['dispatch-regist/show-all','unread' => $data->id]) . " / ".
                Html::a("<span>{$send} <i class='glyphicon glyphicon-envelope'></i></span>", ['dispatch-regist/show-all','send' => $data->id]) . " / ".
                Html::a("<span>{$view} <i class='glyphicon glyphicon-eye-open'></i></span>", ['dispatch-regist/show-all','view' => $data->id]) . " / ".
                Html::a("<span style='color:green'>{$conv} <i class='glyphicon glyphicon-thumbs-up'></i></span>", ['dispatch-regist/show-all','conv' => $data->id]) . " / " .
                Html::a("<span style='color:red'>{$ref} <i class='glyphicon glyphicon-thumbs-down' ></i></span>", ['dispatch-regist/show-all','ref' => $data->id]);

            return "{$hrefs}";
        }
    ],
    [
        'attribute'=>'process',
        'label'=>'Процесс',
        'content' =>  function($data){
            if($data->status == 'job' || $data->status == 'wait' || $data->status == Dispatch::STATUS_DAILY_LIMIT || $data->status == 'finish')
            {
                $all = DispatchStatus::find()->where(['dispatch_id' => $data->id])->count();
                $send = DispatchStatus::find()->where(['send' => true, 'dispatch_id' => $data->id])->count();
                $null = DispatchStatus::find()->where('account_id is null and dispatch_id = :dispatch_id',
                    [':dispatch_id' => $data->id])->count();
                if ($null == 0) {
                    $null = '';
                }
                $a = 0;
                $color = 'active';
                if ($send == 0 || $data->status == "finish") {
                    $send = $all;
                    $a = 100;
                    $color = 'success';
                } elseif ($all != $send) {
                    $a = round((($send/$all) * 100),0);
                }
                $clickAll = Html::a("{$all}", ['show','all' => $data->id]);
                $clickSend = Html::a("{$send}", ['show','send' => $data->id]);

                return
                    "<div class=\"progress progress-sm active\">
                    <div class=\"progress-bar progress-bar-{$color} progress-bar-striped\" role=\"progressbar\" aria-valuenow=\"{$a}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: {$a}%\">
                      <span class=\"sr-only\">{$a}% Complete</span>
                    </div>
                  </div>{$a}% / {$clickAll} / {$clickSend} / {$null}"
                    ;
            } else if($data->status == DispatchStatus::STATUS_HANDLE)
            {
                $clientsNotHandled = DispatchStatus::find()->where('(photo is  null or name is null) and (dispatch_id = :dispatch_id)', [':dispatch_id' => $data->id])->count();
                $clientsAll = DispatchStatus::find()->where('(dispatch_id = :dispatch_id)', [':dispatch_id' => $data->id])->count();
                $clientsHandled = $clientsAll - $clientsNotHandled;
                $clientsHandledPer = round($clientsHandled / ($clientsAll / 100),0);

                return
                    "<div class=\"progress progress-sm active\">
                    <div class=\"progress-bar progress-bar-primary progress-bar-striped\" role=\"progressbar\" aria-valuenow=\"{$clientsHandledPer}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: {$clientsHandledPer}%\">
                    </div>
                  </div> Всего $clientsAll / Обработано $clientsHandled ($clientsHandledPer%)"
                    ;
            } else if($data->status == Dispatch::STATUS_ACCOUNTS_HANDLING)
            {
                $accountsCountAll = $data->accounts_count;
                $accountsCountCurrent = DispatchRegist::find()->where(['dispatch_id' => $data->id])->count();
                $percent = round($accountsCountCurrent / ($accountsCountAll / 100), 1);

                return
                    "<div class=\"progress progress-sm active\">
                    <div class=\"progress-bar progress-bar-primary progress-bar-striped\" role=\"progressbar\" aria-valuenow=\"{$percent}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: {$percent}%\">
                    </div>
                  </div>  Всего $accountsCountAll / Готово $accountsCountCurrent ($percent%)";
            }
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Аккаунты',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
        'content' => function($data){
            $accounts = DispatchRegist::find()->where(['dispatch_id' => $data->id])->all();
            $akkBlock = 0;
            $akkAll = 0;
            /** @var DispatchConnect $connects */
            foreach ($accounts as $account) {
                $akkAll ++;
                /** @var DispatchRegist $akk */
                if ($account->status == 'account_blocked') {
                    $akkBlock ++;
                }
            }
            $akkOk = $akkAll - $akkBlock;
//            $countDR = DispatchRegist::find()
//                ->alias('dr')
//                ->joinWith('dispatchs d')
//                ->where(['d.dispatch_id' => $data->id])
//                ->andWhere('dr.status != :status',['status' => 'account_blocked'])
//                ->count();
            $limit = CompanySettings::find()->where(['company_id' => $data->company_id])->one();
            $speed = count($accounts) * $limit->messages_daily_limit;

            return
                Html::a("<span>{$akkAll} <i class='glyphicon glyphicon-user'></i></span>", ['dispatch-regist/show-akk','all' => $data->id]) . " / ".
                Html::a("<span style='color:green'>{$akkOk} <i class='glyphicon glyphicon-user'></i></span>", ['dispatch-regist/show-akk','ok' => $data->id]) . " / ".
                Html::a("<span style='color:red'>{$akkBlock} <i class='glyphicon glyphicon-user' ></i></span>", ['dispatch-regist/show-akk','block' => $data->id])
                ."<br>"."<span style='color:red'><i class='glyphicon glyphicon-fire'></i>{$speed} в сутки</span>";
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company_id',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
        'content' => function ($data){
            $comp = Companies::findOne(['id' => $data->company_id]);
            $info = Html::a($comp->company_name, ['/companies/view', 'id' => $data->company_id], ['target' => '_blank']);
            $info .= '<br/>'.$comp->general_balance.'  <i class="fa fa-rub">    </i>    ';
            return $info;
        }
    ],
];   