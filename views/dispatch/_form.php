<?php

use app\models\DataRecipient;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html,
    app\components\helpers\FunctionHelper,
    yii\widgets\ActiveForm,
    kartik\select2\Select2,
    kartik\datecontrol\DateControl;
//    kartik\switchinput\SwitchInput;

/* @var $this yii\web\View */
/* @var $model app\models\Dispatch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="dispatch-form row">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <div class="col-md-12">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-12">
        <?= $form->field($model, 'text1')->widget(CKEditor::className(), [
            'editorOptions' => [
                'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                'inline' => false, //по умолчанию false
            ],
        ]);
        ?>
<!--        --><?//= $form->field($model, 'text1')->textarea(['rows' => 10, 'cols' => 10]) ?>
    </div>
    <div class="col-md-12">
        <?= $form->field($model, 'text2')->widget(CKEditor::className(), [
            'editorOptions' => [
                'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                'inline' => false, //по умолчанию false
            ],
        ]);
        ?>
<!--        --><?//= $form->field($model, 'text2')->textarea(['rows' => 10, 'cols' => 10]) ?>
    </div>
    <div class="col-md-12">
        <?= $form->field($model, 'text3')->widget(CKEditor::className(), [
            'editorOptions' => [
                'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                'inline' => false, //по умолчанию false
            ],
        ]);
        ?>
<!--        --><?//= $form->field($model, 'text3')->textarea(['rows' => 10, 'cols' => 10]) ?>
    </div>
    <div class="col-md-12">
        <?= $form->field($model, 'text4')->widget(CKEditor::className(), [
            'editorOptions' => [
                'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                'inline' => false, //по умолчанию false
            ],
        ]);
        ?>
<!--        --><?//= $form->field($model, 'text4')->textarea(['rows' => 10, 'cols' => 10]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'days_week')->label()->widget(Select2::classname(), [
            'data' => FunctionHelper::getDaysWeekList(),
            'options' => [
                'placeholder' => 'Выберите',
                'multiple' => true,
                'value' => $model->isNewRecord ? null : explode(',', $model->days_week),
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'start_time')->widget(DateControl::classname(), [
            'type' => DateControl::FORMAT_TIME,
            'displayFormat' => 'php:H:i',
        ]); ?>
    </div>
    <div class="col-md-12">
        <?php if (!isset($_GET)): ?>
            <?= $form->field($model, 'accounts')->label()->widget(Select2::classname(), [
                'data' => $model->getDispatchList(),
                'options' => [
                    'placeholder' => 'Выберите',
                    'multiple' => true,
                    'value' => $model->isNewRecord ? null : $model->DispatchRegistList(),
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>

        <?php else: ?>
            <?= $form->field($model, 'accounts')->label()->widget(\kartik\select2\Select2::classname(), [
                'data' => $model->getDispatchList(),
                'options' => [
                    'placeholder' => 'Выберите',
                    'multiple' => true,
                    'value' => $model->isNewRecord ? null : $model->DispatchRegistList($model->id),
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>

        <?php endif; ?>
    </div>
    <div class="col-md-12">
        <?= $form->field($model, 'chat')->label()->widget(Select2::classname(), [
            'options' => [
                'placeholder' => 'Введите id ...',
                'value' => $model->getChatsId(),
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'tags' => true,
                'multiple' => true,
            ],
        ]); ?>
    </div>
    <div class="col-md-12" style="padding-bottom: 15px">
        <div class="row">
            <div class="col-md-4">
                <?= Html::button('<i class="fa fa-text-height fa-2x"> </i> Написать вручную', ['class' => 'js-hand ja-btns btn btn-default', 'onclick' => 'handMode();']) ?>
            </div>
            <div class="col-md-4 text-center">
                <?= Html::button('<i class="fa fa-upload fa-2x"> </i> Загрузить файлом', ['class' => 'js-upload ja-btns btn btn-default', 'onclick' => 'uploadMode();']) ?>
            </div>
            <div class="col-md-4 text-right">
                <?= Html::button('<i class="fa fa-list-ol fa-2x"> </i> Выбрать из доступных', ['class' => 'js-select ja-btns btn btn-default', 'onclick' => 'selectMode();']) ?>
            </div>
        </div>
    </div>

    <div class="col-md-12 js-file js-mode" style="display: none">
        <?= $form->field($model, 'file')->fileInput() ?>
    </div>

    <div class="col-md-12 js-select-base js-mode" style="display: none">
        <?= $form->field($model, 'select_data')->widget(Select2::className(), [
            'data' => DataRecipient::getAllInArray(),
            'options' => [
                'placeholder' => 'Выберите базу',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
    </div>

    <div class="col-md-12 js-input-hand js-mode" style="display: none;">
        <?= $form->field($model, 'text_data')->textarea(['rows' => 10, 'placeholder' => 'Введите аккаунты получателей по одному на строку']); ?>
    </div>


    <div class="col-md-12">
        <?= $form->field($model, 'to_stat')->checkbox() ?>
    </div>
    <div class="col-md-12">
        <?= $form->field($model, 'autoAnswer')->checkbox() ?>
    </div>

    <div class="col-md-12">
        <?= $form->field($model, 'botId', ['options' => ['class' => !$model->botId ? 'hidden' : null]])->widget(Select2::className(), [
            'data' => \yii\helpers\ArrayHelper::map($bots, 'id', 'name'),
            'options' => [
                'placeholder' => 'Выбор Бота',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
    </div>
    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $(function () {
        $('.dispatch-form').on('change', '#dispatch-autoanswer', function () {
            if ($(this).is(":checked")) {
                $('.field-dispatch-botid').removeClass('hidden');
            } else {
                $('.field-dispatch-botid').addClass('hidden');
                $('#dispatch-botid').select2('val', 0);

            }
        });
    });
    function handMode() {
        $('.ja-btns').removeClass('btn-warning');
        $('.js-hand').addClass('btn-warning');
        emptyMode();
        $('.js-mode').hide();
        $('.js-input-hand').show();
    }

    function uploadMode() {
        $('.ja-btns').removeClass('btn-warning');
        $('.js-upload').addClass('btn-warning');
        emptyMode();
        $('.js-mode').hide();
        $('.js-file').show();
    }

    function selectMode() {
        $('.ja-btns').removeClass('btn-warning');
        $('.js-select').addClass('btn-warning');
        emptyMode();
        $('.js-mode').hide();
        $('.js-select-base').show();
    }

    function emptyMode() {
        $('#dispatch-text_data').val('');
        $('#dispatch-file').val(null);
        $('#dispatch-select_data').select2('val', 0);
    }
</script>


