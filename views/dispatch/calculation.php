<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\Settings;

/** @var app\models\calculation\Calculation $model */
/** @var \yii\data\ArrayDataProvider $dataProvider */

$priceAkkSetting = Settings::findByKey('account_price')->value;
$proxyPriceSetting = Settings::findByKey('proxy_price')->value;
$maxProxySetting = Settings::findByKey('max_proxy')->value;

?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">Форма</div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <?php $form = ActiveForm::begin(['method' => 'GET']) ?>
                    <div class="row">
                        <div class="col-md-2">
                            <?= $form->field($model, 'base')->textInput(['value' => 700]) ?>
                        </div>
                        <div class="col-md-2">
                            <?= $form->field($model, 'step')->textInput(['value' => 20]) ?>
                        </div>
                        <div class="col-md-2">
                            <?= $form->field($model, 'speed_akk')->textInput(['value' => 20]) ?>
                        </div>
                        <div class="col-md-2">
                            <?= $form->field($model, 'price_akk')->textInput(['value' => $priceAkkSetting]) ?>
                        </div>
                        <div class="col-md-2">
                            <?= $form->field($model, 'proxy_day')->textInput(['value' => $proxyPriceSetting]) ?>
                        </div>
                        <div class="col-md-2">
                            <?= $form->field($model, 'proxy')->textInput(['value' => $maxProxySetting]) ?>
                        </div>
                    </div>
                    <?= Html::submitButton('Рассчитать', ['class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 10px;']) ?>
                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>

<?php if($model->calculatedData != null && $dataProvider != null): ?>
    <div class="panel panel-success" style="margin-top: 15px;">
        <div class="panel-heading">
            <div class="panel-title">Результат</div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <?= \kartik\grid\GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            [
                                'class' => 'kartik\grid\DataColumn',
                                'attribute' => 'number',
                                'label' => '№',
                                'width' => '10px',
                                'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
                            ],
                            [
                                'class' => 'kartik\grid\DataColumn',
                                'attribute' => 'speed',
                                'label' => 'Скорость в день',
                                'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
                            ],
                            [
                                'class' => 'kartik\grid\DataColumn',
                                'attribute' => 'day',
                                'label' => 'Кол-во дней',
                                'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
                            ],
                            [
                                'class' => 'kartik\grid\DataColumn',
                                'attribute' => 'all_akk',
                                'label' => 'Кол-во акк',
                                'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
                            ],
                            [
                                'class' => 'kartik\grid\DataColumn',
                                'attribute' => 'sum',
                                'label' => 'Общая сумма',
                                'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>