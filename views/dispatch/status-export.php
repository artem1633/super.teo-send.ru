<?php

use app\models\Statuses;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/** @var $model \app\models\DispatchStatusExport */


?>


<?php $form = ActiveForm::begin(['id' => 'export-form']) ?>


<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'statuses')->widget(\kartik\select2\Select2::class, [
            'data' => ArrayHelper::merge(ArrayHelper::map(Statuses::find()->where(['type' => Statuses::TYPE_CLIENT])->all(), 'id', 'name'), [
                's0' => 'Не отправлено',
                's1' => 'Отправлено'
            ]),
            'options' => [
                'multiple' => true,
            ],
        ]) ?>
    </div>
</div>

<?php ActiveForm::end() ?>
