<?php

use app\models\Dispatch;
use yii\helpers\ArrayHelper;
use yii\helpers\Html,
    app\components\helpers\FunctionHelper,
    yii\widgets\ActiveForm,
    kartik\select2\Select2,
    kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model app\models\Dispatch */
/* @var $form yii\widgets\ActiveForm */
/* @var $templateModel app\models\template\DispatchTemplateSettings */

$help_text = Dispatch::TAG_DISPATCH_NAME . " - Наименование раcсылки <br>" .
    Dispatch::TAG_MESSAGE_SENDER . " - Отправитель сообщения <br>".
    Dispatch::TAG_MESSAGE_RECIPIENT . " - Получатель сообщения <br>".
    Dispatch::TAG_CITY_RECIPIENT . " - Город получателя сообщения <br>".
    Dispatch::TAG_BASE_NAME . " - Наименование базы";


\app\assets\HandlebarsAsset::register($this);

?>

<style>
    .info {
        text-align: center;
    }

    .info-header {
        text-transform: uppercase;
        color: #6d6d6d;
    }

    .info-body {
        font-size: 20px;
        font-weight: 600;
    }
</style>

<div class="dispatch-form row">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <div class="first-step">
        <!--        Наименование-->
        <div class="col-md-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>

        <!--        Текст-->

        <div class="col-md-12">

            <div class="panel panel-default tabs">

                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#txt-tab1" data-toggle="tab"><i class="fa fa-file-text-o"
                                                                                aria-hidden="true"></i></a></li>
                    <li><a href="#txt-tab2" data-toggle="tab"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></li>
                    <li><a href="#txt-tab3" data-toggle="tab"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></li>
                    <li><a href="#txt-tab4" data-toggle="tab"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></li>
                </ul>
                <div class="panel-body tab-content">
                    <div class="tab-pane active" id="txt-tab1">
                        <?= $form->field($model, 'text1')->textarea(['rows' => 10, 'cols' => 10]) ?>
                    </div>
                    <div class="tab-pane" id="txt-tab2">
                        <?= $form->field($model, 'text2')->textarea(['rows' => 10, 'cols' => 10]) ?>
                    </div>
                    <div class="tab-pane" id="txt-tab3">
                        <?= $form->field($model, 'text3')->textarea(['rows' => 10, 'cols' => 10]) ?>
                    </div>
                    <div class="tab-pane" id="txt-tab4">
                        <?= $form->field($model, 'text4')->textarea(['rows' => 10, 'cols' => 10]) ?>
                    </div>

                </div>
            </div>
            <p>Пример использования тегов: <i>«Привет {dispatchStatus.name}, меня зовут {dispatchRegist.username}, приглашаю вас на распродажу  городе {dispatchStatus.city}!»</i></p>
            <p>Шаг 1. Пишем тексты <?= Html::a('https://youtu.be/ktgeGvT5JBM ', 'https://youtu.be/ktgeGvT5JBM ', ['target' => '_blank']) ?></p>
        </div>

    </div>

    <?php ActiveForm::end(); ?>
</div>