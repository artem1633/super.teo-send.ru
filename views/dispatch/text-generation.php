<?php

use app\components\helpers\FunctionHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\TemplateMessages;
use yii\widgets\DetailView;
use app\models\Companies;

/** @var app\models\textGeneration\TextGeneration $model */
/** @var \yii\data\ArrayDataProvider $dataProvider */

$rows = [];
$tags = ArrayHelper::map(TemplateMessages::find()->where(['or', ['company_id' => FunctionHelper::getCompanyId()], ['company_id' => 1]])->all(), 'title', 'tag');

foreach($tags as $title => $tag){
	$rows[] = [
		'label' => $title,
		'value' => $tag,
	];
}


?>

<div class="panel panel-success panel-toggled" style="margin-bottom: 10px;">
    <div class="panel-heading">
        <div class="panel-title">Теги</div>
         <ul class="panel-controls">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
         </ul>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
				<?= DetailView::widget([
					'model' => (new TemplateMessages()),
					'attributes' => $rows
				]) ?>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-title">Форма</div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <?php $form = ActiveForm::begin() ?>
                <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'companyId')->dropDownList(ArrayHelper::map(Companies::find()->all(), 'id', 'company_name')) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'base')->input('number') ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'text')->textInput() ?>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'base')->input('number') ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'text')->textInput() ?>
                        </div>
                    </div>
                <?php endif; ?>
                <?= Html::submitButton('Рассчитать', ['class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 10px;']) ?>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>

<?php if($dataProvider != null && $model->generatedText != null): ?>
    <div class="panel panel-success" style="margin-top: 15px;">
        <div class="panel-heading">
            <div class="panel-title">Результат</div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <?= \kartik\grid\GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            [
                                'class' => 'kartik\grid\DataColumn',
                                'attribute' => 'text',
                                'label' => 'Текст',
                                'width' => '10px',
                                'hAlign' => \kartik\grid\GridView::ALIGN_LEFT,
                            ],
                            [
                                'class' => 'kartik\grid\DataColumn',
                                'attribute' => 'repeats',
                                'label' => 'Повторы',
                                'width' => '10px',
                                'hAlign' => \kartik\grid\GridView::ALIGN_LEFT,
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>