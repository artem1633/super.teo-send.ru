<?php
use yii\helpers\Html,
    app\components\helpers\FunctionHelper,
    kartik\grid\GridView;

?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Мои рефералы</h3>
    </div>
    <div class="panel-body">
        <?= GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $referalsProvider,
            //'filterModel' => $referalsSearchModel,
            'pjax' => true,
            'responsiveWrap' => false,
            'toolbar' => false,
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'columns' => [

                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'company_name',
                    'content' => function ($model) {
                        return $model->referalModel ? $model->referalModel->company_name : null;
                    },
                    'visible' => Yii::$app->user->identity->isSuperAdmin(),
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'company_name',
                    'label' => 'Реферал',
                    'content' => function ($model) {
                        return $model->company_name;
                    }
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'admin.telephone',
                    'label' => 'Телефон',
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'created_date',
                    'content' => function ($model) {
                        return FunctionHelper::dateForForm($model->created_date);
                    }
                ],

                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'rate_id',
                    'content' => function ($model) {
                        return $model->rate ? $model->rate->name : null;
                    }
                ],

            ],
        ]); ?>
    </div>
    <div class="panel-footer">

    </div>
</div>

