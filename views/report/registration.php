<?php

use app\models\AccountingReport;
use app\models\Companies;
use app\models\DailyReport;
use app\models\UniqueMessage;
use skeeks\widget\highcharts\Highcharts;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
use dosamigos\chartjs\ChartJs;


/**
 * @var $report array
 * @var $model \app\models\RegistrationReportFilter
 */
?>


<div class="panel panel-success">
    <div class="panel-heading">
        <div class="panel-title">Отчет о регистрации</div>
    </div>
    <div class="panel-body">

        <?php $form = ActiveForm::begin(['method' => 'get']) ?>

        <div class="row">
            <div class="col-md-10">
                <?= $form->field($model, 'dates')->widget(\kartik\daterange\DateRangePicker::class, [
                    'options' => [
                        'autocomplete' => 'off',
                        'class' => 'form-control'
                    ],
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= \yii\helpers\Html::submitButton('Поиск', ['class' => 'btn btn-block btn-success', 'style' => 'margin-top: 21px;']) ?>
            </div>
        </div>

        <?php ActiveForm::end() ?>

        <?= Highcharts::widget([
            'options' => [
                'chart' => [
                    'height' => 225,
                    'type' => 'line'

                ],
                'title' => false,//['text' => 'Всего отправлено сообщений'],
                //'subtitle' => ['text' => 'Notice the difference between a 0 value and a null point'],
                'plotOptions' => [
                    'column' => ['depth' => 25],
                    'line' => [
                        'dataLabels' => [
                            'enabled' => true
                        ]
                    ],
                    'enableMouseTracking' => false,
                ],
                'xAxis' => [
                    'categories' => $report[0],
                    'labels' => [
                        'skew3d' => true,
                        'style' => ['fontSize' => '10px']
                    ]
                ],
                'yAxis' => [
                    'title' => ['text' => null]
                ],
                'series' => [
                    ['name' => 'Регистрация', 'data' => $report[1]],
                    ['name' => 'Пополнения', 'data' => $report[2]],
                    ['name' => 'Онлайн пользоватей', 'data' => $report[5]],
                ],
                'credits' => ['enabled' => false],
                'legend' => ['enabled' => false],
            ]
        ]);
        ?>
    </div>

    <div class="panel-body introduction-first" data-introindex="17" >

        <div class="col-md-12">
            <?= ChartJs::widget([
                'type' => 'line',
                'id' => 'lines',
                'options' => [
                    'class' => 'chartjs-render-monitor',
                    'height' => 80,
                    'width' => 300
                ],
                'data' => [
                    'labels' => $report[0],
                    'datasets' => [

                        [
                            'label' => "Кол-во оплат",
                            'backgroundColor' => "rgba(255,99,132,0.2)",
                            'borderColor' => "rgba(255,99,132,1)",
                            'pointBackgroundColor' => "rgba(255,99,132,1)",
                            'pointBorderColor' => "#fff",
                            'pointHoverBackgroundColor' => "#fff",
                            'pointHoverBorderColor' => "rgba(255,99,132,1)",
                            'data' => $report[3]
                        ],
                        [
                            'label' => "Отправленно сообщений",
                            'backgroundColor' => "rgb(84, 153, 199  )",
                            'borderColor' => "rgb(84, 153, 199  )",
                            'pointBackgroundColor' => "rgb(84, 153, 199  )",
                            'pointBorderColor' => "#fff",
                            'pointHoverBackgroundColor' => "#fff",
                            'pointHoverBorderColor' => "rgb(84, 153, 199  )",
                            'data' => $report[7]
                        ],
                    ]
                ]
            ]);
            ?>
        </div>
    </div>

    <div class="panel-body introduction-first" data-introindex="17" >

        <?= Highcharts::widget([
            'options' => [
                'chart' => [
                    'height' => 225,
                    'type' => 'line'

                ],
                'title' => false,//['text' => 'Всего отправлено сообщений'],
                //'subtitle' => ['text' => 'Notice the difference between a 0 value and a null point'],
                'plotOptions' => [
                    'column' => ['depth' => 25],
                    'line' => [
                        'dataLabels' => [
                            'enabled' => true
                        ]
                    ],
                    'enableMouseTracking' => false,
                ],
                'xAxis' => [
                    'categories' => $report[0],
                    'labels' => [
                        'skew3d' => true,
                        'style' => ['fontSize' => '10px']
                    ]
                ],
                'yAxis' => [
                    'title' => ['text' => null]
                ],
                'series' => [
                    ['name' => 'Блок акк', 'data' => $report[4]],
                    ['name' => 'Купили акк', 'data' => $report[6]],
                ],
                'credits' => ['enabled' => false],
                'legend' => ['enabled' => false],
            ]
        ]);
        ?>
    </div>

</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default" style="margin-top: 20px;">
            <div class="panel-heading">
                <div class="panel-title">Статистика по финансам</div>
            </div>
            <div class="panel-body">
                <table class="table table-bordered" style="padding:0px;">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Дата</th>
                        <th>Сумма пополнений</th>
                        <th>Купили ботов</th>
                        <th>Списали за отправку</th>
                        <th>Сумма итого</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $counter = 1;
                    ?>
                    <tr style="background: darkorange;">
                        <td><?='Итого'?></td>
                        <td></td>
                        <td><?=array_sum($report[3])."<br/> (кол-во ".array_sum($report[2]).")"?></td>
                        <td>
                            <?php
                            $sumG1 = array_sum($report[6])*40;
                            $sumCH1 = array_sum($report[6]) *7;
                            echo array_sum($report[6])."<br/> Сумма {$sumG1} / Чистыми {$sumCH1}";
                            ?>
                        </td>
                        <td>
                            <?php
                            $sumG2 = array_sum($report[7]) * 1;
                            $sumCH2 = array_sum($report[7]) * 0.5;
                            echo array_sum($report[7])."<br/> Сумма {$sumG2} / Чистыми {$sumCH2}";
                            ?>
                        </td>
                        <td>
                            <?php
                            $finishG = $sumG1 + $sumG2;
                            $finishC = $sumCH1 + $sumCH2;
                            echo " Сумма {$finishG} / Чистыми {$finishC}";
                            ?>
                        </td>
                    </tr>
                    <?php for ($i=0; $i < count($report[0]); $i++): ?>

                        <tr>
                            <td><?=$counter?></td>
                            <td><?=$report[0][$i]?></td>
                            <td><?=$report[3][$i]."<br/> (кол-во {$report[2][$i]})"?></td>
                            <td>
                                <?php
                                $sumG1 = $report[6][$i]*40;
                                $sumCH1 = $report[6][$i]*7;
                                echo $report[6][$i]."<br/> Сумма {$sumG1} / Чистыми {$sumCH1}";
                                ?>
                            </td>
                            <td>
                                <?php
                                $sumG2 = $report[7][$i]*1;
                                $sumCH2 = $report[7][$i]*0.5;
                                echo $report[7][$i]."<br/> Сумма {$sumG2} / Чистыми {$sumCH2}";
                                ?>
                            </td>
                            <td>
                                <?php
                                $finishG = $sumG1 + $sumG2;
                                $finishC = $sumCH1 + $sumCH2;
                                echo "<br/> Сумма {$finishG} / Чистыми {$finishC}";
                                ?>
                            </td>
                        </tr>
                        <?php $counter++; ?>
                    <?php endfor; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default" style="margin-top: 20px;">
            <div class="panel-heading">
                <div class="panel-title">Статистика по пользователям</div>
            </div>
            <div class="panel-body">
                <table class="table table-bordered" style="padding:0px;">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Компания</th>
                        <th>Сумма пополнений</th>
                        <th>Купили ботов</th>
                        <th>Списали за отправку</th>
                        <th>Сумма итого</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $counter = 1;
                    $sumCounOperAll = 0;
                    $sumSumOperAll = 0;
                    $sumVisitAll = 0;
                    $sumBotAll = 0;
                    $sumSendAll = 0;
                    ?>

                    <?php foreach ($report[8] as $item) : ?>
                        <?php
                        /** @var AccountingReport $item */

                        //var_dump($item->company_id);
                        $company = Companies::findOne($item->company_id);
                        $sumCounOper = 0;
                        $sumSumOper = 0;
                        $sumVisit = 0;
                        $sumBot = 0;
                        $sumSend = 0;
                        foreach ($report[0] as $date)
                        {
                            $sumCounOper += intval(AccountingReport::find()->andfilterWhere(['like','date_report',$date])
                                ->andfilterWhere(['=','operation_type','1'])
                                ->andfilterWhere(['=','company_id',$item->company_id])
                                ->count());
                            $sumSumOper += intval(AccountingReport::find()->andfilterWhere(['like','date_report',$date])
                                ->andfilterWhere(['=','operation_type','1'])
                                ->andfilterWhere(['=','company_id',$item->company_id])
                                ->sum('amount'));
                            $sumVisit += intval(DailyReport::find()->andfilterWhere(['like','date_event',$date])
                                ->andfilterWhere(['=','type',DailyReport::TYPE_VISIT])
                                ->andfilterWhere(['=','company_id',$item->company_id])
                                ->count());
                            $sumBot += intval(AccountingReport::find()->andfilterWhere(['like','date_report',$date])
                                ->andfilterWhere(['=','operation_type','6'])
                                ->andfilterWhere(['=','company_id',$item->company_id])
                                ->count());
                            $sumSend += intval(AccountingReport::find()->andfilterWhere(['like','date_report',$date])
                                ->andfilterWhere(['=','operation_type','3'])
                                ->andfilterWhere(['=','company_id',$item->company_id])
                                ->sum('amount'));

                        }
                        $sumCounOperAll += $sumCounOper;
                        $sumSumOperAll += $sumSumOper;
                        $sumVisitAll += $sumVisit;
                        $sumBotAll += $sumBot;
                        $sumSendAll += $sumSend;

                        $info = Html::a($company->company_name, ['/companies/view', 'id' => $company->id], ['target' => '_blank']);
                        $info .= " - Визитов {$sumVisit} <br/>".$company->general_balance.'  <i class="fa fa-rub">    </i>    ';

                        ?>
                        <tr>
                            <td><?=$counter?></td>
                            <td><?=$info?></td>
                            <td><?=" {$sumCounOper} <br/>  Сумма {$sumSumOper}"?></td>
                            <td>
                                <?php
                                $sumG1 = $sumBot * 40;
                                $sumCH1 = $sumBot*7;
                                echo $sumBot."<br/> Сумма {$sumG1} / Чистыми {$sumCH1}";
                                ?>
                            </td>
                            <td>
                                <?php
                                $sumG2 = $sumSend*1;
                                $sumCH2 = $sumSend*0.5;
                                echo $sumSend."<br/> Сумма {$sumG2} / Чистыми {$sumCH2}";
                                ?>
                            </td>
                            <td>
                                <?php
                                $finishG = $sumG1 + $sumG2;
                                $finishC = $sumCH1 + $sumCH2;
                                echo "<br/> Сумма {$finishG} / Чистыми {$finishC}";
                                ?>
                            </td>
                        </tr>
                        <?php $counter++; ?>
                    <?php endforeach; ?>
                    <tr style="background: darkorange;">
                        <td><?='Итого'?></td>
                        <td><?=" Компаний {$counter} <br/> Визитов {$sumVisitAll}"?></td>
                        <td><?= " Кол - во {$sumCounOperAll} <br/> Сумма {$sumSumOperAll}";?></td>
                        <td>
                            <?php
                            $sumG1 = $sumBotAll*40;
                            $sumCH1 = $sumBotAll *7;
                            echo $sumBotAll."<br/>  Сумма {$sumG1} / Чистыми {$sumCH1}";
                            ?>
                        </td>
                        <td>
                            <?php
                            $sumG2 = $sumSendAll * 1;
                            $sumCH2 = $sumSendAll * 0.5;
                            echo $sumSendAll."<br/> Сумма {$sumG2} / Чистыми {$sumCH2}";
                            ?>
                        </td>
                        <td>
                            <?php
                            $finishG = $sumG1 + $sumG2;
                            $finishC = $sumCH1 + $sumCH2;
                            echo " Сумма {$finishG} / Чистыми {$finishC}";
                            ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>