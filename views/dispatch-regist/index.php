<?php

use app\models\DispatchRegist;
use app\models\Proxy;
use app\models\Settings;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

CrudAsset::register($this);
$this->title = 'Акаунты';
$this->params['breadcrumbs'][] = $this->title;
$allProxiy = Proxy::find()->count();
$allAkk = DispatchRegist::find()->where(['status' => 'ok'])->count();
$allAkk2 = DispatchRegist::find()->where(['status' => 'limit_exceeded'])->count();
$blockedAkk = DispatchRegist::find()->where(['status' => DispatchRegist::STATUS_BLOCKED])->count();
$sleepAkk = DispatchRegist::find()->where(['status' => DispatchRegist::STATUS_SLEEPING])->count();
$limitAkk = Settings::findByKey('max_proxy')->value;

$all_suc_akk = $allProxiy * $limitAkk;
$all_dar_akk = $allAkk + $allAkk2;
$coud_los = $all_suc_akk -  $all_dar_akk;
$info = '';
if (Yii::$app->user->identity->isSuperAdmin()) {

    $info = "
                    <span class='text-info'>Всего: {$all_suc_akk}<span/> /  
                     <span class='text-danger'>Занято: {$all_dar_akk}</span> /
                     <span class='text-warning'>Заблокировано: {$blockedAkk}</span> /
                     <span class='text-success'> Доступно: {$coud_los}</span> /
                     <span style='color: #b75da2'> Спит: {$sleepAkk}</span>";
}
?>
<?php if( Yii::$app->session->hasFlash('status_info') ): ?>
    <div class="callout callout-danger" role="callout">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo Yii::$app->session->getFlash('status_info'); ?>
    </div>
<?php endif;?>

<div class="dispatch-regist-index">

    <div id="ajaxCrudDatatable">

        <?php

        $toolbarContent =                     //Html::a('Получить Токен',['get-token'], [ 'target' => '_blank', 'class'=>'btn btn-success', 'data' => ['pjax' => 0]]).
            Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['create'],
                ['role'=>'modal-remote','title'=> 'Создать новый акаунт','class'=>'btn btn-info']).
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
            '{toggleData}'.
            '{export}';
        // $toolbarContent = Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
        // ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
        // '{toggleData}'.
        // '{export}';

        ?>

        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'pjax'=>true,
            'responsiveWrap' => false,
            'columns' => require(__DIR__.'/_columns.php'),
            'toolbar'=> [
                ['content' => $toolbarContent],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
            'panel' => [
                'type' => 'default',
                'heading' => '<i class="glyphicon glyphicon-list"></i> Акаунты ' . $info ,
                'afterheader' => [
                    'content' => ' <div id="vk_contact_us"></div>',
                ],
                'after'=>BulkButtonWidget::widget([
                        'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Delete All',
                            ["bulk-delete"] ,
                            [
                                "class"=>"btn btn-danger btn-xs",
                                'role'=>'modal-remote-bulk',
                                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                'data-request-method'=>'post',
                                'data-confirm-title'=>'Ты уверен?',
                                'data-confirm-message'=>'Вы действительно хотите удалить этот элемент'
                            ]),
                    ]).
                    '<div class="clearfix"></div>',
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<script>
    ////    $(document).one("pjax:end", function(event) {
    ////        $(event.target).find("#crud-datatable").each(function() {
    ////            $.pjax.reload(this);
    ////            console.log($.pjax);
    ////        })
    ////    });
    //
    //    $(document).one("pjax:end", function(event) {
    //        console.log(event);
    //    });
</script>