<?php

use app\models\Companies;
use app\models\CompanySettings;
use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\Proxy;
use yii\bootstrap\Html;
use yii\helpers\Url;
use app\models\Dispatch;

/** @var DispatchRegist $data */

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => "Фото",
        'content' => function ($data) {

            return Html::img(
                $data->photo ? $data->photo :  "@web/images/user.png" ,
                ['alt' => 'message user image', 'class' => 'direct-chat-img']);


        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'username',
        'content' => function($data){

            $find_copy = DispatchRegist::find()->where(['username' => $data->username])->count();
            if ($find_copy > 1) {
                $text = "<span class='text-danger'> - {$find_copy}</span>";
            }
            $content = Html::a($data->username . $text, $data->account_url,
                [ 'target' => '_blank',  'data' => ['pjax' => 0]]);

//            if($data->templateProcessing){
//                $content .= \app\widgets\dispatchRegist\TemplateProgress::widget([
//                    'model' => $data
//                ]);
//            }

            $content .= '<br>';

            $url = Url::to(['make-sleep', 'id' => $data->id]);
            $content .= Html::a('<span class="glyphicon glyphicon-bed"></span>', $url, [
                'class' => 'btn btn-success btn-xs',
                'title' => 'Усыпить',
                'style' => 'background-color: #b75da2',
                'data-request-method' => 'post',
                'role' => 'modal-remote',
                'data-toggle' => 'tooltip',
                'data-confirm-title' => 'Подтвердите действие',
                'data-confirm-message' => 'Вы уверены что хотите "усыпить" аккаунт?',
            ]);

            if (!$data->auto_view) {
                $url = Url::to(['auto', 'id' => $data->id]);
                $content .= ' '.Html::a('<span class="fa fa-cloud"></span>', $url, [
                        'class' => 'btn btn-danger btn-xs',
                        //'role' => 'modal-remote', 'title' => 'Оформить',
                        //'data-confirm' => false, 'data-method' => false,// for overide yii data api
                        'data-request-method' => 'post',
                        'data-toggle' => 'tooltip',
                        'data-confirm-title' => 'Подтвердите действие',
                        'data-confirm-message' => 'Вы уверены что хотите автоматически оформить акк?',
                    ]);
            } else {
                $url = Url::to(['auto', 'id' => $data->id]);
                $content .= ' '.Html::a('<span class="fa fa-cloud"></span>', $url, [
                        'class' => 'btn btn-success btn-xs',
                        //'role' => 'modal-remote', 'title' => 'Оформить',
                        //'data-confirm' => false, 'data-method' => false,// for overide yii data api
                        'data-request-method' => 'post',
                        'data-toggle' => 'tooltip',
                        'data-confirm-title' => 'Подтвердите действие',
                        'data-confirm-message' => 'Вы уверены что хотите автоматически оформить акк?',
                    ]);
            }

            $url = Url::to(['show-status', 'id' => $data->id]);
            $content .= ' '.Html::a('S', $url, [
                    'class' => 'btn btn-info btn-xs',
                    'data-pjax' => 0,
                    "onclick" => '
                        event.preventDefault();
                        $.get("'.$url.'").done(function(){
                            $.pjax.reload("#crud-datatable-pjax");
                        });',
                ]);

            $content .= ' '.Html::a('<i class="fa fa-eye"></i>', ['view', 'id' => $data->id], [
                    'class' => 'btn btn-primary btn-xs', 'role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'
                ]);

            $content .= ' '.Html::a('<i class="fa fa-pencil"></i>', ['update', 'id' => $data->id], [
                    'class' => 'btn btn-success btn-xs', 'role'=>'modal-remote','title'=>'Update','data-toggle'=>'tooltip'
                ]);

            $content .= ' '.Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete', 'id' => $data->id], [
                    'class' => 'btn btn-danger btn-xs', 'role'=>'modal-remote','title'=>'Delete',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Ты уверен?',
                    'data-confirm-message'=>'Вы действительно хотите удалить этот элемент'
                ]);

            return $content;
        }
    ],
    [
        'attribute'=>'process',
        'label'=>'Процесс',
        'content' =>  function($data){

            /** @var DispatchRegist $data */
            $setting = CompanySettings::find()->where(['company_id' => $data->company_id])->one();
            $a = 0;
            $all = $setting->messages_daily_limit;
            $send = $data->sended_message_count;
            $color = 'active';
            if ($send == 0 or  $send >= $all) {
                $send = $all;
                $a = 100;
                $color = 'success';
            } elseif ($all != $send) {
                $a = round((($send/$all) * 100),0);
            }
            return
                "<div class='progress progress-sm active'>
                    <div class='progress-bar progress-bar-{$color} progress-bar-striped' role='progressbar' 
                    aria-valuenow='{$a}' aria-valuemin='0' aria-valuemax='100' style='width: {$a}%'>
                    <span class='sr-only'>{$a}% Complete</span>
                    </div>
                </div>{$a}% / {$send} из {$all} / "
                ;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'unread_message',
        'content' => function($data){
            $unread = DispatchStatus::find()->where(['new_message' => true,'send_account_id' => $data->id])->count();
            $send = DispatchStatus::find()->where(['send' => true,'send_account_id' => $data->id])->count();
            $conv = DispatchStatus::find()
                ->where('status != :status and status IS NOT NULL  and send_account_id = :send_account_id',
                    ['status' => 4,'send_account_id' => $data->id])->count();
            $ref = DispatchStatus::find()->where(['status' => 4,'send_account_id' => $data->id])->count();
            $view = DispatchStatus::find()->where(['read' => 'yes','send_account_id' => $data->id])->count();


            $dispatch = Dispatch::findOne($data->dispatch_id);
            if($dispatch != null){
                $dispatchName = $dispatch->name;
            } else {
                $dispatchName = '';
            }

            return
                Html::a("<span style='color:orange'>{$unread} <i class='glyphicon glyphicon-bell'></i></span>", ['show','unread' => $data->id]) . " / ".
                Html::a("<span>{$send} <i class='glyphicon glyphicon-ok'></i></span>", ['show','send' => $data->id]) . " / ".
                Html::a("<span>{$view} <i class='glyphicon glyphicon-eye-open'></i></span>", ['dispatch-regist/show','view' => $data->id]) . " / ".
                Html::a("<span style='color:green'>{$conv} <i class='glyphicon glyphicon-thumbs-up'></i></span>", ['show','conv' => $data->id]) . " / " .
                Html::a("<span style='color:red'>{$ref} <i class='glyphicon glyphicon-thumbs-down' ></i></span>", ['show','ref' => $data->id]) .'<div>'.$dispatchName.'</div>';



        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'content' => function ($model){

            if($model->status == DispatchRegist::STATUS_OK){
                return "<span class='text-success'>Доступен</span>";
            }

            if($model->status == "limit_exceeded" ) {
                $d = date("Y-m-d H:i:s", strtotime($model->last_dispatch_time . " +1 days"));
                $now = date("Y-m-d H:i:s");
                $hour = round((strtotime($d) - strtotime($now)) / (60 * 60));

                return "<span class='text-black'><b>Суточный лимит<br>Повтор через {$hour} часов<b></span>";
            }

            if($model->status == "interval_not_end"){
                $date_2 = date("Y-m-d H:i:s");

                $interval_min = \app\models\Settings::find()->where(['key' => 'interval_in_minutes'])->one();
                $diff = strtotime($date_2) - strtotime($model->last_dispatch_time) ;
                $diff = round($interval_min->value - $diff /60);

                return "<span class='text-yellow'><b> Интервал<br>Повтор через {$diff} минут</b></span>";

            }
            if($model->status == DispatchRegist::STATUS_BLOCKED){
                if($model->proxy == null) {
                    return "<span class='text-danger'><b>Акаунт не доступен<br>Заморожен</b></span>";
                } else {
                    return "<span class='text-danger'><b>Акаунт не доступен<br>{$model->coment}</b></span>";
                }
            }

            if($model->status == DispatchRegist::STATUS_SLEEPING){
                return "<span class='text-gray'>Спящий</span>";
            }

        }

    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Без блока',
        'content' => function ($data){
            $blockingDate = date('Y-m-d');
//            $blockingDate = !$data->blocking_date ? date('Y-m-d') : $data->blocking_date;
            $createDate = $data->data;
            return round((strtotime($blockingDate)-strtotime($data->data))/(60*60*24)). ' дня(ей)<br>Создан: ' . $createDate;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company_id',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
        'content' => function ($data){
            $text = '';

            $company = Companies::find()->where(['id' => $data->company_id])->one();

            if($company != null)
            {
                $text .= "{$company->company_name}<br>";
            }

            if($data->proxy != null)
            {
                $proxy = Proxy::findOne(['id' => $data->proxy]);
                if($proxy != null)
                {
                    $text .= "{$proxy->ip_adress}:{$proxy->port}";
                }
            } else {
                $proxy = Proxy::findOne(['id' => $data->proxy_blocked]);
                if($proxy != null)
                {
                    $text .= "<span class='text-danger'>{$proxy->ip_adress}:{$proxy->port}</span>";
                }
            }

            return $text;
        }
    ],
//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
//        'contentOptions' => ['style' => 'white-space: nowrap;'],
//        'template' => '{makeSleep} {leadAuto} {S} {view} {update} {delete}',
//        'buttons' => [
//            'makeSleep' => function($url, $model){
//                $url = Url::to(['make-sleep', 'id' => $model->id]);
//                return Html::a('<span class="glyphicon glyphicon-bed"></span>', $url, [
//                    'class' => 'btn btn-success btn-xs',
//                    'title' => 'Усыпить',
//                    'style' => 'background-color: #b75da2',
//                    'data-request-method' => 'post',
//                    'role' => 'modal-remote',
//                    'data-toggle' => 'tooltip',
//                    'data-confirm-title' => 'Подтвердите действие',
//                    'data-confirm-message' => 'Вы уверены что хотите "усыпить" аккаунт?',
//                ]);
//            },
//            'leadAuto' => function ($url, $model) {
//                if (!$model->auto_view) {
//                    $url = Url::to(['auto', 'id' => $model->id]);
//                    return Html::a('<span class="fa fa-cloud"></span>', $url, [
//                        'class' => 'btn btn-danger btn-xs',
//                        //'role' => 'modal-remote', 'title' => 'Оформить',
//                        //'data-confirm' => false, 'data-method' => false,// for overide yii data api
//                        'data-request-method' => 'post',
//                        'data-toggle' => 'tooltip',
//                        'data-confirm-title' => 'Подтвердите действие',
//                        'data-confirm-message' => 'Вы уверены что хотите автоматически оформить акк?',
//                    ]);
//                } else {
//                    $url = Url::to(['auto', 'id' => $model->id]);
//                    return Html::a('<span class="fa fa-cloud"></span>', $url, [
//                        'class' => 'btn btn-success btn-xs',
//                        //'role' => 'modal-remote', 'title' => 'Оформить',
//                        //'data-confirm' => false, 'data-method' => false,// for overide yii data api
//                        'data-request-method' => 'post',
//                        'data-toggle' => 'tooltip',
//                        'data-confirm-title' => 'Подтвердите действие',
//                        'data-confirm-message' => 'Вы уверены что хотите автоматически оформить акк?',
//                    ]);
//                }
//            },
//            'S' => function ($url, $model) {
//                $url = Url::to(['show-status', 'id' => $model->id]);
//                return Html::a('S', $url, [
//                    'class' => 'btn btn-info btn-xs',
//                    'data-pjax' => 0,
//                    "onclick" => '
//                        event.preventDefault();
//                        $.get("'.$url.'").done(function(){
//                            $.pjax.reload("#crud-datatable-pjax");
//                        });',
//                ]);
//            },
//        ],
//        'vAlign'=>'middle',
//        'urlCreator' => function($action, $model, $key, $index) {
//            return Url::to([$action,'id'=>$key]);
//        },
//        'viewOptions'=>['class' => 'btn btn-primary btn-xs', 'role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
//        'updateOptions'=>['class' => 'btn btn-success btn-xs', 'role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
//        'deleteOptions'=>['class' => 'btn btn-danger btn-xs', 'role'=>'modal-remote','title'=>'Delete',
//            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//            'data-request-method'=>'post',
//            'data-toggle'=>'tooltip',
//            'data-confirm-title'=>'Ты уверен?',
//            'data-confirm-message'=>'Вы действительно хотите удалить этот элемент'],
//    ],

];   