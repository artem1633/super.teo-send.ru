<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Shop */

$this->title = $model->name;

?>
<div class="panel ">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-3" style="border: 1px solid #c2bec4; border-radius: 10px">
                <?= Html::img([$model->image], ['style' => 'width: 100%']) ?>
            </div>
            <div class="col-xs-9">
                <h2><?= $model->name ?></h2>
                <h3 class="text-success"><?= number_format($model->price, 2, ',', ' ') ?> <i class="fa fa-rub"></i></h3>
                <h4 style="padding: 30px 0px"> <?= $model->description_short ?></h4>
                <div class="row">
                    <div class="col-md-2">
                        <?= Html::input('number', 'count', 1, ['class' => 'form-control', 'min' => 1, 'step' => 1]); ?>
                    </div>
                    <div class="col-md-2">
                        <?= Html::button('Купить сейчас', ['class' => 'btn btn-success']); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding: 30px 0px;">
            <?php
            echo \yii\bootstrap\Tabs::widget([
                'items' => [
                    [
                        'label' => 'Описание',
                        'content' => '<div style="padding: 20px 10px">' . $model->description_long . '</div>',
                        'active' => true,
                    ],
                    [
                        'label' => 'Дополнительная информация',
                        'content' => '<div style="padding: 20px 10px">' . $model->additional_info . '</div>',
                    ],

                ]
            ]);

            ?>

        </div>
    </div>
</div>
