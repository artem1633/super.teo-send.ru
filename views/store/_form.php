<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\Shop;
use kartik\file\FileInput;
use app\models\DispatchRegist;
use app\models\Proxy;
use app\models\DataRecipient;
use app\models\Bots;
use yii\helpers\ArrayHelper;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Shop */
/* @var $form yii\widgets\ActiveForm */


$classMD3 = ['options' => ['class' => 'col-md-3']];
$classMD4 = ['options' => ['class' => 'col-md-4']];
$classMD4Down = ['options' => ['class' => 'col-md-4', 'style' => 'margin-top: 15px']];
$classMD6 = ['options' => ['class' => 'col-md-6']];
$classMD12 = ['options' => ['class' => 'col-md-12']];

$listAccounts = $model->isNewRecord
    ? DispatchRegist::find()->where(['busy' => 1])->all()
    : DispatchRegist::find()->where([
        'OR',
        ['busy' => 1],
        [
            'AND',
            [
                'OR',
                ['busy' => null],
                ['busy' => 0],
            ],
            ['shop_id' => $model->id]
        ]
    ])->all();

$listProxy = $model->isNewRecord
    ? Proxy::find()->where(['busy' => 1])->all()
    : Proxy::find()->where([
        'OR',
        ['busy' => 1],
        [
            'AND',
            [
                    'OR',
                    ['busy' => null],
                    ['busy' => 0]
            ],
            ['shop_id' => $model->id]
        ]
    ])->all();

?>

<div class="shop-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= !$model->isNewRecord ? $form->field($model, 'type')->hiddenInput()->label(false) : null ?>
    <div class="row">
        <?= $form->field($model, 'name', $classMD6)->textInput() ?>
        <?= $form->field($model, 'price', $classMD4)->textInput(['type' => 'number', 'min' => '0.01', 'step' => '0.01']) ?>
        <div class="col-md-2">
            <label class="control-label" for="shop-name">&nbsp;</label>
            <?= $form->field($model, 'access')->checkbox()->label(false) ?>
        </div>
    </div>
    <div class="row js-accounts">
        <?= $model->isNewRecord ? $form->field($model, 'type', $classMD3)->label()->widget(Select2::classname(), [
            'data' => [
                Shop::TYPE_ACCOUNT => 'Аккаунт',
                Shop::TYPE_PROXY => 'Прокси',
                Shop::TYPE_DATABASE => 'База данных',
                Shop::TYPE_BOT => 'Бот',
                Shop::TYPE_ACCOUNT_DATA => 'Аккаунты',
            ],
            'options' => [
                'placeholder' => 'Выберите тип',
                'disabled'
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
            'pluginEvents' => [
                'change' => "function() {
                if($(this).val() == " . Shop::TYPE_ACCOUNT . ") {
                    $('.js-items').hide();
                    $('.js-items-accounts').show();
                    setEmpty();
                }
                if($(this).val() == " . Shop::TYPE_PROXY . ") {
                    $('.js-items').hide();
                    $('.js-items-proxy').show();
                    setEmpty();
                }
                if($(this).val() == " . Shop::TYPE_DATABASE . ") {
                    $('.js-items').hide();
                    $('.js-items-data').show();
                    setEmpty();
                }
                if($(this).val() == " . Shop::TYPE_BOT . ") {
                    $('.js-items').hide();
                    $('.js-items-bots').show();
                    setEmpty();
                }
            }"
            ]
        ])
            : '<div class="col-md-3"><label class="control-label" for="shop-type">Тип товара</label><span class="form-control">' . $model->getNameType() . '</span></div>' ?>

        <div class="col-md-9 js-items js-items-accounts"
             style="<?= (!$model->isNewRecord && $model->type == Shop::TYPE_ACCOUNT) ? null : 'display: none;' ?>">
            <?= $form->field($model, 'itemsAccounts')->label()->widget(Select2::classname(), [
                'data' => ArrayHelper::map($listAccounts, 'id', 'username'),
                'options' => [
                    'placeholder' => 'Пусто',
                    'multiple' => true,
                    'value' => $model->isNewRecord ? null : $model->getSelectAccounts(),
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]); ?>
        </div>
        <div class="col-md-9 js-items js-items-proxy"
             style="<?= (!$model->isNewRecord && $model->type == Shop::TYPE_PROXY) ? null : 'display: none;' ?>">
            <?= $form->field($model, 'itemsProxy')->label()->widget(Select2::classname(), [
                'data' => ArrayHelper::map($listProxy, 'id', 'name'),
                'options' => [
                    'placeholder' => 'Пусто',
                    'multiple' => true,
                    'value' => $model->isNewRecord ? null : $model->getSelectProxy(),
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]); ?>
        </div>
        <div class="col-md-9 js-items js-items-data"
             style="<?= (!$model->isNewRecord && $model->type == Shop::TYPE_DATABASE) ? null : 'display: none;' ?>">
            <?= $form->field($model, 'itemsData')->label()->widget(Select2::classname(), [
                'data' => ArrayHelper::map(DataRecipient::find()->all(), 'id', 'name'),
                'options' => [
                    'placeholder' => 'Пусто',
                    'multiple' => true,
                    'value' => $model->isNewRecord ? null : $model->getSelectData(),
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]); ?>
        </div>
        <div class="col-md-9 js-items js-items-bots"
             style="<?= (!$model->isNewRecord && $model->type == Shop::TYPE_BOT) ? null : 'display: none;' ?>">
            <?= $form->field($model, 'itemsBots')->label()->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Bots::find()->all(), 'id', 'name'),
                'options' => [
                    'placeholder' => 'Пусто',
                    'multiple' => true,
                    'value' => $model->isNewRecord ? null : $model->getSelectBot(),
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <?= $form->field($model, 'description_short', $classMD12)->textInput() ?>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'accounts_data')->textarea(['rows' => 6]); ?>
        </div>
    </div>

    <?= $form->field($model, 'description_long', $classMD12)->widget(CKEditor::className(), [
        'editorOptions' => [
            'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
            'inline' => false, //по умолчанию false
        ],
    ]); ?>

    <?= $form->field($model, 'additional_info', $classMD12)->widget(CKEditor::className(), [
        'editorOptions' => [
            'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
            'inline' => false, //по умолчанию false
        ],
    ]); ?>

    <?php ActiveForm::end(); ?>

</div>
<script>
    function setEmpty() {
        $('#shop-itemsAccounts').select2('val', 0);
        $('#shop-itemsProxy').select2('val', 0);
        $('#shop-itemsData').select2('val', 0);
        $('#shop-itemsBots').select2('val', 0);
    }
</script>