<?php
use yii\helpers\Url;
use yii\helpers\Html;









return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'bossName',
        'label' => 'ФИО',
        'content' => function($data){
            $name = $data->admin->name;
            if ($data->isConnectedTelegram) {
                $name .= "  <span style='color:green'>  <i class='fa fa-envelope-o'></i></span>";
            }
            return "{$name}";
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'user_login',
        'label' => 'Email',
        'value'=>'admin.login',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'user_telephone',
        'label' => 'Телефон',
        'value'=>'admin.telephone',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'access_end_datetime',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'rate.name',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'general_balance',
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'last_activity_datetime',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{view} {delete}',
        'contentOptions' => ['style' => 'white-space: nowrap;'],
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                    'class' => 'btn btn-danger btn-xs',
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'view' => function ($url, $model) {
                return Html::a('<i class="fa fa-eye" style="font-size: 16px;"></i>', $url, [
                    'target' => '_blank',
                    'data-pjax' => 0,
                    'class' => 'btn btn-success btn-xs',
                    'title'=>'Изменить',
                ])."&nbsp;";
            }
        ],
    ],

];