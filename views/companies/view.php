<?php

use app\components\helpers\FunctionHelper;
use app\models\AccountingReport;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\DetailView;

\johnitvn\ajaxcrud\CrudAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Companies */
?>
<div class="companies-view">

    <div class="row">
        <div class="col-md-9">
            <div class="row">
                <?= \app\widgets\Dashboard::widget(['companyId' => $model->id]) ?>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-success panel-toggled" style="margin-top: 20px;">
                <div class="panel-heading ui-draggable-handle">
                    <h1 class="panel-title"> <b data-introindex="5-3">Информация о компании</b>

                    </h1>

                    <ul class="panel-controls">
                        <li>
                            <?= Html::a('<i class="fa fa-pencil"></i>', ['update', 'id' => $model->id], [
                                'title' => 'Редактировать',
                                'role' => 'modal-remote'
                            ]); ?>
                        </li>
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <?php \yii\widgets\Pjax::begin(['id' => 'company-info-pjax', 'enablePushState' => false]) ?>
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'id',
                                'admin.name',
                                'admin.login',
                                'admin.telephone',
                                'admin.data_cr',
                                'admin.last_activity_datetime',
                                'general_balance',
                                'company_name',
                                'created_date',
                                'is_super',
                                'account_count',
                                'dispatch_count',
                                'referal',
                                'affiliate_amount',
                            ],
                        ]) ?>
                    <?php \yii\widgets\Pjax::end() ?>
                </div>
            </div>

            <div class="panel panel-success panel-toggled" style="margin-top: 15px;">
                <div class="panel-heading ui-draggable-handle">
                    <h1 class="panel-title"> <b data-introindex="5-3">Пополнения</b>

                    </h1>

                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <?= GridView::widget([
                        'id' => 'crud-datatable-1',
                        'dataProvider' => $dataProviderDebit,
                        //'filterModel' => $searchModel,
                        'pjax' => true,
                        'responsiveWrap' => false,
                        'columns' => [
                            [
                                'class' => '\kartik\grid\DataColumn',
                                'attribute' => 'amount',
                                'content' => function ($model) {
                                    return number_format($model->amount, 2, ',', ' ') . ' <i class="fa fa-rub"></i>';
                                }
                            ],
                            [
                                'class' => '\kartik\grid\DataColumn',
                                'attribute' => 'date_report',
                                'content' => function ($model) {
                                    return FunctionHelper::dateForForm($model->date_report, 1);
                                }
                            ],
                        ],

                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
                        'panel' => null,
                    ]); ?>
                </div>
            </div>

            <div class="panel panel-success panel-toggled" style="margin-top: 15px;">
                <div class="panel-heading ui-draggable-handle">
                    <h1 class="panel-title"> <b data-introindex="5-3">Списания</b>

                    </h1>

                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <?= GridView::widget([
                        'id' => 'crud-datatable-1',
                        'dataProvider' => $dataProviderCredit,
                        //'filterModel' => $searchModel,
                        'pjax' => true,
                        'responsiveWrap' => false,
                        'columns' => [
                            [
                                'class' => '\kartik\grid\DataColumn',
                                'attribute' => 'amount',
                                'content' => function ($model) {
                                    return number_format($model->amount, 2, ',', ' ') . ' <i class="fa fa-rub"></i>';
                                }
                            ],
                            [
                                'class' => '\kartik\grid\DataColumn',
                                'attribute' => 'date_report',
                                'content' => function ($model) {
                                    return FunctionHelper::dateForForm($model->date_report, 1);
                                }
                            ],
                        ],

                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
                        'panel' => null,
                    ]); ?>
                </div>
            </div>

        </div>
    </div>

</div>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>


<?php Modal::end(); ?>