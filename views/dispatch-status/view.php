<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DispatchStatus */
?>
<div class="dispatch-status-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'account_id',
            'status',
            'data',
            [
                'attribute' => 'send_account_id',
                'value' => function ($model){
                    $arry = '';
                    $ids = $model->send_account_id;
                    $ids = explode(',',$ids);
                    foreach ($ids as $id){
                        $d =  \app\models\DispatchRegist::findOne(['id' => $id]);
                        $arry .= $d->username . ",";
                    }
                    return $arry;
                },

            ],
            'dispatch_id',
        ],
    ]) ?>

</div>
