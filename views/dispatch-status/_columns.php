<?php

use app\models\Dispatch;
use app\models\DispatchRegist;
use yii\helpers\Url,
    yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => "Диалог",
        'content' => function ($data) {
            $account = DispatchRegist::findOne(['id' => $data->send_account_id]);

            return Html::a(Html::img(
                $data->photo ? $data->photo :  "@web/images/user.png" ,
                ['alt' => 'message user image', 'class' => 'direct-chat-img']), ['/dispatch-status/dialog', 'id' => $data->id],
                ['target' => '_blank',  'data-pjax'=>0]);


        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'name',
        'content' => function ($data) {
            Yii::warning($data, __METHOD__);
            $age = $data->age ? $data->age : '';
            $city = $data->city ? ' - '. $data->city : "";
            $gender = $data->gender ? ' ('. $data->gender.')' : "";
            return Html::a($data->name ? $data->name : "https://vk.com/id{$data->account_id}" ."{$gender}",
                    "https://vk.com/id{$data->account_id}", ['target' => '_blank', 'data' => ['pjax' => 0]]).
                "{$city} <br/> {$age}";

        },
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'status',
        'content' => function ($data) {
//            $account = DispatchRegist::find()->where(['id' => $data->send_account_id])->all();
//            if (!$account) {
//                return 'Аккаунт удален';
//            }
            if (!$data->statusModel){
                return null;
            };
            return '<span class="text-' . $data->statusModel->color . '"><b>' . $data->statusModel->name . '</b></span>';

        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'send',
        'content' => function ($data) {
            return $data->send
                ? "<span class='text-success'><b>Отправлено</b> <br/>{$data->data}</span>"
                : '<span class="text-danger"><b>Не отправлено<b/></span>';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'read',
        'content' => function ($data) {
            if ($data->read == 'yes') {
                return "<span class='text-success'><b>Прочитано</b></span>";
            }

            if ($data->read == 'no') {
                return "<span class='text-danger'><b>Не прочитано<b/></span>";
            }

        }
    ],
    // [
    //     'class' => '\kartik\grid\DataColumn',
    //     'attribute' => 'send_account_id',
    //     'content' => function ($data) {
    //         $dr = DispatchRegist::findOne(['id' => $data->send_account_id]);
    //         $d = Dispatch::findOne(['id' => $data->dispatch_id]);
    //         if($dr->status == "ok"){
    //             $status = "<span class='text-success'>Доступен</span>";
    //         }

    //         if($dr->status == "limit_exceeded" ) {
    //             $status = "<span class='text-black'><b>Суточный лимит<b></span>";
    //         }

    //         if($dr->status == "interval_not_end"){
    //             return "<span class='text-yellow'><b> Интервал </b></span>";

    //         }
    //         if($dr->status == "account_blocked"){
    //             $status = "<span class='text-danger'><b>Акаунт не доступен</b></span>";
    //         }

    //         $account = DispatchRegist::find()->where(['id' => $data->send_account_id])->all();
    //         if (!$account) {
    //             $status = "<span class='text-danger'><b>Аккаунт удален</b></span>";
    //         }

    //         return $d->name ."<br/> От: ".

    //             Html::a($dr->username, $dr->account_url,
    //                 [ 'target' => '_blank',  'data' => ['pjax' => 0]]).
    //                 ' ('.$status.')';

    //     }
    // ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'template' => '{delete}',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to(['/dispatch-status/delete', 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Delete',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item'],
    ],

];   