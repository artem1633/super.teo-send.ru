<?php

use yii\helpers\Html;


$a = \app\models\DispatchRegist::findOne(['id' => $app_id]);

?>

<div class="messages messages-img"><?php foreach ($results['response']['items'] as $result):?>
        <!-- Message. Default to the left -->
        <?php if($result['from_id'] == $clients->account_id):?>
            <div class="item in item-visible">
                <div class="image">
                    <?= Html::img($clients->photo ? $clients->photo :  "@web/images/user.png", ['alt' => 'message user image', 'class' => 'direct-chat-img'])?>
                </div>
                <div class="text">
                    <div class="heading">
                        <a href="#"><?=Html::a($clients->name ? $clients->name : "https://vk.com/id{$result['from_id']}","https://vk.com/id{$result['from_id']}", [ 'target' => '_blank',  'data' => ['pjax' => 0]]);?></a>
                    </div>
                    <?= $result['body']?>
                </div>
            </div>

        <?php else:?>
            <div class="item item-visible">
                <div class="image">
                    <?= Html::img($a->photo ? $a->photo :  "@web/images/user.png", ['alt' => 'message user image', 'class' => 'direct-chat-img'])?>
                </div>
                <div class="text">
                    <div class="heading">
                        <a href="#"><?php echo $a->username;?></a>
                        <span class="date"><?= $date = date("Y-m-d H:i:s", $result['date']);?> </span>
                    </div>
                    <?= $result['body']?>
                </div>
            </div>
        <?php endif;?>

    <?php endforeach;?>

</div><!--/.direct-chat-messages-->