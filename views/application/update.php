<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Application */
?>
<div class="application-update">

    <?= $this->render('update_form', [
        'model' => $model,
    ]) ?>

</div>
