<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\GlobalMessages */
?>
<div class="global-messages-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'text',
            'views_count',
            'created_at',
        ],
    ]) ?>

</div>
