<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


CrudAsset::register($this);

?>
<div class="proxy-index">
    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
            'id' => 'crud-datatable-database',
            'dataProvider' => $dataProviderDataBases,
            'pjax' => true,
            'responsiveWrap' => false,
            'columns' => [
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'name',
                    'content' => function ($model) {
                        return $model->name;
                    }
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'count',
                    'content' => function ($model) {
                        return $model->count;
                    }
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'type',
                    'content' => function ($model) {
                        return $model->type = \app\models\DataRecipient::TYPE_PAYD ? 'Куплена' : 'Загружена';
                    }
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'date_added',
                    'content' => function ($model) {
                        return \app\components\helpers\FunctionHelper::dateForForm($model->date_added);
                    }
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'company_id',
                    'label' => 'Владелец',
                    'content' => function ($model) {
                        return $model->companyModel ? $model->companyModel->company_name : '-';
                    },
                    'visible' => Yii::$app->user->identity->isSuperAdmin(),
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'headerOptions' => ['width' => '50'],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        return Url::to([$action, 'id' => $key]);
                    },
                    'contentOptions' => ['style' => 'white-space: nowrap;'],

                    'template' => '{leadDelete}',
                    'buttons' => [
                        'leadDelete' => function ($url, $model) {
                            $url = Url::to(['/settings/delete-database', 'id' => $model->id]);
                            return Html::a('<span class="fa fa-trash-o"></span>', $url, [
                                'class' => 'btn btn-danger btn-xs',
                                'role' => 'modal-remote', 'title' => 'Удалить',
                                'data-confirm' => false, 'data-method' => false,// for overide yii data api
                                'data-request-method' => 'post',
                                'data-toggle' => 'tooltip',
                                'data-confirm-title' => 'Подтвердите действие',
                                'data-confirm-message' => 'Вы уверены что хотите удалить этого элемента?',
                            ]);
                        },
                    ]
                ]
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'toolbar'=> [
                ['content'=>
                    Html::a('Создать базу по результатам рассылки <i class="fa fa-level-up" aria-hidden="true"></i>', ['dispatch/result'],
                        ['role' => 'modal-remote', 'title' => 'Создать базу по результатам рассылки', 'class' => 'btn btn-default']) .
                    Html::a('<i class="glyphicon glyphicon-plus"></i>', ['/settings/create-database'],
                        ['role'=>'modal-remote','title'=> 'Create new Proxies','class'=>'btn btn-info']).
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                        ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'.
                    '{export}'
                ],
            ],
            'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
            'panel' => [
                'type' => 'default',
                'heading' => '<i class="glyphicon glyphicon-list"></i> Базы данных',
                'after' => false,
            ]
        ]) ?>
    </div>
</div>
