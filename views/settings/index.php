<?php
use Yii,
    yii\helpers\Html,
    yii\helpers\Url,
    yii\bootstrap\Tabs;
use yii\bootstrap\Modal;


$this->title = 'Партнерская программа';

$this->params['breadcrumbs'][] = ['label' => $this->title];


?>
<?php
echo \yii\bootstrap\Tabs::widget([
    'items' => [
        [
            'label' => 'Настройки',
            'content' => $this->render('_general', [
                'settings' => $settings,
                'company' => $company,
                'company_settings' => $company_settings,

            ]),
            'active' => true,
            'visible' => Yii::$app->user->identity->isSuperAdmin(),
        ],
        [
            'label' => !Yii::$app->user->identity->isSuperAdmin() ? 'Профиль' : 'Профили пользователей',
            'content' => $this->render('_profile', [
                'searchModelUser' => $searchModelUser,
                'dataProviderUser' => $dataProviderUser,
                'model' => \app\models\Users::findOne(Yii::$app->user->getId()),
            ]),
            'active' => !Yii::$app->user->identity->isSuperAdmin(),
        ],
        [
            'label' => 'Настройки',
            'content' => $this->render('_company', [
                'settings' => $settings,
                'company' => $company,
                'company_settings' => $company_settings,
            ]),
        ],
        [
            'label' => 'Запрещенные слова',
            'content' => $this->render('@app/views/words-black-list/index', [
                'searchModel' => $searchModelWordsBlackList,
                'dataProvider' => $dataProviderWordsBlackList
            ]),
            'visible' => Yii::$app->user->identity->isSuperAdmin(),
        ],
        [
            'label' => 'Слайдер',
            'content' => $this->render('@app/views/slider/index', [
                'searchModel' => $searchModelSlider,
                'dataProvider' => $dataProviderSlider
            ]),
            'visible' => Yii::$app->user->identity->isSuperAdmin(),
        ],
        [
            'label' => 'Шаблоны ответов',
            'content' => $this->render('_templates', [
                'dataProviderTemplates' => $dataProviderTemplates,
                'searchModelTemplates' => $searchModelTemplates,
            ]),

        ],
//        [
//            'label' => 'Прокси',
//            'content' => $this->render('_proxy', [
//                'dataProviderProxy' => $dataProviderProxy,
//                'searchModelProxy' => $searchModelProxy,
//            ]),
//            'visible' => !Yii::$app->user->identity->isSuperAdmin(),
//        ],
        // [
        //     'label' => 'Базы данных',
        //     'content' => $this->render('_databases', [
        //         'dataProviderDataBases' => $dataProviderDataBases,
        //     ]),
        // ],
        [
            'label' => 'Шаблоны заполнение аккаунта',
            'content' => $this->render('_auto_regists_templates', [
                'searchModel' => $searchModelAutoReg,
                'dataProvider' => $dataProviderAutoReg,
            ]),
        ],
        [
            'label' => 'Шаблоны сообщений',
            'content' => $this->render('@app/views/template-messages/index', [
                'searchModel' => $searchTemplateMessages,
                'dataProvider' => $dataProviderTemplateMessages,
            ]),
        ],

    ]
]);

?>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    'options' => [
        'tabindex' => false,
    ],
])?>
<?php Modal::end(); ?>

