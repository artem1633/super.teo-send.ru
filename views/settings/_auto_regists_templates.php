<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AutoRegistsTemplatesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

CrudAsset::register($this);

?>
<div class="row">
    <div class="col-md-12">

    </div>
</div>
<div class="auto-regists-templates-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable-pjax-4',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => [
                [
                    'class' => 'kartik\grid\CheckboxColumn',
                    'width' => '20px',
                ],
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'width' => '30px',
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'name',
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'company.company_name',
                    'width' => '200px',
                    'label' => 'Компания',
                ],
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'dropdown' => false,
                    'vAlign'=>'middle',
                    'template' => '{view} {update} {delete}',
                    'urlCreator' => function($action, $model, $key, $index) {
                        return Url::to(['auto-regists-templates/'.$action,'id'=>$key]);
                    },
                    'buttons' => [
                        'view' => function ($url, $model) {
                            $url = Url::to(['auto-regists-templates/view', 'id' => $model->id]);
                            return Html::a('<span class="fa fa-eye"></span>', $url, [
                                'class' => 'btn btn-primary btn-xs',
                                'data-pjax' => 0,
                                'title' => 'Просмотр',
                                'data-toggle' => 'tooltip'
                            ]);
                        },
                        'update' => function ($url, $model) {
                            $url = Url::to(['auto-regists-templates/update', 'id' => $model->id]);
                            return Html::a('<span class="fa fa-pencil-square-o"></span>', $url, [
                                'class' => 'btn btn-success btn-xs',
                                'role' => 'modal-remote',
                                'title' => 'Изменить',
                                'data-toggle' => 'tooltip'
                            ]);
                        },
                        'delete' => function ($url, $model) {
                            $url = Url::to(['auto-regists-templates/delete', 'id' => $model->id]);
                            return Html::a('<span class="fa fa-trash-o"></span>', $url, [
                                'class' => 'btn btn-danger btn-xs',
                                'role' => 'modal-remote', 'title' => 'Удалить',
                                'data-confirm' => false, 'data-method' => false,// for overide yii data api
                                'data-request-method' => 'post',
                                'data-toggle' => 'tooltip',
                                'data-confirm-title' => 'Подтвердите действие',
                                'data-confirm-message' => 'Вы уверены что хотите удалить этого элемента?',
                            ]);
                        },
                    ],
                ],

            ],
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-plus"></i>', ['auto-regists-templates/create'],
                        ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-info']).
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                        ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'.
                    '{export}'
                ],
            ],
            'panelAfterTemplate' => "<p>Заполнение аккаунтов ".Html::a('https://youtu.be/A6RsT1i7u1c', 'https://youtu.be/A6RsT1i7u1c', ['target' => '_blank'])."</p>",
            'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'default',
                'heading' => '<i class="glyphicon glyphicon-list"></i> Автоматическое заполнение аккаунта',
                'after'=>BulkButtonWidget::widget([
                        'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
                            ["auto-regists-templates/bulk-delete"] ,
                            [
                                "class"=>"btn btn-danger btn-xs",
                                'role'=>'modal-remote-bulk',
                                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                'data-request-method'=>'post',
                                'data-confirm-title'=>'Вы уверены?',
                                'data-confirm-message'=>'Вы уверены, что хотите удалить эти записи?'
                            ]),
                    ]).
                    '<div class="clearfix"></div>',
            ]
        ])?>
    </div>
</div>
