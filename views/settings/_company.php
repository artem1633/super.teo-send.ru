<?php

/**
 * @var $this \yii\web\View
 * @var $settings \app\models\Settings[]
 * @var $company \app\models\Companies[]
 */

use app\models\Companies;
use app\models\Statuses;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use app\components\widgets\BootstrapModal;

BootstrapModal::widget([
    ['id' => 'status-modal', 'header' => true, 'footer' => true, 'size' => BootstrapModal::SIZE_LARGE],
]);

$this->title = "Настройки системы";
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row js-settings">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Настройки</h4>
            </div>
            <div class="panel-body panel-form">
                <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($company, 'company_name')->textInput() ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($company_settings, 'time_diff')->textInput()->label('Разница во времени с Москвой') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($company_settings, 'sales_access_token')->textInput() ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($company_settings, 'notify_start')->checkbox(['label' => 'Уведомлять о запуске рассылки после суточного лимита']);?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($company_settings, 'notify_promo')->checkbox(['label' => 'Уведомления об акциях и новостях'])?>
                    </div>
                </div>

                <?php if (!Yii::$app->request->isAjax){ ?>
                    <div class="form-group">
                        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                    </div>
                <?php } ?>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>
