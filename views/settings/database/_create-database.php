<?php
use yii\helpers\Html;
use kartik\form\ActiveForm;

?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-12" style="padding-bottom: 15px">
        <div class="row">
            <div class="col-md-4">
                <?= Html::button('<i class="fa fa-text-height fa-2x"> </i> Написать вручную', ['class' => 'js-hand ja-btns btn btn-warning', 'onclick' => 'handMode();']) ?>
            </div>
            <div class="col-md-4 text-center">
                <?= Html::button('<i class="fa fa-upload fa-2x"> </i> Загрузить файлом', ['class' => 'js-upload ja-btns btn btn-default', 'onclick' => 'uploadMode();']) ?>
            </div>
        </div>
    </div>
    <div class="col-md-12 js-file js-mode" style="display: none">
        <?= $form->field($model, 'file_upload')->fileInput() ?>
    </div>

    <div class="col-md-12 js-input-hand js-mode">
        <?= $form->field($model, 'text_data')->textarea(['rows' => 10, 'placeholder' => 'Введите аккаунты получателей по одному на строку']); ?>
    </div>
</div>
<?php if (!Yii::$app->request->isAjax) { ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?php } ?>

<?php ActiveForm::end(); ?>

<script>

    function handMode() {
        $('.ja-btns').removeClass('btn-warning');
        $('.js-hand').addClass('btn-warning');
        emptyMode();
        $('.js-mode').hide();
        $('.js-input-hand').show();
    }

    function uploadMode() {
        $('.ja-btns').removeClass('btn-warning');
        $('.js-upload').addClass('btn-warning');
        emptyMode();
        $('.js-mode').hide();
        $('.js-file').show();
    }


    function emptyMode() {
        $('#datarecipient-text_data').val('');
        $('#datarecipient-file_upload').val(null);
    }
</script>
