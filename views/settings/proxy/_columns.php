<?php

use app\models\Proxy;
use yii\helpers\Html,
    yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'name',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'ip_adress',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'port',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'status',
        'content' => function ($data) {
            if ($data->status == "no") {
                return "<span class='text-danger'><b>Не работает</b></span>";
            }
            if ($data->status == "yes") {
                return "<span class='text-success'><b>Работает</b></span>";
            }

        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'accounts',
        'label' => 'Аккаунты',
        'content' => function ($data) {
            return Proxy::getAccountsUsesProxy($data->id);
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'filter' => true,
        'attribute' => 'companyId',
        'content' => function ($data) {
            return $data->companyModel ? $data->companyModel->company_name : null;
        },
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'headerOptions' => ['width' => '140'],
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },


        'template' => '{view}  {leadUpdate}  {leadDelete}',
        'buttons' => [
            'view' => function ($url, $model) {
                $url = Url::to(['/proxy/view', 'id' => $model->id]);
                return Html::a('<span class="fa fa-eye"></span>', $url, [
                    'class' => 'btn btn-primary btn-sm',
                    'role' => 'modal-remote',
                    'title' => 'Просмотр',
                    'data-toggle' => 'tooltip'
                ]);
            },
            'leadUpdate' => function ($url, $model) {
                $url = Url::to(['/proxy/update', 'id' => $model->id]);
                return Html::a('<span class="fa fa-pencil-square-o"></span>', $url, [
                    'class' => 'btn btn-success btn-sm',
                    'role' => 'modal-remote',
                    'title' => 'Изменить',
                    'data-toggle' => 'tooltip'
                ]);
            },
            'leadDelete' => function ($url, $model) {
                $url = Url::to(['/proxy/delete', 'id' => $model->id]);
                return Html::a('<span class="fa fa-trash-o"></span>', $url, [
                    'class' => 'btn btn-danger btn-sm',
                    'role' => 'modal-remote', 'title' => 'Удалить',
                    'data-confirm' => false, 'data-method' => false,// for overide yii data api
                    'data-request-method' => 'post',
                    'data-toggle' => 'tooltip',
                    'data-confirm-title' => 'Подтвердите действие',
                    'data-confirm-message' => 'Вы уверены что хотите удалить этого элемента?',
                ]);
            },
        ]
    ],

];   