<?php
use yii\helpers\Html,
    yii\widgets\ActiveForm,
    kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Proxy */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proxy-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= Yii::$app->user->identity->isSuperAdmin()
        ? $form->field($model, 'companyId')->label()->widget(Select2::classname(), [
            'data' => $model->getCompaniesList(),
            'options' => [
                'placeholder' => 'Выберите',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])
        : null; ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ip_adress')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'port')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList(
        ['yes' => 'Работает', 'no' => 'Нет']
    ); ?>

    <?php if (!isset($_GET)): ?>
        <?= $form->field($model, 'accounts')->label()->widget(\kartik\select2\Select2::classname(), [
            'data' => $model->getDispatchList(),
            'options' => [
                'placeholder' => 'Выберите',
                'multiple' => true,
                'value' => $model->isNewRecord ? null : $model->DispatchRegistList(),
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

    <?php else: ?>
        <?= $form->field($model, 'accounts')->label()->widget(\kartik\select2\Select2::classname(), [
            'data' => $model->getDispatchList(),
            'options' => [
                'placeholder' => 'Выберите',
                'multiple' => true,
                'value' => $model->isNewRecord ? null : $model->DispatchRegistList($model->id),
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

    <?php endif; ?>

    <?= Yii::$app->user->identity->isSuperAdmin()
        ? $form->field($model, 'busy')->dropDownList(
            ['0' => 'Занят', '1' => 'Свободен']
        )
        : null; ?>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
