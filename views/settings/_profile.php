<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\ActiveForm;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AtelierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

CrudAsset::register($this);

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title"></h4>
    </div>
    <div class="panel-body panel-form">

        <div class="atelier-index">
            <div id="ajaxCrudDatatable">
                <?php if (\Yii::$app->user->identity->isSuperAdmin()): ?>
                    <?= GridView::widget([
                        'id' => 'crud-datatable',
                        'dataProvider' => $dataProviderUser,
                        'filterModel' => $searchModelUser,
                        'pjax' => true,
                        'responsiveWrap' => false,
                        'columns' => require(__DIR__ . '/profile/_columns.php'),
                        'toolbar' => [

                            ['content' =>
                                Html::a('Создать', ['/users/create'],
                                    ['role' => 'modal-remote', 'title' => 'Создать', 'class' => 'btn btn-info']) .
                                Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                                    ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Обновить']) .
                                '{toggleData}'
                            ],
                        ],
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
                        'panel' => [
                            'type' => 'default',
                            'heading' => '<i class="glyphicon glyphicon-list"></i> Пользователи',
                            'before' => '',
                            'after' => BulkButtonWidget::widget([
                                    'buttons' => Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить все',
                                        ["bulk-delete"],
                                        [
                                            "class" => "btn btn-danger btn-xs",
                                            'role' => 'modal-remote-bulk',
                                            'data-confirm' => false, 'data-method' => false,// for overide yii data api
                                            'data-request-method' => 'post',
                                            'data-confirm-title' => 'Подтвердите действие',
                                            'data-confirm-message' => 'Вы уверены что хотите удалить этих элементов?'
                                        ]),
                                ]) .
                                '<div class="clearfix"></div>',
                        ]
                    ]) ?>
                <?php else: ?>
                    <?php
                    if (isset($_GET['update'])) {
                        echo Alert::widget([
                            'options' => [
                                'class' => 'alert-success'
                            ],
                            'body' => 'Изменения сохранены.'
                        ]);
                    }
                    ?>
                    <div class="users-form">

                        <?php $form = ActiveForm::begin(['action' => Url::to(['/users/update/?id=' . Yii::$app->user->getId() . '&profile=1'])]); ?>

                        <div class="row">
                            <div class="col-md-8">
                                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-4">
                                <?= $model->isNewRecord ? $form->field($model, 'password')->textInput(['maxlength' => true]) : $form->field($model, 'new_password')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($model, 'vk_id')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div>
                                <?= $form->field($model, 'role_id')->hiddenInput()->label('') ?>
                            </div>
                        </div>

                        <div style="display: none;">
                            <?= $form->field($model, 'auth_key')->textInput(['maxlength' => true]) ?>
                            <?= $form->field($model, 'data_cr')->textInput() ?>
                        </div>

                        <?php if (!Yii::$app->request->isAjax) { ?>
                            <div class="form-group">
                                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            </div>
                        <?php } ?>

                        <?php ActiveForm::end(); ?>

                    </div>

                <?php endif; ?>

            </div>
        </div>
    </div>
</div>
