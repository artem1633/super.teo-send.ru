<?php
use yii\helpers\Url,
    yii\helpers\Html,
    app\models\Rates,
    kartik\grid\GridView;

return [
    [
        'class' => 'kartik\grid\ExpandRowColumn',
        'value' => function ($model) {
            return GridView::ROW_COLLAPSED;
            },
        'detail' => function ($model) {
            return Yii::$app->controller->renderPartial('details/_details-rate', ['model' => $model]);
        },
        'headerOptions' => ['class' => 'kartik-sheet-style'],
        'expandOneOnly' => true,
        'format' => 'html',
    ],
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'default',
        'label' => '',
        'content' => function($model){
            return $model->default ? '<i class="fa fa-check-square-o text-success"> </i> ' : null;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'price',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'time',
        'content' => function($model){

            $timeList = Rates::getTimeList();
            if(isset($timeList[$model->time])) {
                return $timeList[$model->time];
            }

            return Yii::$app->formatter->asRaw($model->time);
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{update}{delete}',
        'contentOptions' => ['style' => 'white-space: nowrap;'],
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                    'class' => 'btn btn-danger btn-xs',
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil" style="font-size: 16px;"></i>', $url, [
                    'class' => 'btn btn-success btn-xs',
                    'role'=>'modal-remote', 'title'=>'Изменить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            }
        ],
    ],

];   