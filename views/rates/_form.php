<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Rates;

/* @var $this yii\web\View */
/* @var $model app\models\Rates */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rates-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'price')->textInput(['type' => 'number']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'bonus')->textInput(['type' => 'number', 'step' => '0.1']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'affiliate_percent')->textInput(['type' => 'number', 'step' => '0.1']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'price_message')->textInput(['type' => 'number', 'step' => '0.1']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'sort')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'time')->dropDownList(Rates::getTimeList(), ['prompt' => 'Выберите срок']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'messages_daily_limit')->textInput(['type' => 'number']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'messages_in_transaction')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'send_interval')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'dispatch_count')->textInput(['type' => 'number']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'account_count')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'proxy_count')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'bots_count')->textInput(['type' => 'number']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'add_accounts')->checkbox() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'add_bots')->checkbox() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'add_proxy')->checkbox() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'dispatch_rules')->checkbox() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'default')->checkbox() ?>
        </div>
    </div>


    <?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
