<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\WordsBlackList */
?>
<div class="words-black-list-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'content',
            'created_at',
        ],
    ]) ?>

</div>
