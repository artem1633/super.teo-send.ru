<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Slider */
?>
<div class="slider-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'header',
            'content:ntext',
            'image_path',
            'image_alt',
            'sort',
            'enable',
            'created_at',
        ],
    ]) ?>

</div>
