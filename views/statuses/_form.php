<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

$classXS3 = ['options' => ['class' => 'col-xs-3']];

$classXS6 = ['options' => ['class' => 'col-xs-6']];

/* @var $this yii\web\View */
/* @var $model app\models\Statuses */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="statuses-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <?= $form->field($model, 'name', $classXS6)->textInput() ?>
                <?= $form->field($model, 'color', $classXS3)->label()->widget(Select2::classname(), [
                    'data' => ['default' => 'Белый', 'success' => 'Зеленый', 'primary' => 'Синий', 'info' => 'Голубой', 'warning' => 'Оранжевый', 'danger' => 'Красный'],
                    'options' => [
                        'placeholder' => 'Цвет',
                    ],
                ]); ?>
                <?= $form->field($model, 'sort', $classXS3)->textInput(['type' => 'number']) ?>
            </div>
        </div>
    </div>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
