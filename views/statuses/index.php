<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статусы';

CrudAsset::register($this);

?>
<div class="proxy-index">
    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'pjax' => true,
            'responsiveWrap' => false,
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
                        ['role'=>'modal-remote','title'=> 'Новый статус','class'=>'btn btn-info']).
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                        ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid'])
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
            'panel' => [
                'type' => 'default',
                'heading' => '<i class="fa fa-check-circle-o"></i> Статусы клиента',
                'before' => '',
                'after' => false,
            ],
            'columns' => [
                [
                    'class' => '\kartik\grid\DataColumn',
                    'headerOptions' => ['width' => '60'],
                    'attribute' => 'sort',
                    'label' => '',
                    'content' => function ($model) {
                        return $model->sort;
                    }
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'name',
                    'content' => function ($model) {
                        return '<button class="btn btn-sm btn-' . $model->color . '">' . $model->name . '</button>';
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'headerOptions' => ['width' => '50'],
                    'template' => '{edit} {delete}',
                    'buttons' => [
                        'edit' => function ($url, $model) {
                            $url = Url::to(['/statuses/update', 'id' => $model->id]);
                            return Html::a('<span class="fa fa-pencil-square-o"></span>', $url, [
                                'class' => 'btn btn-success btn-xs',
                                'role' => 'modal-remote',
                                'title' => 'Изменить',
                                'data-toggle' => 'tooltip'
                            ]);
                        },
                        'delete' => function ($url, $model) {
                            $url = Url::to(['/statuses/delete', 'id' => $model->id]);
                            return $model->id > 4 ? Html::a('<span class="fa fa-trash-o"></span>', $url, [
                                'class' => 'btn btn-danger btn-xs',
                                'role' => 'modal-remote', 'title' => 'Удалить',
                                'data-confirm' => false, 'data-method' => false,// for overide yii data api
                                'data-request-method' => 'post',
                                'data-toggle' => 'tooltip',
                                'data-confirm-title' => 'Подтвердите действие',
                                'data-confirm-message' => 'Вы уверены что хотите удалить этот элемент?',
                            ]) : null;
                        },
                    ]
                ]
            ],
        ]) ?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
