<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Statuses */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="statuses-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            array(
                'attribute' => 'color',
                'value' => Html::button('Цвет', ['class' => 'btn btn-'.$model->color]),
                'format' => 'raw'
            ),
            'sort',
        ],
    ]) ?>

</div>
