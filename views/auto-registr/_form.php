<?php
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AutoRegistr */
/* @var $form yii\widgets\ActiveForm */

$script = <<< JS
    $('#template-create-btn').click(function(){
        $('#template-creator-wrapper').slideDown();
    });
JS;

$this->registerJs($script, View::POS_READY);

?>

<div class="auto-registr-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'type')->dropDownList(\app\models\AutoRegistr::getTypesList()) ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'status')->dropDownList(\app\models\AutoRegistr::getStatusList()) ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'action')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="hidden">
            <?= $form->field($model, 'template_id')->hiddenInput() ?>
        </div>

    </div>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>

    
</div>
