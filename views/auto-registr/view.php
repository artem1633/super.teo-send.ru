<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AutoRegistr */
?>
<div class="auto-registr-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'type',
            'status',
            'action',
        ],
    ]) ?>

</div>
