<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TemplateMessages */
?>
<div class="template-messages-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'tag',
            'company_id',
            'created_at',
        ],
    ]) ?>

</div>
