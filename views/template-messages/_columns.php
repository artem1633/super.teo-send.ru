<?php
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'title',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'tag',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company_id',
        'value' => 'company.company_name',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to(['template-messages/'.$action]);
        },
        'buttons' => [
            'view' => function ($url, $model) {
                $url = Url::to([$url, 'id' => $model->id]);
                return Html::a('<span class="fa fa-eye"></span>', $url, [
                    'class' => 'btn btn-primary btn-xs',
                    'title' => 'Просмотр',
                    'data-toggle' => 'tooltip',
                    'data-pjax' => 0,
                ]);
            },
            'update' => function ($url, $model) {
                $url = Url::to([$url, 'id' => $model->id]);
                return Html::a('<span class="fa fa-pencil-square-o"></span>', $url, [
                    'class' => 'btn btn-success btn-xs',
                    'role' => 'modal-remote',
                    'title' => 'Изменить',
                    'data-toggle' => 'tooltip'
                ]);
            },
            'delete' => function ($url, $model) {
                $url = Url::to([$url, 'id' => $model->id]);
                return Html::a('<span class="fa fa-trash-o"></span>', $url, [
                    'class' => 'btn btn-danger btn-xs',
                    'role' => 'modal-remote', 'title' => 'Удалить',
                    'data-confirm' => false, 'data-method' => false,// for overide yii data api
                    'data-request-method' => 'post',
                    'data-toggle' => 'tooltip',
                    'data-confirm-title' => 'Подтвердите действие',
                    'data-confirm-message' => 'Вы уверены что хотите удалить этого элемента?',
                ]);
            },
        ],
        'viewOptions'=>['title'=>'View','data-toggle'=>'tooltip', 'data-pjax' => 0],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   