<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */


if (Yii::$app->controller->action->id === 'login') { 
/**
 * Do not use this code in your template. Remove it. 
 * Instead, use the code  $this->layout = '//main-login'; in your controller.
 */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {

    if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }

    dmstr\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>

        <?php $this->head() ?>
    </head>
    <?php 
        $session = Yii::$app->session;
        if($session['menu'] == null | $session['menu'] == 'large') $position="";
        else $position="sidebar-collapse";
    ?>
    <body class="hold-transition skin-blue sidebar-mini fixed <?=$position ?>">
    <?php $this->beginBody() ?>
    <div id="background-wait" style="position: fixed; margin: 0px; width: 100%; height: 100%; background: rgba(127,130,137,0.3); z-index: 1111; display: none; ">
        <img src="/images/working.gif" style="position: absolute; width:12%; left:44%; top: 44%; ">
    </div>
    <!-- VK Widget -->
    <div id="vk_community_messages"></div>
    <div class="wrapper">

        <?= $this->render(
            'header.php',
            ['directoryAsset' => $directoryAsset]
        ) ?>

        <?= $this->render(
            '_old_left.php',
            ['directoryAsset' => $directoryAsset]
        )
        ?>

        <?= $this->render(
            'content.php',
            ['content' => $content, 'directoryAsset' => $directoryAsset]
        ) ?>

    </div>
    <script type="text/javascript" src="https://vk.com/js/api/openapi.js?158"></script>

    <!-- VK Widget -->

    <script type="text/javascript">
        VK.Widgets.CommunityMessages("vk_community_messages", 166230711, {tooltipButtonText: "Есть вопросы?"});
    </script>
    <script type="text/javascript">
        VK.Widgets.ContactUs("vk_contact_us", {text: "Заказать аккаунты для рассылки"}, -166230711);
    </script>
    <script type="text/javascript">
        VK.Widgets.ContactUs("vk_contact_us1", {text: "Заказать базу для рассылки", height: 30}, -166230711);
    </script>

    <?php $this->endBody() ?>

    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
