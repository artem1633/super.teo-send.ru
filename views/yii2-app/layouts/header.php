<?php
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;
use yii\helpers\Html;
use app\models\Settings;
use app\models\Resume;
use yii\helpers\Url;

?>

                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" onclick="$.post('/site/menu-position');" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SEARCH -->
                    <!-- <li class="xn-search">
                        <form role="form">
                            <input type="text" name="search" placeholder="Search..."/>
                        </form>
                    </li>   --> 
                    <!-- END SEARCH -->
                    <!-- SIGN OUT -->
                    <!-- END SIGN OUT -->
                    <!-- MESSAGES -->
                    <!-- <li class="xn-icon-button pull-right">
                        <a href="#"><span class="fa fa-comments"></span></a>
                        <div class="informer informer-danger">4</div>
                        <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="fa fa-comments"></span> Messages</h3>                                
                                <div class="pull-right">
                                    <span class="label label-danger">4 new</span>
                                </div>
                            </div>
                            <div class="panel-body list-group list-group-contacts scroll" style="height: 200px;">
                                <a href="#" class="list-group-item">
                                    <div class="list-group-status status-online"></div>
                                    <img src="/../examples/images/users/user2.jpg" class="pull-left" alt="John Doe"/>
                                    <span class="contacts-title">John Doe</span>
                                    <p>Praesent placerat tellus id augue condimentum</p>
                                </a>
                                <a href="#" class="list-group-item">
                                    <div class="list-group-status status-away"></div>
                                    <img src="/../examples/images/users/user.jpg" class="pull-left" alt="Dmitry Ivaniuk"/>
                                    <span class="contacts-title">Dmitry Ivaniuk</span>
                                    <p>Donec risus sapien, sagittis et magna quis</p>
                                </a>
                                <a href="#" class="list-group-item">
                                    <div class="list-group-status status-away"></div>
                                    <img src="/../examples/images/users/user3.jpg" class="pull-left" alt="Nadia Ali"/>
                                    <span class="contacts-title">Nadia Ali</span>
                                    <p>Mauris vel eros ut nunc rhoncus cursus sed</p>
                                </a>
                                <a href="#" class="list-group-item">
                                    <div class="list-group-status status-offline"></div>
                                    <img src="/../examples/images/users/user6.jpg" class="pull-left" alt="Darth Vader"/>
                                    <span class="contacts-title">Darth Vader</span>
                                    <p>I want my money back!</p>
                                </a>
                            </div>     
                            <div class="panel-footer text-center">
                                <a href="pages-messages.html">Show all messages</a>
                            </div>                            
                        </div>                        
                    </li> -->
                    <!-- TASKS -->

                    <li class="pull-right">
                        <a href="#"><span><?=Yii::$app->user->identity->name ?></span></a>
                        <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="fa fa-user"></span> Профиль</h3>
                            </div>
                            <div class="panel-body profile" style="height: 200px; background: #fff;">
                                <div class="profile-image">
                                    <img src="<?= 'http://' . $_SERVER['SERVER_NAME'] ?>/images/nouser.png" alt="Nadia Ali">
                                </div>
                                <div class="profile-data">
                                    <div class="profile-data-name" style="color: #000;"><?=Yii::$app->user->identity->name ?></div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="pull-left">
                                    <?= Html::a('Изменить пароль', ['users/change', 'id' => Yii::$app->user->identity->id],
                                        ['role'=>'modal-remote','title'=> 'Изменить пароль','class'=>'btn btn-default btn-flat']); ?>
                                </div>
                                <div class="pull-right">
                                    <?= Html::a(
                                        'Выход',
                                        ['/site/logout'],
                                        ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                    ) ?>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="pull-right">
                        <a href="#"><span>Уведомления</span></a>
                        <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="fa fa-user"></span> Секретный код</h3>
                            </div>
                            <div class="panel-body profile" style=" background: #fff; text-align: center;">
                                <p style="font-size: 20px;">
                                    <?= \app\components\helpers\FunctionHelper::getCompanyModel()->unique_code ?>
                                </p>
                                <p style="margin-top: 5px; color: gray;">введите код в телеграмм бот @teo_vk_bot</p>
                            </div>
                            <div class="panel-footer">
                            </div>
                        </div>
                    </li>

                    <li class="pull-right">
                        <?=Html::a(
                            Html::img('/images/icon/yandex.svg', ['style' => 'margin-top:-5px'])
                            . ' '
                            . number_format(Yii::$app->user->identity->getCompanyInstance()->general_balance, 2, ',', ' ')
                            . ' <i class="fa fa-rub"></i>',
                            ['/payment'])?>
                    </li>

                    <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                        <li class="pull-right">
                            <?=Html::a('proxy: '.Yii::$app->proxy->balance.' '.Yii::$app->proxy->currency, ['#'])?>
                        </li>
                    <?php endif; ?>

                    <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                        <li class="xn-icon-button pull-right">
                            <?= Html::a(
                                '<i class="fa fa-cog fa-lg"></i>',
                                ['/settings'],
                                ['title' => 'Настройки']
                            ) ?>
                        </li>
                    <?php endif; ?>

                    <!-- END TASKS -->
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                    
                
                <!-- START BREADCRUMB -->
                <!-- <ul class="breadcrumb push-down-0">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Layouts</a></li>
                    <li class="active">Custom Navigation</li>                    
                </ul> -->

                <section class="breadcrumb push-down-0">
                <?php
               /* echo Breadcrumbs::widget(
                    [
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]
                )*/ ?>
                <?php if (isset($this->blocks['content-header'])) { ?>
                    <h1><?php $this->blocks['content-header'] ?></h1>
                <?php } else { ?>
                    <!-- <h1>
                        <?php
                        /*if ($this->title !== null) {
                            echo \yii\helpers\Html::encode($this->title);
                        } else {
                            echo \yii\helpers\Inflector::camel2words(
                                \yii\helpers\Inflector::id2camel($this->context->module->id)
                            );
                            echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                        }*/ ?>
                    </h1> -->
                <?php } ?>

            </section>

                <!-- END BREADCRUMB -->                           
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                <br>                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->
