<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;
use yii\helpers\Url;
use app\models\Users;
use yii\widgets\Pjax;


?>
<?php
//        $session = Yii::$app->session;
//        if($session['left'] == null | $session['left'] == 'small') $left="x-navigation-minimized";
//        else $left="x-navigation-custom";
?>
<!-- START PAGE SIDEBAR -->
<div class="page-sidebar page-sidebar-fixed scroll">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation x-navigation-minimized">
        <li class="xn-logo">
            <a href="<?=Url::toRoute([Yii::$app->homeUrl])?>"><?=Yii::$app->name?></a>
            <a href="#" class="x-navigation-control"></a>
        </li>
        <li class="xn-profile">
            <a href="#" class="profile-mini">
                <img src="/favicon.png" alt="John Doe"/>
            </a>
            <div class="profile">
                <div class="profile-image">
                    <img src="/favicon.png" alt="John Doe"/>
                </div>
                <div class="profile-data">
                    <div class="profile-data-name"></div>
                    <div class="profile-data-title"></div>
                </div>
                <div class="profile-controls">
                    <?= Html::a('<span class="fa fa-info"></span>', ['/users/profile'], ['title'=> 'Профиль','class'=>'profile-control-left']); ?>
                    <!-- <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a> -->
                    <!-- <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a> -->
                    <?= Html::a('<span class="fa fa-envelope"></span>', ['/users/profile'], ['title'=> 'Профиль','class'=>'profile-control-right']); ?>
                </div>
            </div>
        </li>
        <li class="xn-title">Меню</li>
        <li class="hidden">
            <a href="<?=Url::toRoute(['/indicators'])?>"><span class="fa fa-bar-chart-o"></span> <span class="xn-text">Показатели</span></a>
        </li>
        <li>
            <a href="<?=Url::toRoute(['/dispatch'])?>"><span class="fa fa-envelope"></span> <span class="xn-text">Рассылки</span></a>
        </li>
        <?php if(Yii::$app->user->identity->isSuperAdmin()):  ?>
            <li>
                <a href="<?=Url::toRoute(['/dispatch-regist'])?>"><span class="fa fa-users"></span> <span class="xn-text">Аккаунты</span></a>
            </li>
        <?php endif; ?>
        <li>
            <a href="<?=Url::toRoute(['/affiliate-program'])?>"><span class="fa fa-sitemap"></span> <span class="xn-text">Партнерская программа</span></a>
        </li>
        <?php if(Yii::$app->user->identity->isSuperAdmin()):  ?>
            <li>
                <a href="<?=Url::toRoute(Yii::$app->user->identity->isSuperAdmin() ? ['/store'] : ['/trade'])?>"><span class="fa fa-shopping-cart"></span> <span class="xn-text">Магазин</span></a>
            </li>
        <?php endif; ?>
        <li>
            <a href="<?=Url::toRoute(['/settings'])?>"><span class="fa fa-cog"></span> <span class="xn-text">Настройки</span></a>
        </li>
        <li>
            <a href="<?=Url::toRoute(['/bots'])?>"><span class="fa fa-external-link-square"></span> <span class="xn-text">Авто ответы</span></a>
        </li>
        <li class="hidden">
            <a href="<?=Url::toRoute(['/dispatch/report'])?>"><span class="fa fa-calendar"></span> <span class="xn-text">Отчет</span></a>
        </li>
        <li>
            <a href="<?=Url::toRoute(['/payment'])?>"><span class="fa fa-credit-card"></span> <span class="xn-text">Финансы</span></a>
        </li>
        <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
            <li class="xn-openable">
                <a href="#"><span class="fa fa-star"></span> <span class="xn-text">Администратор</span></a>
                <ul>
                    <li><a href="<?=Url::toRoute(['/companies'])?>"><span class="fa fa-star-o"></span> <span>Компании</span></a></li>
                    <li><a href="<?=Url::toRoute(['/logs'])?>"><span class="fa fa-star-o"></span> <span>Логи</span></a></li>
                    <li><a href="<?=Url::toRoute(['/rates'])?>"><span class="fa fa-star-o"></span> <span>Тарифы</span></a></li>
                    <li><a href="<?=Url::toRoute(['/proxy'])?>"><span class="fa fa-star-o"></span> <span>Прокси</span></a></li>
                    <li><a href="<?=Url::toRoute(['/email-templates'])?>"><span class="fa fa-star-o"></span> <span>Шаблоны сообщений</span></a></li>
                    <li><a href="<?=Url::toRoute(['/statuses'])?>"><span class="fa fa-star-o"></span> <span>Статусы</span></a></li>
                    <li><a href="<?=Url::toRoute(['/yandex-money'])?>"><span class="fa fa-star-o"></span> <span>Яндекс Деньги</span></a></li>
                    <li><a href="<?=Url::toRoute(['/accounting'])?>"><span class="fa fa-star-o"></span> <span>Движение средств</span></a></li>
                    <li><a href="<?=Url::toRoute(['/global-messages'])?>"><span class="fa fa-star-o"></span> <span>Рассылка</span></a></li>
                </ul>
            </li>
            <li class="xn-openable">
                <a href="#"><span class="fa fa-star"></span> <span class="xn-text">Отчеты</span></a>
                <ul>
                    <li><a href="<?=Url::toRoute(['/report/registration'])?>"><span class="fa fa-star-o"></span> <span>Регистрация</span></a></li>
                </ul>
            </li>
        <?php endif; ?>

    </ul>
    <!-- END X-NAVIGATION -->
</div>
<!-- END PAGE SIDEBAR -->