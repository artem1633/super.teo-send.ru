<?php
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;

?>
<div class="content-wrapper">

    <section class="content">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.0
    </div>
    <strong>Copyright &copy; 2019 <a href="https://shop-crm.ru/portfolio?utm=teo_job">Разработчик TEO</a>.</strong> All rights
    reserved.
</footer>

<!-- Control Sidebar -->

<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->

<?php

$script = <<< JS
    $('#notifications-modal').modal('show');
JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>
