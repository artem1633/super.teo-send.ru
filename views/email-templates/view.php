<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\EmailTemplates */
?>
<div class="email-templates-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'key',
            'key_ru',
            'body:html',
            'deletable',
        ],
    ]) ?>

</div>
