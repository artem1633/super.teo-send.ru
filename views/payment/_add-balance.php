<?php

use yii\helpers\Html,
    yii\helpers\Url,
    kartik\form\ActiveForm,
    kartik\select2\Select2,
    app\models\Companies;

/* @var $this yii\web\View */
/* @var $model \app\models\CasesAddMoney */
/* @var $agents array */
/* @var $accounts array */

$classXS6 = ['options' => ['class' => 'col-xs-6']];
$classXS4 = ['options' => ['class' => 'col-xs-4']];
$classXS3 = ['options' => ['class' => 'col-xs-3']];
?>
<?php $form = ActiveForm::begin(['id' => 'form']); ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <?= $form->field($model, 'company_id', $classXS6)->widget(Select2::classname(), [
                    'data' => Companies::getAllInArray(),
                    'options' => ['placeholder' => "Выберите компанию"],
                ]); ?>
                <?= $form->field($model, 'amount', $classXS6)->textInput(['type' => 'number', 'step' => '0.01']) ?>
            </div>
        </div>
    </div>


<?php ActiveForm::end(); ?>
