<?php
use yii\helpers\Html,
    yii\bootstrap\Modal,
    app\components\helpers\FunctionHelper,
    kartik\grid\GridView,
    app\models\AccountingReport,
    johnitvn\ajaxcrud\CrudAsset;


CrudAsset::register($this);
$this->title = 'Мои финансы';
?>
<?php
if (!Yii::$app->user->identity->isSuperAdmin()) {
    ?>
    <iframe src="https://money.yandex.ru/quickpay/shop-widget?label=<?= FunctionHelper::getCompanyId() ?>&writer=seller&targets=%D0%9F%D0%BE%D0%BF%D0%BE%D0%BB%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5%20%D0%B1%D0%B0%D0%BB%D0%B0%D0%BD%D1%81%D0%B0&targets-hint=&default-sum=100&button-text=11&payment-type-choice=on&hint=&successURL=&quickpay=shop&account=<?= $configure->wallet ?>"
            width="100%" height="222" frameborder="0" allowtransparency="true" scrolling="no"></iframe>
    <?php
}
?>

    <div class="panel panel-default tabs">
        <ul class="nav nav-tabs nav-justified">
            <li class="active"><a href="#tab1" data-toggle="tab" style="margin-right: 5px; border-radius: 3px 3px 0px 0px;">Пополнение</a></li>
            <li><a href="#tab2" data-toggle="tab" style="margin-right: 5px; border-radius: 3px 3px 0px 0px;">Списание</a></li>
            <li><a href="#tab3" data-toggle="tab" style="border-radius: 3px 3px 0px 0px;">Магазин</a></li>
        </ul>
        <div class="panel-body tab-content">
            <div class="tab-pane active" id="tab1">
                <p>
                    <?=Yii::$app->user->identity->isSuperAdmin() ? Html::a('Ручное пополнение', ['add-balance'], ['role' => 'modal-remote', 'title' => 'Создать', 'class' => 'btn btn-warning']) : ''?>
                </p>
                <?= GridView::widget([
                    'id' => 'crud-datatable-1',
                    'dataProvider' => $dataProviderDebit,
                    //'filterModel' => $searchModel,
                    'pjax' => true,
                    'responsiveWrap' => false,
                    'columns' => [
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'attribute' => 'date_report',
                            'content' => function ($model) {
                                return FunctionHelper::dateForForm($model->date_report, 1);
                            }
                        ],
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'attribute' => 'company_id',
                            'content' => function ($model) {
                                return $model->companyModel ? $model->companyModel->company_name : null;
                            },
                            'visible' => Yii::$app->user->identity->isSuperAdmin(),
                        ],
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'attribute' => 'amount',
                            'content' => function ($model) {
                                return number_format($model->amount, 2, ',', ' ') . ' <i class="fa fa-rub"></i>';
                            }
                        ],
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'attribute' => 'operation_type',
                            'content' => function ($model) {
                                switch ($model->operation_type) {
                                    case AccountingReport::TYPE_INCOME_BALANCE:
                                        return 'Пополнение баланса';
                                        break;
                                    case AccountingReport::TYPE_INCOME_AFFILIATE:
                                        return 'Реферальное отчисление';
                                        break;
                                    case AccountingReport::TYPE_DISPATCH_PAYED:
                                        return 'Оплата рассылки';
                                        break;
                                    case AccountingReport::TYPE_OUTPUT_AFFILIATE:
                                        return 'Оплата рассылки';
                                        break;
                                    case AccountingReport::TYPE_SHOP_PAYED:
                                        return 'Оплата магазина';
                                        break;
                                    default:
                                        return null;
                                }
                            }
                        ],
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'attribute' => 'description',
                            'content' => function ($model) {
                                return $model->description;
                            }
                        ],
                    ],

                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
                    'panel' => null,
                ]); ?>
            </div>
            <div class="tab-pane" id="tab2">
                <?= GridView::widget([
                    'id' => 'crud-datatable-2',
                    'dataProvider' => $dataProviderCredit,
                    //'filterModel' => $searchModel,
                    'pjax' => true,
                    'responsiveWrap' => false,
                    'columns' => [
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'attribute' => 'date_report',
                            'content' => function ($model) {
                                return FunctionHelper::dateForForm($model->date_report, 1);
                            }
                        ],
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'attribute' => 'company_id',
                            'content' => function ($model) {
                                return $model->companyModel ? $model->companyModel->company_name : null;
                            },
                            'visible' => Yii::$app->user->identity->isSuperAdmin(),
                        ],
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'attribute' => 'amount',
                            'content' => function ($model) {
                                return number_format($model->amount, 2, ',', ' ') . ' <i class="fa fa-rub"></i>';
                            }
                        ],
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'attribute' => 'operation_type',
                            'content' => function ($model) {
                                switch ($model->operation_type) {
                                    case AccountingReport::TYPE_INCOME_BALANCE:
                                        return 'Пополнение баланса';
                                        break;
                                    case AccountingReport::TYPE_INCOME_AFFILIATE:
                                        return 'Реферальное отчисление';
                                        break;
                                    case AccountingReport::TYPE_DISPATCH_PAYED:
                                        return 'Оплата рассылки';
                                        break;
                                    case AccountingReport::TYPE_OUTPUT_AFFILIATE:
                                        return 'Оплата рассылки';
                                        break;
                                    case AccountingReport::TYPE_SHOP_PAYED:
                                        return 'Оплата магазина';
                                        break;
                                    default:
                                        return null;
                                }
                            }
                        ],
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'attribute' => 'description',
                            'content' => function ($model) {
                                return $model->description;
                            }
                        ],
                    ],

                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
                    'panel' => null,
                ]); ?>
            </div>
            <div class="tab-pane" id="tab3">
                <?= GridView::widget([
                    'id' => 'crud-datatable-3',
                    'dataProvider' => $dataProviderShop,
                    //'filterModel' => $searchModel,
                    'pjax' => true,
                    'responsiveWrap' => false,
                    'columns' => [
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'attribute' => 'date_report',
                            'content' => function ($model) {
                                return FunctionHelper::dateForForm($model->date_report, 1);
                            }
                        ],
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'attribute' => 'company_id',
                            'content' => function ($model) {
                                return $model->companyModel ? $model->companyModel->company_name : null;
                            },
                            'visible' => Yii::$app->user->identity->isSuperAdmin(),
                        ],
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'attribute' => 'amount',
                            'content' => function ($model) {
                                return number_format($model->amount, 2, ',', ' ') . ' <i class="fa fa-rub"></i>';
                            }
                        ],
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'attribute' => 'operation_type',
                            'content' => function ($model) {
                                switch ($model->operation_type) {
                                    case AccountingReport::TYPE_INCOME_BALANCE:
                                        return 'Пополнение баланса';
                                        break;
                                    case AccountingReport::TYPE_INCOME_AFFILIATE:
                                        return 'Реферальное отчисление';
                                        break;
                                    case AccountingReport::TYPE_DISPATCH_PAYED:
                                        return 'Оплата рассылки';
                                        break;
                                    case AccountingReport::TYPE_OUTPUT_AFFILIATE:
                                        return 'Оплата рассылки';
                                        break;
                                    case AccountingReport::TYPE_SHOP_PAYED:
                                        return 'Оплата магазина';
                                        break;
                                    default:
                                        return null;
                                }
                            }
                        ],
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'attribute' => 'description',
                            'content' => function ($model) {
                                return $model->description;
                            }
                        ],
                    ],

                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
                    'panel' => null,
                ]); ?>
            </div>
        </div>
    </div>

<div class="atelier-index">
    <div id="ajaxCrudDatatable">
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>