<?php

use app\models\Statuses;
use yii\helpers\ArrayHelper;
use yii\helpers\Html,
    kartik\select2\Select2,
    kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BotsDialogs */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="bots-dialog-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="bots-dialog-form row">
        <?php $form = ActiveForm::begin(); ?>
        <div class="col-md-3">
            <?= $form->field($model, 'stepNumber')->input('number', ['min' => 1]); ?>
        </div>
        <div class="col-md-3">
            <?php
            $statusItems = ArrayHelper::map(Statuses::find()->all(), 'id', 'name');
            echo $form->field($model, 'status')->dropDownList($statusItems, ['prompt' => 'Выберите статус']);
            ?>
        </div>
        <div class="col-md-6">
            <div class="row chk-noanswer">
                <div class="col-md-4">
                    <?= $form->field($model, 'notify')->checkbox() ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'anyAnswer')->checkbox() ?>
                </div>
                <div class="col-md-4">
                    <?= Html::checkbox('noAnswer', $model->delayMin, [
                        'id' => 'chk-noanswer',
                        'label' => 'Нет ответа от пользователя',
                    ]) ?>
                </div>
            </div>

        </div>
            <div class="col-md-12">
                <?= $form->field($model, 'delayMin')->input('number', [
                    'id' => 'delay-min',
                    'min' => 1,
                    'class' => $model->isNewRecord  || !$model->delayMin ? 'hidden' : null,
                    'placeholder' => 'Время ожидания перед отправкой сообщения',
                ])->label(''); ?>

                <div class="row answers">
                    <div class="col-md-2">
                        <?= $form->field($model, 'check_search',['options' => [
                            'class' => $model->anyAnswer || $model->delayMin ? 'hidden' : null,
                        ]])->checkbox()->label('Точный поиск'); ?>
                    </div>

                    <div class="col-md-10">
                        <?= $form->field($model, 'answerSet', ['options' => [
                            'class' => $model->anyAnswer || $model->delayMin ? 'hidden' : null,
                        ]])->label()->widget(Select2::classname(), [
                            'options' => [
                                'placeholder' => 'Введите варианты ответа...',
                                'value' => $model->getAnswers(),
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'tags' => true,
                                'multiple' => true,
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'result')->textInput(['maxlength' => true]) ?>
            </div>



            <div class="col-md-12">
                <?php if (!Yii::$app->request->isAjax) { ?>
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                <?php } ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

    <script>
        $(function () {
            $('.bots-dialog-update').on('change', '#botsdialogs-anyanswer', function () {
                if ($(this).is(":checked")) {
                    // debugger;
                    $('.field-botsdialogs-answerset').addClass('hidden');
                    $('#botsdialogs-answerset').select2('val', 0);
                    $('#chk-noanswer').removeAttr("checked");
                    $('#delay-min').addClass('hidden');
                } else {
                    $('.field-botsdialogs-answerset').removeClass('hidden');
                }
            });
            $('.bots-dialog-update').on('change', '#chk-noanswer', function () {
                if ($(this).is(":checked")) {
                    // $('.field-botsdialogs-answerset').addClass('hidden');
                    $('.answers').addClass('hidden');
                    $('#botsdialogs-answerset').select2('val', 0);
                    $('#delay-min').removeClass('hidden');
                    $('#botsdialogs-anyanswer').removeAttr("checked");
                } else {
                    // $('.field-botsdialogs-answerset').removeClass('hidden');
                    $('.answers').removeClass('hidden');
                    $('#delay-min').val(null);
                    $('#delay-min').addClass('hidden');
                }
            });
        });
    </script>


