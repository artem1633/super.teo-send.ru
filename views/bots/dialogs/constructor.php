<?php

use yii\helpers\Html,
    yii\helpers\Url,
    yii\bootstrap\Modal,
    kartik\form\ActiveForm,
    kartik\select2\Select2,
    johnitvn\ajaxcrud\CrudAsset,
    kartik\grid\GridView,
    johnitvn\ajaxcrud\BulkButtonWidget,
    yii\widgets\Pjax;

CrudAsset::register($this);

$this->title = 'Конструктор диалогов';
$this->params['breadcrumbs'][] = ['label' => 'Боты', 'url' => ['/bots']];
$this->params['breadcrumbs'][] = $this->title;

$botName = $model->botModel ? $model->botModel->name : 'no name';
$classXS6 = ['options' => ['class' => 'col-xs-6']];
?>

<?= Html::a('<i class="fa fa-plus"></i> Добавить ответ',
    Url::to(['/bots/dialog-create', 'id' => $model->botId]), [
        'role' => 'modal-remote',
        'data-toggle' => 'tooltip',
        'class' => 'btn btn-primary',
        'style' => 'margin-top:20px;'
    ]); ?>


<div class="atelier-index">
    <div id="ajaxCrudDatatable">
        <div id="crud-datatable">
            <?php
            if ($steps) {
                foreach ($steps as $step) {
                    $urlDelete = Url::to(['/bots/dialog-delete', 'id' => $step->id]);
                    $urlEdit = Url::to(['/bots/dialog-update', 'id' => $step->id]);
                    ?>
                    <div class="panel panel-primary" style="margin-top: 30px">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="panel-title">
                                        <i class="fa fa-comments-o"></i> Номер ответа <?= $step->stepNumber; ?>
                                    </h3>
                                </div>
                                <div class="col-md-5 text-right">
                                    <?= Html::a('<i class="fa fa-pencil"></i> Редактировать', $urlEdit, ['role' => 'modal-remote', 'data-toggle' => 'tooltip', 'class' => 'btn btn-default btn-sm']); ?>
                                </div>
                                <div class="col-md-1 text-right">
                                    <?= Html::a('<i class="fa fa-trash-o"></i>', $urlDelete, [
                                        'role' => 'modal-remote', 'title' => '',
                                        'data-confirm' => false, 'data-method' => false,// for overide yii data api
                                        'data-request-method' => 'post',
                                        'data-toggle' => 'tooltip',
                                        'data-confirm-title' => 'Подтвердите действие',
                                        'data-confirm-message' => 'Вы уверены что хотите удалить этот элемент?',
                                        'class' => 'btn btn-danger btn-sm'
                                    ]) ?>

                                </div>
                            </div>


                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <?php if ($step->delayMin){ ?>
                                        <label class="control-label">Если пользователь не ответил:</label>
                                        <?= $step->delayMin ?> мин.
                                    <?php } else { ?>
                                    <label class="control-label">Возможные ответы пользователя:</label>
                                        <?= $step->anyAnswer ? 'Любой ответ' : $step->getAnswers(1) . ';'; ?>
                                    <?php } ?>

                                </div>
                                <div class="col-xs-12">
                                    <label class="control-label">Наш ответ:</label>
                                    <?= $step->result; ?>
                                </div>
                                <div class="col-xs-12">
                                    <label class="control-label">Уведомление об ответе:</label>
                                    <?= $step->notify ? 'ДА' : 'НЕТ'; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>

</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>




