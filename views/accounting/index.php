<?php

use yii\helpers\Html,
    kartik\grid\GridView,
    app\components\helpers\FunctionHelper,
    app\models\AccountingReport;

?>

<?= \kartik\grid\GridView::widget([
    'id' => 'crud-datatable',
    'dataProvider' => $dataProvider,
    'pjax' => ['enablePushState' => false],
    'responsiveWrap' => false,
    'columns' => [
        [
            'class' => '\kartik\grid\DataColumn',
            'attribute' => 'date_report',
            'content' => function ($model) {
                return FunctionHelper::dateForForm($model->date_report, 1);
            }
        ],
        [
            'class' => '\kartik\grid\DataColumn',
            'attribute' => 'company_id',
            'content' => function ($model) {
                return $model->companyModel ? $model->companyModel->company_name : null;
            }
        ],
        [
            'class' => '\kartik\grid\DataColumn',
            'attribute' => 'amount',
            'content' => function ($model) {
                return number_format($model->amount, 2, ',', ' ') . ' <i class="fa fa-rub"></i>';
            }
        ],
        [
            'class' => '\kartik\grid\DataColumn',
            'attribute' => 'operation_type',
            'content' => function ($model) {
                switch ($model->operation_type) {
                    case AccountingReport::TYPE_INCOME_BALANCE:
                        return 'Пополнение баланса';
                        break;
                    case AccountingReport::TYPE_INCOME_AFFILIATE:
                        return 'Реферальное отчисление';
                        break;
                    case AccountingReport::TYPE_DISPATCH_PAYED:
                        return 'Оплата рассылки';
                        break;
                    case AccountingReport::TYPE_OUTPUT_AFFILIATE:
                        return 'Оплата рассылки';
                        break;
                    default:
                        return null;
                }
            }
        ],
    ],

    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
    'panel' => [
        'type' => 'default',
        'heading' => '<i class="glyphicon glyphicon-list"></i> Движение средств',
        'before' => '',
        'after' => false,
    ]
]) ?>
