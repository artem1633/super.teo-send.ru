<?php
use Yii,
    yii\helpers\Html,
    yii\helpers\Url,
    app\models\Shop;
use yii\bootstrap\Modal;


$this->title = 'Магазин';

$this->params['breadcrumbs'][] = ['label' => $this->title];

$this->registerJsFile("js/plugins/equalheights/equalheights.min.js", [
    'depends' => [\app\assets\AppAsset::className()],
], 'js-equals-heights');
$this->registerCssFile("css/pages/trade.css", [
    'depends' => [\app\assets\AppAsset::className()],
], 'css-page');

?>
<div class="panel shop-panel">
    <div class="panel-body">
        <div class="row" style="padding: 20px; 5px;">
            <?php
            if ($items) {
                $col = 0;
                foreach ($items as $item) {
                    $remains = $item->getRemains();
                    $col++;
                    if ($col == 1) {
                        ?>
                        <div class="row">
                        <?php
                    }
                    ?>
                    <div class="col-md-3" style="padding-bottom: 30px;">
                        <form id="form-<?= $item->id ?>">
                            <input type="hidden" name="item" value="<?= $item->id ?>">
                            <div style="padding: 10px; border-radius: 10px; border: 1px solid #c2bec4; box-shadow: 0 5px 15px rgba(0,0,0,0.3);">
                                <div class="prod-info">
                                    <h3><?= Html::a($item->name, Url::to(['trade/item/', 'id' => $item->id]), ['target' => '_blank']) ?></h3>
                                    <h4 class="shop-card-description"><?= Html::a($item->description_short, Url::to(['trade/item/', 'id' => $item->id]), ['target' => '_blank']) ?></h4>


                                </div>
                                <div>
                                    <?php
                                    if ($item->type == Shop::TYPE_ACCOUNT_DATA || $item->type == Shop::TYPE_ACCOUNT || $item->type == Shop::TYPE_PROXY) {
                                        ?>
                                        В наличии: <span class="count-item-<?= $item->id ?>"><?= $remains ?></span>
                                        <?php
                                    } else {
                                        echo '&nbsp';
                                    }
                                    ?>
                                </div>
                                <div class="shop-card-full-description">
                                    <?=$item->description_long?>
                                </div>
                                <div class="row shop-card-floor">
                                    <div class="col-xs-6" style="padding-left: 0;">
                                        <h4 class="text-success js-price-<?= $item->id ?>" style="margin-top: 6px;">
                                            <?= number_format($item->price, 2, ',', ' ') ?>
                                            <i class="fa fa-ruble"></i>
                                            <br/>
                                            <span style="font-size: 12px" class="js-alert-<?= $item->id ?>"></span>
                                        </h4>
                                    </div>
                                    <div class="col-xs-3">
                                        <?php
                                        if ($item->type == Shop::TYPE_ACCOUNT_DATA || $item->type == Shop::TYPE_ACCOUNT || $item->type == Shop::TYPE_PROXY) {
                                            ?>
                                            <?= Html::input('hidden', 'count', 1, ['class' => 'js-count-' . $item->id . ' form-control', 'min' => 1, 'max' => 1, 'step' => 1]); ?>
                                            <?php
                                        } else {
                                            echo '<input type="hidden" name="count" value="1" class="js-count-' . $item->id . '">';
                                        }
                                        ?>

                                    </div>
                                    <?php if($item->type != Shop::TYPE_ACCOUNT_DATA){ ?>
                                        <div class="col-xs-2 text-right item" data-key="<?= $item->id ?>"
                                             data-name="<?= $item->name ?>" data-price="<?= $item->price ?>">
                                            <?= Html::button('<i class="fa fa-shopping-cart"></i>', [
                                                'class' => 'btn btn-success js-buy',
                                                'data-loading-text' => "<i class='fa fa-circle-o-notch fa-spin'></i>",
                                            ]); ?>
                                        </div>
                                    <?php } else { ?>
                                        <div class="col-xs-2 text-right item-accounts" data-key="<?= $item->id ?>"
                                             data-name="<?= $item->name ?>" data-price="<?= $item->price ?>">
                                            <?= Html::button('<i class="fa fa-shopping-cart"></i>', [
                                                'class' => 'btn btn-success js-buy',
                                                'data-loading-text' => "<i class='fa fa-circle-o-notch fa-spin'></i>",
                                            ]); ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </form>
                    </div>

                    <?php
                    if ($col == 4) {
                        $col = 0;
                        ?>
                        </div>
                        <?php
                    }
                }
            }
            ?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxErrorModal",
    'header' => 'Ошибка',
    'bodyOptions' => ['id' => 'ajaxErrorModalBody', 'class' => 'modal-body'],
    "footer"=>"",// always need it for jquery plugin
])?>
    <p>
    <div id="error-alert" class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
    </div>
    </p>
<?php Modal::end(); ?>
<?php

$redirectUrl = Url::to(['/dispatch-regist/index']);
$buyUrl = Url::to(['/trade/buy']);
$script = <<< JS
    $(function () {
        $('.shop-card-full-description').equalHeights();
        $('.item').on('click', '.js-buy', function () {
            var button = $(this);
            var id = $(this).closest('div').data('key');
            var name = $(this).closest('div').data('name');
            var price = $(this).closest('div').data('price');
            var count = $('.js-count-' + id).val();
            price = price * count;
            var data = $('#form-' + id).serialize();

            // setTimeout(function(){
                
              // $(this).off('click', function(event){
              //     console.log(event);
              //    
              //     $(".item-accounts").each(function(e) {
              //           e.unbind();
              //     });
              // });
            // }, 1000);
            
            if (confirm("Купить " + count + " " + name + " за " + price + " рублей?")) {

                $.ajax({
                    url: '{$buyUrl}',
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    beforeSend: function () {
                        button.button('loading');
                        $('#background-wait').show();
                    },
                    success: function (json) {
                        $('#background-wait').hide();
                        if (json.buy) {
                            $('.js-alert-' + id).html(json.reason);
                            $('.js-alert-' + id).html(json.reason);
                            $('.count-item-' + id).html(json.count);
                            $('.js-alert-' + id).removeClass('text-danger').addClass('text-success');
                            $('.js-price-' + id).removeClass('text-danger').addClass('text-success');
                        } else {
                            $('.js-alert-' + id).html(json.reason);
                            $('.js-alert-' + id).removeClass('text-success').addClass('text-danger');
                            $('.js-price-' + id).removeClass('text-success').addClass('text-danger');

                        }
                        
                        button.button('reset');
                        // window.location = '$redirectUrl';
                    }
                });
            }
            ;
        });
        $('.item-accounts').on('click', '.js-buy', function () {
                var button = $(this);
                var id = $(this).closest('div').data('key');
                var name = $(this).closest('div').data('name');
                var price = $(this).closest('div').data('price');
                var count = $('.js-count-' + id).val();
                price = price*count;
                var data = $('#form-' + id).serialize();
    
                if (confirm('Купить ' + count + ' ' + name + ' за ' + price + ' рублей?')) {
                    $.ajax({
                        url: '{$buyUrl}',
                        type: 'POST',
                        data: data,
                        dataType: 'json',
                        // xhrFields: {
                        //     responseType: 'blob'
                        // },
                        beforeSend: function () {
                            button.button('loading');
                            $('#background-wait').show();
                        },
                        success: function (responseData, textStatus, xhr) {
                            if(responseData.errorText == ''){
                                window.location = '$redirectUrl';
                            } else {
                                $('#error-alert').text(responseData.errorText);
                                $('#ajaxErrorModal').modal();
                            }
                        },
                        error: function(xhr, textStatus, errorThrown){
                            if(xhr.status == 402)
                            {
                                alert('Недостаточно средств');
                            }
                            if(xhr.status == 400)
                            {
                                alert('Товара нет в наличии');
                            }
                        },
                    }).always(function(){
                        button.button('reset');
                        // window.location = '$redirectUrl';
                        
                        // $(this).unbind('click');
                        // console.log($(this));
                    });
                    // jQuery._data( $(this), "events" );
                }
            });
        
            // setTimeout(function(){
                // console.log($('.item-accounts').event('click'));
            // },1000);
          // $(".item-accounts").off('click', function(event){
          //     console.log(event);
          //    
          //     $(".item-accounts").each(function(e) {
          //           e.unbind();
          //     });
          // });
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

