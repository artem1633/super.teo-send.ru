<?php

use app\models\CompanySettings;
use app\models\DailyReport;
use yii\helpers\Html,
    yii\helpers\Url,
    yii\widgets\Breadcrumbs,
    app\components\helpers\FunctionHelper,
    app\models\DispatchRegist,
    skeeks\widget\highcharts\Highcharts;
use yii\widgets\Pjax;

$this->title = 'Показатели';
Yii::$app->user->identity->isSuperAdmin() ? $this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['/indicators']] : null;
$this->params['breadcrumbs'][] = $company->company_name;


?>
    <div class="row">
        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="widget widget-warning widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-file"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?= $company->getCountAccounts() ?></div>
                    <div class="widget-title">Количество аккаунтов</div>
                    <!-- <div class="widget-subtitle">That visited our site today</div> -->
                </div>
                <div class="widget-controls">
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="widget widget-warning widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-files-o"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?= $company->getCountSendedMessages() ?></div>
                    <div class="widget-title">Отправлено сообщений</div>
                </div>
                <div class="widget-controls">
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="widget widget-warning widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-envelope"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?= $generalReport[0] ?></div>
                    <div class="widget-title">Скорость рассылки</div>
                </div>
                <div class="widget-controls">
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="widget widget-warning widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-send"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?= $generalReport[1] ?></div>
                    <div class="widget-title">Активных получателей, % </div>
                </div>
                <div class="widget-controls">
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
                </div>
            </div>
        </div>

    </div>

       <?= \app\widgets\Dashboard::widget(['companyId' => $company_id]) ?>