<?php

use yii\helpers\Html,
    yii\helpers\Url,
    app\components\helpers\FunctionHelper,
    kartik\grid\GridView,
    skeeks\widget\highcharts\Highcharts;

$this->title = 'Показатели';
$this->params['breadcrumbs'][] = $this->title;

//$report[1] = [254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233,
//    254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233,
//    254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233,
//    254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233,
//    254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233,
//    254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233,
//    254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233,
//    254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233,
//    254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233,
//    254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233, 254, 234, 122, 233, 567, 233, 122, 67, 233,
//    ];

?>
<div class="row">
    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="widget widget-warning widget-item-icon">
            <div class="widget-item-left">
                <span class="fa fa-file"></span>
            </div>
            <div class="widget-data">
                <div class="widget-int num-count"><?= FunctionHelper::getCountAccounts() ?></div>
                <div class="widget-title">Количество аккаунтов</div>
                <!-- <div class="widget-subtitle">That visited our site today</div> -->
            </div>
            <div class="widget-controls">
                <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="widget widget-warning widget-item-icon">
            <div class="widget-item-left">
                <span class="fa fa-files-o"></span>
            </div>
            <div class="widget-data">
                <div class="widget-int num-count"><?= FunctionHelper::getCountSendedMessages() ?></div>
                <div class="widget-title">Отправлено сообщений</div>
            </div>
            <div class="widget-controls">
                <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="widget widget-warning widget-item-icon">
            <div class="widget-item-left">
                <span class="fa fa-envelope"></span>
            </div>
            <div class="widget-data">
                <div class="widget-int num-count"><?= $generalReport[0] ?></div>
                <div class="widget-title">Скорость рассылки</div>
            </div>
            <div class="widget-controls">
                <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="widget widget-warning widget-item-icon">
            <div class="widget-item-left">
                <span class="fa fa-send"></span>
            </div>
            <div class="widget-data">
                <div class="widget-int num-count"><?= $generalReport[1] ?></div>
                <div class="widget-title">Активных получателей, % </div>
            </div>
            <div class="widget-controls">
                <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title">Сообщения</h3>
            </div>
            <div class="panel-body" style="min-height: 200px;padding-bottom: 15px">
                <?php
                echo Highcharts::widget([
                    'options' => [
                        'chart' => [
                            'height' => 225,
                            'type' => 'line',
                            'zoomType' => 'xy',
                        ],
                        'title' => false,//['text' => 'Всего отправлено сообщений'],
                        //'subtitle' => ['text' => 'Notice the difference between a 0 value and a null point'],
                        'plotOptions' => [
                            'column' => ['depth' => 25],
                            'line' => [
                                'dataLabels' => [
//                                    'enabled' => true
                                ]
                            ],
                            'enableMouseTracking' => false,
                        ],
                        'xAxis' => [
                            'categories' => $report[0],
                            'labels' => [
                                'skew3d' => true,
                                'style' => ['fontSize' => '10px'],
                                'step' => 10,
                            ]
                        ],
                        'yAxis' => [
                            'title' => ['text' => null]
                        ],
                        'series' => [
                            ['name' => 'Сообщений отправлено', 'data' => $report[1]],
//                            ['name' => 'Прочитано', 'data' => $report[2]],
//                            ['name' => 'Ответило', 'data' => $report[3]],
                            ['name' => 'Заинтересовано', 'data' => $report[4]],
//                            ['name' => 'Продажи', 'data' => $report[5]],
                            ['name' => 'Отказы', 'data' => $report[6]],
                        ],
                        'credits' => ['enabled' => false],
                        'legend' => ['enabled' => true],
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>


</div>

<?= $this->render('@app/views/dispatch/report', ['stts' => $stts]) ?>