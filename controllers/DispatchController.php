<?php

namespace app\controllers;

use app\components\Calculations;
use app\components\helpers\FunctionHelper;
use app\models\AccountingReport;
use app\models\Bots;
use app\models\calculation\Calculation;
use app\models\Companies;
use app\models\CompanySettings;
use app\models\DailyReport;
use app\models\DataRecipient;
use app\models\Dispatch\DispatchSearch;
use app\models\DispatchConnect;
use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\DispatchStatusExport;
use app\models\template\DispatchTemplateSettings;
use app\models\Users;
use app\modules\api\controllers\ClientController;
use app\modules\api\controllers\VkController;
use app\models\TemplateMessages;
use PHPExcel;
use Yii;
use app\models\Dispatch;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\data\Sort;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;
use app\models\textGeneration\TextGeneration;
use app\modules\api\controllers\DispatchController as ApiDispatchController;

/**
 * DispatchController implements the CRUD actions for Dispatch model.
 */
class DispatchController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            // 'permissions' => [
            //     'class' => \app\filters\PermissionsFilter::class,
            //     'rules' => [
            //         [
            //             'actions' => 'view',
            //             'roles' => Users::USER_TYPE_SUPER_ADMIN,
            //         ],
            //     ],
            // ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Dispatch models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DispatchSearch();

        if (Yii::$app->user->identity->isSuperAdmin()) {
            $dataProvider = new ActiveDataProvider([
                'query' => Dispatch::find(),
            ]);
            $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];
            $dataProvider->pagination = ['pageSize' => 40];
            $stts = DispatchStatus::find()->groupBy(['dispatch_id'])->all();

            $searchModel->load(Yii::$app->request->get());

            $dataProvider->query->andFilterWhere(['like', 'name', $searchModel->name]);
            $dataProvider->query->andFilterWhere(['status' => $searchModel->status]);
            $dataProvider->query->andFilterWhere(['company_id' => $searchModel->company_id]);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'stts' => $stts
            ]);
        } else {
            $dataProvider = new ActiveDataProvider([
//                'query' => Dispatch::find()->where(['company_id' => Yii::$app->user->getId()]),
                'query' => Dispatch::find()->where(['company_id' => FunctionHelper::getCompanyId()]),
            ]);
            $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];
            $dataProvider->pagination = ['pageSize' => 40];
            $stts = DispatchStatus::find()->where(['company_id' => FunctionHelper::getCompanyId()])->groupBy(['dispatch_id'])->all();

            $searchModel->load(Yii::$app->request->get());

            $dataProvider->query->andFilterWhere(['like', 'name', $searchModel->name]);
            $dataProvider->query->andFilterWhere(['status' => $searchModel->status]);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'stts' => $stts
            ]);
        }

    }

    /**
     * Displays a single Dispatch model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Dispatch #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Edit', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    public function actionCalculation()
    {
        $request = Yii::$app->request;
        $model = new Calculation();

        if($model->load($request->get()) && $model->calculate())
        {
            $sort = new Sort([
                'attributes' => [
                    'number',
                    'speed',
                    'day',
                    'all_akk',
                    'akk',
                    'price_by_message',
                    'total_price',
                    'sum' => [
                        'asc' => ['sum' => SORT_ASC],
                        'desc' => ['sum' => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                ],
            ]);
            $dataProvider = new ArrayDataProvider([
                'allModels' => $model->calculatedData,
                'sort' => $sort,
                'pagination' => false,
            ]);
            return $this->render('calculation', [
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            return $this->render('calculation', [
                'model' => $model,
                'dataProvider' => null,
            ]);
        }
    }

    public function actionTextGeneration()
    {
        $request = Yii::$app->request;
        $model = new TextGeneration();

        if($model->load($request->post()) && $model->generate())
        {
            $dataProvider = new ArrayDataProvider([
                'allModels' => $model->generatedText,
                'pagination' => false,
            ]);
            return $this->render('text-generation', [
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            return $this->render('text-generation', [
                'model' => $model,
                'dataProvider' => null,
            ]);
        }
    }

    public function actionStrTest($line)
    {
        if(stripos($line, 'vk.com/'))
        {
            if(stripos($line, 'vk.com/id'))
            {
                $line = explode('vk.com/id', $line)[1];
            } else {
                $line = explode('vk.com/', $line)[1];
            }
        } else if(strripos($line, 'id') === 0){
            $line = substr($line, 2);
        }

        echo $line;
    }

    /**
     * Creates a new Dispatch model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $request = Yii::$app->request;
        $model = new Dispatch;
        $templateModel = new DispatchTemplateSettings();
        $maxDispatch = $model->getMaxCountDispatch();
        $tags = ArrayHelper::map(TemplateMessages::find()->where(['or', ['company_id' => FunctionHelper::getCompanyId()], ['company_id' => 1]])->orderBy('company_id desc')->all(), 'title', 'tag');
        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->getCountDispatch() >= $maxDispatch && !Yii::$app->user->identity->isSuperAdmin()) {
                return [
                    'title' => "Вы создали максимальное разрешенное количество рассылок!",
                    'content' => 'Ваш тарифный план предусматривает создание ' . $maxDispatch . ' рассылок',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }

            if ($request->isGet) {
                return [
                    'title' => "Создать новую рассылку. Шаг 1 из 4",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'templateModel' => $templateModel,
                        'tags' => $tags,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', [
                            'class' => 'btn btn-primary saveBtn hidden',
                            'type' => "submit",
                        ]) . Html::button('Далее', [
                            'id' => '1',
                            'class' => 'btn btn-primary nextBtn',
                            'onclick' => 'step();',
                        ])


                ];
            } else if ($model->load($request->post()) && $model->save()) {

                $model->company_id = FunctionHelper::getCompanyId();

                foreach ($request->post()['Dispatch']['accounts'] as $value) {

                    $dispatchRegist = DispatchRegist::findOne($value);
                    if($dispatchRegist->status == DispatchRegist::STATUS_SLEEPING){
                        $dispatchRegist->status = 'ok';
                        if($dispatchRegist->proxy == null){
                            $proxy = $dispatchRegist->getOneAvailableProxy();
                            if($proxy != null)
                            {
                                $dispatchRegist->proxy = $proxy->id;
                            }
                        }
                    }

                    if($model->use_auto_registr == 1 && $model->auto_regists_template_id != null)
                    {
                        $dispatchRegist->template_id = $model->auto_regists_template_id;
                        $dispatchRegist->template_process = 1;
                        $dispatchRegist->auto_view = 0;
                    }

                    $dispatchRegist->save();

                    $projectsUser = new DispatchConnect();
                    $projectsUser->dispatch_id = $model->id;
                    $projectsUser->company_id = $model->company_id;
                    $projectsUser->dispatch_regist_id = $value;
                    $projectsUser->save();

                }

                $model->data = date("Y-m-d H:i:s");
                /*
                 * status can be
                 * wait - в очереди
                 * job - В работе
                 * finish - Отправленно
                 */
                $model->status = DispatchStatus::STATUS_HANDLE;
                $model->save();

                $model->file = UploadedFile::getInstance($model, 'file');
                $dispatchStatusCount = 0; // Счетчик добавлений получателя к аккаунту
                if ($model->file) {

                    if ($model->upload()) {
                        if ($file = fopen($model->filename, "r")) {
                            $counter = 0;
                            while (!feof($file) && $counter < 5000) {
                                $line = fgets($file);
                                if ($line != null) {
                                    FunctionHelper::addRecipient($line, $model);
                                    $dispatchStatusCount++;
                                }
                                $counter++;
                            }
                            FunctionHelper::saveDataRecipient($model, $file);
                            fclose($file);
                            unlink($file);
                        }
                    }
                }

                if ($model->text_data) {

                    $lines = explode("\n", $model->text_data);

                    $counter = 0;

                    foreach ($lines as $line) {
                        if($counter >= 5000)
                            break;

                        if ($line != null) {
                            FunctionHelper::addRecipient($line, $model);
                            $dispatchStatusCount++;
                        }
                        $counter++;
                    }

                    // FunctionHelper::saveDataRecipient($model, $model->text_data);
                }

                if ($model->recived_data) {
                    $recived_data = Yii::$app->session->get(Dispatch::USERS_SESSION_KEY);
                    $lines = explode(PHP_EOL, $recived_data);
                    Yii::$app->session->remove(Dispatch::USERS_SESSION_KEY);

                    $counter = 0;

                    foreach ($lines as $line) {
                        if($counter >= 5000)
                            break;

                        if ($line != null) {
                            FunctionHelper::addRecipient($line, $model);
                            $dispatchStatusCount++;
                        }

                        $counter++;
                    }

                    FunctionHelper::saveDataRecipient($model, $recived_data);
                }

                if ($model->select_data) {
                    $file_data = DataRecipient::findOne($model->select_data);
                    if ($file = fopen($file_data->file, "r")) {
                        $counter = 0;
                        while (!feof($file) && $counter < 5000) {
                            $line = fgets($file);
                            if ($line != null) {
                                FunctionHelper::addRecipient($line, $model);
                                $dispatchStatusCount++;
                            }
                            $counter++;
                        }
                        fclose($file);
                    }
                }

                $countMessages = DispatchStatus::find()->where(['dispatch_id' => $model->id])->count();
                $company = CompanySettings::findOne(['company_id' => $model->company_id]);
                $priceMessage = $company->getPriceByMessage();
                $generalBalance = $company->companyModel->general_balance;

                $dispatch_ammount = floatval($countMessages * $priceMessage);
                //$company->companyModel->general_balance >= $dispatch_ammount
                if (($generalBalance >= $priceMessage)
                    || ($generalBalance >= $dispatch_ammount)
                ) {

                    $danger = null;

                    if (($generalBalance >= $priceMessage)
                        && ($generalBalance < $dispatch_ammount)
                    ) {
                        $danger = '<span class="text-warning">Ваша рассылка будет обработана частично!</span><br/>';
                    }
                }
                if ($generalBalance <= 0 || $generalBalance < $priceMessage) {
                    $danger = '<span class="text-danger">Для выполнения выполнения рассылки пополните Ваш баланс</span><br/>';
                }

                $templateModel->dispatch = $model;
                if($templateModel->load($request->post()) && $model->auto_regists_template_id == null) {
                    $templateModel->makeTemplate();
                }

                // Проверяем на возможную попытку подставить другие значения в переменные
                $calculationResult = Calculations::defaultCalculation($dispatchStatusCount);
                $akks = array_values(ArrayHelper::map($calculationResult, 'akk', 'akk'));
                $speeds = array_values(ArrayHelper::map($calculationResult, 'speed', 'speed'));
                if((in_array($model->accounts_count, $akks) && in_array($model->day_speed, $speeds)) == false)
                {
                    if(count($akks) > 0){
                        $model->accounts_count = $akks[0];
                    } else {
                        $model->accounts_count = null;
                    }

                    if(count($speeds) > 0){
                        $model->day_speed = $speeds[0];
                    } else {
                        $model->day_speed = null;
                    }

                    $model->save(false);
                }

                // Оплачиваем рассылку
                $companySettings = FunctionHelper::getCompanySettings();
                $company = FunctionHelper::getCompanyModel();
                $price = $companySettings->getPriceByMessage() * $dispatchStatusCount;
                $company->general_balance -= $price;
                $company->save(false);

                $accountReport = new AccountingReport([
                    'company_id' => $company->id,
                    'operation_type' => AccountingReport::TYPE_DISPATCH_PAYED,
                    'amount' => $price,
                    'description' => 'Списание средств за рассылку "' . $model->name . '". Отправлено ' . $dispatchStatusCount . ' сообщений',
                ]);
                $accountReport->save(false);

                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Создать новый",
                    'content' => $danger . '<span class="text-success">Успешно создан</span>',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Создать еще', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => "Создать новый",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'templateModel' => $templateModel,
                        'tags' => $tags,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {

                return $this->render('create', [
                    'model' => $model,
                    'templateModel' => $templateModel,
                    'tags' => $tags,
                ]);
            }
        }
    }

    public function actionExcel()
    {
        // Создаем объект класса PHPExcel
        $xls = new PHPExcel();
        // Устанавливаем индекс активного листа
        $xls->setActiveSheetIndex(0);
        // Получаем активный лист
        $sheet = $xls->getActiveSheet();
        // Подписываем лист
        $sheet->setTitle('Таблица умножения');

        $sheet->setCellValue('A1', 'ПРивет');
        $sheet->setCellValue('A2', 'Как дела');

        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header ( "Cache-Control: no-cache, must-revalidate" );
        header ( "Pragma: no-cache" );
        header ( "Content-type: application/vnd.ms-excel" );
        header ( "Content-Disposition: attachment; filename=report.xls" );

        $objWriter = new \PHPExcel_Writer_Excel5($xls);
        $objWriter->save('php://output');
    }

    public function actionReportExcel($id)
    {
        $model = $this->findModel($id);

        $dates = DailyReport::getDates($model->company_id, $model->id);

        $report = DailyReport::getReportBySendMesasges($model->id, $model->company_id, $dates, 1);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $report,
        ]);

        // Создаем объект класса PHPExcel
        $xls = new PHPExcel();
        // Устанавливаем индекс активного листа
        $xls->setActiveSheetIndex(0);
        // Получаем активный лист
        $sheet = $xls->getActiveSheet();
        // Подписываем лист
        $sheet->setTitle('Таблица умножения');

        $sheet->setCellValue('A1', 'Дата');
        $sheet->setCellValue('B1', 'Отправлено (Всего: 0)');
        $sheet->setCellValue('C1', 'Заинтересовались (Всего: 0)');
        $sheet->setCellValue('D1', 'Заинтересовались (%) (Всего: 0%)');
        $sheet->setCellValue('E1', 'Купило (Всего: 0)');
        $sheet->setCellValue('F1', 'Купило (%) (Всего: 0%)');
        $sheet->setCellValue('G1', 'Отказалось (Всего: 0)');
        $sheet->setCellValue('H1', 'Отказалось (%) (Всего: 0%)');

        $sheet->getColumnDimension('A')->setWidth(25);
        $sheet->getColumnDimension('B')->setWidth(25);
        $sheet->getColumnDimension('C')->setWidth(25);
        $sheet->getColumnDimension('D')->setWidth(25);
        $sheet->getColumnDimension('E')->setWidth(25);
        $sheet->getColumnDimension('F')->setWidth(25);
        $sheet->getColumnDimension('G')->setWidth(25);
        $sheet->getColumnDimension('H')->setWidth(25);

        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header ( "Cache-Control: no-cache, must-revalidate" );
        header ( "Pragma: no-cache" );
        header ( "Content-type: application/vnd.ms-excel" );
        header ( "Content-Disposition: attachment; filename=report.xls" );

        $counter = 2;
        foreach ($dataProvider->models as $model) {

            $sheet->setCellValue('A'.$counter, $model['date']);
            $sheet->setCellValue('B'.$counter, $model['messages']);
            $sheet->setCellValue('C'.$counter, $model['interest']);
            $sheet->setCellValue('D'.$counter, $model['interest_per']);
            $sheet->setCellValue('E'.$counter, $model['buy']);
            $sheet->setCellValue('F'.$counter, $model['buy_per']);
            $sheet->setCellValue('G'.$counter, $model['refuse']);
            $sheet->setCellValue('H'.$counter, $model['refuse_per']);

            $counter++;
        }

        $objWriter = new \PHPExcel_Writer_Excel5($xls);
        $objWriter->save('php://output');
    }

    public function actionStatusExport($id)
    {
        $request = Yii::$app->request;
        $dispatch = $this->findModel($id);

        $model = new DispatchStatusExport();
        $model->dispatchId = $dispatch->id;

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post())){
            $path = $model->export();


            return [
                'title' => 'Экспорт',
                'content' => '<span class="text-success">Файл сформирован</span>',
                'footer' => Html::button('Скачать',['class'=>'btn btn-success btn-block','onclick'=>"
                  var element = document.createElement('a');
                  element.setAttribute('href', 'export.txt');
                  element.setAttribute('download', 'Экспорт.txt');
                
                  element.style.display = 'none';
                  document.body.appendChild(element);
                
                  element.click();
                
                  document.body.removeChild(element);
                "]),
            ];

        } else {
            return [
                'title' => 'Экспорт',
                'content' => $this->renderAjax('status-export', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Экспорт',['class'=>'btn btn-primary btn-block','type'=>"submit"]),
            ];

        }
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function actionBackToHandle($id)
    {
        $model = $this->findModel($id);
        $model->status = DispatchStatus::STATUS_HANDLE;
        $model->save(false);

        return $this->redirect(['index']);
    }

    /**
     * @return string
     */
    public function actionShow()
    {

        if (isset($_GET['all'])) {
            $id = $_GET['all'];
            $dataProvider = new ActiveDataProvider([
                'query' => DispatchStatus::find()->where(['dispatch_id' => $id]),
            ]);
            $dataProvider->pagination = ['pageSize' => 40];
            return $this->render('/dispatch-status/index', [
                'dataProvider' => $dataProvider,
            ]);
        }
        if (isset($_GET['send'])) {
            $id = $_GET['send'];
            $dataProvider = new ActiveDataProvider([
                'query' => DispatchStatus::find()->where(['send' => true, 'dispatch_id' => $id]),
            ]);
            $dataProvider->pagination = ['pageSize' => 40];
            return $this->render('/dispatch-status/index', [
                'dataProvider' => $dataProvider,
            ]);
        }


    }

    /**
     * @return string
     */
    public function actionReport()
    {
        if (Yii::$app->user->identity->isSuperAdmin()) {
            $stts = DispatchStatus::find()->groupBy(['dispatch_id'])->all();

            return $this->render('report', compact('stts'));
        } else {
            $stts = DispatchStatus::find()->where(['company_id' => FunctionHelper::getCompanyId()])->groupBy(['dispatch_id'])->all();

            return $this->render('report', compact('stts'));
        }
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function actionKill($id)
    {
        DispatchStatus::deleteAll('`account_id` IS NULL AND `dispatch_id` = :id',[':id' => $id]);

        return $this->redirect(['index']);
    }

    /**
     * @return mixed
     */
    public function actionFaster()
    {
        $stts = Dispatch::find()->where(['id' => $_GET['id']])->one();

        $countMessages = DispatchStatus::find()->where(['dispatch_id' => $_GET['id']])->count();
        $company = CompanySettings::findOne(['company_id' => $stts->company_id]);
        $priceMessage = $company->getPriceByMessage();
        $generalBalance = $company->companyModel->general_balance;
        $dispatch_ammount = floatval($countMessages * $priceMessage);
        //$company->companyModel->general_balance >= $dispatch_ammount
        // if (($generalBalance >= $priceMessage)
        //     || ($generalBalance >= $dispatch_ammount)
        //     && $stts != null
        //     && ($_GET['status'] == 'job' || $_GET['status'] == 'wait')
        // ) {

        $companyModel = FunctionHelper::getCompanyModel();
        $stts->status = $_GET['status'];
        if($_GET['status'] == 'job'){
            ApiDispatchController::sendTelMessage('247187885', "Рассылка «{$stts->name}» компании «{$companyModel->company_name}» запущена");
        } else if($_GET['status'] == 'wait') {
            ApiDispatchController::sendTelMessage('247187885', "Рассылка «{$stts->name}» компании «{$companyModel->company_name}» остановлена");
        }
        if ($stts->update()) {
            return $this->actionIndex();
        }

        // }

        if ($generalBalance <= 0 || $generalBalance < $priceMessage) {
            Yii::$app->session->setFlash('danger', 'Для выполнения этого действия пополните Ваш баланс');
        }

        return $this->redirect(['index']);
    }

    /**
     * Updates an existing Dispatch model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Изменить  #" . $id,
                    'content' => $this->renderAjax('_edit_form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', [
                            'class' => 'btn btn-primary',
                            'type' => "submit",
                        ])
                ];
            } else if ($model->load($request->post()) && $model->save()) {

                $conns = DispatchConnect::findAll(['dispatch_id' => $model->id]);
                $arrdb = array();
                $arrpost = array();
                foreach ($conns as $con) {
                    array_push($arrdb, $con->dispatch_regist_id);
                }
                foreach ($request->post()['Dispatch']['accounts'] as $value) {
                    array_push($arrpost, $value);
                }
                $delete = array_diff($arrdb, $arrpost);
                $update = array_diff($arrpost, $arrdb);
                $new = array_intersect($arrdb, $arrpost);


                if ($model->text_data) {

                    $lines = explode("\n", $model->text_data);

                    $counter = 0;
                    foreach ($lines as $line) {
                        if ($counter >= 5000)
                            break;

                        if ($line != null) {
                            FunctionHelper::addRecipient($line, $model);
                        }
                        $counter++;
                    }

                    FunctionHelper::saveDataRecipient($model, $model->text_data);
                }

                if ($model->recived_data) {
                    $recived_data = Yii::$app->session->get(Dispatch::USERS_SESSION_KEY);
                    $lines = explode(PHP_EOL, $recived_data);
                    Yii::$app->session->remove(Dispatch::USERS_SESSION_KEY);

                    $counter = 0;

                    foreach ($lines as $line) {
                        if($counter >= 5000)
                            break;

                        if ($line != null) {
                            FunctionHelper::addRecipient($line, $model);
                        }
                        $counter++;
                    }

                    FunctionHelper::saveDataRecipient($model, $recived_data);
                }

                if ($model->select_data) {
                    $file_data = DataRecipient::findOne($model->select_data);
                    if ($file = fopen($file_data->file, "r")) {
                        $counter = 0;
                        while (!feof($file) && $counter < 5000) {
                            $line = fgets($file);
                            if ($line != null) {
                                FunctionHelper::addRecipient($line, $model);
                            }
                            $counter++;
                        }
                        fclose($file);
                    }
                }

                foreach ($delete as $r) {
                    $d = DispatchConnect::findOne(['dispatch_id' => $model->id, 'dispatch_regist_id' => $r]);
                    $d->delete();
                }

                foreach ($update as $u) {
                    $projectsUser = new DispatchConnect();
                    $projectsUser->dispatch_id = $model->id;
                    $projectsUser->dispatch_regist_id = $u;
                    $projectsUser->company_id = Users::getCompanyId($model->id);
                    $projectsUser->save();
                }


                $model->file = UploadedFile::getInstance($model, 'file');

                if ($model->file) {


                    if ($model->upload()) {

                        if ($file = fopen($model->filename, "r")) {
                            $counter = 0;
                            while (!feof($file) && $counter < 5000) {
                                $line = fgets($file);
                                if ($line != null) {
                                    FunctionHelper::addRecipient($line, $model);
                                }
                                $counter++;
                            }
                            FunctionHelper::saveDataRecipient($model, $file);
                            fclose($file);
                            unlink($file);
                        }

                        //                        if ($file = fopen("uploads/{$model->filename}", "r")) {
//                            while (!feof($file)) {
//                                $line = fgets($file);
//                                if ($line != null) {
//                                    FunctionHelper::addRecipient($line, $model);
//                                    $id = explode('/id', $line);
//                                    if ((int)$id[1] != 0) {
//                                        $status = new DispatchStatus();
//                                        $status->account_id = $id[1];
//                                        $status->company_id = Yii::$app->user->getId();
//                                        //$status->status = "wait";
//                                        $status->dispatch_id = $model->id;
//                                        $status->save();
//                                    }
//
//                                }
//
//                            }
//                            FunctionHelper::saveDataRecipient($model, $file);
//                            fclose($file);
//                            unlink($file);
//                        }

                    }


                }

                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Изменить  #" . $id,
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Редактировать', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Изменить #" . $id,
                    'content' => $this->renderAjax('_edit_form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Dispatch model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Dispatch model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * @param string $urlGroup Ссылка на группу
     * @return array|null|string
     */
    public function actionParsegroup()
    {

        $postUrl = Yii::$app->request->post('url');
        if ($postUrl) {
            $users = Dispatch::getGroupUsers($postUrl);
            $data = '';
            foreach ($users as $user) {
                $data = $data . 'http://vk.com/id' . $user . PHP_EOL;
            }

            Yii::$app->session->set(Dispatch::USERS_SESSION_KEY, $data);

            $response = [];

            if (count($users) > 0) {
                $response[0] = true;
                $response[1] = 'Получены данные на ' . count($users) . ' пользователей';
            } else {
                $response[0] = false;
                $response[1] = 'Ошибка получения данных. Проверьте прокси и URL адрес группы.';
            }

            return Json::encode($response);
        }
    }

    /**
     * @param $value
     * @param $filename
     */
    public static function object2file($value, $filename)
    {
        $f = fopen($filename, 'w');
        fwrite($f, $value);
        fclose($f);
    }

    /**
     * Finds the Dispatch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Dispatch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Dispatch::findOne($id)) !== null) {
            if($model->company_id != FunctionHelper::getCompanyId() && Yii::$app->user->identity->isSuperAdmin() == false){
                throw new NotFoundHttpException('The requested page does not exist.');
            }
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionResult()
    {
        $request = Yii::$app->request;
        $model = new Dispatch;

        if ($request->isAjax) {

            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($request->isGet) {

                Yii::warning($request->isGet, __METHOD__);

                return [
                    'title' => "Создать новую базу",
                    'content' => $this->renderAjax('result', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сгенерировать', [
                            'class' => 'btn btn-primary',
                            'type' => "submit",
                        ])

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                $model->company_id = FunctionHelper::getCompanyId();
                $resultBase = '';

                Yii::warning($model, __METHOD__);

                //Перебираем все рассылки
                foreach ($model->selected_dispatches as $dispatch) {

                    Yii::warning($dispatch, __METHOD__);

                    //Перебираем статусы
                    foreach ($model->selected_statuses as $status) {
                        Yii::warning($status, __METHOD__);
                        $tempBase = DispatchStatus::find()
                            ->where(['dispatch_id' => $dispatch])
                            ->andWhere(['status' => $status])->asArray()->all();

                        Yii::warning($tempBase, __METHOD__);

                        if (count($tempBase) > 0) {
                            //Переводим в строку
                            $resultBase .= implode(',', $tempBase);
                        }

                    }
                }

                if ($resultBase) {
//                   $resultBase = str_replace('Array', '', $resultBase);
                    $ids = explode(',', $resultBase);
                }

                Yii::warning($ids, __METHOD__);


                if ($ids) {
                    FunctionHelper::addRecipientFromArray($ids, $model);
                    $data = '';
                    foreach ($ids as $id) {
                        if ($id) {
                            $data = $data . 'https://vk.com/id' . $id . PHP_EOL;
                        }
                    }

                    FunctionHelper::saveDataRecipient($model, $data);

                    Yii::warning($data, __METHOD__);

                    $countUsers = DispatchStatus::find()->where(['dispatch_id' => $model->id])->count();
                    return[
                        'title' => "Создание новой базы",
                        'content' => 'База создана успешно. Добавлено ' . $countUsers . ' записей' ,
                        'forceReload' => '#crud-datatable-database-pjax',
                        'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Создать еще', ['result'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                    ];
                } else {
                    return [
                        'title' => "Создание новой базы",
                        'content' => $this->renderAjax('result', [
                            'model' => $model,
                        ]),
                        'footer' => Html::tag('div', 'Не найдено ни одного совпадения удовлетворяющего запросу', ['class' => 'alert alert-warning text-center']) .
                            Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сгенерировать', [
                                'class' => 'btn btn-primary',
                                'type' => "submit",
                            ])
                    ];
                }


            } else {

                Yii::warning('Не запись', __METHOD__);

                return [
                    'title' => "Создать новый",
                    'content' => $this->renderAjax('result', [
                        'model' => $model,
                    ]),
                ];
            }
        } else {

            Yii::warning('Не аякс', __METHOD__);

            return $this->redirect('index');
        }

//        return $this->render('result', ['model' => $model]);
    }

    public function actionChangeStatus($id)
    {
        $model = $this->findModel($id);
        $model->status = 'finish';
        $model->save();
        $this->redirect('index');
    }

    public function actionCreateBase()
    {
        $resultBase = [];
        $model = new Dispatch;


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->company_id = FunctionHelper::getCompanyId();
            Yii::warning($model, __METHOD__);

            //проходимся по всем файлам базы
            foreach ($model->selected_dispatches as $dispatch) {
                Yii::warning($dispatch, __METHOD__);
                //Перебираем статусы
                foreach ($model->selected_statuses as $status) {
                    Yii::warning($status, __METHOD__);
                    $tempBase = ArrayHelper::map(DispatchStatus::find()
                        ->where(['dispatch_id' => $dispatch])
                        ->andWhere(['status' => $status])->all(), 'id', 'account_id');
                    //Переводим в строку
                    $resultBase .= implode(',', $tempBase);
                }
            }
            $resultBase = str_replace('Array', '', $resultBase);
            $ids = explode(',', $resultBase);
            FunctionHelper::addRecipientFromArray($ids, $model);
            $data = '';
            foreach ($ids as $id) {
                $data = $data . 'http://vk.com/id' . $id . PHP_EOL;
            }

            FunctionHelper::saveDataRecipient($model, $data);

            Yii::warning($data, __METHOD__);

        }

        $this->redirect('result');
    }
}
