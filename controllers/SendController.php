<?php
/**
 * Created by PhpStorm.
 * User: Abbos
 * Date: 8/3/2018
 * Time: 10:31 PM
 */

namespace app\controllers;


use app\models\Dispatch;
use app\models\DispatchConnect;
use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\Settings;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;

class SendController extends Controller
{
    public function behaviors()
    {
        return [

            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }
    public function actionMessage()
    {

        $interval_min = Settings::find()->where(['key' => 'interval_in_minutes'])->one();
        $count_message = Settings::find()->where(['key' => 'kol_vo_soobshenii'])->one();
        $acc_message_count = Settings::find()->where(['key' => 'allowed_message_per_account'])->one();

        $messages = Dispatch::find()->where(['status'=>'job'])->all();


        foreach ($messages as $message){


            $connects = DispatchConnect::find()->where(['dispatch_id' => $message->id])->all();
            foreach ($connects as $conn){
                $account = DispatchRegist::find()->where(['id' => $conn->dispatch_regist_id])->one();


                if($account != null &&  (round($this->dateDifference($account->last_dispatch_time)/ 60) > $interval_min->value ) ){



                    $users = DispatchStatus::find()->where(['status' => 'wait','dispatch_id' => $message->id])->orderBy(['id' => SORT_ASC,])->limit($count_message->value)->all();
                    if(!$users){
                        $message->status = "finish";
                        $message->update();
                    }

                    $last = date("Y-m-d H:i:s");
                    foreach ($users as $user){

                        if( ($account->sended_message_count <  $acc_message_count->value) ){

                            $result = $this->send( (int)$user->account_id, $message->text, $account->token);
                            $result = json_decode($result,true);

                            if($result['response'] != null){
                                $user->status= "finish";
                                $user->data = date("Y-m-d H:i:s");
                                $user->send_account_id = $account->id;
                                $user->response_id = $result['response'];
                                $user->dispatch_id = $message->id;
                                $user->update();
                                $account->status = $result;
                                $account->sended_message_count++;
                                $account->last_dispatch_time = $user->data;
                                $account->update();
                                Yii::$app->session->setFlash('message_success', "Сообщение c аккаунто { $account->username } успешно отправлен");
                            }else{
                                $user->status = 'account_blocked';
                                $user->update();
                            }



                        }else{
                            $account->status = 'limit_exceeded';
                            $account->update();
                        }


                    }

                    if ( round($this->dateDifference($account->last_dispatch_time) / (60 * 60 * 24)) > 1) {

                        $account->sended_message_count = 0;
                        $account->status = 'ok';
                        $account->update();
                    }





                }else{


                    if ( round($this->dateDifference($account->last_dispatch_time) / (60 * 60 * 24)) > 1){

                        $account->sended_message_count = 0;
                        $account->status = 'ok';
                        $account->update();

                    }elseif ($account->sended_message_count >  $acc_message_count->value){

                        $account->status = 'limit_exceeded';
                        $account->update();

                    }else{
                        $account->status = 'interval_not_end';
                        $account->update();
                    }
                }

            }



        }

        return $this->redirect(['/dispatch-status/index']);


    }

    function send($userVk, $text, $token)
    {
        $url = 'https://api.vk.com/method/messages.send';
        $params = array(
            'user_id' => $userVk,    // Кому отправляем
            'message' => $text,   // Что отправляем
            'access_token' => $token, // Токен того кто отправляет
            'v' => '5.37',
        );

        // В $result вернется id отправленного сообщения
        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));

        return $result;
    }

    function dateDifference($date_1 )
    {

        $date_2 = date("Y-m-d H:i:s");

        $diff = strtotime($date_2) - strtotime($date_1);

        return $diff;

    }
    function sendTo()
    {
        $url = 'https://api.vk.com/method/messages.send';
        $params = array(
            'user_id' => 353225680,    // Кому отправляем
            'message' => "Testmessage",   // Что отправляем
            'access_token' => '3e61fd83ab1a950d6fa3cea8aa6182f262b3a7bb3027cbbc032fae5fc97a778036a81204136d8ccca7956',  // access_token можно вбить хардкодом, если работа будет идти из под одного юзера
            'v' => '5.37',
        );

        // В $result вернется id отправленного сообщения
        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));
    }

}