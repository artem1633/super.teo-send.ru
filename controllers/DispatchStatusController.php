<?php

namespace app\controllers;

use app\components\helpers\FunctionHelper;
use app\models\AnswerTemplate;
use app\models\DailyReport;
use app\models\Dispatch;
use app\models\DispatchConnect;
use app\models\DispatchRegist;
use app\models\Message;
use app\models\Proxy;
use app\models\Settings;
use app\modules\api\controllers\MessagesController;
use Yii;
use app\models\DispatchStatus;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * DispatchStatusController implements the CRUD actions for DispatchStatus model.
 */
class DispatchStatusController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DispatchStatus models.
     * @return mixed
     */
    public function actionIndex()
    {

        if(Yii::$app->user->identity->isSuperAdmin()){

            $dataProvider = new ActiveDataProvider([
                'query' => DispatchStatus::find(),
            ]);
            $dataProvider->pagination = ['pageSize' => 40];
            return $this->render('index', [
                'dataProvider' => $dataProvider,
            ]);

        }else{
            $dataProvider = new ActiveDataProvider([
                'query' => DispatchStatus::find()->where(['company_id' => Yii::$app->user->getId()]),
            ]);
            $dataProvider->pagination = ['pageSize' => 40];
            return $this->render('index', [
                'dataProvider' => $dataProvider,
            ]);
        }

    }


    /**
     * Displays a single DispatchStatus model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Статус Рассылок #".$id,
                'content'=>$this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
            ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    public function actionDialog($id)
    {

        //$app_id = $_GET['app_id'];

        /** @var DispatchRegist $account */
        /** @var DispatchStatus $clients */
        $clients = DispatchStatus::find()->where(['id' => $id])->one();
        $from_id = $clients->account_id;
        $app_id = $clients->send_account_id;
        $id = $clients->id;
        $account = DispatchRegist::find()->where(['id' => $clients->send_account_id])->one();
        $token = $account->token;
        $proxy = Proxy::findOne(['id' => $account->proxy]);
        $send_proxy = "tcp://{$proxy->ip_adress}:{$proxy->port}";



        //$results = getHistory($from_id,$token,$send_proxy);

        $myHistory = Message::find()->where([
            'dispatch_id' =>  $clients->dispatch_id,
            'dispatch_status_id' =>  $clients->id,
        ])->all();
        if ($myHistory) {
            $dispatch = Dispatch::find()->where(['id' => $clients->dispatch_id])->one();
        }

        $results = '';
//        if ($results['error']) {
//            var_dump($results['error']["error_msg"]);
//        }

        $answers = AnswerTemplate::find()->where(['company_id' => FunctionHelper::getCompanyId()])->all();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (Yii::$app->request->get('changeStatus')) {
                if(Yii::$app->request->post('update') && Yii::$app->request->post('status')){
                    $model = DispatchStatus::findOne(Yii::$app->request->post('update'));
                    $model->status = Yii::$app->request->post('status');
                    $model->new_message = false;
                    $report = new DailyReport([
                        'company_id' => FunctionHelper::getCompanyId(),
                        'account_id' => $model->id,
                        'sended_message_count' => 1,
                        'type' =>DailyReport::TYPE_USER_STATUS,
                        'status' => $model->status,
                        'dispatch_id' => $model->dispatch_id,
                    ]);
                    $report->save();
                    return [
                        'status' => $model->save(),
                    ];
                }
            }
        }

        return $this->render('dialog',compact('results','dispatch','myHistory','clients','from_id','answers','token','id','app_id', 'account'));
    }

    public function actionChangeStatus()
    {
        $id = trim($_SESSION['change_id']);

        $st = DispatchStatus::find()->where(['id' => $id])->one();
        $st->status = "refused";

        $st->update();

        Yii::$app->session->setFlash('status_was_changed', "Статус аккаунта https://vk.com/id{$st->account_id} изменено на Отказался!");
        return $this->actionIndex();
    }


    /**
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionPushsls($id)
    {
        //$id = trim($_SESSION['change_id']);

        /** @var DispatchStatus $st */
        $st = DispatchStatus::find()->where(['id' => $id])->one();
        $st->status_push_sales = true;

        /** @var Dispatch $dis */
        //$dis = Dispatch::find()->where(['id' => $st->dispatch_id])->one();

        $st->save();

        $url = "https://demo.teo-sales.ru/api/client/newclient/";

        $url=$url.'?'.http_build_query([
                'access' => FunctionHelper::getCompanySettings()->sales_access_token,
                'name' => $st->name,
                'phone' => $st->account_id,
                'utm' => 'send_avto',
                'city' => $st->city
            ]);
        // В $result вернется id отправленного сообщения
        $result = file_get_contents($url, false);

        Yii::$app->session->setFlash('status_was_changed', "Статус аккаунта https://vk.com/id{$st->account_id} push!");
        return $this->redirect(['dialog', 'id' => $id]);
    }

    public function actionSendToClient()
    {
        $from_id = trim($_POST['from_id']);
        $token = trim($_POST['token']);
        $message = $_POST['message'];
        $id = trim($_POST['id']);
        $app_id = trim($_POST['app_id']);

        // Взять аккаунт доступен для отправки
        /** @var DispatchRegist $account */
        $account = DispatchRegist::find()->where(['id' => $app_id])->one();
        $proxy = Proxy::findOne(['id' => $account->proxy]);
        $send_proxy = "tcp://{$proxy->ip_adress}:{$proxy->port}";


        $answers = AnswerTemplate::find()->where(['company_id' => \Yii::$app->user->getId()])->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        //$result = send($from_id, $message, $token,$send_proxy);
        $result = MessagesController::sendMessage($from_id, $message, $account, $send_proxy);
        //echo '222222222222 - ' .$result;


        if ($result == 1) {
            //echo '1111111111-  11';
            $connect = DispatchConnect::find()->where(['dispatch_regist_id' => $account->id])->one();
            $dispatch = Dispatch::find()->where(['id' => $connect->dispatch_id])->one();
            /** @var DispatchStatus $status */

            $status = DispatchStatus::findOne(['id' => $id]);
            $status->new_message = false;
            $status->check_bot = false;
            $status->response_id = $result['response'];
            $status->data = date("Y-m-d H:i:s");
            $status->update();
            $results = getHistory($from_id,$token,$send_proxy);

            /** @var Message $mesHistory */
            $mesHistory = new Message();
            $mesHistory->date_time = date("Y-m-d H:i:s");
            $mesHistory->company_id = $account->company_id;
            $mesHistory->dispatch_id = $status->dispatch_id;
            $mesHistory->dispatch_registr_id = $account->id;
            $mesHistory->dispatch_status_id = $status->id;
            if ($dispatch->text_data) $mesHistory->database_id = $dispatch->text_data;
            $mesHistory->text = $message;
            $mesHistory->from = Message::FROM_ACCOUNT;
            $mesHistory->save();



            Yii::$app->session->setFlash('status_info_success', "Сообщение отправлено!");

            return $this->redirect(['dialog', 'id' => $id]);
//                return $this->render('dialog',compact('results','from_id','answers','token','id','app_id'));

        } else {
            $results = getHistory($from_id,$token,$send_proxy);
            Yii::$app->session->setFlash('status_info', "Аккаунт получателя заблокирован!");

            return $this->redirect(['dialog', 'id' => $id]);
//                return $this->render('dialog',compact('results','from_id','answers','token','id','app_id'));
        }



    }


    /**
     * Creates a new DispatchStatus model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new DispatchStatus();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать новый",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Создать новый",
                    'content'=>'<span class="text-success">Create DispatchStatus success</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Создать новый",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing DispatchStatus model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "DispatchStatus #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('Сохранить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
            }else{
                return [
                    'title'=> "Update DispatchStatus #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing DispatchStatus model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing DispatchStatus model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the DispatchStatus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DispatchStatus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DispatchStatus::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

//    function getHistoryByToken($id,$token)
//    {
//        $url = 'https://api.vk.com/method/messages.getHistory';
//        $params = array(
//            'count' => '200',
//            'user_id' => $id,
//            'access_token' => $token,
//            'v' => '5.80'
//
//        );
//
//        // В $result вернется id отправленного сообщения
//        $result = file_get_contents($url, false, stream_context_create(array(
//            'http' => array(
//
//                'method'  => 'POST',
//                'header'  => 'Content-type: application/x-www-form-urlencoded',
//                'content' => http_build_query($params)
//            )
//        )));
//        $result = json_decode($result,true);
//        return $result;
//
//    }




}
