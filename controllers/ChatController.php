<?php

namespace app\controllers;

use app\components\helpers\FunctionHelper;
use app\models\bots\BotSearch;
use app\models\bots\DialogSearch;
use app\models\BotsDialogs;
use app\models\Companies;
use app\models\companies\CompaniesSearch;
use app\models\DailyReport;
use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\Rates;
use Yii;
use app\models\Bots;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * BotsController implements the CRUD actions for Bots model.
 */
class ChatController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionIndex()
    {

        //if (Yii::$app->user->identity->isSuperAdmin()) {


            return $this->render('index', [
                'company' => Companies::findOne($companyId),
                'report' => DailyReport::getReportBySendMesasges($companyId),
            ]);
//        } else {
//            throw new ForbiddenHttpException('Доступ запрещен');
//        }

    }
    /**
     * @return string
     * @throws \Exception
     */
    public function actionList()
    {
        //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $a = [
            'elements' =>
                [
                  'name' => 'Linda Gahleitner',
                  'img' => 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/1089577/contact1.jpg',
                  'online' => '16:48',
                      'messages' =>
                      [
                          [
                              'text' => 'Hallo! Was schenken wir unseren Eltern zu Weihnachten?',
                              'name' => 'Linda Gahleitner',
                              'time' => '16:01',
                              'type' => 'true',
                          ]
                      ],
                ],
                [
                  'name' => 'Linda Gahleitner',
                  'img' => 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/1089577/contact1.jpg',
                  'online' => '16:48',
                      'messages' =>
                      [
                          [
                              'text' => 'Hallo! Was schenken wir unseren Eltern zu Weihnachten?',
                              'name' => 'Linda Gahleitner',
                              'time' => '16:01',
                              'type' => 'true',
                          ]
                      ],
                ],


        ];
        echo json_encode($a);

    }

}
