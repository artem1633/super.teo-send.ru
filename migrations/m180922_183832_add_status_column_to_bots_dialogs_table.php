<?php

use yii\db\Migration;

/**
 * Handles adding status to table `bots_dialogs`.
 */
class m180922_183832_add_status_column_to_bots_dialogs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('bots_dialogs', 'status', $this->integer()->comment('Статус'));
        $this->createIndex(
            'idx-bots_dialogs-statuses',
            'bots_dialogs',
            'status'
        );

        $this->addForeignKey(
            'fk-bots_dialogs-statuses',
            'bots_dialogs',
            'status',
            'statuses',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-bots_dialogs-statuses',
            'bots_dialogs'
        );

        $this->dropIndex(
            'idx-bots_dialogs-statuses',
            'bots_dialogs'
        );

        $this->dropColumn('bots_dialogs', 'status');
    }
}
