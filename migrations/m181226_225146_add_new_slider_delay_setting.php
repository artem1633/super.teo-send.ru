<?php

use yii\db\Migration;

/**
 * Class m181226_225146_add_new_slider_delay_setting
 */
class m181226_225146_add_new_slider_delay_setting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'slider_delay',
            'label' => 'Время задержки слайдера (мс.)'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
