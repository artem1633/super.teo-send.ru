<?php

use yii\db\Migration;

/**
 * Handles adding template_id to table `dispatch_regist`.
 */
class m181216_110145_add_template_id_column_to_dispatch_regist_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch_regist', 'template_id', $this->integer()->comment('Шаблон автозаполнения'));

        $this->createIndex(
            'idx-dispatch_regist-template_id',
            'dispatch_regist',
            'template_id'
        );

        $this->addForeignKey(
            'fk-dispatch_regist-template_id',
            'dispatch_regist',
            'template_id',
            'auto_regists_templates',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-dispatch_regist-template_id',
            'dispatch_regist'
        );

        $this->dropIndex(
            'idx-dispatch_regist-template_id',
            'dispatch_regist'
        );

        $this->dropColumn('dispatch_regist', 'template_id');
    }
}
