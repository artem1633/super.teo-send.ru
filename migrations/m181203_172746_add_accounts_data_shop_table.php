<?php

use yii\db\Migration;

/**
 * Class m181203_172746_add_accounts_data_shop_table
 */
class m181203_172746_add_accounts_data_shop_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('shop', 'accounts_data', $this->text()->comment('Аккаунты на продажу'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('shop', 'accounts_data');

    }
}
