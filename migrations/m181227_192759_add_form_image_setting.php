<?php

use yii\db\Migration;

/**
 * Class m181227_192759_add_form_image_setting
 */
class m181227_192759_add_form_image_setting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'home_form_image',
            'label' => 'Изображение формы при входе'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
