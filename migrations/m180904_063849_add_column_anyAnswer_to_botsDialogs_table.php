<?php

use yii\db\Migration;

/**
 * Class m180904_063849_add_column_anyAnswer_to_botsDialogs_table
 */
class m180904_063849_add_column_anyAnswer_to_botsDialogs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('bots_dialogs', 'anyAnswer', $this->boolean()->defaultValue(false)->comment('Отвечать на любой вопрос'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180904_063849_add_column_anyAnswer_to_botsDialogs_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180904_063849_add_column_anyAnswer_to_botsDialogs_table cannot be reverted.\n";

        return false;
    }
    */
}
