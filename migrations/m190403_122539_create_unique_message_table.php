<?php

use yii\db\Migration;

/**
 * Handles the creation of table `unique_message`.
 */
class m190403_122539_create_unique_message_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('unique_message', [
            'id' => $this->primaryKey(),
            'dispatch_id' => $this->integer()->comment('Рассылка'),
            'text' => $this->binary()->comment('Текст'),
        ]);

        $this->createIndex(
            'idx-unique_message-dispatch_id',
            'unique_message',
            'dispatch_id'
        );

        $this->addForeignKey(
            'fk-unique_message-dispatch_id',
            'unique_message',
            'dispatch_id',
            'dispatch',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-unique_message-dispatch_id',
            'unique_message'
        );

        $this->dropIndex(
            'idx-unique_message-dispatch_id',
            'unique_message'
        );

        $this->dropTable('unique_message');
    }
}
