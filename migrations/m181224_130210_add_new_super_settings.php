<?php

use yii\db\Migration;

/**
 * Class m181224_130210_add_new_super_settings
 */
class m181224_130210_add_new_super_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'account_price',
            'label' => 'Стоимость аккаунта',
        ]);

        $this->insert('settings', [
            'key' => 'proxy_price',
            'label' => 'Стоимость прокси',
        ]);

        $this->insert('settings', [
            'key' => 'calculation_result_count',
            'label' => 'Количество результатов калькуляции',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return false;
    }
}
