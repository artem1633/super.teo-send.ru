<?php

use yii\db\Migration;

/**
 * Class m180824_104159_add_colum_dispatch_status
 */
class m180824_104159_add_colum_dispatch_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('dispatch_status','status_push_sales',  $this->boolean()->defaultValue(0)->comment('Отправили в слс'));

    }

    public function down()
    {
        $this->dropColumn('dispatch_status','status_push_sales');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180824_104159_add_colum_dispatch_status cannot be reverted.\n";

        return false;
    }
    */
}
