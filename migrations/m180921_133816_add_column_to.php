<?php

use yii\db\Migration;

/**
 * Class m180921_133816_add_column_to
 */
class m180921_133816_add_column_to extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch_status', 'age', $this->integer()->comment('Возраст'));
        $this->addColumn('dispatch_status', 'gender', $this->integer()->comment('Пол'));
        $this->addColumn('dispatch_status', 'city', $this->integer()->comment('Город'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dispatch_status', 'age');
        $this->dropColumn('dispatch_status', 'gender');
        $this->dropColumn('dispatch_status', 'city');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180921_133816_add_column_to cannot be reverted.\n";

        return false;
    }
    */
}
