<?php

use yii\db\Migration;

/**
 * Class m180805_143941_update_table_settings
 */
class m180805_143941_update_table_settings extends Migration
{



    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->execute("UPDATE settings SET `label` = 'Cуточный лимит' WHERE `id` = 8");


    }

    public function down()
    {
        echo "m180805_143941_update_table_settings cannot be reverted.\n";

        return false;
    }

}
