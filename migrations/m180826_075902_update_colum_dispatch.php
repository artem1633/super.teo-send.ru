<?php

use yii\db\Migration;

/**
 * Class m180826_075902_update_colum_dispatch
 */
class m180826_075902_update_colum_dispatch extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180826_075902_update_colum_dispatch cannot be reverted.\n";

        return false;
    }


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
       // ALTER TABLE `dispatch` CHANGE `text` `text` BLOB NULL DEFAULT NULL;
       // $this->alterColumn('dispatch','text', $this->bilob());
        $this->execute('ALTER TABLE `dispatch` CHANGE `text` `text` BLOB NULL DEFAULT NULL');
    }

    public function down()
    {
        $this->execute('ALTER TABLE `dispatch` CHANGE `text` `text` TEXT NULL DEFAULT NULL');
    }

}
