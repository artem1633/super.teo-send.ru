<?php

use yii\db\Migration;

/**
 * Class m190102_184509_add_dispatch_id_to_dispatch_regist_table
 */
class m190102_184509_add_dispatch_id_to_dispatch_regist_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch_regist', 'dispatch_id', $this->integer()->comment('Рассылка'));

        $this->createIndex(
            'idx-dispatch_regist-dispatch_id',
            'dispatch_regist',
            'dispatch_id'
        );

        $this->addForeignKey(
            'fk-dispatch_regist-dispatch_id',
            'dispatch_regist',
            'dispatch_id',
            'dispatch',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-dispatch_regist-dispatch_id',
            'dispatch_regist'
        );

        $this->dropIndex(
            'idx-dispatch_regist-dispatch_id',
            'dispatch_regist'
        );

        $this->dropColumn('dispatch_regist', 'dispatch_id');
    }
}
