<?php

use yii\db\Migration;

/**
 * Class m180830_173900_add_colum_dispatch_registr_notif_error
 */
class m180830_173900_add_colum_dispatch_registr_notif_error extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180830_173900_add_colum_dispatch_registr_notif_error cannot be reverted.\n";

        return false;
    }

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('dispatch_regist','error_notif',  $this->boolean()->defaultValue(0)->comment('Уведомление об ошибки'));
    }

    public function down()
    {
        $this->dropColumn('dispatch_regist','error_notif');
    }
}
