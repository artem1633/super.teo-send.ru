<?php

use yii\db\Migration;

/**
 * Class m180913_164620_create_tables
 */
class m180913_164620_create_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('message', [
            'id' => $this->primaryKey(),
            'date_time' => $this->dateTime()->null()->comment('Дата и время'),
            'dispatch_registr_id' => $this->integer()->null()->comment('Аккаунт'),
            'dispatch_status_id' => $this->integer()->null()->comment('Статус'),
            'dispatch_id' => $this->integer()->null()->comment('Рассылка'),
            'database_id' => $this->integer()->null()->comment('База'),
            'company_id' => $this->integer()->null()->comment('Компания'),
        ], $tableOptions);

        $this->createTable('auto_registr', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->null()->comment('Название'),
            'type' => $this->integer()->null()->comment('Тип'),
            'status' => $this->integer()->null()->comment('Статус'),
            'action' => $this->integer()->null()->comment('Действие'),
            'company_id' => $this->integer()->null()->comment('Компания'),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180913_164620_create_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180913_164620_create_tables cannot be reverted.\n";

        return false;
    }
    */
}
