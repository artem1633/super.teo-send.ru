<?php

use yii\db\Migration;

/**
 * Class m180804_185043_add_column_to_table_dispatch_connect
 */
class m180804_185043_add_column_to_table_dispatch_connect extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('dispatch_connect', 'company_id', $this->integer()->comment('Ид компании'));


    }

    public function down()
    {
        $this->dropColumn('dispatch_connect', 'company_id');


    }
}
