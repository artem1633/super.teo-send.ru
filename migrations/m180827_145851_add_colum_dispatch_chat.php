<?php

use yii\db\Migration;

/**
 * Class m180827_145851_add_colum_dispatch_chat
 */
class m180827_145851_add_colum_dispatch_chat extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180827_145851_add_colum_dispatch_chat cannot be reverted.\n";

        return false;
    }


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('dispatch','chat',  $this->integer()->comment('Чат для уведомлений'));
    }

    public function down()
    {
        $this->dropColumn('dispatch','chat');
    }

}
