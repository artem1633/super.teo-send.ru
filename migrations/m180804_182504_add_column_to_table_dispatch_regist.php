<?php

use yii\db\Migration;

/**
 * Class m180804_182504_add_column_to_table_dispatch_regist
 */
class m180804_182504_add_column_to_table_dispatch_regist extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('dispatch_regist', 'status', $this->string()->comment('Доступенли отправка сообщения')->defaultValue('ok'));

    }

    public function down()
    {
        $this->dropColumn('dispatch_regist', 'status');

    }
}
