<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `column_from_bots`.
 */
class m180829_120610_drop_column_from_bots_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('bots', 'dialogId');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180829_120610_drop_column_from_bots_table cannot be reverted.\n";

        return false;
    }
}
