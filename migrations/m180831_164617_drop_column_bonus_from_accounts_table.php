<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `dispatch_regist`.
 */
class m180831_164617_drop_column_bonus_from_accounts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('dispatch_regist', 'bonus');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180831_164617_drop_column_bonus_from_accounts_table cannot be reverted.\n";

        return false;
    }
}
