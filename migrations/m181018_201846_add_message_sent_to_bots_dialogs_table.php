<?php

use yii\db\Migration;

/**
 * Class m181018_201846_add_message_sent_to_bots_dialogs_table
 */
class m181018_201846_add_message_sent_to_bots_dialogs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('bots_dialogs', 'message_sent', $this->timestamp()->comment('Дата и время отправки последнего сообщения'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181018_201846_add_message_sent_to_bots_dialogs_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181018_201846_add_message_sent_to_bots_dialogs_table cannot be reverted.\n";

        return false;
    }
    */
}
