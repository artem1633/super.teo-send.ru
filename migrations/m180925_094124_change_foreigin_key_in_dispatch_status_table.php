<?php

use yii\db\Migration;

/**
 * Class m180925_094124_change_foreigin_key_in_dispatch_status_table
 */
class m180925_094124_change_foreigin_key_in_dispatch_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey(
            'fk-dispatch_status-send_account_id',
            'dispatch_status'
        );
        $this->addForeignKey(
            'fk-dispatch_status-send_account_id',
            'dispatch_status',
            'send_account_id',
            'dispatch_regist',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180925_094124_change_foreigin_key_in_dispatch_status_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180925_094124_change_foreigin_key_in_dispatch_status_table cannot be reverted.\n";

        return false;
    }
    */
}
