<?php

use yii\db\Migration;

/**
 * Handles the creation of table `applied_auto_registr`.
 */
class m181221_084845_create_applied_auto_registr_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('applied_auto_registr', [
            'id' => $this->primaryKey(),
            'dispatch_id' => $this->integer()->comment('Рассылка'),
            'dispatch_regist_id' => $this->integer()->comment('Аккаунт'),
            'template_id' => $this->integer()->comment('Шаблон'),
            'auto_registr_id' => $this->integer()->comment('Запись шаблона'),
            'status' => $this->string()->comment('Статус'),
            'comment' => $this->text()->comment('Комментарий'),
            'created_at' => $this->dateTime()
        ]);
        $this->addCommentOnTable('applied_auto_registr', 'Примененные записи шаблонов');

        $this->createIndex(
            'idx-applied_auto_registr-dispatch_id',
            'applied_auto_registr',
            'dispatch_id'
        );

        $this->addForeignKey(
            'fk-applied_auto_registr-dispatch_id',
            'applied_auto_registr',
            'dispatch_id',
            'dispatch',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-applied_auto_registr-dispatch_regist_id',
            'applied_auto_registr',
            'dispatch_regist_id'
        );

        $this->addForeignKey(
            'fk-applied_auto_registr-dispatch_regist_id',
            'applied_auto_registr',
            'dispatch_regist_id',
            'dispatch_regist',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-applied_auto_registr-template_id',
            'applied_auto_registr',
            'template_id'
        );

        $this->addForeignKey(
            'fk-applied_auto_registr-template_id',
            'applied_auto_registr',
            'template_id',
            'auto_regists_templates',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-applied_auto_registr-auto_registr_id',
            'applied_auto_registr',
            'auto_registr_id'
        );

        $this->addForeignKey(
            'fk-applied_auto_registr-auto_registr_id',
            'applied_auto_registr',
            'auto_registr_id',
            'auto_registr',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-applied_auto_registr-auto_registr_id',
            'applied_auto_registr'
        );

        $this->dropIndex(
            'idx-applied_auto_registr-auto_registr_id',
            'applied_auto_registr'
        );

        $this->dropForeignKey(
            'fk-applied_auto_registr-template_id',
            'applied_auto_registr'
        );

        $this->dropIndex(
            'idx-applied_auto_registr-template_id',
            'applied_auto_registr'
        );

        $this->dropForeignKey(
            'fk-applied_auto_registr-dispatch_regist_id',
            'applied_auto_registr'
        );

        $this->dropIndex(
            'idx-applied_auto_registr-dispatch_regist_id',
            'applied_auto_registr'
        );

        $this->dropForeignKey(
            'fk-applied_auto_registr-dispatch_id',
            'applied_auto_registr'
        );

        $this->dropIndex(
            'idx-applied_auto_registr-dispatch_id',
            'applied_auto_registr'
        );

        $this->dropTable('applied_auto_registr');
    }
}
