<?php

use yii\db\Migration;

/**
 * Class m180922_171101_add_countproxy_setting_to_settings_table
 */
class m180922_171101_add_countproxy_setting_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'max_proxy',
            'value' => 6,
            'label' => 'Количество аккаунтов на прокси',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180922_171101_add_countproxy_setting_to_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180922_171101_add_countproxy_setting_to_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
