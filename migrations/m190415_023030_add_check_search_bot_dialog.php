<?php

use yii\db\Migration;

/**
 * Class m190415_023030_add_check_search_bot_dialog
 */
class m190415_023030_add_check_search_bot_dialog extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('bots_dialogs', 'check_search', $this->boolean()->defaultValue(false)->comment('Точный поиск'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('bots_dialogs', 'check_search');
    }
}
