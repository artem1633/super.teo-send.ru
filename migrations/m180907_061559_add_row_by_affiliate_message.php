<?php

use yii\db\Migration;

/**
 * Class m180907_061559_add_row_by_affiliate_message
 */
class m180907_061559_add_row_by_affiliate_message extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('email_templates', ['key' => 'affiliate_program', 'key_ru' => 'Партнерская программа', 'body' => 'информация о партнерской программе', 'deletable' => 0]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180907_061559_add_row_by_affiliate_message cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180907_061559_add_row_by_affiliate_message cannot be reverted.\n";

        return false;
    }
    */
}
