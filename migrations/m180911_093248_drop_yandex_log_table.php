<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `yandex_log`.
 */
class m180911_093248_drop_yandex_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('yandex_log');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
