<?php

use yii\db\Migration;

/**
 * Class m180818_153002_add_column_to_table_dispatch_regist
 */
class m180818_153002_add_column_to_table_dispatch_regist extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('dispatch_regist', 'proxy', $this->integer()->comment('Прокси'));


    }

    public function down()
    {
        $this->dropColumn('dispatch_regist', 'proxy');

    }
}
