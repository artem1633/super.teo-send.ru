<?php

use yii\db\Migration;

/**
 * Class m181210_121348_add_new_admin_settings
 */
class m181210_121348_add_new_admin_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'account_create_enable',
            'value' => '1',
            'label' => 'Добавление аккаунта',
            'type' => \app\models\Settings::TYPE_CHECKBOX,
        ]);

        $this->insert('settings', [
            'key' => 'dispatch_create_enable',
            'value' => '1',
            'label' => 'Добавление рассылки',
            'type' => \app\models\Settings::TYPE_CHECKBOX,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
