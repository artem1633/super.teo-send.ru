<?php

use yii\db\Migration;

/**
 * Handles the creation of table `statuses`.
 */
class m180905_183239_create_statuses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('statuses', [
            'id' => $this->primaryKey(),
            'type' => $this->integer()->notNull()->defaultValue(1)->comment('Тип статуса'),
            'name' => $this->string()->null()->comment('Название статуса'),
            'color' => $this->string(25)->null()->comment('Стиль кнопки'),
            'sort' => $this->integer()->null()->comment('Сортировка'),
        ], $tableOptions);

        $this->insert('statuses', ['name' => 'Ожидает', 'type' => 1, 'color' => 'default', 'sort' => 1]);
        $this->insert('statuses', ['name' => 'В работе', 'type' => 1, 'color' => 'info', 'sort' => 2]);
        $this->insert('statuses', ['name' => 'Закончена', 'type' => 1, 'color' => 'success', 'sort' => 3]);
        $this->insert('statuses', ['name' => 'Непрочитанные сообщения', 'type' => 1, 'color' => 'warning', 'sort' => 4]);
        $this->insert('statuses', ['name' => 'Переписка', 'type' => 1, 'color' => 'primary', 'sort' => 5]);
        $this->insert('statuses', ['name' => 'Аккаунт заблокирован', 'type' => 1, 'color' => 'danger', 'sort' => 6]);

        $this->insert('statuses', ['name' => 'Переписка', 'type' => 2, 'color' => 'info', 'sort' => 1]);
        $this->insert('statuses', ['name' => 'Заинтересован', 'type' => 2, 'color' => 'warning', 'sort' => 2]);
        $this->insert('statuses', ['name' => 'Купил', 'type' => 2, 'color' => 'success', 'sort' => 3]);
        $this->insert('statuses', ['name' => 'Отказался', 'type' => 2, 'color' => 'danger', 'sort' => 4]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('statuses');
    }
}
