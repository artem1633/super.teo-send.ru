<?php

use yii\db\Migration;

/**
 * Class m180910_083213_add_column_to_rates_table
 */
class m180910_083213_add_column_to_rates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\app\models\Rates::tableName(), 'bonus', $this->decimal(10,2)->null()->comment('Бонус'));
        $this->addColumn(\app\models\Companies::tableName(), 'general_balance', $this->decimal(10,2)->null()->comment('Основной баланс'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180910_083213_add_column_to_rates_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180910_083213_add_column_to_rates_table cannot be reverted.\n";

        return false;
    }
    */
}
