<?php

use yii\db\Migration;

/**
 * Class m190408_105739_add_new_setting_warning_blocking_accounts_count
 */
class m190408_105739_add_new_setting_warning_blocking_accounts_count extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'warning_blocking_accounts_count',
            'value' => '3',
            'label' => 'Лимит блокировков за 24 часа',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
