<?php

use yii\db\Migration;

/**
 * Handles the creation of table `template_messages_content`.
 */
class m190402_120911_create_template_messages_content_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('template_messages_content', [
            'id' => $this->primaryKey(),
            'content' => $this->text()->comment('Содержание'),
            'template_message_id' => $this->integer()->comment('Шаблон сообщения'),
            'created_at' => $this->dateTime(),
        ]);
        $this->addCommentOnTable('template_messages_content', 'Содержание шаблона сообщения');

        $this->createIndex(
            'idx-template_messages_content-template_message_id',
            'template_messages_content',
            'template_message_id'
        );

        $this->addForeignKey(
            'fk-template_messages_content-template_message_id',
            'template_messages_content',
            'template_message_id',
            'template_messages',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-template_messages_content-template_message_id',
            'template_messages_content'
        );

        $this->dropIndex(
            'idx-template_messages_content-template_message_id',
            'template_messages_content'
        );

        $this->dropTable('template_messages_content');
    }
}
