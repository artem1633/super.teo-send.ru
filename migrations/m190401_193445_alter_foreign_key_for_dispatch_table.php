<?php

use yii\db\Migration;

/**
 * Class m190401_193445_alter_foreign_key_for_dispatch_table
 */
class m190401_193445_alter_foreign_key_for_dispatch_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190401_193445_alter_foreign_key_for_dispatch_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190401_193445_alter_foreign_key_for_dispatch_table cannot be reverted.\n";

        return false;
    }
    */
}
