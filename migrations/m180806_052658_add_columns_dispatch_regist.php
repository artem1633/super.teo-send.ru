<?php

use yii\db\Migration;

/**
 * Class m180806_052658_add_columns_dispatch_regist
 */
class m180806_052658_add_columns_dispatch_regist extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('dispatch_regist', 'bonus', $this->string()->comment('бонусный'));
        $this->addColumn('dispatch_regist', 'busy', $this->string()->comment('Занятость'));
    }

    public function down()
    {
        $this->dropColumn('dispatch_regist', 'bonus');
        $this->dropColumn('dispatch_regist', 'busy');


    }
}
