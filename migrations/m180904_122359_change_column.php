<?php

use yii\db\Migration;

/**
 * Class m180904_122359_change_column
 */
class m180904_122359_change_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('dispatch_regist', 'all_sended_message_count', $this->integer()->notNull()->defaultValue(0)->after('sended_message_count')->comment('Общее кол-во отправленных сообщений'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180904_122359_change_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180904_122359_change_column cannot be reverted.\n";

        return false;
    }
    */
}
