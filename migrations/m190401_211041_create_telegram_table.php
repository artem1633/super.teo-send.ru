<?php

use yii\db\Migration;

/**
 * Handles the creation of table `telegram`.
 */
class m190401_211041_create_telegram_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('telegram', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'telegram_id' => $this->string(),
            'company_id' => $this->integer()
        ]);

        $this->createIndex(
            'idx-telegram-company_id',
            'telegram',
            'company_id'
        );

        $this->addForeignKey(
            'fk-telegram-company_id',
            'telegram',
            'company_id',
            'companies',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-telegram-company_id',
            'telegram'
        );

        $this->dropIndex(
            'idx-telegram-company_id',
            'telegram'
        );

        $this->dropTable('telegram');
    }
}
