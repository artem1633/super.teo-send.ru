<?php

use yii\db\Migration;

/**
 * Class m180804_085234_drop_colum_from_table_dispatch
 */
class m180804_085234_drop_colum_from_table_dispatch extends Migration
{



    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->dropColumn('dispatch', 'account_id');
    }

    public function down()
    {
        echo "m180804_085234_drop_colum_from_table_dispatch cannot be reverted.\n";

        return false;
    }

}
