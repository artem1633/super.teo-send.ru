<?php

use yii\db\Migration;

/**
 * Class m190402_111341_add_status_read_column_to_dipsatch_regist
 */
class m190402_111341_add_status_read_column_to_dipsatch_regist extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch_regist', 'status_read', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dispatch_regist', 'status_read');
    }
}
