<?php

use yii\db\Migration;

/**
 * Handles dropping chat from table `dispatchChats`.
 */
class m180902_084435_drop_chat_column_from_dispatchChats_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('dispatch', 'chat');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
