<?php

use yii\db\Migration;

/**
 * Handles the creation of table `yandex_money`.
 */
class m180910_155950_create_yandex_money_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('yandex_money', [
            'id' => $this->primaryKey(),
            'secret' =>  $this->string(250)->null()->comment('Секрет'),
            'wallet' => $this->string(20)->null()->comment('Кошелек'),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('yandex_money');
    }
}
