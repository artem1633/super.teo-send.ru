<?php

use yii\db\Migration;

/**
 * Class m180804_121033_add_row_to_table_settings
 */
class m180804_121033_add_row_to_table_settings extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->insert('settings',array(
            'key' => 'allowed_message_per_account',
            'value' => '20',
            'label' =>'Максимальное колво писем на аккаунт',

        ));


    }

    public function down()
    {
        echo "m180802_183130_add_rows_to_settings cannot be reverted.\n";

        return false;
    }
}
