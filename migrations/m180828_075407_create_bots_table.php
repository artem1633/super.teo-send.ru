<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bots`.
 */
class m180828_075407_create_bots_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('bots', [
            'id' => $this->primaryKey(),
            'name' => $this->string('255')->notNull()->comment('Имя бота'),
            'dialogId' => $this->integer()->null()->comment('ID диалога'),
            'companyId' => $this->integer()->null()->comment('ID компании'),
        ], $tableOptions);

        $this->batchInsert('bots', ['name'], [
            ['Тестовый бот'],
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('bots');
    }
}
