<?php

use yii\db\Migration;

/**
 * Class m180906_080021_update_statuses_table
 */
class m180906_080021_update_statuses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('statuses');
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('statuses', [
            'id' => $this->primaryKey(),
            'type' => $this->integer()->notNull()->defaultValue(1)->comment('Тип статуса'),
            'name' => $this->string()->null()->comment('Название статуса'),
            'color' => $this->string(25)->null()->comment('Стиль кнопки'),
            'sort' => $this->integer()->null()->comment('Сортировка'),
        ], $tableOptions);

        $this->insert('statuses', ['name' => 'Переписка', 'type' => 1, 'color' => 'info', 'sort' => 1]);
        $this->insert('statuses', ['name' => 'Заинтересован', 'type' => 1, 'color' => 'warning', 'sort' => 2]);
        $this->insert('statuses', ['name' => 'Купил', 'type' => 1, 'color' => 'success', 'sort' => 3]);
        $this->insert('statuses', ['name' => 'Отказался', 'type' => 1, 'color' => 'danger', 'sort' => 4]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180906_080021_update_statuses_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180906_080021_update_statuses_table cannot be reverted.\n";

        return false;
    }
    */
}
