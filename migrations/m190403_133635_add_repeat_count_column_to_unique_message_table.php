<?php

use yii\db\Migration;

/**
 * Handles adding repeat_count to table `unique_message`.
 */
class m190403_133635_add_repeat_count_column_to_unique_message_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('unique_message', 'repeat_count', $this->integer()->comment('Кол-во повторений'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('unique_message', 'repeat_count');
    }
}
