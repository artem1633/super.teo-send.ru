<?php

use yii\db\Migration;

/**
 * Handles adding type to table `settings`.
 */
class m181210_120729_add_type_column_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('settings', 'type', $this->string()->defaultValue('text')->comment('Тип поля'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('settings','type');
    }
}
