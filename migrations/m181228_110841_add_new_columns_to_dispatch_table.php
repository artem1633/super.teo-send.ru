<?php

use yii\db\Migration;

/**
 * Handles adding new to table `dispatch`.
 */
class m181228_110841_add_new_columns_to_dispatch_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch', 'accounts_count', $this->integer()->comment('Кол-во аккаунтов на данную рассылку'));
        $this->addColumn('dispatch', 'day_speed', $this->integer()->comment('Суточная скорость (акк/день)'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dispatch', 'accounts_count');
        $this->dropColumn('dispatch', 'day_speed');
    }
}
