<?php

use yii\db\Migration;

/**
 * Class m180920_125315_add_column_to_dispatch_status_table
 */
class m180920_125315_add_column_to_dispatch_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch_status', 'photo', $this->text()->comment('Фотография'));

        $this->addColumn('dispatch_status', 'name', $this->string()->comment('Имя'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dispatch_status', 'photo');
        $this->dropColumn('dispatch_status', 'name');

        return true;
//        echo "m180920_125315_add_column_to_dispatch_status_table cannot be reverted.\n";
//
//        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180920_125315_add_column_to_dispatch_status_table cannot be reverted.\n";

        return false;
    }
    */
}
