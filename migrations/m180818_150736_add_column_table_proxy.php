<?php

use yii\db\Migration;

/**
 * Class m180818_150736_add_column_table_proxy
 */
class m180818_150736_add_column_table_proxy extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('proxy', 'status', $this->string()->comment('Статус'));
        $this->addColumn('proxy', 'accounts', $this->string()->comment('Аккаунты'));

    }

    public function down()
    {
        $this->dropColumn('proxy', 'accounts');
        $this->dropColumn('proxy', 'status');

    }
}
