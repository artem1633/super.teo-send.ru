<?php

use yii\db\Migration;

/**
 * Class m180812_072410_alter_table_dispatch_status_v2
 */
class m180812_072410_alter_table_dispatch_status_v2 extends Migration
{


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

        $this->execute("UPDATE dispatch_status SET `read` = 'no' WHERE `read` = NULL OR `read` = '' ");

    }

    public function down()
    {
        echo "m180812_072410_alter_table_dispatch_status_v2 cannot be reverted.\n";

        return false;
    }

}
