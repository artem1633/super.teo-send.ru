<?php

use yii\db\Migration;

/**
 * Handles the creation of table `edit_instruction`.
 */
class m180805_161456_create_edit_instruction_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('edit_instruction', [
            'id' => $this->primaryKey(),
            'text' => $this->text(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('edit_instruction');
    }
}
