<?php

use yii\db\Migration;

/**
 * Handles the creation of table `referal_redirects`.
 */
class m181211_100748_create_referal_redirects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('referal_redirects', [
            'id' => $this->primaryKey(),
            'refer_company_id' => $this->integer()->comment('Компания'),
            'ip' => $this->string()->comment('IP-адрес'),
            'user_agent' => $this->string()->comment('Браузер'),
            'created_at' => $this->dateTime()->comment('Дата и время'),
        ]);
        $this->addCommentOnTable('referal_redirects', 'Счетчик реферальных переходах');

        $this->createIndex(
            'idx-referal_redirects-refer_company_id',
            'referal_redirects',
            'refer_company_id'
        );

        $this->addForeignKey(
            'fk-referal_redirects-refer_company_id',
            'referal_redirects',
            'refer_company_id',
            'companies',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
        'fk-referal_redirects-refer_company_id',
        'referal_redirects'
        );

        $this->dropIndex(
            'idx-referal_redirects-refer_company_id',
            'referal_redirects'
        );

        $this->dropTable('referal_redirects');
    }
}
