<?php

use yii\db\Migration;

/**
 * Handles adding blockingDate to table `dispatchRegister`.
 */
class m180903_092746_add_blockingDate_column_to_dispatchRegister_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch_regist', 'blocking_date',  $this->date()->null()->comment('Дата блокировки'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
