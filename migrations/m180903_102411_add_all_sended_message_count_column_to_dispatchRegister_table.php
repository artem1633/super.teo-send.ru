<?php

use yii\db\Migration;

/**
 * Handles adding all_sended_message_count to table `dispatchRegister`.
 */
class m180903_102411_add_all_sended_message_count_column_to_dispatchRegister_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch_regist', 'all_sended_message_count', $this->integer()->notNull()->after('sended_message_count')->comment('Общее кол-во отправленных сообщений'));
}

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
