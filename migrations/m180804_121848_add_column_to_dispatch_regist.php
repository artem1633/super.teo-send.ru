<?php

use yii\db\Migration;

/**
 * Class m180804_121848_add_column_to_dispatch_regist
 */
class m180804_121848_add_column_to_dispatch_regist extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('dispatch_regist', 'sended_message_count', $this->integer()->comment('Количество отправленных сообщении')->defaultValue(0));

    }

    public function down()
    {
        $this->dropColumn('dispatch_regist', 'sended_message_count');

    }
}
