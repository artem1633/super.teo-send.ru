<?php

use yii\db\Migration;

/**
 * Class m180907_062420_add_column_with_dispatch_to_report_table
 */
class m180907_062420_add_column_with_dispatch_to_report_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\app\models\DailyReport::tableName(), 'dispatch_id', $this->integer()->null()->after('type')->comment('ID рассылки'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180907_062420_add_column_with_dispatch_to_report_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180907_062420_add_column_with_dispatch_to_report_table cannot be reverted.\n";

        return false;
    }
    */
}
