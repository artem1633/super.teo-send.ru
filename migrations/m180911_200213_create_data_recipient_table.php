<?php

use yii\db\Migration;

/**
 * Handles the creation of table `data_recipient`.
 */
class m180911_200213_create_data_recipient_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('data_recipient', [
            'id' => $this->primaryKey(),
            'date_added' =>  'timestamp DEFAULT NOW() COMMENT "Дата и время сохранения"',
            'name' => $this->string(255)->null()->comment('Название базы'),
            'description' => $this->string()->null()->comment('Описание базы'),
            'count' => $this->integer()->notNull()->comment('Кол-во адресатов'),
            'type' => $this->integer()->notNull()->comment('Тип базы'),
            'file' => $this->text()->notNull()->comment('Файл'),
            'company_id' => $this->integer()->notNull()->comment('ID  компании'),

        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('data_recipient');
    }
}
