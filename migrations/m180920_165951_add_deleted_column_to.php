<?php

use yii\db\Migration;

/**
 * Class m180920_165951_add_deleted_column_to
 */
class m180920_165951_add_deleted_column_to extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch_regist', 'deleted', $this->tinyInteger()->comment('Признак удаления аккаунта')->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dispatch_regist', 'deleted');
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180920_165951_add_deleted_column_to cannot be reverted.\n";

        return false;
    }
    */
}
