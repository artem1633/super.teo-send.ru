<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `column_proxy`.
 */
class m180818_152643_drop_column_proxy_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('proxy', 'accounts');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180818_152643_drop_column_proxy_table cannot be reverted.\n";

        return false;
    }
}
