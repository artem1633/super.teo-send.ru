<?php

use yii\db\Migration;

/**
 * Handles dropping deleted from table `diaspatch_regist`.
 */
class m180925_100141_drop_deleted_column_from_diaspatch_regist_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('dispatch_regist', 'deleted');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
