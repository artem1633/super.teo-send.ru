<?php

use yii\db\Migration;

/**
 * Class m180802_153804_table_dispatch
 */
class m180802_153804_table_dispatch extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

        $this->createTable('dispatch', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'text' => $this->string(),
            'status' => $this->string(),
            'account_id' => $this->string(),
            'data' => $this->dateTime(),
        ]);
    }

    public function down()
    {
        $this->dropTable('dispatch_regist');
    }
}
