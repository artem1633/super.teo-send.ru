<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop`.
 */
class m180913_060336_create_shop_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('shop', [
            'id' => $this->primaryKey(),
            'date_added' =>  'timestamp DEFAULT NOW() COMMENT "Дата и время создания"',
            'date_updated' => $this->dateTime()->null()->comment('Дата обновления'),
            'name' => $this->string(255)->null()->comment('Заголовок'),
            'description_short' => $this->string()->null()->comment('Краткое описание'),
            'description_long' => $this->string()->null()->comment('Описание'),
            'additional_info' => $this->string()->null()->comment('Дополнительная информация'),
            'type' => $this->integer()->notNull()->comment('Тип товара'),
            'image' => $this->string(255)->null()->comment('Путь к изображению'),
            'access' => $this->boolean()->defaultValue(0)->comment('Доступность'),
            'price' => $this->decimal(10,2)->notNull()->comment('Цена'),
            'remains' => $this->integer()->defaultValue(0)->comment('Остаток'),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('shop');
    }
}
