<?php

use yii\db\Migration;

/**
 * Class m180831_163215_update_column_in_accounts_table
 */
class m180831_163215_update_column_in_accounts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch_regist', 'login',  $this->string(250)->null()->comment('Логин'));
        $this->addColumn('dispatch_regist', 'password',  $this->string(250)->null()->comment('Пароль'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180831_163215_update_column_in_accounts_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180831_163215_update_column_in_accounts_table cannot be reverted.\n";

        return false;
    }
    */
}
