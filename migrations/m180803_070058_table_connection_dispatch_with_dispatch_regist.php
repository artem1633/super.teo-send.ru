<?php

use yii\db\Migration;

/**
 * Class m180803_070058_table_connection_dispatch_with_dispatch_regist
 */
class m180803_070058_table_connection_dispatch_with_dispatch_regist extends Migration
{


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('dispatch_connect', [
            'id' => $this->primaryKey(),
            'dispatch_id' => $this->integer()->comment('Ид рассылок'),
            'dispatch_regist_id' => $this->integer()->comment('Ид акаунтов'),
        ]);

        // creates index for column `dispatch_id`
        $this->createIndex(
            'idx-dispatch_connect-dispatch_id',
            'dispatch_connect',
            'dispatch_id'
        );

        // add foreign key for table `dispatch`
        $this->addForeignKey(
            'fk-dispatch_connect-dispatch_id',
            'dispatch_connect',
            'dispatch_id',
            'dispatch',
            'id',
            'CASCADE'
        );

        // creates index for column `dispatch_regist_id'`
        $this->createIndex(
            'idx-dispatch_connect-dispatch_regist_id',
            'dispatch_connect',
            'dispatch_regist_id'
        );

        // add foreign key for table `dispatch`
        $this->addForeignKey(
            'fk-dispatch_connect-dispatch_regist_id',
            'dispatch_connect',
            'dispatch_regist_id',
            'dispatch_regist',
            'id',
            'CASCADE'
        );


    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-dispatch_connect-dispatch_id',
            'dispatch_connect'
        );

        $this->dropIndex(
            'idx-dispatch_connect-dispatch_id',
            'dispatch_connect'
        );

        $this->dropForeignKey(
            'fk-dispatch_connect-dispatch_regist_id',
            'dispatch_connect'
        );

        $this->dropIndex(
            'idx-dispatch_connect-dispatch_regist_id',
            'dispatch_connect'
        );

        $this->dropTable('companies');
    }

}
