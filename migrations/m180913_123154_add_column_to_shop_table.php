<?php

use yii\db\Migration;

/**
 * Class m180913_123154_add_column_to_shop_table
 */
class m180913_123154_add_column_to_shop_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180913_123154_add_column_to_shop_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180913_123154_add_column_to_shop_table cannot be reverted.\n";

        return false;
    }
    */
}
