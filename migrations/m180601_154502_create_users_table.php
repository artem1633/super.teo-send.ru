<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m180601_154502_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('ФИО'),//required
            'login' => $this->string(255)->unique()->comment('Логин/Е-mail'),//required
            'password' => $this->string(255)->comment('Пароль'),//required
            'access_for_moving' => $this->boolean()->defaultValue(0)->comment('Доступ для перемещения товара'),
            'telephone' => $this->string(255)->comment('Телефон'),//required
            'role_id' => $this->string(255)->comment('Должность'),
            'status' => $this->string(255)->comment('Статус'),
            'atelier_id' => $this->integer()->comment('Ателье'),
            'sales_percent' => $this->float()->comment('Процент, который сотрудник получает с продаж'),
            'cleaner_percent' => $this->float()->comment('Процент, который сотрудник получает с услуг химчистки'),
            'sewing_percent' => $this->float()->comment('Процент, который сотрудник получает с пошива'),
            'repairs_percent' => $this->float()->comment('Процент, который сотрудник получает с ремонта'),
            'auth_key' => $this->string(255)->comment('Пароль'),//required
            'data_cr' => $this->date()->comment('Дата регистрации'),
        ]);

        $this->createIndex('idx-users-atelier_id', 'users', 'atelier_id', false);
        $this->addForeignKey("fk-users-atelier_id", "users", "atelier_id", "atelier", "id");

        $this->insert('users',array(
            'name' => 'Иванов Иван Иванович',
            'login' => 'admin@gmail.com',
            'password' => md5('admin121'),
            'access_for_moving' => 1,
            'telephone' => '+7 961 123 45 67',
            'role_id' => 'administrator', //администратор
            'status' => 'working',
            'atelier_id' => null,
            'sales_percent' => null,
            'cleaner_percent' => null,
            'sewing_percent' => null,
            'repairs_percent' => null,
            'auth_key' => 'admin121',
            'data_cr' => date('Y-m-d'),
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-users-atelier_id','users');
        $this->dropIndex('idx-users-atelier_id','users');  

        $this->dropTable('users');
    }
}
