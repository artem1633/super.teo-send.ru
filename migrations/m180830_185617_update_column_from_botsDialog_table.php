<?php

use yii\db\Migration;

/**
 * Class m180830_185617_update_column_from_botsDialog_table
 */
class m180830_185617_update_column_from_botsDialog_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `bots_dialogs` CHANGE `result` `result` BLOB NOT NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180830_185617_update_column_from_botsDialog_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180830_185617_update_column_from_botsDialog_table cannot be reverted.\n";

        return false;
    }
    */
}
