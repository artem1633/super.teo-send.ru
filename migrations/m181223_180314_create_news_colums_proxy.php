<?php

use yii\db\Migration;

/**
 * Class m181223_180314_create_news_colums_proxy
 */
class m181223_180314_create_news_colums_proxy extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('proxy', 'count_block', $this->integer()->defaultValue(0)->comment('Кол-во заблокированых'));
        $this->addColumn('proxy', 'count_add', $this->string()->defaultValue(0)->comment('Кол-во подключеных'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('proxy', 'count_block');
        $this->dropColumn('proxy', 'count_add');
    }
}
