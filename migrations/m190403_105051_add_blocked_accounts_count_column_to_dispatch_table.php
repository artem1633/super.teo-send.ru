<?php

use yii\db\Migration;

/**
 * Handles adding blocked_accounts_count to table `dispatch`.
 */
class m190403_105051_add_blocked_accounts_count_column_to_dispatch_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch', 'blocked_accounts_count', $this->integer()->comment('Кол-во заблокированных аккаунтов'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dispatch', 'blocked_accounts_count');
    }
}
