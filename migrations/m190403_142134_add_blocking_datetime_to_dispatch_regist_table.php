<?php

use yii\db\Migration;

/**
 * Class m190403_142134_add_blocking_datetime_to_dispatch_regist_table
 */
class m190403_142134_add_blocking_datetime_to_dispatch_regist_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch_regist', 'blocking_datetime', $this->dateTime()->comment('Дата и время блокировки'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dispatch_regist', 'blocking_datetime');
    }
}
