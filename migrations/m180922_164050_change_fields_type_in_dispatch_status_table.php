<?php

use yii\db\Migration;

/**
 * Class m180922_164050_change_fields_type_in_dispatch_status_table
 */
class m180922_164050_change_fields_type_in_dispatch_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('dispatch_status', 'gender', $this->string());
        $this->alterColumn('dispatch_status', 'city', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180922_164050_change_fields_type_in_dispatch_status_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180922_164050_change_fields_type_in_dispatch_status_table cannot be reverted.\n";

        return false;
    }
    */
}
